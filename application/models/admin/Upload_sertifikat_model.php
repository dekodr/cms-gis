<?php defined('BASEPATH') OR exit('No direct script access allowed');
class upload_sertifikat_model extends MY_Model{
	public $table = 'ms_certificate';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$query = "	SELECT 
    					a.upload_sertifikat_file,
    					a.id

    				FROM ".$this->table." a WHERE a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	
	function selectData($id){

		$query = "	SELECT 
    					a.upload_sertifikat_file,
                        a.id
                    FROM 
                        ".$this->table." a 
                    WHERE 
                        a.id= ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	 function insert($save) {
            $save_login['id_user']      			= $this->db->insert_id();
            $save_login['id_certificate']   		= $save['id_certificate'];
            $save_login['upload_sertifikat_file']   = $save['upload_sertifikat_file'];
            $save_login['entry_stamp']  			= timestamp();
            $save_login['del']          			= 0; 
            return $this->db->insert('ms_certificate',$save_login);
    }

    function update($id, $save)	
    {                      
            $save_login['id_certificate']   		= $save['id_certificate'];
            $save_login['upload_sertifikat_file']   = $save['upload_sertifikat_file'];
            $save_login['edit_stamp']  				= timestamp();
            $save_login['del']          			= 0; 
            return $this->db->where('id_user',$id)
                            ->update('ms_certificate',$save_login);
    }


}

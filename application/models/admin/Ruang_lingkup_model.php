<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Ruang_lingkup_model extends MY_Model{
	public $table = 'ms_lingkup_pemohon';
	function __construct(){
		parent::__construct();
	}

	function getData($form,$id){
		// echo $id;die;
		if ($id == 1) {
			$and = " AND a.is_lingkup_pemohon = 1";
		} elseif ($id == 2) {
			$and = " AND a.is_sistem_sertifikasi = 1";
		} else {
			$and = " ";
		}
		$query = "	SELECT 
    					b.jenis,
    					a.name,
    					a.id

    				FROM 
    					".$this->table." a 
					
					JOIN
						ms_jenis_permohonan b ON b.id=a.jenis_permohonan_id

    				WHERE 
    					a.del = 0 $and";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	
	function selectData($id){

		$query = "	SELECT 
						a.jenis_permohonan_id,
    					b.jenis,
    					a.name,
    					a.id

    				FROM 
    					".$this->table." a 
					
					JOIN
						ms_jenis_permohonan b ON b.id=a.jenis_permohonan_id

    				WHERE 
                        a.id= ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	public function getRuangLingkup()
	{
		$query = $this->db->where('del',0)->get('ms_jenis_permohonan')->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['jenis'];
		}
		return $data;
	}

}

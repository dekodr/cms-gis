<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Penerbitan_sertifikat_model extends MY_Model{
	public $table = 'ms_vendor';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$query = "	SELECT 
    					a.name,
    					a.id

    				FROM 
                        ms_vendor a 
                    WHERE 
                        a.del = 0 AND a.vendor_status = 3";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	
	function selectData($id){

		$query = "	SELECT 
    					b.Sertifikat_Draft_File,
                        b.Sertifikat_File,
                        a.id
                    FROM
    					ms_permohonan_sertifikasi a
                    JOIN 
                        ms_certificate b on a.id = b.id_certificate
                    WHERE 
                        a.id= ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	public function get_data_sertifikat($id)
	{
		$query = "SELECT 
                        c.jenis jenis_permohonan,
                        d.name sistem_sertifikasi,
                        b.Sertifikat_Draft_File,
                        b.Sertifikat_File,
                        b.reason,
                        a.id,
                        a.name,
                        b.status
					FROM
    					ms_permohonan_sertifikasi a
                    JOIN 
                        ms_certificate b on a.id = b.id_certificate
                    JOIN 
                        ms_lingkup_pemohon d on a.sistem_sertifikasi_id = d.id
                    JOIN 
                        ms_jenis_permohonan c on a.jenis_permohonan_id = c.id
				  WHERE 
				  		a.del = 0 AND a.id_vendor = ".$id;
		return $query;
	}

	function update($id, $save) 
	{
            // $user = $this->session->userdata('user'); 
            // $insert_certificate['reason']                       = $save['reason'];  
            $insert_certificate['status']                       = 2;
            $insert_certificate['Sertifikat_File']       = $save['Sertifikat_File'];
            $insert_certificate['Sertifikat_Draft_File'] = $save['Sertifikat_Draft_File'];
            $insert_certificate['del']                          = 0;            
            $insert_certificate['entry_stamp']                  = timestamp();
            return $this->db->where('id_certificate',$id)
                            ->update('ms_certificate',$insert_certificate);

    }

    function upload_sertifikat_asli($id, $save) 
    {
            // $user = $this->session->userdata('user'); 
            // $insert_certificate['reason']                       = $save['reason'];  
            $insert_certificate['status']                       = 5;
            $insert_certificate['Sertifikat_File']              = $save['Sertifikat_File'];
            $insert_certificate['Sertifikat_Draft_File']        = $save['Sertifikat_Draft_File'];
            $insert_certificate['del']                          = 0;            
            $insert_certificate['entry_stamp']                  = timestamp();
            return $this->db->where('id_certificate',$id)
                            ->update('ms_certificate',$insert_certificate);

    }

}

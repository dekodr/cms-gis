<?php 
/**
 * 
 */
class Nrp_model extends MY_Model
{
	public $table = 'ms_nrp';

	public function getData($form)
	{
		$query = "	SELECT 
    					b.name client,
    					a.nrp_file,
    					a.id
    				FROM 
    					".$this->table." a 
    				JOIN
    					ms_vendor b ON b.id=a.id_client
    				WHERE 
    					a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}
}
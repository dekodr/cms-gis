<?php defined('BASEPATH') OR exit('No direct script access allowed');
class No_pengajuan_model extends MY_Model{
	public $table = 'ms_no_pengajuan';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$query = "	SELECT 
    					b.name perusahaan,
    					a.no,
    					c.jenis,
    					a.id,
    					e.name sistem_sertifikasi
    				FROM 
    					ms_no_pengajuan a 
    				JOIN
    					ms_vendor b ON b.id=a.id_vendor
					JOIN
						ms_permohonan_sertifikasi d ON d.id=a.id_certificate
    				JOIN
    					ms_jenis_permohonan c ON c.id=d.jenis_permohonan_id
    				JOIN
    					ms_lingkup_pemohon e ON e.id=d.sistem_sertifikasi_id	
    				WHERE 
    					a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	
	function selectData($id){

		$query = "	SELECT 
    					a.no_pengajuan_file,
                        a.id
                    FROM 
                        ".$this->table." a 
                    WHERE 
                        a.id= ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

}

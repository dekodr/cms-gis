<?php defined('BASEPATH') OR exit('No direct script access allowed');
class jenis_industri_model extends MY_Model{
	public $table = 'ms_sistem_sertifikasi';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$query = "	SELECT 
						b.name as name_lingkup_pemohon,
    					a.name,
    					a.id

    				FROM 

    				ms_sistem_sertifikasi a 

    				JOIN

    				ms_lingkup_pemohon b on b.id = a.id_lingkup_pemohon

    				WHERE a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	
	function selectData($id){

		$query = "	SELECT 
						b.name as name_lingkup_pemohon,
    					a.name,
    					a.id

    				FROM 

    				ms_sistem_sertifikasi a 

    				JOIN

    				ms_lingkup_pemohon b on b.id = a.id_lingkup_pemohon
    				
                    WHERE 
                        a.id= ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	public function getSistemSertifikasi()
	{
		 $query = "  SELECT 
                        a.name,
                        a.id
                    FROM
                        ms_lingkup_pemohon a
                    WHERE 
                     a.del = 0 AND a.is_sistem_sertifikasi = 1";

        $query = $this->db->query($query)->result_array();
        $data = array();
         // $data['17'] = 'Pilih Salah Satu';
        foreach ($query as $key => $value) {
            $data[$value['id']] = $value['name'];
        }
        return $data;
	}

}

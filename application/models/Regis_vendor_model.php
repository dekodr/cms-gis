<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Regis_vendor_model extends MY_Model{
	public $table = 'ms_vendor';
	function __construct(){
		parent::__construct();
	}

    public function insert_vendor($data){
        $this->db->insert('ms_vendor',$data);
        // print_r($this->db->last_query());
        return $this->db->insert_id();
    }

    public function insert_data_vendor($table, $data){
        return $this->db->insert($table,$data);
    }

    public function get_npwp($npwp)
    {
        $query = $this->db->select('npwp_code')->where('npwp_code',$npwp)->get('ms_vendor_admistrasi');
        return $query;
    }

    public function get_email($email)
    {
        $query = $this->db->select('vendor_email')->where('vendor_email',$email)->get('ms_vendor_admistrasi');
        return $query;
    }

    public function get_city($id)
    {
        $query = "  SELECT
                        id,
                        name

                        FROM ms_city WHERE del=0 AND id_province = ".$id;

        $query = $this->db->query($query)->result_array();
        $data = array();
        foreach ($query as $key => $value) {
            $data[$value['id']] = $value['name'];
        }
        return $data;
    }
}

<?php

class Main_model extends CI_model{



	function check($data = array()){

		$username = $this->input->post('username');

		$_password = $this->input->post('password');
		$password = do_hash($this->input->post('password'),'sha1');

		
		$sql = "SELECT * FROM ms_login WHERE username = ? AND password = ?";

		$sql = $this->db->query($sql, array($username, $password));
		// echo $this->db->last_query();
		$_sql = $sql->row_array();
		$ct_sql = '';
		if($sql){

			if($_sql['type'] == "user"){

				$ct_sql = "SELECT ms_vendor.*  FROM ms_vendor WHERE ms_vendor.id=? AND ms_vendor.vendor_status != 4";
				$ct_sql = $this->db->query($ct_sql,array($_sql['id_user']));
				$data = $ct_sql->row_array();
					

				if ($ct_sql->num_rows() > 0) {
					$set_session = array(
						'id_user' 		=> 	$data['id'],
						'name'			=>	$data['name'],
						'vendor_status'	=>	$data['vendor_status'],
						'is_active'		=>	$data['is_active'],
						'id_role'		=>	11,
						'npwp_code'		=>  $data['npwp_code']
					);
				
					$this->session->set_userdata('user',$set_session);

					return true;
				}
				
			}else if($_sql['type'] == "admin"){
				$ct_sql = "SELECT
							 	ms_admin.id id_admin,
								ms_admin.name,
								ms_admin.id_role,
								tb_role.name role_name
							FROM 
								ms_admin 
							JOIN 
								tb_role ON ms_admin.id_role = tb_role.id 
							WHERE 
								ms_admin.id=?";
				$ct_sql = $this->db->query($ct_sql, array($_sql['id_user']));

				$data = $ct_sql->row_array();
				
				$set_session = array(
					'id_user' 		=> 	$data['id_admin'],
					'name'			=>	$data['name'],
					'id_role'		=>	$data['id_role'],
					'role_name'		=>	$data['role_name'],
					'sbu_name'		=>	$data['sbu_name'],
				);
				
				$this->session->set_userdata('admin',$set_session);

				return true;
			}

		}
		else{
			return false;
		}
	}
	
	function get_daftar_tunggu_chart(){

		$query = " 	SELECT 
						*

					FROM 
						ms_vendor a

					WHERE 
						a.vendor_status = 2
						AND a.is_active = 1
					";
		// if($this->session->userdata('admin')['id_role']==8){
		// 	$query .= " AND a.need_approve = 1 ";
		// }
		// if(!in_array($this->session->userdata('admin')['id_role'], array(1,2))){
		// 	$query .= " AND a.id_sbu = ".$this->session->userdata('admin')['id_sbu'];
		// }
		$query .=	" ORDER BY 
						a.edit_stamp DESC
						
					";
		$result = $this->db->query($query);
		return $result;

	}

	function dpt_chart(){

		$query = " 	SELECT 
						*

					FROM 
						ms_vendor a

					LEFT JOIN 
						tr_dpt b ON b.id_vendor = a.id 

					WHERE 
						a.is_active = 1
						AND a.vendor_status = 0
						AND a.del = 0";
		// if(!in_array($this->session->userdata('admin')['id_role'], array(1,2,8))){
		// 	$query .= " AND a.id_sbu = ".$this->session->userdata('admin')['id_sbu'];
		// }
		$query .= "
					GROUP BY
						a.id, 
						b.id

					ORDER BY 
						b.start_date DESC


					";
		$result = $this->db->query($query);
		return $result;

	}
}

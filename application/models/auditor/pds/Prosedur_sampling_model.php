<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Prosedur_sampling_model extends MY_Model{
	public $table = 'ms_prosedur_sampling';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$query = "	SELECT 
    					a.prosedur_sampling_file,
    					a.id

    				FROM ".$this->table." a WHERE a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	
	function selectData($id){

		$query = "	SELECT 
    					a.prosedur_sampling_file,
                        a.id
                    FROM 
                        ".$this->table." a 
                    WHERE 
                        a.id= ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	public function get_lampiran($id)
	{
		return $this->db->where('id',$id)->get('ms_prosedur_sampling')->row_array();
	}


}

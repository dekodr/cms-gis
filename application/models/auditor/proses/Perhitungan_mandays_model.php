<?php
class Perhitungan_mandays_model extends MY_Model
{
    public $table = 'ms_mandays';

    public function getData($id,$id_certificate)
    {
        $admin = $this->session->userdata('admin');
        $query = " SELECT
                        mandays_file,
                        id,
                        id_client
                    FROM
                        ms_mandays
                    WHERE
                        del = 0 AND id_client = ".$id." AND id_certificate = ".$id_certificate;
        if ($admin['id_role'] != 4 && $admin['id_role'] != 7) {
            $query .= " AND id_auditor = ".$admin['id_user'];
        }
        return $query;
    }
    
    public function selectData($id)
    {
       $query = "   SELECT
                        *
                    FROM
                        ms_mandays
                    WHERE
                        del = 0 AND id = ? ";
        $query = $this->db->query($query,array($id))->row_array();
        return $query;
    }

    public function getMandaysFile()
    {
        return $this->db->where('del',0)->where('is_master',1)->get('ms_mandays')->result_array();
    }
}

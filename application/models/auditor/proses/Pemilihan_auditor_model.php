<?php
class Pemilihan_auditor_model extends MY_Model
{
    public $table = 'tr_auditor';

    public function getData($id,$id_certificate)
    {
        $query = "  SELECT
						b.jenis,
						c.name as name_sistem_sertifikasi,
						d.name as name_lingkup_pemohon,
						e.name as name_jenis_sertifikasi,
						a.id id_doc,
						a.data_status,
						a.id_vendor,
						( SELECT COUNT(*) FROM tr_note f WHERE f.document_type = 'ms_permohonan_sertifikasi' AND f.is_active = 1 AND f.id_vendor = $id AND f.id_document = a.id) total_note
				   	FROM 
				   		ms_permohonan_sertifikasi a
				   	JOIN 
				   		ms_jenis_permohonan b on b.id = a.jenis_permohonan_id
				   	JOIN
				   		ms_lingkup_pemohon c on c.id = a.sistem_sertifikasi_id
				   	JOIN
				   		ms_lingkup_pemohon d on d.id = a.lingkup_pemohon_id
				   	JOIN 
				   		ms_sistem_sertifikasi e on e.id = a.id_jenis_sertifikasi
				   	WHERE 
				   		a.del = 0 AND a.id_vendor = ".$id;
        // echo $this->db->last_query();die;
        return $query;
    }

    public function getCertificate($id)
    {
        $admin = $this->session->userdata('admin');
        $query = "  SELECT
                        b.jenis,
                        c.name as name_sistem_sertifikasi,
                        d.name as name_lingkup_pemohon,
                        e.name as name_jenis_sertifikasi,
                        a.id id_doc,
                        a.data_status,
                        a.id_vendor,
                        ( SELECT COUNT(*) FROM tr_note f WHERE f.document_type = 'ms_permohonan_sertifikasi' AND f.is_active = 1 AND f.id_vendor = $id AND f.id_document = a.id) total_note
                    FROM 
                        ms_permohonan_sertifikasi a
                    JOIN 
                        ms_jenis_permohonan b on b.id = a.jenis_permohonan_id
                    JOIN
                        ms_lingkup_pemohon c on c.id = a.sistem_sertifikasi_id
                    JOIN
                        ms_lingkup_pemohon d on d.id = a.lingkup_pemohon_id
                    JOIN 
                        ms_sistem_sertifikasi e on e.id = a.id_jenis_sertifikasi
                    JOIN
                        tr_auditor g ON g.id_certificate=a.id
                    WHERE 
                        a.del = 0 AND a.id_vendor = ".$id;

        $query .= " GROUP BY a.id";
        // echo $this->db->last_query();die;
        return $query;
    }

    public function getCertificateAssignment($id)
    {
        $admin = $this->session->userdata('admin');
        $query = "  SELECT
                        b.jenis,
                        c.name as name_sistem_sertifikasi,
                        d.name as name_lingkup_pemohon,
                        e.name as name_jenis_sertifikasi,
                        a.id id_doc,
                        a.data_status,
                        a.id_vendor,
                        ( SELECT COUNT(*) FROM tr_note f WHERE f.document_type = 'ms_permohonan_sertifikasi' AND f.is_active = 1 AND f.id_vendor = $id AND f.id_document = a.id) total_note
                    FROM 
                        ms_permohonan_sertifikasi a
                    JOIN 
                        ms_jenis_permohonan b on b.id = a.jenis_permohonan_id
                    JOIN
                        ms_lingkup_pemohon c on c.id = a.sistem_sertifikasi_id
                    JOIN
                        ms_lingkup_pemohon d on d.id = a.lingkup_pemohon_id
                    JOIN 
                        ms_sistem_sertifikasi e on e.id = a.id_jenis_sertifikasi
                    JOIN
                        tr_auditor g ON g.id_certificate=a.id
                    WHERE 
                        g.del = 0 AND g.id_client = ".$id." AND g.id_auditor = ".$admin['id_user'];

        $query .= " GROUP BY a.id";
        // echo $this->db->last_query();die;
        return $query;
    }
    public function getDataAuditor($id_certificate,$id_client)
    {
    	$query = "	SELECT
    					b.name,
    					c.name role,
    					a.id,
                        a.date_start,
                        a.date_end
    				FROM
    					tr_auditor a
    				JOIN
    					ms_admin b ON b.id=a.id_auditor
    				JOIN
    					tb_role c ON c.id=b.id_role
    				WHERE
    					a.del = 0 AND a.id_certificate = ".$id_certificate." AND a.id_client = ".$id_client;
    	return $query;
    }
    
    public function selectData($id)
    {
    	$query = "	SELECT
    					b.name,
    					c.name role,
    					a.id,
                        a.role_auditor
    				FROM
    					tr_auditor a
    				JOIN
    					ms_admin b ON b.id=a.id_auditor
    				JOIN
    					tb_role c ON c.id=b.id_role
    				WHERE
    					a.del = 0 AND a.id = ? ";
      //  $query = "   SELECT
						// b.jenis,
						// c.name as name_sistem_sertifikasi,
						// d.name as name_lingkup_pemohon,
						// e.name as name_jenis_sertifikasi,
						// a.id id_doc,
						// a.data_status,
						// a.id_vendor,
						// ( SELECT COUNT(*) FROM tr_note f WHERE f.document_type = 'ms_permohonan_sertifikasi' AND f.is_active = 1 AND f.id_vendor = $id AND f.id_document = a.id) total_note
				  //  	FROM 
				  //  		ms_permohonan_sertifikasi a
				  //  	JOIN 
				  //  		ms_jenis_permohonan b on b.id = a.jenis_permohonan_id
				  //  	JOIN
				  //  		ms_lingkup_pemohon c on c.id = a.sistem_sertifikasi_id
				  //  	JOIN
				  //  		ms_lingkup_pemohon d on d.id = a.lingkup_pemohon_id
				  //  	JOIN 
				  //  		ms_sistem_sertifikasi e on e.id = a.id_jenis_sertifikasi
				  //  	WHERE 
				  //  		a.del = 0 AND a.id = ? ";
        $query = $this->db->query($query,array($id))->row_array();
        return $query;
    }

    public function getTeam($id_role)
    {
        $query = $this->db->where('id_role',$id_role)->where('del',0)->get('ms_admin')->result_array();
        $data = array();
        foreach ($query as $key => $value) {
            $data[$value['id']] = $value['name'];
        } 
        return $data;
    }
    
    public function getRole()
    {
        $query = $this->db->where('(id != 1 AND id != 2 AND id != 4)')->where('del',0)->get('tb_role')->result_array();
        $data = array();
        foreach ($query as $key => $value) {
            $data[$value['id']] = $value['name'];
        } 
        return $data;
    }

    public function saveRole($id,$role)
    {
        $arr = array(
            'role_auditor'  => $role,
            'edit_stamp'    => date('Y-m-d H:i:s')
        );
        return $this->db->where('id',$id)->update('tr_auditor',$arr);
    }

    public function getRoleByUser($id_auditor,$id_client,$id_certificate)
    {
        $query = $this->db->where('id_auditor',$id_auditor)->where('id_client',$id_client)->where('id_certificate',$id_certificate)->get('tr_auditor')->row_array();
        // echo $this->db->last_query();die;
        return $query;
    }
}

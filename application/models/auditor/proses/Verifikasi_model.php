<?php 
/**
 * 
 */
class Verifikasi_model extends MY_Model
{
	public $table = "ms_verifikasi";

	public function getDataVerifikasi($id_client,$id_certificate)
	{
		$query = "	SELECT
						a.*,
						a.id id,
						( SELECT COUNT(*) FROM tr_note e WHERE e.document_type = 'ms_verifikasi' AND e.is_active = 1 AND e.id_vendor = $id_client AND e.id_document = a.id) total_note
					FROM
						ms_verifikasi a
					WHERE
						a.del = 0 AND a.id_client = ? AND a.id_certificate = ?
				";
				
		$query = $this->db->query($query,array($id_client,$id_certificate));
		// echo $this->db->last_query();die;
		return $query->row_array();
	}

	public function cek_action($id_client,$id_certificate)
	{
		$get_data = $this->db->where('del',0)->where('id_client',$id_client)->where('id_certificate',$id_certificate)->get($this->table)->row_array();

		if (!empty($get_data)) {
			$action = 'update';
		} else {
			$action = 'insert';
		}

		return $action;
	}

	public function update_data($id_client,$id_certificate, $data)
	{
		return $this->db->where('del',0)->where('id_client',$id_client)->where('id_certificate',$id_certificate)->update($this->table, $data);
	}

	public function verifikasi_sertifikat($id_client)
	{
		$arr = array(
			'status' => 1,
			'edit_stamp' => date('Y-m-d H:i:s')
		);
		$this->db->where('del',0)->where('id_client',$id_client)->update('ms_verifikasi',$arr);

		$arr = array(
			'vendor_status' => 3,
			'edit_stamp' => date('Y-m-d H:i:s')
		);
		return $this->db->where('del',0)->where('id',$id_client)->update('ms_vendor',$arr);
	}
}
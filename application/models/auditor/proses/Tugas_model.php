<?php
/**
 * 
 */
class Tugas_model extends MY_Model
{
	public $table = 'ms_tugas';
	public function getData($id_client,$id_certificate,$form)
	{
		$admin = $this->session->userdata('admin');
		$query = "	SELECT
						b.name id_auditor,
						a.tugas_file,
						a.id
					FROM
						ms_tugas a
					JOIN
						ms_admin b ON b.id=a.id_auditor
					WHERE
						a.del = 0 AND a.id_client = ".$id_client." AND a.id_certificate = ".$id_certificate;
		if ($admin['id_role'] != 4) {
			$query .= " AND a.id_auditor = ".$admin['id_user'];
		}
		return $query;
	}

	public function selectData($id)
	{
		$query = "	SELECT
						b.name id_auditor,
						a.tugas_file,
						a.id
					FROM
						ms_tugas a
					JOIN
						ms_admin b ON b.id=a.id_auditor
					WHERE
						a.del = 0 AND a.id = ?";
		$query = $this->db->query($query,array($id))->row_array();
		return $query;
	}

	public function getTugasFile()
    {
        return $this->db->where('del',0)->where('is_master',1)->get('ms_tugas')->result_array();
    }
}
<?php 
/**
 * 
 */
class Audit_tahap_model extends MY_Model
{
	public $table = 'ms_tahap_audit';
	public function getData($id_client,$id_certificate,$tahap)
	{
		$admin = $this->session->userdata('admin');
		$query = "	SELECT
						b.name id_auditor,
						a.tahap_audit_file,
						a.id,
						a.data_status,
						 ( SELECT COUNT(*) FROM tr_note e WHERE e.document_type = 'ms_tahap_audit' AND e.is_active = 1 AND e.id_vendor = $id_client AND e.id_document = a.id) total_note
					FROM
						ms_tahap_audit a
					JOIN
						ms_admin b ON b.id=a.id_auditor
					WHERE
						a.del = 0 AND a.tahap = ".$tahap." AND a.id_client = ".$id_client." AND a.id_certificate = ".$id_certificate;
		if ($admin['id_role'] != 4) {
			$query .= " AND a.id_auditor = ".$admin['id_user'];
		}
		return $query;
	}

	public function selectData($id)
	{
		$query = "	SELECT
						b.name id_auditor,
						a.tahap_audit_file,
						a.id,
						a.*
					FROM
						ms_tahap_audit a
					JOIN
						ms_admin b ON b.id=a.id_auditor
					WHERE
						a.del = 0 AND a.id = ? ";
		$query = $this->db->query($query,array($id))->row_array();
		return $query;
	}

	public function insert($data)
	{
		$this->db->insert($this->table,$data);
		$id_log = $this->db->insert_id();
		$data['id_log'] = $id_log;
		return $this->db->insert('tr_log_audit',$data);
	}

	public function update($id,$data)
	{
		$this->db->where('id',$id)->update($this->table,$data);
		
		$data['id_log'] = $id;
		return $this->db->insert('tr_log_audit',$data);
	}

	public function delete($id)
	{
		$this->db->where('id',$id)->delete($this->table);
		
		$arr = array(
			'id_log'	=>	$id,
			'edit_stamp'=>	date('Y-m-d H:i:s'),
			'del'		=>	1
		);
		return $this->db->where('id_log',$id)->update('tr_log_audit',$arr);
	}

	public function getDataHistory($id_log)
	{
		$query = "	SELECT
						a.name client_name,
						b.name auditor_name,
						c.tgl_audit,
						c.id,
						c.analisis
					FROM
						tr_log_audit c
					JOIN
						ms_admin b ON b.id=c.id_auditor
					JOIN
						ms_vendor a ON a.id=c.id_client
					WHERE
						c.del = 0 AND c.id_log = ".$id_log;
		return $query;
	}

	public function selectDataHistory($id)
	{
		$query = "	SELECT
						b.name id_auditor,
						a.tahap_audit_file,
						a.id,
						a.*
					FROM
						tr_log_audit a
					JOIN
						ms_admin b ON b.id=a.id_auditor
					WHERE
						a.del = 0 AND a.id = ? ";
		$query = $this->db->query($query,array($id))->row_array();
		return $query;
	}
}
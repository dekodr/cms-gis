<?php
/**
 * 
 */
class Kabupaten_model extends MY_Model
{
	public $table = 'tb_kabupaten'; 
	public function getData($form)
	{
		$query = "	SELECT
						b.name nama_provinsi, 
    					a.name nama_kabupaten,
    					a.id
					FROM
    					ms_city a
    				JOIN 
    					ms_province b ON b.id=a.id_province
        			WHERE
        				a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	public function selectData($id)
	{
		$query = "	SELECT
						b.name nama_provinsi, 
    					a.name,
    					a.id
					FROM
    					tb_kabupaten a
    				JOIN 
    					tb_province b ON b.id=a.id_province
        			WHERE
        				a.del = 0 AND a.id = ".$id;
		$query = $this->db->query($query)->row_array();
		return $query;
	}
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Mandays_model extends MY_Model{
	public $table = 'ms_mandays';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$query = "	SELECT 
    					a.mandays_file,
    					a.jumlah_mandays,
    					a.id

    				FROM ".$this->table." a WHERE a.del = 0 AND a.is_master = 1";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	
	function selectData($id){

		$query = "	SELECT 
    					a.*
                    FROM 
                        ".$this->table." a 
                    WHERE 
                        a.id= ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}
}

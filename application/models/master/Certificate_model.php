<?php
/**
 * 
 */
class Certificate_model extends MY_Model
{
	public $table = 'tb_certificate'; 

	public function getData($form)
	{
		$user = $this->session->userdata('user');
		$query = "SELECT 
						a.name,
						a.id
				  FROM 
				  		tb_certificate a
				  WHERE 
				  		a.del = 0";
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}
	public function getDataForm($id, $form)
	{
		$user = $this->session->userdata('user');
		$query = "SELECT 
						a.name,
						a.id,
						a.template_file
				  FROM 
				  		tb_form a
				  WHERE 
				  		a.del = 0";
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}
	public function selectData($id)
	{
		$query = "	SELECT 
						a.name,
						a.id
				  FROM 
				  		tb_certificate a
				  WHERE
        				a.del = 0 AND a.id = ".$id;
		$query = $this->db->query($query)->row_array();
		return $query;
	}
	function insertDetail($data){
		$this->table = 'tb_form';
		return parent::insert($data);
	}
	function deleteDetail($id){
		$this->table = 'tb_form';
		return parent::delete($id);
	}
	
}
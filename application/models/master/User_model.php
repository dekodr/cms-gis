<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User_model extends MY_Model{
	public $table = 'ms_admin';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$query = "	SELECT 
    					b.name role_name,
    				    a.name name,
    				    a.email email, 
    				    c.password_raw password, 
    				    a.id id,
                        b.id id_role
					FROM
    					ms_admin a
        			LEFT JOIN
    					tb_role b ON b.id = a.id_role
        			LEFT JOIN
    					ms_login c ON c.id_user = a.id
                    WHERE
                        a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
    				    a.name name,
    				    a.email email,
    				    b.name role_name, 
    				    c.password_raw password,
                        c.username,
                        b.id id_role
					FROM
    					ms_admin a
        			LEFT JOIN
    					tb_role b ON b.id = a.id_role
        			LEFT JOIN
    			        ms_login c ON c.id_user = a.id
                    WHERE 
                     c.type = 'admin' AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

    function insert($save) {
            $save_admin['entry_stamp'] = timestamp();
            $save_admin['id_role']     = $save['id_role'];
            $save_admin['name']        = $save['name'];
            $save_admin['email']       = $save['email'];
            // $save['username']    = $username;
            //$save['status']      = $save['status'];
            $save_admin['del']         = 0;

            $this->db->insert('ms_admin',$save_admin);

            $save_login['id_user']      = $this->db->insert_id();
            $save_login['type']         = 'admin';
            $save_login['username']     = $save['name'];
            $save_login['password']     = do_hash($save['password'],'sha1');
            $save_login['password_raw'] = $save['password'];
            $save_login['entry_stamp']  = timestamp();
            $save_login['del']          = 0; 
            return $this->db->insert('ms_login',$save_login);
    }

    function update($id, $save)
    { 
            $save_admin['edit_stamp'] = timestamp();
            $save_admin['id_role']     = $save['id_role'];
            $save_admin['name']        = $save['name'];
            $save_admin['email']       = $save['email'];
            // $save['username']    = $username;
            //$save['status']      = $save['status'];
            $save_admin['del']         = 0;

            $this->db->where('id',$id)
                     ->update('ms_admin',$save_admin);
                     
            $save_login['type']         = 'admin';
            $save_login['username']     = $save['name'];
            $save_login['password']     = do_hash($save['password'],'sha1');
            $save_login['password_raw'] = $save['password'];
            $save_login['edit_stamp']  = timestamp();
            $save_login['del']          = 0; 
            return $this->db->where('id_user',$id)
                            ->update('ms_login',$save_login);
    }

    function delete($id) {
        $this->db->where('id',$id)
                 ->delete('ms_admin');          
        $final = $this->db->where('id_user',$id)
                          ->delete('ms_login');
        return $final;
    }
}
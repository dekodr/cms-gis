<?php
/**
 * 
 */
class Temuan_model extends MY_Model
{
	public function getData()
	{
		$query = "SELECT
						b.name certificate,
						c.name leader,
						a.desc,
						a.temuan_file,
						d.id id_certifacate 
				  FROM
				  		ms_laporan_temuan a
				  JOIN 
				  		ms_certificate d ON d.id=a.id_certificate
				  JOIN
				  		tb_certificate b ON b.id=d.id_certificate
				  JOIN
				  		ms_admin c ON c.id=a.id_leader
				  WHERE
				  		a.del = 0";
		return $query;
	}

	public function get_data_temuan($id)
	{
		$query = "SELECT 
						a.entry_by,
						a.key,
						a.desc,
						a.temuan_file
				  FROM 
				  		tr_temuan a
				  WHERE 
				  		a.del = 0 AND a.id_certificate = ".$id;
		return $query;
	}
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard_model extends MY_Model{
	
	function __construct(){
		parent::__construct();
	}

	function get_note_admin(){
		$admin = $this->session->userdata('admin');
		$query = 	"	SELECT
							a.*,
							b.name
						FROM
							tr_note a
						LEFT JOIN
							ms_vendor b ON b.id=a.id_vendor
						WHERE
							a.is_active = ?
							AND a.is_admin_close = ?
					";
		$result = $this->db->query($query,array(1 , 0));
		return $result;
	}

	public function get_assignment()
	{
		$admin = $this->session->userdata('admin');
		$query = " 	SELECT
						a.name vendor_name,
						c.date_start,
						b.name auditor_name,
						c.status,
						d.id_vendor,
						c.date_end,
						e.name role_name
					FROM
						tr_auditor c
					JOIN
						ms_permohonan_sertifikasi d ON c.id_certificate=d.id
					JOIN
						ms_vendor a ON a.id=d.id_vendor
					JOIN
						ms_admin b ON c.id_auditor=b.id
					JOIN
						tb_role e ON e.id=b.id_role
					WHERE
						c.del=0 ";
		if ($admin['id_role'] != 4) {
			$query .= " AND c.id_auditor = ".$admin['id_user'];
		}

		return $query;
	}

	public function get_notification()
	{
		$query = "	SELECT 
						b.name,
						a.value,
						b.id
					FROM
						tr_note a
					JOIN
						ms_vendor b ON b.id=a.id_vendor
					WHERE
						a.is_active = 1";
		return $query;
	}

	public function getAuditor()
	{
		$query = $this->db->where('del',0)->where('id_role',3)->get('ms_admin')->result_array();
		$data = array();
		$data[] = 'Pilih Salah Satu';
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
	}

	public function search_client()
	{
		$query = " 	SELECT
						id,
						name
					FROM
						ms_vendor
					WHERE
						del = 0 AND vendor_status = 2";
		$query = $this->db->query($query)->result_array();
		$data[] = 'Pilih Salah Satu';
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
	}
}

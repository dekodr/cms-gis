<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Penerbitan_sertifikat_vendor_model extends MY_Model{
  public $table = 'ms_certificate';
  function __construct(){
    parent::__construct();
  }

  function getData($form){
        $user = $this->session->userdata('user');
    $query = "  SELECT 
                        c.jenis,
                        d.name sistem_sertifikasi,
                        a.Sertifikat_Draft_File,
                        a.Sertifikat_File,
                        a.id,
                        a.status
          FROM
              ms_certificate a
          JOIN 
              ms_jenis_permohonan c on a.jenis_permohonan_id = c.id
          JOIN 
              ms_lingkup_pemohon d on a.sistem_sertifikasi_id = d.id
          WHERE
                        a.del = 0 AND a.id_user= ".$user['id_user'];

    if($this->input->post('filter')){

      $query .= $this->filter($form, $this->input->post('filter'), false);

    }
    return $query;

  }

  function selectData($id){

    $query = "  SELECT 
                        a.Sertifikat_Draft_File,
                        b.jenis,
                        a.reason,
                        a.id
          FROM
              ms_certificate a
          JOIN
              ms_permohonan_sertifikasi b on b.id = a.id_certificate
          WHERE
                        a.del = 0 AND a.id = ".$id;

    $query = $this->db->query($query);
    return $query->row_array();

  }

  function approve($id, $save)
  {   
          $update_certificate['status']                 = 3;
          $update_certificate['del']                    = 0;
          $update_certificate['edit_stamp']             = timestamp();
          return $this->db->where('id',$id)
                          ->update('ms_certificate',$update_certificate);
  }

  function reject($id, $save)
  {   
          $reject_certificate['reason']                 = $this->input->post('reason');
          $reject_certificate['status']                 = 1;
          $reject_certificate['del']                    = 0;
          $reject_certificate['edit_stamp']             = timestamp();
          return $this->db->where('id',$id)
                          ->update('ms_certificate',$reject_certificate);
  }

  function delete($id) {
      return $this->db->where('id',$id)
                      ->update('ms_certificate',array('edit_stamp'=>date('Y-m-d H:i:s'), 'del' => 1));
  }
}
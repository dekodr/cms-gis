<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Nrp_model extends MY_Model{
	public $table = 'ms_nrp';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
    $user = $this->session->userdata('user');
		$query = "	SELECT 
                      a.nrp_file,             
                      a.id
					     FROM
    					        ms_nrp a
               WHERE    
                      a.del = 0 AND a.id_client= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
                      a.nrp_file,
                      a.id
                FROM
                      ms_nrp a
                WHERE
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

  // function insert($save) 
  // {
  //         $user = $this->session->userdata('user');
  //         $insert['id_client']    = $user['id_user'];
  //         $insert['nrp_file']     = $save['nrp_file']; 
  //         $insert['del']          = 0;            
  //         $insert['entry_stamp']  = timestamp();
  //         return $this->db->where('id',$id)
  //                         ->insert('ms_nrp',$insert);
  // }
	
  //   function update($id, $save)
  //   { 
  //         $update['nrp_file']       = $save['nrp_file'];
  //         $update['entry_stamp']    = timestamp();
  //         $update['del']            = 0;

  //       return $this->db->where('id',$id)
  //                           ->update('ms_nrp',$update);
  //   }

  //   function delete($id) {
  //      $final = $this->db->where('id',$id)
  //                       ->update('ms_nrp',array('edit_stamp'=>date('Y-m-d H:i:s'), 'del' => 1));
  //       return $final;
  //   }
}
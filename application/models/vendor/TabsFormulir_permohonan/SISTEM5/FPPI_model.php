<?php defined('BASEPATH') OR exit('No direct script access allowed');
class FPPI_model extends MY_Model{
	public $table = 'ms_formulir_permohonan';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
    $user = $this->session->userdata('user');
		$query = "	SELECT 
                      a.fppi_file,             
                      a.id
					     FROM
    					        ms_formulir_permohonan a
               WHERE
                      a.del = 0 AND a.fppi_file IS NOT NULL AND a.jenis_sertifikasi = 5 AND a.id_vendor= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
                      a.fppi_file,
                      a.id
                FROM
                      ms_formulir_permohonan a
                WHERE
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

  function insert($save) 
  {
          $user = $this->session->userdata('user');
          $insert['id_vendor']    = $user['id_user'];
          $insert['fppi_file']      = $save['fppi_file'];
          $insert['jenis_sertifikasi']          = 5;    
          $insert['del']          = 0;            
          $insert['entry_stamp']  = timestamp();
          return $this->db->where('id',$id)
                          ->insert('ms_formulir_permohonan',$insert);
  }
	
  function update($id, $save)
  {         
        $update['fppi_file']        = $save['fppi_file'];
          $update['jenis_sertifikasi']            = 5;
        $update['entry_stamp']    = timestamp();
        $update['del']            = 0;

      return $this->db->where('id',$id)
                          ->update('ms_formulir_permohonan',$update);
  }

  function delete($id) {
     $final = $this->db->where('id',$id)
                      ->update('ms_formulir_permohonan',array('edit_stamp'=>date('Y-m-d H:i:s'), 'fppi_file' => null));
      return $final;
  }
}
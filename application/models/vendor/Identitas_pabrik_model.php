<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Identitas_pabrik_model extends MY_Model{
	public $table = 'ms_identitas_pabrik';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
        $user = $this->session->userdata('user');
		$query = "	SELECT 
                        a.nama_pabrik,
                        a.alamat,
                        a.kota,
                        a.provinsi,
                        a.kode_pos,
                        a.telepon,
                        a.id
					FROM
    					ms_identitas_pabrik a
                    WHERE
                        a.del = 0 AND a.id_vendor= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
    				    a.*
					FROM
    					ms_identitas_pabrik a 
                    WHERE 
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

    function insert($save) {
            $user = $this->session->userdata('user');
            $insert_login['id_vendor']              = $user['id_user'];
            $insert_login['nama_pabrik']            = $save['nama_pabrik'];
            $insert_login['alamat']                 = $save['alamat'];
            $insert_login['kota']                   = $save['kota'];
            $insert_login['provinsi']               = $save['provinsi'];
            $insert_login['kode_pos']               = $save['kode_pos'];
            $insert_login['telepon']                = $save['telepon'];
            $insert_login['del']                    = 0;            
            $insert_login['entry_stamp']            = timestamp();
            return $this->db->where('id',$id)
                            ->insert('ms_identitas_pabrik',$insert_login);
    }

    function update($id, $save)
    {   
            $update_login['nama_pabrik']            = $save['nama_pabrik'];
            $update_login['alamat']                 = $save['alamat'];
            $update_login['kota']                   = $save['kota'];
            $update_login['provinsi']               = $save['provinsi'];
            $update_login['kode_pos']               = $save['kode_pos'];
            $update_login['telepon']                = $save['telepon'];
            $update_login['del']            = 0;
            $update_login['edit_stamp']     = timestamp();
            return $this->db->where('id',$id)
                            ->update('ms_identitas_pabrik',$update_login);
    }

    function delete($id) {
        return $this->db->where('id',$id)
                        ->update('ms_identitas_pabrik',array('edit_stamp'=>date('Y-m-d H:i:s'), 'del' => 1));
    }
}
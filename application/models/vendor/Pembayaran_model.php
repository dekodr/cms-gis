<?php defined('BASEPATH') OR exit('No direct script access allowed');
class pembayaran_model extends MY_Model{
	public $table = 'ms_pembayaran';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
        $user = $this->session->userdata('user');
		$query = "	SELECT 
                        a.pembayaran_file,
                        a.id
					FROM
    					ms_pembayaran a
                    WHERE
                        a.del = 0 AND a.id_vendor= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
    				    a.*
					FROM
    					ms_pembayaran a 
                    WHERE 
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

    function insert($save) {
            $user = $this->session->userdata('user');
            $insert_pembayaran['id_vendor']              = $user['id_user'];
            $insert_pembayaran['pembayaran_file']        = $save['pembayaran_file'];
            $insert_pembayaran['del']                    = 0;            
            $insert_pembayaran['entry_stamp']            = timestamp();
            return $this->db->where('id',$id)
                            ->insert('ms_pembayaran',$insert_pembayaran);
    }

    function update($id, $save)
    {   
            $update_pembayaran['pembayaran_file']   = $save['pembayaran_file'];
            $update_pembayaran['del']               = 0;
            $update_pembayaran['edit_stamp']        = timestamp();
            return $this->db->where('id',$id)
                            ->update('ms_pembayaran',$update_pembayaran);
    }

    function delete($id) {
        return $this->db->where('id',$id)
                        ->update('ms_pembayaran',array('edit_stamp'=>date('Y-m-d H:i:s'), 'del' => 1));
    }
}
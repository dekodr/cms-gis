<?php
class Certificate_model extends MY_Model
{
	public $table = 'ms_certificate';
	
	public function getLaporanFinal($id)
	{
		$admin = $this->session->userdata('admin');
		$query = "SELECT 
					a.desc,
					a.temuan_file
				FROM
					ms_laporan_temuan a
				WHERE
					a.id_certificate = $id AND a.id_pelapor = ".$admin['id_user'];
		return $query;
	}

	public function getData($form)
	{
		$user = $this->session->userdata('user');
		$query = "SELECT 
						b.name category_name,
						c.name certificate_name,
						a.id,
						a.id_certificate,
						a.status
				  FROM 
				  		ms_certificate a
				  LEFT JOIN tb_category b ON a.id_category = b.id
				  LEFT JOIN tb_certificate c ON a.id_certificate = c.id
				  -- JOIN
				  -- 		tb_certificate b ON b.id=a.id_certificate
				  WHERE 
				  		a.del = 0 AND a.id_user = ".$user['id_user'];
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	public function get_laporan_temuan($id)
	{
		$query = "	SELECT 
						a.name nama_sertifikat,
						b.name pelapor,
						c.desc,
						c.temuan_file,
						c.lampiran_admin,
						c.id_certificate,
						c.id
					FROM
						ms_laporan_temuan c
					JOIN
						ms_certificate ON ms_certificate.id=c.id_certificate
					JOIN
						tb_certificate a ON a.id=ms_certificate.id_certificate
					JOIN
						ms_admin b ON b.id=c.id_leader
					WHERE 
						c.del=0 AND c.id_certificate = ".$id;
		return $query;
	}

	public function upload_lampiran_admin($id,$data,$table)
	{
		return $this->db->where('id',$id)
				 		->update($table,$data);
	}

	public function get_data_status($id_user)
	{
		$query = "SELECT * FROM ms_vendor_admistrasi WHERE id_vendor = ".$id_user;
		return $this->db->query($query)->row_array();
	}

	public function getDataAdmin($form)
	{
		$admin = $this->session->userdata('admin');
		$query = "SELECT 
						c.name nama_user,
						b.name nama_sertifikat,
						d.name jenis,
						a.id,
						a.status,
						e.is_leader,
						a.is_reject
				  FROM 
				  		ms_certificate a
				  JOIN
				  		tb_certificate b ON b.id=a.id_certificate
				  JOIN 
				  		ms_vendor c ON c.id=a.id_user
				  JOIN  
				  		tb_category d ON d.id=b.id_category
				  LEFT JOIN
				  		tr_auditor e ON e.id_sertifikat=a.id AND e.is_leader IS NOT NULL	
				  WHERE 
				  		a.del = 0";
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	public function insert($data)
	{
		$data['del'] = 0;
		$a =  $this->db->insert($this->table, $data);
		if($a){
			$id = $this->db->insert_id();
			$query = $this->db->where('id_certificate', $data['id_certificate'])->get('tb_form');
			foreach ($query->result_array() as $key => $value) {
				$this->db->insert('tr_form', array(
					'id_user'=>$data['id_user'],
					'id_form'=>$value['id'],
					'id_certificate'=>$id,
					'entry_stamp'=>timestamp(),
					'del'=>0
				));
			}
			$save_history = array(
				'id_certificate' => $id,
				'value'			 => date('Y-m-d H:i:s').' Pengajuan sertfikat',
				'entry_stamp'	 => date('Y-m-d H:i:s'),
				'del'			 => 0
			);
			$this->db->insert('tr_history',$save_history);
		}
		return $a;
	}

	public function getDataForm($id, $id_certificate, $form)
	{
		$user = $this->session->userdata('user');
		$query = "SELECT 
						b.name,
						a.id,
						a.template_file,
						a.name temp_name,
						b.template_file temp_template_file,
						a.client_upload_file
				  FROM 
				  		tr_form a
				  		LEFT JOIN tb_form b ON a.id_form = b.id 
				  		LEFT JOIN ms_certificate c ON c.id_certificate = ".$id."
				  		LEFT JOIN tb_certificate d ON d.id=c.id_certificate
				  WHERE 
				  		a.del = 0 AND a.id_certificate = ".$id;
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	public function getDataExport($id)
	{
		$admin = $this->session->userdata('admin')['id_user'];
		$query = "SELECT
						a.desc,
						a.entry_by,
						a.temuan_file
				  FROM 
						tr_temuan a
				  JOIN
						tr_auditor c on c.id_sertifikat = a.id_certificate 
				  WHERE
						a.is_approved = 1 AND a.del = 0 AND c.is_leader = ".$admin." AND a.id_certificate = ".$id;

		return $query;
	}

	public function cek_leader($id)
	{
		$query = "SELECT
						c.*
				  FROM 
						tr_auditor c
				  WHERE
						c.del = 0 AND c.id_sertifikat = ".$id;

		return $this->db->query($query)->row_array();
	}

	public function exportTemuan($value='')
	{
		$admin = $this->session->userdata('admin')['id_user'];
		$query = "SELECT
						a.desc,
						a.entry_by,
						a.temuan_file
						FROM 
						tr_temuan a
						JOIN
						ms_certificate b on b.id = a.id_certificate
						JOIN
						tr_auditor c on c.id_sertifikat = a.id_certificate 
						WHERE
						a.del = 0 AND c.is_leader = ".$admin;

						return $this->db->query($query)->result_array();
	}

	public function getDataFormClient($id, $id_certificate, $form)
	{
		$query = "SELECT 
						name,
						template_file,
						id
				  FROM 
				  		tb_form
				  WHERE 
				  		del = 0 AND id_certificate = ".$id;
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	public function selectDetailCertificate($id)
	{
		$query = "SELECT 
						a.*
				  FROM
				  		ms_certificate a
				  	LEFT JOIN tb_category b ON a.id_category = b.id
				  	LEFT JOIN tb_certificate c ON a.id_certificate = c.id
				  WHERE 
				  		a.id = ".$id;
		// LEFT JOIN tr_client_upload b ON a.id_form = b.id_form AND id_client = 				  		
		return $this->db->query($query);
	}

	public function download_form($params = array())
    {
	    $this->db->select('*');
        $this->db->from('tb_form');
        $this->db->where('del','0');
        $this->db->order_by('id','desc');
        if(array_key_exists('id',$params) && !empty($params['id'])){
            $this->db->where('id',$params['id']);
            //get records
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            //get records
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
        //return fetched data
        return $result;
	}

	public function upload_file($id,$data)
	{
		// echo $this->db->last_query();die;
		return $this->db->where('id',$id)->update('ms_certificate',$data);
	}

	public function print_certificate($id_certificate)
	{
		$save_history = array(
			'id_certificate' => $id_certificate,
			'value'			 => date('Y-m-d H:i:s').' Admin Cetak Sertifikat.',
			'entry_stamp'	 => date('Y-m-d H:i:s'),
			'del'			 => 0
		);
		$this->db->insert('tr_history',$save_history);

		$query = "SELECT 
						a.name user,
						b.name certificate_name,
						a.npwp_code,
						d.vendor_address,
						d.vendor_phone,
						d.vendor_fax
				  FROM 
				  		ms_certificate c
				  JOIN
				  		ms_vendor a ON a.id=c.id_user
				  JOIN
				  		tb_certificate b ON b.id=c.id_certificate
				  JOIN 
				  		ms_vendor_admistrasi d ON d.id_vendor=a.id
				  WHERE 
				  		c.del = 0 AND c.id = ".$id_certificate;
		return $this->db->query($query)->row_array();
	}

	public function insertUploadClient($save,$id_certificate)
	{
		$this->table = 'tr_form';

		$save_history = array(
			'id_certificate' => $id_certificate,
			'value'			 => date('Y-m-d H:i:s').' Vendor upload form.',
			'entry_stamp'	 => date('Y-m-d H:i:s'),
			'del'			 => 0
		);

		$this->db->insert('tr_history',$save_history);

		return parent::update($save['id'], $save);
	}

	public function updateAddition($id,$save){
		$this->table = 'tr_temuan';

		return parent::update($id, $save);
	}

	public function submit_process($id_certificate)
	{
		$query = "UPDATE ms_certificate SET status = status + 1 WHERE id = ?";
		$query = $this->db->query($query, array($id_certificate));
		return $this->db->where('id', $id_certificate)->get('ms_certificate')->row_array();
	}

	public function save_auditor($data, $id)
	{
		// $this->db->where('id', $id)
		// 		 ->update('ms_certificate', array('edit_stamp' => date("Y-m-d H:i:s")));
				// echo $this->db->last_query();
		$save_history = array(
			'id_certificate' => $id,
			'value'			 => date('Y-m-d H:i:s').' Pemilihan Auditor.',
			'entry_stamp'	 => date('Y-m-d H:i:s'),
			'del'			 => 0
		);

		$this->db->insert('tr_history',$save_history);

		return $this->db->insert('tr_auditor', $data);
	}

	public function save_laporan($data)
	{
		$this->table = 'ms_laporan_temuan';
		return parent::insert($data);
	}

	public function selesai_laporan($id)
	{		
		return $this->db->where('id',$id)->update('ms_certificate',array('status'=>2,'edit_stamp'=>date('Y-m-d H:i:s')));
	}

	public function get_auditor($id_leader)
	{
		$query = "SELECT * FROM ms_admin WHERE id_role = 3 AND del = 0 ";
		$query = $this->db->query($query)->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
	}

	public function approve_temuan($id)
	{
		return $this->db->where('id',$id)->update('tr_temuan',array('is_approved'=>1,'edit_stamp'=>date('Y-m-d H:i:s')));
	}

	public function get_temuan($id_certificate){
		$auditor = $this->session->userdata('admin')['name'];
		$query = "SELECT 
						*
				  FROM 
				  		tr_temuan 
				  -- JOIN
				  -- 		tb_certificate b ON b.id=a.id_certificate
				  WHERE 

				  		del = 0 AND id_certificate = ".$id_certificate." AND entry_by = '".$auditor."'";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}

		return $query;
	}

	function selectDataTemuan($id){
		$query = "	SELECT 
						*
				  FROM 
				  		tr_temuan a
				  WHERE
        				a.del = 0 AND a.id = ".$id;
		$query = $this->db->query($query)->row_array();
		return $query;
	}

	public function save_temuan($data)
	{
		return $this->db->insert('tr_temuan',$data);
	}

	public function submit_laporan($id)
	{
		return $this->db->where('id',$id)->update('ms_certificate',array('status'=>3,'edit_stamp'=>date('Y-m-d H:i:s')));
	}

	public function get_data_verifikasi($id)
	{
		$query = "SELECT 
						a.entry_by,
						a.key,
						a.desc,
						a.temuan_file,
						a.lampiran_admin,
						a.id,
						a.id_certificate
				  FROM 
				  		tr_temuan a
				  WHERE 
				  		a.del = 0 AND a.id_certificate = ".$id;
		return $query;
	}

	public function approve($id,$data)
	{
		$this->db->where('id',$id)->update('ms_certificate',$data);

		$query = "SELECT 
						a.name vendor_name,
						b.name certificate_name,
						c.id,
						d.vendor_email
				  FROM
				  		ms_certificate c
				  JOIN
				  		ms_vendor a ON c.id_user=a.id
				  JOIN
				  		ms_vendor_admistrasi d ON d.id_vendor=a.id
				  JOIN
				  		tb_certificate b ON c.id_certificate=b.id
				  WHERE
				  		c.del = 0 AND c.id = ".$id;
		return $this->db->query($query)->row_array();
	}

	public function getTaskAuditor()
	{
		$admin = $this->session->userdata('admin');
		$query = "SELECT 
						c.name nama_user,
						b.name nama_sertifikat,
						d.name jenis,
						f.name nama_leader,
						a.id,
						a.status
				  FROM 
				  		ms_certificate a
				  JOIN
				  		tb_certificate b ON b.id=a.id_certificate
				  JOIN 
				  		ms_vendor c ON c.id=a.id_user
				  JOIN  
				  		tb_category d ON d.id=b.id_category
				  JOIN  
				  		tr_auditor e ON a.id=e.id_sertifikat
				  JOIN
				  		ms_admin f ON f.id=e.is_leader
				  WHERE 
				  		a.del = 0 AND e.id_auditor LIKE '%".$admin['id_user']."%' ";
		return $query;
	}

	public function get_certificate($id)
	{
		$query = "SELECT
					b.name nama_sertifikat,
					c.name nama_user,
					e.name legal_name,
					a.id
				  FROM
				  	ms_certificate a
				  LEFT JOIN
				  	tb_certificate b ON b.id=a.id_certificate
				  LEFT JOIN
				  	ms_vendor c ON c.id=a.id_user
				  LEFT JOIN
				  	ms_vendor_admistrasi d ON d.id_vendor=c.id
				  LEFT JOIN
				  	tb_legal e ON e.id=d.id_legal
				  WHERE
				  	a.del = 0 AND a.id = ".$id;
		return $this->db->query($query);
	}

	public function get_email_leader($id)
	{
		$query = "SELECT
					a.email
				  FROM
				  	ms_admin a
				  WHERE
				  	del = 0 AND a.id = ".$id;
		return $this->db->query($query);
	}

	public function get_email_anggota($id)
	{
		$query = "SELECT
					a.email
				  FROM
				  	ms_admin a
				  WHERE
				  	del = 0 AND a.id IN (".$id.") ";
		return $this->db->query($query);
	}
}
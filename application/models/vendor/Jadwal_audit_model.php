<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Jadwal_audit_model extends MY_Model{
  public $table = 'ms_jadwal_audit';
  function __construct(){
    parent::__construct();
  }

  function getData($form){
        $user = $this->session->userdata('user');
    $query = "  SELECT 
                        a.jadwal_audit_file,
                        c.name as role_name,
                        b.name,
                        a.id
          FROM
              ms_jadwal_audit a
          JOIN
              ms_admin b on a.id_auditor = b.id
          JOIN
              tb_role c on b.id_role = c.id
          WHERE
                        a.del = 0 AND a.id_client = ".$user['id_user'];

    if($this->input->post('filter')){

      $query .= $this->filter($form, $this->input->post('filter'), false);

    }
    return $query;

  }

  function selectData($id){

    $query = "  SELECT 
                a.*
          FROM
              ms_jadwal_audit a 
                    WHERE 
                      a.del = 0 AND a.id = ".$id;

    $query = $this->db->query($query);
    return $query->row_array();

  }

    function insert($save) {
            $user = $this->session->userdata('user');
            $insert_login['id_client']              = $user['id_user'];
            $insert_login['jadwal_audit_file']      = $save['jadwal_audit_file'];
            $insert_login['del']                    = 0;            
            $insert_login['entry_stamp']            = timestamp();
            return $this->db->where('id',$id)
                            ->insert('ms_jadwal_audit',$insert_login);
    }

    function update($id, $save)
    {   
            $update_login['jadwal_audit_file']      = $save['jadwal_audit_file'];
            $update_login['del']            = 0;
            $update_login['edit_stamp']     = timestamp();
            return $this->db->where('id',$id)
                            ->update('ms_jadwal_audit',$update_login);
    }

    function delete($id) {
        return $this->db->where('id',$id)
                        ->update('ms_jadwal_audit',array('edit_stamp'=>date('Y-m-d H:i:s'), 'del' => 1));
    }
}
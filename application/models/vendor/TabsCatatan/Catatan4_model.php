<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Catatan4_model extends MY_Model{
	public $table = 'ms_catatan';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
    $user = $this->session->userdata('user');
		$query = "	SELECT 
                      a.Catatan4_file,             
                      a.id
					     FROM
    					        ms_catatan a
               WHERE    
                      a.del = 0 AND a.Catatan4_file IS NOT NULL AND a.id_vendor= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
                      a.Catatan4_file,
                      a.id
                FROM
                      ms_catatan a
                WHERE
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

  function insert($save) 
  {
          $user = $this->session->userdata('user');
          $insert['id_vendor']    = $user['id_user'];
          $insert['Catatan4_file']     = $save['Catatan4_file'];
          $insert['del']          = 0;            
          $insert['entry_stamp']  = timestamp();
          return $this->db->where('id',$id)
                          ->insert('ms_catatan',$insert);
  }
	
    function update($id, $save)
    { 
          $update['Catatan4_file']       = $save['Catatan4_file'];
          $update['entry_stamp']    = timestamp();
          $update['del']            = 0;

        return $this->db->where('id',$id)
                            ->update('ms_catatan',$update);
    }

    function delete($id) {
       $final = $this->db->where('id',$id)
                        ->update('ms_catatan',array('edit_stamp'=>date('Y-m-d H:i:s'), 'Catatan4_file' => null));
        return $final;
    }
}
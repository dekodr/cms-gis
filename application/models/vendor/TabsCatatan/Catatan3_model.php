<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Catatan3_model extends MY_Model{
	public $table = 'ms_catatan';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
    $user = $this->session->userdata('user');
		$query = "	SELECT 
                      a.Catatan3_file,             
                      a.id
					     FROM
    					        ms_catatan a
               WHERE    
                      a.del = 0 AND a.Catatan3_file IS NOT NULL AND a.id_vendor= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
                      a.Catatan3_file,
                      a.id
                FROM
                      ms_catatan a
                WHERE
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

  function insert($save) 
  {
          $user = $this->session->userdata('user');
          $insert['id_vendor']    = $user['id_user'];
          $insert['Catatan3_file']     = $save['Catatan3_file'];
          $insert['del']          = 0;            
          $insert['entry_stamp']  = timestamp();
          return $this->db->where('id',$id)
                          ->insert('ms_catatan',$insert);
  }
	
    function update($id, $save)
    { 
          $update['Catatan3_file']       = $save['Catatan3_file'];
          $update['entry_stamp']    = timestamp();
          $update['del']            = 0;

        return $this->db->where('id',$id)
                            ->update('ms_catatan',$update);
    }

    function delete($id) {
       $final = $this->db->where('id',$id)
                        ->update('ms_catatan',array('edit_stamp'=>date('Y-m-d H:i:s'), 'Catatan3_file' => null));
        return $final;
    }
}
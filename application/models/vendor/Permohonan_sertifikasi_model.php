<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Permohonan_sertifikasi_model extends MY_Model{
	public $table = 'ms_permohonan_sertifikasi';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
        $user = $this->session->userdata('user');
		$query = "	SELECT 
                        c.jenis jenis_permohonan,
                        d.name sistem_sertifikasi,
                        b.name nama_lingkup,
                        e.name lingkup_sertifikasi,
                        a.id,
                        a.name
					FROM
    					ms_permohonan_sertifikasi a
                    JOIN 
                        ms_lingkup_pemohon b on a.lingkup_pemohon_id = b.id
                    JOIN 
                        ms_lingkup_pemohon d on a.sistem_sertifikasi_id = d.id
                    JOIN 
                        ms_jenis_permohonan c on a.jenis_permohonan_id = c.id
                    JOIN 
                        ms_sistem_sertifikasi e on a.id_jenis_sertifikasi = e.id
                    WHERE
                        a.del = 0 AND a.id_vendor= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
    				    a.*
					FROM
    					ms_permohonan_sertifikasi a 
                    WHERE 
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

    function insert($save) {
            $user = $this->session->userdata('user'); 
            $insert['id_vendor']                = $user['id_user'];
            $insert['jenis_permohonan_id']      = $save['jenis_permohonan_id'];
            $insert['sistem_sertifikasi_id']    = $save['sistem_sertifikasi_id'];
            $insert['lingkup_pemohon_id']       = $save['lingkup_pemohon_id']; 
            $insert['id_jenis_sertifikasi']     = $save['id_jenis_sertifikasi'];
            $insert['name']                     = $save['name'];
            $insert['del']                      = 0;            
            $insert['entry_stamp']              = timestamp();
            $this->db->where('id',$id)
                            ->insert('ms_permohonan_sertifikasi',$insert);

            $insert_certificate['id_user']                      = $user['id_user'];
            $insert_certificate['id_certificate']               = $this->db->insert_id();
            $insert_certificate['jenis_permohonan_id']          = $save['jenis_permohonan_id'];
            $insert_certificate['sistem_sertifikasi_id']        = $save['sistem_sertifikasi_id'];
            $insert_certificate['status']                       = 1;
            $insert_certificate['reason']                       = $save['reason'];  
            $insert_certificate['Sertifikat_File']              = $save['Sertifikat_File'];
            $insert_certificate['Sertifikat_Draft_File']        = $save['Sertifikat_Draft_File'];
            $insert_certificate['del']                          = 0;            
            $insert_certificate['entry_stamp']                  = timestamp();
            return $this->db->where('id',$id)
                            ->insert('ms_certificate',$insert_certificate);



    }

    function update($id, $save)
    {   
            $update['jenis_permohonan_id']      = $save['jenis_permohonan_id'];
            $update['sistem_sertifikasi_id']    = $save['sistem_sertifikasi_id'];
            $update['lingkup_pemohon_id']       = $save['lingkup_pemohon_id'];
            $update['id_jenis_sertifikasi']     = $save['id_jenis_sertifikasi'];            
            $update['name']                     = $save['name'];
            $update['del']                      = 0;
            $update['edit_stamp']               = timestamp();
            return $this->db->where('id',$id)
                            ->update('ms_permohonan_sertifikasi',$update);
    }

    function delete($id) {
        $this->db->where('id',$id)
                        ->update('ms_certificate',array('edit_stamp'=>date('Y-m-d H:i:s'), 'del' => 1));
        return $this->db->where('id',$id)
                        ->update('ms_permohonan_sertifikasi',array('edit_stamp'=>date('Y-m-d H:i:s'), 'del' => 1));
    }
    public function getJenispermohonan()
    {
        $query = "  SELECT 
                        a.jenis,
                        a.id
                    FROM
                        ms_jenis_permohonan a 
                    WHERE 
                      a.del = 0";

        $query = $this->db->query($query)->result_array();
        $data = array();
        foreach ($query as $key => $value) {
            $data[$value['id']] = $value['jenis'];
        }
        return $data;
    }

    public function getSistemSertifikasi()
    {
       $query = "  SELECT 
                        a.name,
                        a.id
                    FROM
                        ms_lingkup_pemohon a 
                    WHERE 
                      a.del = 0 AND a.jenis_permohonan_id = 3 OR a.jenis_permohonan_id = 0";

        $query = $this->db->query($query)->result_array();
        $data = array();
         // $data['17'] = 'Pilih Salah Satu';
        foreach ($query as $key => $value) {
            $data[$value['id']] = $value['name'];
        }
        return $data;
    }

     public function getLingkupPemohon()
    {
       $query = "  SELECT 
                        a.name,
                        a.id
                    FROM
                        ms_lingkup_pemohon a 
                    WHERE 
                      a.del = 0 AND a.jenis_permohonan_id != 3 OR a.jenis_permohonan_id = 0";

        $query = $this->db->query($query)->result_array();
        $data = array();
         // $data['17'] = 'Pilih Salah Satu';
        foreach ($query as $key => $value) {
            $data[$value['id']] = $value['name'];
        }
        return $data;
    }

    public function getLP($id)
    {
        $query = "  SELECT
                        id,
                        name

                        FROM 
                        ms_lingkup_pemohon 

                        WHERE 
                        del=0 AND jenis_permohonan_id  = ".$id." OR jenis_permohonan_id = 0";

        $query = $this->db->query($query)->result_array();
        $data = array();
        // $data['17'] = 'Pilih Salah Satu';
        foreach ($query as $key => $value) {
            $data[$value['id']] = $value['name'];
        }
        return $data;
    }

    public function getJI($id)
    {
        $query = "  SELECT
                        id,
                        name

                        FROM 
                        ms_sistem_sertifikasi 

                        WHERE 
                        del=0 AND id_lingkup_pemohon  = ? OR id_lingkup_pemohon = 0";

        $query = $this->db->query($query,array($id))->result_array();
        $data = array();
        // $data['17'] = 'Pilih Salah Satu';
        foreach ($query as $key => $value) {
            $data[$value['id']] = $value['name'];
        }
        return $data;
    }

    public function getPermohonan($id_user)
    {
        $query= "
                SELECT
                        jenis_permohonan_id,
                        lingkup_pemohon_id,
                        sistem_sertifikasi_id
                FROM 
                    ms_permohonan_sertifikasi
                WHERE
                    del = 0 AND id_vendor = ?";
        $query =$this->db->query($query, array($id_user));
        return $query; 
    }

     public function getLingkupSertifikasi()
    {
       $query = "  SELECT 
                        name,
                        id
                    FROM
                        ms_sistem_sertifikasi 
                    WHERE 
                      del = 0";

        $query = $this->db->query($query)->result_array();
        $data = array();
         // $data['17'] = 'Pilih Salah Satu';
        foreach ($query as $key => $value) {
            $data[$value['id']] = $value['name'];
        }
        return $data;
    }
}
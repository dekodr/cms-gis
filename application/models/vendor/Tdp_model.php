<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Tdp_model extends MY_Model{
	public $table = 'ms_tdp';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$user = $this->session->userdata('user');
		$query = "SELECT 
						a.no,
						a.issue_date,
						a.authorize_by,
						a.expire_date,
						a.tdp_file,
						a.id
				   FROM 
				   		".$this->table." a
				   WHERE 
				   		a.del = 0 AND id_vendor = ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT
						a.*
					FROM 
						".$this->table." a 
					WHERE 
						a.id = ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	function get_data_by_vendor($id) 
    {
		$this->db->select('*');
		$this->db->where('del',0);
		$this->db->where('id_vendor',$id);
		
		$query = $this->db->get('ms_tdp');		
		return $query;
    }
	
}

<?php 
/**
 * 
 */
class Perbaikan_model extends MY_Model
{
	public function getData($id_user)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
		$query = "	SELECT
						b.jenis,
						a.id,
						d.name,
						c.id id_certificate
					FROM
						ms_tahap_audit a
					JOIN
						ms_permohonan_sertifikasi c ON c.id=a.id_certificate
					JOIN
						ms_jenis_permohonan b ON c.jenis_permohonan_id=b.id
					JOIN
						ms_lingkup_pemohon d ON d.id=c.sistem_sertifikasi_id
					WHERE
						a.del = 0 AND a.id_client = ".$id_user." AND a.tahap = 2";

		$query .= " GROUP BY c.id";

		return $query;
	}

	public function getDataPerbaikan($id_certificate)
	{
		$query = "	SELECT
						b.name,
						a.tahap,
						a.entry_stamp,
						a.id
					FROM
						ms_tahap_audit a
					JOIN
						ms_admin b ON b.id=a.id_auditor
					WHERE
						a.del = 0 AND a.id_certificate =".$id_certificate." AND a.tahap = 2";
		return $query;
	}

	public function selectData($id)
	{
		return $this->db->where('id',$id)->get('ms_tahap_audit')->row_array();
	}
	public function selectDataPerbaikan($id)
	{
		$query = "	SELECT
						b.name id_auditor,
						a.tahap_audit_file,
						a.id,
						a.*
					FROM
						ms_tahap_audit a
					JOIN
						ms_admin b ON b.id=a.id_auditor
					WHERE
						a.del = 0 AND a.id = ? ";
		$query = $this->db->query($query,array($id))->row_array();
		return $query;
	}

	public function selectDataHistory($id)
	{
		$query = "	SELECT
						b.name id_auditor,
						a.tahap_audit_file,
						a.id,
						a.*
					FROM
						tr_log_audit a
					JOIN
						ms_admin b ON b.id=a.id_auditor
					WHERE
						a.del = 0 AND a.id = ? ";
		$query = $this->db->query($query,array($id))->row_array();
		return $query;
	}

	public function saveTindakLanjut($id,$save_)
	{
		$get = $this->db->where('id',$id)->get('ms_tahap_audit')->row_array();

		$save['id_log'] 			= $id;
		$save['id_auditor'] 		= $get['id_auditor'];
		$save['id_client'] 			= $get['id_client'];
		$save['id_certificate'] 	= $get['id_certificate'];
		$save['tahap_audit_file'] 	= $get['tahap_audit_file'];
		$save['tahap'] 				= $get['tahap'];
		$save['jenis_audit'] 		= $get['jenis_audit'];
		$save['standar_audit'] 		= $get['standar_audit'];
		$save['tgl_audit'] 			= $get['tgl_audit'];
		$save['no'] 				= $get['no'];
		$save['uraian'] 			= $get['uraian'];
		$save['bagian_audit'] 		= $get['bagian_audit'];
		$save['klausul'] 			= $get['klausul'];
		$save['kategori'] 			= $get['kategori'];
		$save['tgl_rencana'] 		= $get['tgl_rencana'];
		$save['analisis'] 			= $save_['analisis'];
		$save['koreksi'] 			= $save_['koreksi'];
		$save['tindakan'] 			= $save_['tindakan'];
		$save['tgl_perbaikan'] 		= $save_['tgl_perbaikan'];
		$save['dokumen_perbaikan_file']= $save_['dokumen_perbaikan_file'];

		$this->db->insert('tr_log_audit',$save);

		return $this->db->where('id',$id)->update('ms_tahap_audit',$save_);
	}

	function getHistori($id){

		$query = "	SELECT
						a.*
					FROM 
						ms_tahap_audit a 
					WHERE 
						a.id = ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	public function getDataHistory($id_log)
	{
		$query = "	SELECT
						a.name client_name,
						b.name auditor_name,
						c.tgl_audit,
						c.id,
						c.analisis
					FROM
						tr_log_audit c
					JOIN
						ms_admin b ON b.id=c.id_auditor
					JOIN
						ms_vendor a ON a.id=c.id_client
					WHERE
						c.del = 0 AND c.id_log = ".$id_log;
		return $query;
	}
}
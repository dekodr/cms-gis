<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Izin_model extends MY_Model{
	public $table = 'ms_ijin_usaha';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
		$user = $this->session->userdata('user');
		$query = "SELECT 
						a.type,
                        a.izin_file,
                        a.id
				   FROM 
				   		".$this->table." a
				   WHERE 
				   		a.del = 0 AND a.id_vendor = ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}
	
	function selectData($id){

		$query = "	SELECT 
						a.type,
                        a.izin_file,
                        a.id
				   FROM 
				   		".$this->table." a
                   WHERE 
						a.id = ? AND a.del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}
}

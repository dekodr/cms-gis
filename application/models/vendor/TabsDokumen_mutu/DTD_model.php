<?php defined('BASEPATH') OR exit('No direct script access allowed');
class DTD_model extends MY_Model{
	public $table = 'ms_dokumen_mutu';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
    $user = $this->session->userdata('user');
		$query = "	SELECT 
                      a.dtd_file,             
                      a.id
					     FROM
    					        ms_dokumen_mutu a
               WHERE
                      
    a.del = 0 AND a.dtd_file IS NOT NULL AND a.id_vendor= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
                      a.dtd_file,
                      a.id
                FROM
                      ms_dokumen_mutu a
                WHERE
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

	function insert($save) 
  {
          $user = $this->session->userdata('user');
          $insert_pl['id_vendor']              = $user['id_user'];
          $insert_pl['dtd_file']                = $save['dtd_file'];
          $insert_pl['del']                    = 0;            
          $insert_pl['entry_stamp']            = timestamp();
          return $this->db->where('id',$id)
                          ->insert('ms_dokumen_mutu',$insert_pl);
  }

  function update($id, $save)
  { 
        $update['dtd_file']        = $save['dtd_file'];
        $update['entry_stamp']    = timestamp();
        $update['del']            = 0;

      return $this->db->where('id',$id)
                          ->update('ms_dokumen_mutu',$update);
  }

  function delete($id) {
     $final = $this->db->where('id',$id)
                      ->update('ms_dokumen_mutu',array('edit_stamp'=>date('Y-m-d H:i:s'), 'dtd_file' => null));
      return $final;
  }
}
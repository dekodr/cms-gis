<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Peralatan_produksi_model extends MY_Model{
	public $table = 'ms_dokumen_mutu';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
    $user = $this->session->userdata('user');
		$query = "	SELECT 
                      a.pp_file,             
                      a.id
					     FROM
    					        ms_dokumen_mutu a
               WHERE
                      
               a.del = 0 AND a.pp_file IS NOT NULL AND a.jenis_sertifikasi = 5 AND a.id_vendor= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
                      a.pp_file,
                      a.id
                FROM
                      ms_dokumen_mutu a
                WHERE
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

	function insert($save) {
          $user = $this->session->userdata('user');
          $insert_pp['id_vendor']              = $user['id_user'];
          $insert_pp['pp_file']                = $save['pp_file'];
          $insert_pp['jenis_sertifikasi']                    = 5;    
          $insert_pp['del']                    = 0;            
          $insert_pp['entry_stamp']            = timestamp();
          return $this->db->where('id',$id)
                          ->insert('ms_dokumen_mutu',$insert_pp);
  }

  function update($id, $save)
  {           
        $update['pp_file']        = $save['pp_file'];
        $update['jenis_sertifikasi']            = 5;
        $update['entry_stamp']    = timestamp();
        $update['del']            = 0;

      return $this->db->where('id',$id)
                          ->update('ms_dokumen_mutu',$update);
  }

  function delete($id) {
     $final = $this->db->where('id',$id)
                      ->update('ms_dokumen_mutu',array('edit_stamp'=>date('Y-m-d H:i:s'), 'pp_file' => null));
      return $final;
  }
}
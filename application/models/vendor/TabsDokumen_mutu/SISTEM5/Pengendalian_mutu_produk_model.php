<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pengendalian_mutu_produk_model extends MY_Model{
	public $table = 'ms_dokumen_mutu';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
    $user = $this->session->userdata('user');
		$query = "	SELECT 
                      a.pmp_file,             
                      a.id
					     FROM
    					        ms_dokumen_mutu a
               WHERE
                      
    a.del = 0 AND a.pmp_file IS NOT NULL AND a.jenis_sertifikasi = 5 AND a.id_vendor= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
                      a.pmp_file,
                      a.id
                FROM
                      ms_dokumen_mutu a
                WHERE
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

  function insert($save) {
          $user = $this->session->userdata('user');
          $insert_pmp['id_vendor']              = $user['id_user'];
          $insert_pmp['pmp_file']               = $save['pmp_file'];
          $insert_pmp['jenis_sertifikasi']                    = 5;  
          $insert_pmp['del']                    = 0;            
          $insert_pmp['entry_stamp']            = timestamp();
          return $this->db->where('id',$id)
                          ->insert('ms_dokumen_mutu',$insert_pmp);
  }
	
  function update($id, $save)
  { 
        $update['pmp_file']       = $save['pmp_file'];
        $update['jenis_sertifikasi']            = 5;
        $update['entry_stamp']    = timestamp();
        $update['del']            = 0;

      return $this->db->where('id',$id)
                          ->update('ms_dokumen_mutu',$update);
  }

  function delete($id) {
     $final = $this->db->where('id',$id)
                      ->update('ms_dokumen_mutu',array('edit_stamp'=>date('Y-m-d H:i:s'), 'pmp_file' => null));
      return $final;
  }
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Alur_proses_produksi_model extends MY_Model{
	public $table = 'ms_dokumen_mutu';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
    $user = $this->session->userdata('user');
		$query = "	SELECT 
                      a.app_file,             
                      a.id
					     FROM
    					        ms_dokumen_mutu a
               WHERE
                      a.del = 0 AND a.app_file IS NOT NULL AND a.jenis_sertifikasi = 3 AND a.id_vendor= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
                      a.app_file,
                      a.id
                FROM
                      ms_dokumen_mutu a
                WHERE
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

  function insert($save) 
  {
          $user = $this->session->userdata('user');
          $insert_app['id_vendor']    = $user['id_user'];
          $insert_app['app_file']     = $save['app_file'];
          $insert_app['jenis_sertifikasi']                    = 3;    
          $insert_app['del']          = 0;            
          $insert_app['entry_stamp']  = timestamp();
          return $this->db->where('id',$id)
                          ->insert('ms_dokumen_mutu',$insert_app);
  }
  function update($id, $save)
  {       
        $update['app_file']       = $save['app_file'];
        $update['jenis_sertifikasi']            = 3;
        $update['entry_stamp']    = timestamp();
        $update['del']            = 0;

      return $this->db->where('id',$id)
                          ->update('ms_dokumen_mutu',$update);
  }

  function delete($id) {
     $final = $this->db->where('id',$id)
                      ->update('ms_dokumen_mutu',array('edit_stamp'=>date('Y-m-d H:i:s'), 'app_file' => null));
      return $final;
  }
}
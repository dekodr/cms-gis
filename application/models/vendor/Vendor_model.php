<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Vendor_model extends MY_Model{
	public $table = 'ms_vendor';
	function __construct(){
		parent::__construct();
	}

	function getDataDpt($form){

		$admin = $this->session->userdata('admin');
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
		$query = "SELECT  
					    a.name name, 
					    b.npwp_code npwp_code,
					    b.nppkp_code nppkp_code,
					    h.name legal_name,
						a.id as id, 
					    b.vendor_city, 
					    b.vendor_province, 
					    a.id msv_id,
					    a.id as id_vendor_list,
						b.vendor_office_status,
					    b.id_legal as id_legal,
					    (SELECT AVG(point) FROM tr_assessment WHERE id_vendor = msv_id  ) npp,
					    -- CONCAT(i.name, '-'  ,j.name) as bsb,
					    b.vendor_type,
						-- f.id_bidang,
						-- f.id_sub_bidang,
						k.id id_agen,
						l.id_agen agen_id,
						l.merk
				   FROM 
				   		ms_vendor a
				   LEFT JOIN
				   		ms_agen k ON k.id_vendor=a.id
				   LEFT JOIN
				   		ms_agen_produk l ON l.id_agen=k.id
				   -- LEFT JOIN
				   -- 		ms_iu_bsb f ON f.id_vendor=a.id
				   LEFT JOIN
				   		tr_dpt g ON g.id_vendor=a.id
				   LEFT JOIN 
				   		ms_vendor_admistrasi b ON b.id_vendor=a.id
				   LEFT JOIN
				   		tb_legal h ON h.id=b.id_legal
				   LEFT JOIN
				   		tr_assessment ON tr_assessment.id_vendor=a.id
				   -- LEFT JOIN
				   -- 		tb_bidang i ON i.id=f.id_bidang
				   -- LEFT JOIN
				   -- 		tb_sub_bidang j ON j.id_bidang=i.id
				   WHERE 
				   		a.del = 0 AND a.vendor_status = 2 AND a.is_active = 1 AND NOT EXISTS(SELECT 1 FROM tr_blacklist d WHERE d.id_vendor = a.id AND (d.del = 0))";
				

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		$query .=" GROUP BY 
				   		a.id";
				   		// echo $query;
		return $query;

	}

	function getDataDaftar($form){
		$admin = $this->session->userdata('admin');

		$query = "SELECT 
						a.id id, 
						a.name name, 
						b.name legal_name, 
						c.username, 
						c.password,  
						c.password_raw
				   FROM 
				   		".$this->table." a
				   LEFT JOIN 
				   		ms_login c ON c.id_user = a.id
				   LEFT JOIN
				   		tb_legal b ON b.id = ms_vendor_admistrasi.id_legal
				   WHERE 
				   		a.del IS NULL OR a.del = 0";

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function getDataTunggu($form){
		$query = "SELECT
						ms_vendor.id as id,
						tb_legal.name legal_name,
						ms_vendor.name name, 
						ms_vendor.edit_stamp last_update, 
						is_new, 
						ba_file

				  FROM 
				  		ms_vendor

				  WHERE 
				  		ms_vendor.vendor_status = 1

				  LEFT JOIN
				  		ms_vendor_admistrasi as mva ON mva.id_vendor=ms_vendor.id

				  LEFT JOIN
				  		tb_legal ON tb_legal.id=mva.id_legal

				  LEFT JOIN
				  		ms_ba ON ms_ba.id_vendor=ms_vendor.id

				  WHERE
				  		ms_vendor.is_active = 1

				  WHERE 
				  		vendor_status = 1
				  		";
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	function selectData($id){

		$query = "	SELECT
						title,
						publisher
					FROM ".$this->table." a WHERE id = ? AND del = 0";

		$query = $this->db->query($query, array($id));
		return $query->row_array();

	}

	public function get_pt(){
		$this->db->select('tb_legal.name legal_name,ms_vendor.name name')
		->join('ms_vendor_admistrasi','ms_vendor_admistrasi.id_vendor = ms_vendor.id')
		->join('tb_legal','tb_legal.id=ms_vendor_admistrasi.id_legal')
		->where('vendor_status',1);
		$query = $this->db->get('ms_vendor');
		return $query->row_array();
	}

	function to_waiting_list(){
		$user = $this->session->userdata('user');

		$pemohon = $this->db->where('del',0)->where('id_vendor',$user['id_user'])->get('ms_identitas_pemohon')->result_array();
		$pabrik	 = $this->db->where('del',0)->where('id_vendor',$user['id_user'])->get('ms_identitas_pabrik')->result_array();
		$sertifikasi = $this->db->where('del',0)->where('id_vendor',$user['id_user'])->get('ms_permohonan_sertifikasi')->result_array();

		if (count($pemohon) < 1) {
			$res['msg']= "Harap Isi Identitas Pemohon!";
			$res['link'] = 'identitas_pemohon';
		} 
		// else if (count($pabrik) < 1) {
		// 	$res['msg'] = "Harap Isi Identitas Pabrik!";
		// 	$res['link'] = 'identitas_pabrik';
		// }
		else if (count($sertifikasi) < 1) {
			$res['msg'] = "Harap Isi Permohonan Sertifikasi!";
			$res['link'] = 'permohonan_sertifikasi';
		}else {
			$this->db->where('id',$user['id_user']);
			$this->db->update('ms_vendor',array(
				'vendor_status'=>1,
				'is_new'=>1
				)
			);

			$get_p_s = $this->db->where('del',0)->where('id_vendor',$user['id_user'])->get('ms_permohonan_sertifikasi')->result_array();

			foreach ($get_p_s as $key => $value) {
				$get_n_s 	= $this->cekNoSer($value['jenis_permohonan_id']);
				$create_n_s = $this->createNoSer($get_n_s['id'],$value['jenis_permohonan_id']);

				$arr = array(
					'id_vendor'			=> $user['id_user'],
					'id_certificate'	=> $value['id'],
					'no'				=> $create_n_s,
					'entry_stamp'		=> date('Y-m-d H:i:s')
				);
				$this->db->insert('ms_no_pengajuan',$arr);
			}

			$res['msg'] = 'Berhasil';
			$res['link'] = 'dashboard';
		}
		return $res;
	}

	public function cekNoSer($jenis_permohonan)
	{
		$last_number = $this->db->where('del',0)->where('jenis_permohonan_id',$jenis_permohonan)->order_by('id','desc')->get('ms_permohonan_sertifikasi')->row_array();
		return $last_number;
	}

	public function createNoSer($last_number,$jenis_permohonan)
	{
		$number = $this->createNumber($last_number);
		$format = $this->createFormat($jenis_permohonan);

		$code_ser = $number.'/'.$format.'/'.date('m').'/'.date('Y');
		return $code_ser;
	}

	public function createNumber($ln)
	{
		if (strlen($ln) == '1') {
			$code = '00'.$ln;
		} else if (strlen($ln) == '2') {
			$code = '0'.$ln;
		} else {
			$code = $ln;
		}

		return $code;
	}

	public function createFormat($jenis)
	{
		if ($jenis == '1') {
			$code = 'LSMM-GIS';
		} else if ($jenis == '2') {
			$code = 'LSML-GIS';
		} else if ($jenis == '3') {
			$code = 'LSPr-GIS';
		}

		return $code;
	}

	public function get_administrasi_list($id){

		$this->db->select('*, ms_vendor.id as id ,ms_vendor.name name, mva.npwp_code npwp_code,mva.nppkp_code nppkp_code,mva.nppkp_date nppkp_date, tb_legal.name id_legal, mva.vendor_address ,mva.npwp_date npwp_date,mva.vendor_office_status kantor, mva.vendor_country country, mva.vendor_province province, mva.vendor_city city, mva.vendor_phone phone, mva.vendor_fax fax, mva.vendor_email email, mva.vendor_website website, ms_akta.no no, ms_akta.notaris notaris, ms_akta.issue_date issue_date, ms_akta.authorize_by authorize_by, ms_akta.authorize_no authorize_no, ms_akta.authorize_date authorize_date, ms_pengurus.name name_pengurus, ms_pengurus.no no_ktp, ms_pengurus.position_expire exp,ms_pengurus.position pos')
		->where('mva.id_vendor',$id)
		->join('ms_vendor_admistrasi as mva','mva.id_vendor=ms_vendor.id','LEFT')
		->join('tb_legal','tb_legal.id=mva.id_legal','LEFT')
		->join('ms_akta','ms_akta.id_vendor=ms_vendor.id','LEFT')
		->join('ms_pengurus','ms_pengurus.id_vendor=ms_vendor.id','LEFT')
		->join('ms_pemilik','ms_pemilik.id_vendor=ms_vendor.id','LEFT')
		->join('ms_situ','ms_situ.id_vendor=ms_vendor.id','LEFT')
		->join('tr_dpt','tr_dpt.id_vendor=ms_vendor.id','LEFT')
		->group_by('ms_vendor.id');

		$query = $this->db->get('ms_vendor');
		return $query->result_array();
	}

	function get_vendor_name($id){
		return $this->db->select('*,ms_vendor_admistrasi.vendor_email email')
						->where('ms_vendor.id',$id)
						->join('ms_vendor_admistrasi','ms_vendor_admistrasi.id_vendor=ms_vendor.id')
						->get('ms_vendor')->row_array();
	}

	function get_data($id=0){

		if(!$id){
			$user = $this->session->userdata('user');
			$id = $user['id_user'];
		}
		$query = "	SELECT 	b.*,
							a.*,
							a.id id,
						 	a.name as name, 
						 	a.npwp_code as npwp_code, 
						 	b.data_status data_status,
						 	a.vendor_status

				 	FROM ms_vendor a
				 	LEFT JOIN ms_vendor_admistrasi b ON a.id = b.id_vendor
					WHERE a.id = ?";
		
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function get_data_sbu($id = '', $type = '', $sub_type = '', $id_approver = '', $kode_transaksi = ''){

		if($type != "all") $sql = "SELECT a.* FROM ms_ijin_usaha a WHERE a.id_vendor = ? AND a.type = 'sbu' AND del = 0";
		else $sql = "SELECT a.* FROM ms_ijin_usaha a WHERE a.id_vendor = ? AND del = 0";
		
		return $this->db->query($sql, array($id ));
	}

	public function export_dpt($filter=array())
	{
		$admin = $this->session->userdata('admin');
		$query = $this->db->select('ms_vendor.id as id, `ms_vendor`.`id` `msv_id`,ms_vendor.id as id_vendor_list , ms_vendor.name name, ms_vendor_admistrasi.npwp_code npwp_code,ms_vendor_admistrasi.nppkp_code nppkp_code, vendor_city, vendor_province,(SELECT AVG(point) FROM tr_assessment WHERE id_vendor = msv_id  ) npp ')
		->where('ms_vendor.vendor_status',2)
		->where('ms_vendor.is_active',1)
		->where('NOT EXISTS(
					SELECT 1 FROM tr_blacklist_nik c JOIN ms_pengurus msp ON msp.no = c.nik WHERE msp.id_vendor = ms_vendor.id AND (c.del = 0 OR c.del IS NULL)
				)')
		->where('NOT EXISTS(
					SELECT 1 FROM tr_blacklist d WHERE d.id_vendor = ms_vendor.id AND (d.del = 0 OR d.del IS NULL)
				)')
		->join('ms_vendor_admistrasi','ms_vendor_admistrasi.id_vendor=ms_vendor.id','LEFT')
		->join('tr_assessment','tr_assessment.id_vendor=ms_vendor.id','LEFT');
		// if(!in_array($admin['id_role'], array(1,2,8))){
		// 	$query .= $this->db->where('id_sbu',$admin['id_sbu']);
		// }
		if($this->input->post('filter')){

			$query .= $this->db->order_by($this->input->post('filter'));

		}
		$query = $this->db->get('ms_vendor');
		return $query->result_array();
	}

	function get_total_daftar_tunggu(){
		$query = " 	SELECT 
						a.*
					FROM
						ms_vendor a
					JOIN
						ms_vendor_admistrasi b ON b.id_vendor=a.id 
					WHERE
						a.del = 0 AND a.vendor_status = 1";
		return $query;
		// return $this->db->select('*')->where('vendor_status',1)->where('ms_vendor.del',0)->join('ms_vendor_admistrasi','ms_vendor.id=ms_vendor_admistrasi.id_vendor')->get('ms_vendor')->num_rows();
	}

	function get_client(){
		$admin = $this->session->userdata('admin');

		$query = " 	SELECT 
						name,
						vendor_status,
						id,
						need_approve
					FROM
						ms_vendor 
					WHERE
						del = 0";
		// echo $this->db->last_query();die;
		return $query;
		// return $this->db->select('*')->where('vendor_status',1)->where('ms_vendor.del',0)->join('ms_vendor_admistrasi','ms_vendor.id=ms_vendor_admistrasi.id_vendor')->get('ms_vendor')->num_rows();
	}

	function get_total_dpt(){
		return $this->db->select('*')->where('vendor_status',2)->where('ms_vendor.del',0)->join('ms_vendor_admistrasi','ms_vendor.id=ms_vendor_admistrasi.id_vendor')->get('ms_vendor')->num_rows();
	}
}

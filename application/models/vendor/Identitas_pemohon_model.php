<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Identitas_pemohon_model extends MY_Model{
	public $table = 'ms_identitas_pemohon';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
        $user = $this->session->userdata('user');
		$query = "	SELECT 
                        a.jenis_perusahaan,
                        a.name,
                        a.alamat,
                        a.kota,
                        a.provinsi,
                        a.kode_pos,
                        a.telepon,
                        a.fax,
                        a.email,
                        a.nama_penanggung_jawab,
                        a.jabatan,
                        a.contact_person,
                        a.no_hp,
                        a.id
					FROM
    					ms_identitas_pemohon a
                    WHERE
                        a.del = 0 AND a.id_vendor= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
    				    a.*
					FROM
    					ms_identitas_pemohon a 
                    WHERE 
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

    function insert($save) 
    {
            $user = $this->session->userdata('user');
            // $insertData['id_sbu']                   = $save['id_sbu'];
            // $insertData['id_role']                  = $save['id_role'];
            // $insertData['npwp_code']                = $save['npwp_code'];
            // $insertData['identitas_pemohon_status'] = 1;
            // $this->db->where('id',$id)
            //          ->update('ms_vendor',$insertData);

            $insert_login['id_vendor']              = $user['id_user'];
            $insert_login['jenis_perusahaan']       = $save['jenis_perusahaan'];
            $insert_login['name']                   = $save['name'];
            $insert_login['alamat']                 = $save['alamat'];
            $insert_login['kota']                   = $save['kota'];
            $insert_login['provinsi']               = $save['provinsi'];
            $insert_login['kode_pos']               = $save['kode_pos'];
            $insert_login['telepon']                = $save['telepon'];
            $insert_login['fax']                    = $save['fax'];
            $insert_login['email']                  = $save['email'];
            $insert_login['nama_penanggung_jawab']  = $save['nama_penanggung_jawab'];
            $insert_login['jabatan']                = $save['jabatan'];
            $insert_login['contact_person']         = $save['contact_person'];
            $insert_login['no_hp']                  = $save['no_hp'];
            $insert_login['del']                    = 0;            
            $insert_login['entry_stamp']            = timestamp();
            return $this->db->where('id',$id)
                            ->insert('ms_identitas_pemohon',$insert_login);
    }

    function update($id, $save)
    {   
            $update_login['jenis_perusahaan']       = $save['jenis_perusahaan'];
            $update_login['name']                   = $save['name'];
            $update_login['alamat']                 = $save['alamat'];
            $update_login['kota']                   = $save['kota'];
            $update_login['provinsi']               = $save['provinsi'];
            $update_login['kode_pos']               = $save['kode_pos'];
            $update_login['telepon']                = $save['telepon'];
            $update_login['fax']                    = $save['fax'];
            $update_login['email']                  = $save['email'];
            $update_login['nama_penanggung_jawab']  = $save['nama_penanggung_jawab'];
            $update_login['jabatan']                = $save['jabatan'];
            $update_login['contact_person']         = $save['contact_person'];
            $update_login['no_hp']                  = $save['no_hp'];
            $update_login['del']            = 0;
            $update_login['edit_stamp']     = timestamp();
            return $this->db->where('id',$id)
                            ->update('ms_identitas_pemohon',$update_login);
    }

    function delete($id) {
        return $this->db->where('id',$id)
                        ->update('ms_identitas_pemohon',array('edit_stamp'=>date('Y-m-d H:i:s'), 'del' => 1));
    }

    public function getIdentitasPemohon($id_user)
    {
        $query= "
                SELECT
                       name
                FROM 
                    ms_identitas_pemohon
                WHERE
                    del = 0 AND id_vendor = ?";
        $query =$this->db->query($query, array($id_user));
        return $query; 
    }
}
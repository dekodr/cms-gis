<?php defined('BASEPATH') OR exit('No direct script access allowed');
class NIB_model extends MY_Model{
	public $table = 'ms_legalitas';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
    $user = $this->session->userdata('user');
		$query = "	SELECT 
                      a.nib_file,             
                      a.id
					     FROM
    					        ms_legalitas a
               WHERE    
                      a.del = 0 AND a.nib_file IS NOT NULL AND a.jenis_sertifikasi = 5 AND a.id_vendor= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
                      a.nib_file,
                      a.id
                FROM
                      ms_legalitas a
                WHERE
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

  function insert($save) 
  {
          $user = $this->session->userdata('user');
          $insert['id_vendor']    = $user['id_user'];
          $insert['nib_file']     = $save['nib_file'];
          $insert['jenis_sertifikasi']          = 5;    
          $insert['del']          = 0;            
          $insert['entry_stamp']  = timestamp();
          return $this->db->where('id',$id)
                          ->insert('ms_legalitas',$insert);
  }
	
    function update($id, $save)
    { 
          $update['nib_file']       = $save['nib_file'];
          $update['jenis_sertifikasi']            = 5;
          $update['entry_stamp']    = timestamp();
          $update['del']            = 0;

        return $this->db->where('id',$id)
                            ->update('ms_legalitas',$update);
    }

    function delete($id) {
       $final = $this->db->where('id',$id)
                        ->update('ms_legalitas',array('edit_stamp'=>date('Y-m-d H:i:s'), 'nib_file' => null));
        return $final;
    }
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
class HAKI_model extends MY_Model{
	public $table = 'ms_legalitas';
	function __construct(){
		parent::__construct();
	}

	function getData($form){
    $user = $this->session->userdata('user');
		$query = "	SELECT 
                      a.haki_file,             
                      a.id
					     FROM
    					        ms_legalitas a
               WHERE    
                      a.del = 0 AND a.haki_file IS NOT NULL AND a.id_vendor= ".$user['id_user'];

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;

	}

	function selectData($id){

		$query = "	SELECT 
                      a.haki_file,
                      a.id
                FROM
                      ms_legalitas a
                WHERE
                      a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query);
		return $query->row_array();

	}

  function insert($save) 
  {
          $user = $this->session->userdata('user');
          $insert['id_vendor']    = $user['id_user'];
          $insert['haki_file']     = $save['haki_file'];
          $insert['del']          = 0;            
          $insert['entry_stamp']  = timestamp();
          return $this->db->where('id',$id)
                          ->insert('ms_legalitas',$insert);
  }
	
    function update($id, $save)
    { 
          $update['haki_file']       = $save['haki_file'];
          $update['entry_stamp']    = timestamp();
          $update['del']            = 0;

        return $this->db->where('id',$id)
                            ->update('ms_legalitas',$update);
    }

    function delete($id) {
       $final = $this->db->where('id',$id)
                        ->update('ms_legalitas',array('edit_stamp'=>date('Y-m-d H:i:s'), 'haki_file' => null));
        return $final;
    }
}
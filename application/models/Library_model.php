<?php
/**
 * 
 */
class Library_model extends MY_Model
{
	public $table = 'tr_folder';

	public function getData()
	{
		return $this->db->where('del',0)->get('tr_folder')->result_array();
	}

	public function getSubFolder($id_parent)
	{
		return $this->db->where('del',0)->where('id_main_folder',$id_parent)->where('is_parent_here',null)->get('tr_sub_folder')->result_array();
	}
	
	public function getSubFolder_2($id_parent)
	{
		$query = "SELECT 
						a.*,
                        b.name as name_sub,
                        b.is_parent_here
				  FROM 
				  		tr_sub_folder as a
				  JOIN
						tr_sub_folder as b ON a.id=b.is_parent_here = 1
				  WHERE
				  		b.del = 0 AND b.is_parent_here = ? ";
		$query = $this->db->query($query, array($id_parent));
		// echo $query;die;
		return $query->result_array();
	}
}
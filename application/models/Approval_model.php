<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Approval_model extends MY_Model {
	
	function __construct(){
		parent::__construct();
	}

	function get_data_administrasi($id = 0){
		// if(!$id){
		// 	$user = $this->session->userdata('user');
		// 	$id = $user['id_user'];
		// }
		$query = "SELECT 
						a.id id,
						id_legal, 
 						a.name as name,
						a.npwp_code as npwp_code, 
						npwp_file, 
						npwp_date,
						nppkp_code, 
						nppkp_file, 
						nppkp_date, 
						vendor_office_status, 
						vendor_address, 
						vendor_country, 
						vendor_phone, 
						vendor_province, 
						vendor_fax, 
						vendor_city, 
						vendor_email, 
						vendor_postal, 
						vendor_website,
						vendor_type, 
						d.name as legal_name,
						b.id id_admistrasi,
						b.email_pic,
						f.sps_file,
						 ( SELECT COUNT(*) FROM tr_note e WHERE e.document_type = 'ms_vendor_admistrasi' AND e.is_active = 1 AND e.id_vendor = $id AND e.id_document = b.id) total_note
 				   FROM 
 				   		ms_vendor a
				   LEFT JOIN
				   		ms_vendor_admistrasi b ON b.id_vendor=a.id
				   LEFT JOIN
				   		tb_legal d ON d.id=b.id_legal
				   LEFT JOIN
				   		ms_formulir_permohonan f ON f.id_vendor=a.id
 				   WHERE 
				   		f.sps_file IS NOT NULL AND a.id = ".$id;

		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	public function getVendorStatus($id)
	{
		$data = $this->db->where('id',$id)->where('del',0)->get('ms_vendor')->row_array();
		return $data['vendor_status'];
	}

	function get_data_Identitas_pemohon($id = 0){
		if(!$id){
			$user = $this->session->userdata('user');
			$id = $user['id_user'];
		}
		$query = "SELECT 
						c.jenis_perusahaan, 
						a.name as name,
						a.npwp_code as npwp_code,
						c.alamat,
						c.kota,
						c.provinsi,
						c.kode_pos,
						c.telepon,
						c.fax,
						c.email,
						c.nama_penanggung_jawab,
						c.jabatan,
						c.contact_person,
						c.no_hp,
						a.id,
						c.data_status,
						c.id id_doc,
						( SELECT COUNT(*) FROM tr_note e WHERE e.document_type = 'ms_identitas_pemohon' AND e.is_active = 1 AND e.id_vendor = $id AND e.id_document = c.id) total_note
				   FROM 
				   		ms_vendor a
				   	JOIN
				   		ms_identitas_pemohon c on c.id_vendor = a.id
				   WHERE 
				   		a.id = ?";

		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	function get_data_Pembayaran($id = 0){
		if(!$id){
			$user = $this->session->userdata('user');
			$id = $user['id_user'];
		}
		$query = "SELECT 
						c.pembayaran_file,
						a.id,
						c.data_status,
						c.id id_doc,
						( SELECT COUNT(*) FROM tr_note e WHERE e.document_type = 'ms_pembayaran' AND e.is_active = 1 AND e.id_vendor = $id AND e.id_document = c.id) total_note
				   FROM 
				   		ms_vendor a
				   	JOIN
				   		ms_pembayaran c on c.id_vendor = a.id
				   WHERE 
				   		a.id = ?";

		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	public function get_bank($id_vendor)
	{
		$query = "SELECT 
					no, 
					name,
					bank_name,
					bank_file
				  FROM 
				  	ms_rekening
				  WHERE 
				  	del = 0 AND id_vendor =".$id_vendor;
		return $query;
	}
	function get_data_situ($id)
	{
		$query = "SELECT 
						a.type,
						a.no,
						a.issue_date,
						a.address,
						a.file_photo,
						a.expire_date,
						a.situ_file,
						a.id,
						a.data_status,
						 ( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_situ' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   FROM 
				   		ms_situ a 

				   WHERE 
				   		a.del = 0 AND a.id_vendor = ".$id;
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}
	function verifikasi_situ($id)
	{
		$query = "SELECT 
						a.type,
						a.no,
						a.issue_date,
						a.issued_by,
						a.address,
						a.file_photo,
						a.expire_date,
						a.situ_file,
						a.id,
						a.data_status,
						a.id_vendor
				   FROM 
				   		ms_situ a 

				   WHERE 
				   		a.del = 0 AND a.id = ".$id;
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function verifikasi_akta($id){

		$query = "SELECT 
						a.type,
						a.notaris,
						a.no,
						a.issue_date,
						a.akta_file,
						a.authorize_by,
						a.authorize_no,
						a.authorize_date,
						a.authorize_file,
						a.id,
						a.expire_date,
						a.id_vendor,
						a.data_status,
						a.id_vendor
				   FROM 
				   		ms_akta a 
				   WHERE 
				   		a.del = 0 AND a.id = ".$id;
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function get_data_tdp($id)
	{
		$query = "SELECT 
						a.no,
						a.issue_date,
						a.authorize_by,
						a.expire_date,
						a.tdp_file,
						a.id,
						a.data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_tdp' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   FROM 
				   		ms_tdp a
				   WHERE 
				   		a.del = 0 AND id_vendor = ".$id;
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}

	function verifikasi_tdp($id)
	{
		$query = "SELECT 
						a.*
				   FROM 
				   		ms_tdp a
				   WHERE 
				   		a.del = 0 AND a.id = ".$id;
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	function get_data_pengalaman($id)
	{
		$query = "SELECT 
						a.job_name,
						b.name bidang,
						c.name sub_bidang,
						a.job_location,
						a.job_giver,
						a.phone_no,
						a.contract_no,
						a.contract_start,
						a.price_idr,
						a.price_foreign,
						a.contract_end,
						a.contract_file,
						a.bast_date,
						a.bast_file,
						a.id id,
						a.data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_pengalaman' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   FROM 
				   		ms_pengalaman a
				   LEFT JOIN
				   		ms_iu_bsb d ON d.id = a.id_iu_bsb
				   LEFT JOIN 
				   		tb_bidang b ON b.id = d.id_bidang
				   LEFT JOIN
				   		tb_sub_bidang c ON c.id = d.id_sub_bidang
				   WHERE 
				   		a.del = 0 AND a.id_vendor = ".$id;
		return $query;
	}

	function verifikasi_pengalaman($id)
	{

		$query = "SELECT 
						a.*,
						a.job_name,
						b.name bidang,
						c.name sub_bidang,
						a.job_location,
						a.job_giver,
						a.phone_no,
						a.contract_no,
						a.contract_start,
						a.currency,
						a.price_idr,
						a.price_foreign,
						a.contract_end,
						a.contract_file,
						a.bast_date,
						a.bast_file,
						a.id id,
						a.data_status
				   FROM 
				   		ms_pengalaman a
				   LEFT JOIN
				   		ms_iu_bsb d ON d.id = a.id_iu_bsb
				   LEFT JOIN 
				   		tb_bidang b ON b.id = d.id_bidang
				   LEFT JOIN
				   		tb_sub_bidang c ON c.id = d.id_sub_bidang
				   WHERE 
				   		a.del = 0 AND a.id = ?";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function get_data_pemilik($id){
		$query = "SELECT 
						a.name,
						a.position,
						a.percentage,
						a.id,
						a.data_status
						-- ( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_pemilik' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   FROM 
				   		ms_pemilik a
				   
				   WHERE 
				   		a.del = 0 AND a.id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}
	function verifikasi_pemilik($id){
		$query = "SELECT 
						a.name,
						a.position,
						a.percentage,
						a.data_status,
						a.id,
						a.id_akta,
						a.id_vendor,
						a.data_status
				   FROM 
				   		ms_pemilik a
				   
				   WHERE 
				   		a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function get_data_pengurus($id)
	{
		$query = "	SELECT 
                        a.position,
                        a.position_expire,
                        a.no,
                        a.name,
                        a.expire_date,
                        a.pengurus_file,
                        a.id,
                        a.data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_pengurus' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
                   FROM 
                        ms_pengurus a
                   WHERE 
                        a.del = 0 AND a.id_vendor = ".$id;
        return $query;
	}
	function verifikasi_pengurus($id)
	{
		$query = "	SELECT 
						a.*,
                        a.position,
                        a.position_expire,
                        a.no,
                        a.name,
                        a.expire_date,
                        a.pengurus_file,
                        a.id,
                        a.data_status,
                        a.id_vendor
                   FROM 
                        ms_pengurus a
                   WHERE 
                        a.del = 0 AND a.id = ".$id;
        $query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function get_data_agen($id)
	{
		$query = "SELECT
						a.no,
						a.issue_date,
						a.type,
						a.expire_date,
						a.agen_file,
						a.id,
						a.data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_agen' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				  FROM  
				  		ms_agen a
				  WHERE 
				  		a.del = 0 AND a.id_vendor = ".$id;
		return $query;
	}
	function get_data_product($id_agen)
	{
		$query = "SELECT
						a.produk,
						a.merk,
						a.data_status,
						a.id
				  FROM  
				  		ms_agen_produk a
				  WHERE 
				  		a.del = 0 AND a.id_agen = ".$id_agen;
		return $query;
	}

	function verifikasi_agen($id)
	{
		$query = "SELECT
						a.no,
						a.issue_date,
						a.type,
						a.expire_date,
						a.agen_file,
						a.id,
						a.data_status,
						a.id,
						a.id_vendor
				  FROM  
				  		ms_agen a
				  WHERE 
				  		a.del = 0 AND a.id = ".$id;
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	function verifikasi_agen_produk($id)
	{
		$query = "SELECT
						 produk,
						 merk,
						 id,
						 data_status
				  FROM  
				  		 ms_agen_produk
				  WHERE 
				  		del = 0 AND id_agen = ".$id;
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	function get_data_izin($id,$type)
	{
		$query = "SELECT 
						a.issue_date,
						a.no,
						a.qualification,
						a.authorize_by,
						a.izin_file,
						a.expire_date,
						a.id as id,
						a.data_status,
						a.type,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_ijin_usaha' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   FROM 
				   		ms_ijin_usaha a
				   -- INNER JOIN
				   -- 		tb_dpt_type b ON b.id = a.id_dpt_type
				   WHERE 
				   		a.del = 0 AND a.id_vendor = $id AND a.type ='".$type."'";

		return $query;
	}

	function get_bsb_dropdown($id)
	{
		$query = "	SELECT
						a.id id,
						CONCAT(b.name, '-'  ,c.name) as name

					FROM 
						ms_iu_bsb a
					LEFT JOIN
						tb_bidang b ON b.id = a.id_bidang
					LEFT JOIN
						tb_sub_bidang c ON c.id = a.id_sub_bidang
					WHERE 
						 a.del = 0 AND a.id_vendor = ".$id;

		$query = $this->db->query($query)->result_array();
		$data = array();
		foreach ($query as $key => $value) {
			$data[$value['id']] = $value['name'];
		}
		return $data;
	}

	function verifikasi_izin($id){
		$query = "SELECT 
						a.*,
						a.issue_date,
						a.qualification,
						a.authorize_by,
						a.izin_file,
						a.expire_date,
						a.id as id,
						-- b.name dpt_name,
						a.id_vendor
				   FROM 
				   		ms_ijin_usaha a
				   -- INNER JOIN
				   -- 		tb_dpt_type b ON b.id = a.id_dpt_type
				   WHERE 
				   		a.del = 0 AND a.id = ".$id;

		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function approve($id){

		$this->db->where('id',$id);
		$update_status = $this->db->update('ms_vendor',array('vendor_status'=>2,'need_approve'=>0,'is_new'=>0));

		$data = $this->db->where('id',$id)->get('ms_vendor')->row_array();
		if($data['dpt_first_date']==''){
			$this->db->where('id',$id)->update('ms_vendor',array('dpt_first_date'=>date('Y-m-d H:i:s')));
		}
		$query = "	UPDATE tr_dpt a 
					RIGHT JOIN ms_ijin_usaha b 
						ON ( 		b.id_dpt_type = a.id_dpt_type 
								AND b.id_vendor = a.id_vendor 
								AND (b.del IS NULL OR b.del = 0)
							)
	   				RIGHT JOIN ms_pengalaman c 
	   					ON ( 		b.id = c.id_ijin_usaha 
	   							AND (c.del = NULL OR c.del = 0)
	   						) 

	   				SET a.status = 1,
	   					a.start_date = ?,
	   					a.edit_stamp = ?

    				WHERE a.id_vendor = ? 
    				AND ( a.status = 0 OR a.status IS NULL)
    				AND c.id IS NOT NULL";

    	return $this->db->query($query, array( date('Y-m-d') , date('Y-m-d H:i:s'), $id));	
	}
	function checkPengalamanBSB($id_vendor){
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
		$query = "	SELECT a.id, e.name bidang_name, f.name sub_bidang_name -- ,b.name bidang_name, c.name sub_bidang_name
					FROM ms_iu_bsb a 
                    LEFT JOIN (
						SELECT b.id, c.id_sub_bidang FROM ms_pengalaman b JOIN ms_iu_bsb c ON b.id_iu_bsb = c.id WHERE b.id_vendor = ?
					) d ON a.id_sub_bidang = d.id_sub_bidang
                    
                    JOIN tb_bidang e ON a.id_bidang=e.id
					JOIN tb_sub_bidang f ON a.id_sub_bidang=f.id
                    WHERE d.id IS NULL AND a.id_vendor = ? GROUP BY a.id_sub_bidang";
		$query = $this->db->query($query,array($id_vendor, $id_vendor));
		
		return $query;
	}

	function get_total_data($id){
		$this->load->library('DPT');
		$table = array(
			'ms_identitas_pemohon'=>'Identitas Pemohon',
			'ms_identitas_pabrik'=>'Identitas Pabrik',
			'ms_permohonan_sertifikasi'=>'Permohonan Sertifikasi',
			'ms_formulir_permohonan'=>array(
				'sps_file' 	 => 'Formulir Permohonan / Surat Permohonan Sertifikasi',
				'spm_file' 	 => 'Formulir Permohonan / Surat Pelimpahan Merk',
				'spjps_file' => 'Formulir Permohonan / Surat Pernyataan Jaminan Proses Sertifikat',
				'spl_file'	 => 'Formulir Permohonan / Surat Perjanjian Lisensi',
				'spks_file'	 => 'Formulir Permohonan / Surat perjanjian Kerjasama Sertifkasi',
				'fa_file'	 => 'Formulir Permohonan / Formulir Aplikasi',
				'dp_file'	 => 'Formulir Permohonan / Data Pemohon',
				'tps_file'	 => 'Formulir Permohonan / Tinjauan Permohonan Sertifikasi',
				'ip_file' 	 => 'Formulir Permohonan / Informasi Produsen'
			),
			'ms_legalitas' => array(
				'ap_file' 	=> 'Legalitas Perusahaan / Akte Perusahaan',
				'skdu_file'	=> 'Legalitas Perusahaan / SKDU',
				'nib_file'	=> 'Legalitas Perusahaan / NIB',
				'iui_file'	=> 'Legalitas Perusahaan / IUI',
				'siup_file'	=> 'Legalitas Perusahaan / SIUP',
				'tdp_file'	=> 'Legalitas Perusahaan / TDP',
				'ktp_file'	=> 'Legalitas Perusahaan / KTP',
				'api_file'	=> 'Legalitas Perusahaan / API',
				'nik_file'	=> 'Legalitas Perusahaan / NIK',
				'haki_file'	=> 'Legalitas Perusahaan / HAKI',
				'lp_file'	=> 'Legalitas Perusahaan / List Produk',
				'so_file'	=> 'Legalitas Perusahaan / Struktur Organisasi'
			),
			'ms_dokumen_mutu'=> array(
				'pm_file'	=> 'Dokumen Mutu / Panduan Mutu',
				'rm_file'	=> 'Dokumen Mutu / Risk Manajemen',
				'so_file'	=> 'Dokumen Mutu / Struktur Organisasi',
				'app_file'	=> 'Dokumen Mutu / Alur Proses Produksi',
				'pp_file'	=> 'Dokumen Mutu / Peralatan Produksi',
				'pmp_file'	=> 'Dokumen Mutu / Pengendalian Mutu Produk',
				'pbb_file'	=> 'Dokumen Mutu / Pengendalian Bahan Baku',
				'pl_file'	=> 'Dokumen Mutu / Packing List (Invoice List)',
				'bl_file'	=> 'Dokumen Mutu / Bill of Landing (BL)',
				'ssm_file'	=> 'Dokumen Mutu / Sertifikasi Sistem Manajemen',
				'did_file'	=> 'Dokumen Mutu / Daftar Induk Dokumen'
			),
			'ms_pembayaran'=>'Bukti Pembayaran'
		);
		$table_surat = array('ms_identitas_pemohon','ms_identitas_pabrik','ms_permohonan_sertifikasi','ms_formulir_permohonan','ms_legalitas','ms_dokumen_mutu','ms_pembayaran');
		$result = array(0=>array(),1=>array(),2=>array(),3=>array(),4=>array(),5=>array(),6=>array());
		$total=0;

		// $adm = $this->db->select('data_status')->where('id_vendor',$id)->get('ms_identitas_pemohon')->row_array();
		// $result[($adm['data_status']==NULL)?0:$adm['data_status']][]['title'] = 'Data Identitas Pemohon';
		// $total+=1;
		// print_r($table);die;
		
		foreach($table as $field=>$label){
			$no = '';
			// print_r($label);die;

			if (is_array($label)) {
				// $this->db->where('id_vendor',$id);
				$this->db->where('('.$field.'.del IS NULL OR '.$field.'.del = 0)');

				foreach ($label as $key_field => $title_field) {
					$this->db->where('('.$key_field.' IS NOT NULL AND id_vendor ='.$id.')');
					
					$res = $this->db->get($field)->result_array();

					foreach($res as $key=>$data){
						if ($data['jenis_sertifikasi'] == 1) {
							$j_s = 'SNI ISO 9001:2015 Sistem Manajemen Mutu';
						} else if ($data['jenis_sertifikasi'] == 2) {
							$j_s = 'SNI ISO 14001:2015 Sistem Manajemen Lingkungan';
						} else if ($data['jenis_sertifikasi'] == 3) {
							$j_s = 'SNI ISO 37001:2016 Sistem Manejemen Anti Penyuapan';
						} else if ($data['jenis_sertifikasi'] == 4) {
							$j_s = 'SPPT-SNI (Sistem 1)';
						} else if ($data['jenis_sertifikasi'] == 5) {
							$j_s = 'SPPT-SNI (Sistem 5)';
						} 
						$arrayResult = array();
						$arrayResult['title'] = $j_s.' / '.$title_field;
						$arrayResult['field'] = $field;
						$arrayResult['data'] = $data;
						$result[(($data['data_status']==NULL)?0:$data['data_status'])][]= $arrayResult;
						
						$total+=1;
					}
				}
				// echo $this->db->last_query();die;

			} else {
				// $this->db->where('id_vendor',$id);
				$this->db->where('('.$field.'.del IS NULL OR '.$field.'.del = 0 AND '.$field.'.id_vendor ='.$id.')');

				$res = $this->db->get($field)->result_array();

				foreach($res as $key=>$data){
					$arrayResult = array();
					$arrayResult['title'] = $table[$field];
					$arrayResult['field'] = $field;
					$arrayResult['data'] = $data;
					$result[(($data['data_status']==NULL)?0:$data['data_status'])][]= $arrayResult;
					
					$total+=1;
				}
			}
		}
		
		$result['total'] = $total;
		// print_r($result);
		return $result;
	}

	public function update_ms_vendor_administrasi($id, $data)
	{
		$this->db->where('id_vendor',$id);
		return $this->db->update('ms_vendor_admistrasi',array('vendor_type'=>$data['vendor_type']));
	}
	function get_recipient_admin($id_role){
		$query = 	"	SELECT 
							email
						FROM 
							ms_admin a
						WHERE 
							a.id_role = ?
					";
		return $this->db->query($query,array($id_role))->result_array();

	}
	function angkat_vendor($id){
		return $this->db->where('id',$id)->update('ms_vendor',array('vendor_status'=>2,'edit_stamp'=>date('Y-m-d H:i:s')));
	}
	function reject($id){
		$this->db->where('id',$id);
		return $this->db->update('ms_vendor',array('need_approve'=>0,'edit_stamp'=>date('Y-m-d H:i:s')));
	}

	function get_data_identitas_pabrik($id = 0){
		if(!$id){
			$user = $this->session->userdata('user');
			$id = $user['id_user'];
		}			
		$query = "SELECT 
						a.id,
						d.nama_pabrik, 
						d.alamat,
						d.kota,
						d.provinsi,
						d.kode_pos,
						d.telepon,
						d.data_status,
						d.id id_doc,
						( SELECT COUNT(*) FROM tr_note e WHERE e.document_type = 'ms_identitas_pabrik' AND e.is_active = 1 AND e.id_vendor = $id AND e.id_document = d.id) total_note
				   FROM 
				   		ms_vendor a
				   	JOIN
				   		ms_identitas_pabrik d on d.id_vendor = a.id
				   WHERE 
				   		a.id = ?";

		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	function get_data_Permohonan_sertifikasi($id = 0){
		if(!$id){
			$user = $this->session->userdata('user');
			$id = $user['id_user'];
		}
		$query = "SELECT
						b.jenis,
						c.name as name_sistem_sertifikasi,
						d.name as name_lingkup_pemohon,
						e.name as name_jenis_sertifikasi,
						a.id id_doc,
						a.data_status,
						a.id_vendor,
						( SELECT COUNT(*) FROM tr_note f WHERE f.document_type = 'ms_permohonan_sertifikasi' AND f.is_active = 1 AND f.id_vendor = $id AND f.id_document = a.id) total_note,
						a.data_status,
						a.name as name__
				   	FROM 
				   		ms_permohonan_sertifikasi a
				   	JOIN 
				   		ms_jenis_permohonan b on b.id = a.jenis_permohonan_id
				   	JOIN
				   		ms_lingkup_pemohon c on c.id = a.sistem_sertifikasi_id
				   	JOIN
				   		ms_lingkup_pemohon d on d.id = a.lingkup_pemohon_id
				   	JOIN 
				   		ms_sistem_sertifikasi e on e.id = a.id_jenis_sertifikasi
				   	WHERE 
				   		a.del = 0 AND a.id_vendor = ?";

		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	function verifikasi_Permohonan_sertifikasi($id = 0){
		if(!$id){
			$user = $this->session->userdata('user');
			$id = $user['id_user'];
		}
		$query = "SELECT
						b.jenis,
						c.name as name_sistem_sertifikasi,
						d.name as name_lingkup_pemohon,
						e.name as name_jenis_sertifikasi,
						a.id id_doc,
						a.data_status,
						a.id_vendor,
						( SELECT COUNT(*) FROM tr_note f WHERE f.document_type = 'ms_permohonan_sertifikasi' AND f.is_active = 1 AND f.id_vendor = $id AND f.id_document = a.id) total_note,
						a.data_last_check,
						a.data_checker_id,
						a.name as name__
				   	FROM 
				   		ms_permohonan_sertifikasi a
				   	JOIN 
				   		ms_jenis_permohonan b on b.id = a.jenis_permohonan_id
				   	JOIN
				   		ms_lingkup_pemohon c on c.id = a.sistem_sertifikasi_id
				   	JOIN
				   		ms_lingkup_pemohon d on d.id = a.lingkup_pemohon_id
				   	JOIN 
				   		ms_sistem_sertifikasi e on e.id = a.id_jenis_sertifikasi
				   	WHERE 
				   		a.del = 0 AND a.id = ?";

		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}

	function get_permohonan_sertifikasi($id = 0){
		if(!$id){
			$user = $this->session->userdata('user');
			$id = $user['id_user'];
		}
		$query = "SELECT
						b.jenis,
						c.name as name_sistem_sertifikasi,
						d.name as name_lingkup_pemohon,
						e.name as name_jenis_sertifikasi,
						a.id id_doc,
						a.data_status,
						a.id_vendor,
						( SELECT COUNT(*) FROM tr_note f WHERE f.document_type = 'ms_permohonan_sertifikasi' AND f.is_active = 1 AND f.id_vendor = $id AND f.id_document = a.id) total_note,
						a.data_last_check,
						a.data_checker_id
				   	FROM 
				   		ms_permohonan_sertifikasi a
				   	JOIN 
				   		ms_jenis_permohonan b on b.id = a.jenis_permohonan_id
				   	JOIN
				   		ms_lingkup_pemohon c on c.id = a.sistem_sertifikasi_id
				   	JOIN
				   		ms_lingkup_pemohon d on d.id = a.lingkup_pemohon_id
				   	JOIN 
				   		ms_sistem_sertifikasi e on e.id = a.id_jenis_sertifikasi
				   	WHERE 
				   		a.del = 0 AND a.id_vendor = ".$id;
		return $query;
	}

	function get_data_Formulir_permohonan($id, $type){
		// echo "string".$id;
		// echo $type;die;
		// if(!$id){
		// 	$user = $this->session->userdata('user');
		// 	$id = $user['id_user'];
		// }
		$query = "SELECT
						$type,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_formulir_permohonan' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_formulir_permohonan a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Formulir_permohonan9001($id, $type){
		// echo "string".$id;
		// echo $type;die;
		// if(!$id){
		// 	$user = $this->session->userdata('user');
		// 	$id = $user['id_user'];
		// }
		$query = "SELECT
						$type,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_formulir_permohonan' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_formulir_permohonan a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 1 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Formulir_permohonan14001($id, $type){
		// echo "string".$id;
		// echo $type;die;
		// if(!$id){
		// 	$user = $this->session->userdata('user');
		// 	$id = $user['id_user'];
		// }
		$query = "SELECT
						$type,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_formulir_permohonan' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_formulir_permohonan a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 2 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Formulir_permohonan37001($id, $type){
		// echo "string".$id;
		// echo $type;die;
		// if(!$id){
		// 	$user = $this->session->userdata('user');
		// 	$id = $user['id_user'];
		// }
		$query = "SELECT
						$type,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_formulir_permohonan' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_formulir_permohonan a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 3 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Formulir_permohonanSPPTS1($id, $type){
		// echo "string".$id;
		// echo $type;die;
		// if(!$id){
		// 	$user = $this->session->userdata('user');
		// 	$id = $user['id_user'];
		// }
		$query = "SELECT
						$type,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_formulir_permohonan' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_formulir_permohonan a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 4 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Formulir_permohonanSPPTS5($id, $type){
		// echo "string".$id;
		// echo $type;die;
		// if(!$id){
		// 	$user = $this->session->userdata('user');
		// 	$id = $user['id_user'];
		// }
		$query = "SELECT
						$type,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_formulir_permohonan' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_formulir_permohonan a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 5 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	public function verifikasi_formulir_permohonan($id,$type)
	{
		$query = " 	SELECT
						$type,
						id_vendor,
						id,
						data_status
					FROM
						ms_formulir_permohonan
					WHERE
						del = 0 AND $type IS NOT NULL AND id = ?
		";
		return $this->db->query($query,array($id))->row_array();
	}

	function get_data_Legalitas_perusahaan($id, $type){
		$query = "SELECT
						$type,
						id_vendor,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_legalitas' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_legalitas a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Legalitas_perusahaan9001($id, $type){
		$query = "SELECT
						$type,
						id_vendor,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_legalitas' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_legalitas a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 1 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Legalitas_perusahaan14001($id, $type){
		$query = "SELECT
						$type,
						id_vendor,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_legalitas' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_legalitas a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 2 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Legalitas_perusahaan37001($id, $type){
		$query = "SELECT
						$type,
						id_vendor,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_legalitas' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_legalitas a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 3 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Legalitas_perusahaanSPPTS1($id, $type){
		$query = "SELECT
						$type,
						id_vendor,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_legalitas' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_legalitas a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 4 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Legalitas_perusahaanSPPTS5($id, $type){
		$query = "SELECT
						$type,
						id_vendor,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_legalitas' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_legalitas a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 5 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	public function verifikasi_Legalitas_perusahaan($id,$type)
	{
		$query = " 	SELECT
						$type,
						id,
						data_status
					FROM
						ms_legalitas
					WHERE
						del = 0 AND $type IS NOT NULL AND id = ?
		";
		return $this->db->query($query,array($id))->row_array();
	}


	function get_data_Dokumen_mutu($id, $type){
		$query = "SELECT
						$type,
						id_vendor,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_dokumen_mutu' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_dokumen_mutu a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Dokumen_mutu9001($id, $type){
		$query = "SELECT
						$type,
						id_vendor,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_dokumen_mutu' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_dokumen_mutu a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 1 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Dokumen_mutu14001($id, $type){
		$query = "SELECT
						$type,
						id_vendor,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_dokumen_mutu' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_dokumen_mutu a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 2 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Dokumen_mutu37001($id, $type){
		$query = "SELECT
						$type,
						id_vendor,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_dokumen_mutu' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_dokumen_mutu a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 3 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Dokumen_mutuSPPTS1($id, $type){
		$query = "SELECT
						$type,
						id_vendor,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_dokumen_mutu' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_dokumen_mutu a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 4 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	function get_data_Dokumen_mutuSPPTS5($id, $type){
		$query = "SELECT
						$type,
						id_vendor,
						id,
						data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_dokumen_mutu' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   	FROM 
				   		ms_dokumen_mutu a
				   	WHERE 
				   		del = 0 AND $type IS NOT NULL AND jenis_sertifikasi = 5 AND id_vendor = ".$id;

		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		// echo $this->db->last_query();die;
		return $query;
	}

	public function verifikasi_Dokumen_mutu($id,$type)
	{
		$query = " 	SELECT
						$type,
						id,
						data_status
					FROM
						ms_dokumen_mutu
					WHERE
						del = 0 AND $type IS NOT NULL AND id = ?
		";
		return $this->db->query($query,array($id))->row_array();
	}

	function get_data_akta($id, $type){
		$query = "SELECT 
						a.type,
						a.notaris,
						a.no,
						a.issue_date,
						a.akta_file,
						a.authorize_by,
						a.authorize_no,
						a.authorize_date,
						a.authorize_file,
						a.id,
						a.expire_date,
						a.data_status,
						( SELECT COUNT(*) FROM tr_note b WHERE b.document_type = 'ms_akta' AND b.is_active = 1 AND b.id_vendor = $id AND b.id_document = a.id) total_note
				   FROM 
				   		ms_akta a
				   WHERE 
				   		a.del = 0 AND id_vendor = $id AND type = '".$type."'";
		if($this->input->post('filter')){

			$query .= $this->filter($form, $this->input->post('filter'), false);

		}
		return $query;
	}
	
}
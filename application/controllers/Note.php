<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Note extends MY_Controller {

	public function __construct(){
		parent::__construct();
		
		$this->load->model('Note_model','nm');
		$this->load->model('vendor/Vendor_model','vm');
		$this->load->library('email');
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama User',
		            'readonly'=>true
		        ),
		        array(
		            'field'	=> 	'document_type',
		            'type'	=>	'text',
		            'label'	=>	'Dokumen',
		            'readonly'=>true
		        ),
		        array(
		            'field'	=> 	'value',
		            'type'	=>	'textarea',
		            'label'	=>	'Note',
		            'rules' => 	'required',
	         	)
	         )
		);
	}
	public function insert($table, $id,$sub_type){
		$data = $this->db->where('id',$id)->get($table)->row_array();

		if ($table == 'ms_tahap_audit' || $table == 'ms_verifikasi') {
			$vendor_data = $this->vm->get_vendor_name($data['id_client']);
		} else {
			$vendor_data = $this->vm->get_vendor_name($data['id_vendor']);
		}
		
		$note =$this->db->where('id_document',$id)->where('document_type',$table)->get('tr_note')->result_array();
		$list_dokumen = array(
			'ms_identitas_pemohon'		=> 'Identitas Pemohon',
			'ms_identitas_pabrik'		=> 'Identitas Pabrik',
			'ms_permohonan_sertifikasi'	=> 'Permohonan Sertifikasi',
			'ms_formulir_permohonan'	=> 'Formulir Permohonan',
			'ms_legalitas'				=> 'Legalitas Perusahaan',
			'ms_dokumen_mutu'			=> 'Pabrikan/Keagenan/Distributor',
			'ms_pembayaran'				=> 'Bukti Pembayaran',
			'ms_tahap_audit'			=> 'Tahap audit 2',
			'ms_verifikasi'				=> 'Verifikasi Audit'
		);
		$this->form['form'][0]['value'] = $vendor_data['name'];
		$this->form['form'][1]['value'] = $list_dokumen[$table];
		if($table=='ms_akta'){
			if($data['type']=='pendirian'){
				$this->form['form'][1]['value'] = 'Akta Pendirian Perusahaan';
			}else{
				$this->form['form'][1]['value'] = 'Akta Perubahan Terakhir';
			}
			
		}
		if($table=='ms_ijin_usaha'){
			$list_ijin_usaha =	array(
								'siujk'=>'SIUJK',
								'sbu'=>'SBU',
								'siup'=>'SIUP',
								'ijin_lain'=>'Surat Izin Usaha Lainnya',
								'asosiasi'=>'Sertifikat Asosiasi/Lainnya',
							);
			$this->form['form'][1]['value'] = $list_ijin_usaha[$data['type']];
		}
		
		$dokumen = array('ms_akta','ms_situ','ms_tdp','ms_ijin_usaha','ms_agen');
		$nama_orang = array('ms_pengurus','ms_pemilik');
		
		$this->form['note'] = $note;
		$this->form['url'] = site_url('note/save/'.$table.'/'. $id . '/' . $sub_type);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Kirim',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
		
	}
	public function save($table, $id, $sub_type){
		$data = $this->db->where('id',$id)->get($table)->row_array();
		if ($table == 'ms_tahap_audit') {
			$query_vendor = $this->vm->get_data($data['id_client']);
		} else {
			$query_vendor = $this->vm->get_data($data['id_vendor']);
		}
		
		// print_r($query_vendor);die;
		$data_note = $this->nm->save($table, $id, $data,$sub_type);
		if($data_note){
			$note = $this->db->where('id', $this->db->insert_id())->get('tr_note')->row_array();
			
			if($note['sub_type'] != '' || $note['sub_type'] != null){
				$document = 'Dokumen 	: '.$note['document'].' / '.str_replace('%20',' ',$note['sub_type']);
			} else {
				$document = 'Dokumen 	: '.$note['document'].'';
			}
			$value = 'Catatan : '.$note['value'];
			
			$message = "Anda mendapatkan catatan pada tahap verifikasi Aplikasi Certificate Management System PT. Global Inspeksi Sertifikasi atas : <br/><br/>
				".$document."<br/>
				".$value."<br/>
				
				Untuk selanjutnya, silahkan memperbaiki data - data dan dokumen saudara di aplikasi CMS.
				<br/><br/>
				Terima kasih.<br/>
				PT Global Inspeksi Sertifikasi";
			// echo $query_vendor['vendor_email'];
				 // dengan mengakses link berikut <a href='vms.pgn.co.id'>vms.pgn.co.id</a>.
			// echo $query_vendor['pic_email'];
			$this->send_mail($query_vendor['vendor_email'], 'Catatan verifikasi Dokumen CMS PT Global Inspeksi Sertifikasi', $message);
			// email($save['pic_email'], $message, 'Catatan verifikasi Dokumen CMS PT Global Inspeksi Sertifikasi');
			
			echo json_encode(array('status'=>'success'));
			// return true;
		}
	}
	public function close($id){
		$this->form_close['url'] = site_url('note/process_close/'. $id);
		$this->form_close['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Ya',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form_close);
	}
	public function process_close($id){
		if($this->nm->process_close($id)){
			// redirect('dashboard');
			echo json_encode(array('status'=>'success'));
		}
	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Nrp extends MY_Controller {

	public $form;

	public $modelAlias = 'nrpm';

	// public $alias = 'ms_Nrp';

	public $module = 'Nrp';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/Nrp_model','nrpm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'nrp_file',
		            'type'	=>	'file',
		            'label'	=>	'Upload File',
		            'upload_path'=>base_url('assets/lampiran/nrp_file/'),
					'upload_url'=>site_url('vendor/Nrp/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
					'rules' => 'required'
	         	)
	         )
		);		
		$this->getData = $this->nrpm->getData($this->form);
		$this->insertUrl = site_url('vendor/Nrp/save/');
		$this->updateUrl = 'vendor/Nrp/update';
		$this->deleteUrl = 'vendor/Nrp/delete/';
		$this->prosesUrl = 'vendor/Nrp/proses/';
	}
	
	public function index($id=''){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/Nrp'),
			'title' => 'Upload Nrp File'
		));
		$this->header = 'Upload Nrp File';
		$this->content = $this->load->view('vendor/Nrp/list',$data, TRUE);
		$this->script = $this->load->view('vendor/Nrp/list_js', $data, TRUE);
		parent::index();
	}
	public function edit($id = null)
	{
		// $this->form['form'][1]['caption'] = "*Biarkan kosong jika tidak ingin diganti";
		parent::edit($id); 
	}
	public function insert()
	{
		$this->form['url'] = site_url('vendor/Nrp/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function save($data = null)
	{
		$modelAlias = $this->nrpm;
		$user = $this->session->userdata('user');
		if ($this->validation()) {
			$save = $this->input->post();
			// print_r($save);die;		
			$save['id_client'] = $user['id_user'];
			$save['entry_stamp'] = timestamp();
			if ($this->nrpm->insert($save)) {
				$this->deleteTemp($save);
				return true;
				// echo json_encode(array('status' => 'success'));
			}	
		}
	}

	public function update($id)
	{
		$modelAlias = $this->nrpm;
		if ($this->validation()) {
			$save = $this->input->post();
			$lastData = $this->nrpm->selectData($id);
			if ($this->nrpm->update($id, $save)) {
				$this->deleteTemp($save, $lastData);
				return true;
				// echo json_encode(array('status' => 'success'));
			}
		}
	}

	public function delete($id) {
		
		if ($this->nrpm->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('vendor/Nrp/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function proses($id){

	}
	function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form_filter['filter'];
		echo json_encode($return);
	}
}

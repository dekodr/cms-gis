<?php
/**
 * 
 */
class Auditor extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('vendor/certificate_model','cm');
	}

	public function index($id='')
	{
		$data = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/admin/auditor'),
			'title' => 'Auditor Task'
		));
		$this->header = 'Auditor Task';
		$this->content = $this->load->view('vendor/admin/auditor/list',$data, TRUE);
		$this->script = $this->load->view('vendor/admin/auditor/list_js', $data, TRUE);
		parent::index();
	}

	public function getTaskAuditor()
	{
		$config['query'] = $this->cm->getTaskAuditor();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}
}
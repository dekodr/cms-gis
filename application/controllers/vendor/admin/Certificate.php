<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Certificate extends MY_Controller {

	public $form;

	public $modelAlias = 'cm';

	public $alias = 'ms_certificate';

	public $module = 'Certificate';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();
		require_once(BASEPATH."plugins/dompdf2/dompdf_config.inc.php");
		$this->load->model('vendor/certificate_model','cm');
		$this->load->model('get_model','gm');
		$this->load->model('approval_model','am');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
				array(
		            'field'	=> 'id_form',
		            'type'	=> 'radioList',
		            'label'	=> 'Form',
		            'source'=> $this->gm->getForm(),
		            'rules' => 'required'
		        ),
		        array(
		            'field'	=> 'nama',
		            'type'	=> 'text',
		            'label'	=> 'Nama Form'
		        ),
	         	array(
		            'field'	=> 	'desc',
		            'type'	=>	'textarea',
		            'label'	=>	'Deskripsi'
	         	),
	         	array(
		            'field'	=> 	'template_form',
		            'type'	=>	'file',
		            'label'	=>	'Template Form',
		            'upload_path'=>base_url('assets/lampiran/template_form/'),
					'upload_url'=>site_url('vendor/admin/certificate/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	)
	         ),
			'filter' => array(
				array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		        ),
	         	array(
		            'field'	=> 	'position',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
	         	)
			)
		);

		$this->form_upload = array(
			'form'=>array(
		        array(
		            'field'	=> 'key',
		            'type'	=> 'text',
		            'label'	=> 'Temuan Ke-',

		            'rules' => 'required'
		        ),
	         	array(
		            'field'	=> 	'desc',
		            'type'	=>	'textarea',
		            'label'	=>	'Deskripsi',
		            'rules' => 'required'
	         	),
	         	array(
		            'field'	=> 	'temuan_file',
		            'type'	=>	'file',
		            'label'	=>	'File Temuan',
		            'upload_path'=>base_url('assets/lampiran/temuan_file/'),
					'upload_url'=>site_url('vendor/admin/certificate/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 'required'
	         	)
	         )
		);
		$this->insertUrl = site_url('vendor/certificate/save/');
		$this->updateUrl = 'vendor/certificate/update/';
		$this->deleteUrl = 'vendor/certificate/delete/';
	}
	
	public function index($id=''){
		$data = $this->session->userdata('user');
		$data['approval_data'] 	= $this->am->get_total_data($user['id_user']);
		$data['bar']			= array(
									'pending'=>array(
										'percentage'=>(count($data['approval_data'][0]))/$data['approval_data']['total']*100,
										'value'=>count($data['approval_data'][0])
									),
									'approved'=>array(
										'percentage'=>(count($data['approval_data'][1])+count($data['approval_data'][2]))/$data['approval_data']['total']*100,
										'value'=>(count($data['approval_data'][1])+count($data['approval_data'][2]))
									),
									'rejected'=>array(
										'percentage'=>(count($data['approval_data'][3])+count($data['approval_data'][4]))/$data['approval_data']['total']*100,
										'value'=>(count($data['approval_data'][3])+count($data['approval_data'][4]))
									)
								);
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/admin/certificate'),
			'title' => 'Usulan Sertifikat'
		));
		$this->header = 'Usulan Sertifikat';
		$this->content = $this->load->view('vendor/admin/certificate/list',$data, TRUE);
		$this->script = $this->load->view('vendor/admin/certificate/list_js', $data, TRUE);
		parent::index();
	}

	public function getDataAdmin($id = null)
	{
		$config['query'] = $this->cm->getDataAdmin();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function laporan_final($id)
	{
		$config['query'] = $this->cm->getLaporanFinal($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	 public function set_form($id)
	 {
	 	$this->form['url'] = site_url('vendor/admin/certificate/save_set_form/'.$id);
	 	$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Simpan'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
	 	echo json_encode($this->form);
	 }

	 public function save_set_form($id)
	 {
	 	$user = $this->session->userdata('user');
	 	$modelAlias = $this->modelAlias;
	 	if ($this->validation()) {
	 		$save = $this->input->post();
			$save['id_user'] = $user['id_user'];
			$save['id_certificate'] = $save['id_certificate'];
			$save['name'] = $save['name'];
			$save['is_status'] = 0;
			$save['entry_stamp'] = date("Y-m-d H:i:s");
			$save['del'] = 0;
	 		if ($this->$modelAlias->insert($save)) {
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 			return true;
	 		}
	 	}
	 }

	public function view_upload_temuan($id,$id_certificate)
	{
	 	$data = $this->session->userdata('user');
	 	$data['id_certificate'] = $id_certificate;
	 	$data['id'] = $id;
	 	$data['cek_leader'] = $this->cm->cek_leader($id);
	 	$data['detail_certificate'] = $this->cm->selectDetailCertificate($id)->row_array();
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/certificate/get_detail_certificate/'.$id),
			'title' => 'Detail Sertifikat'
		));
		$data['indicator'] = $this->indicator($data['detail_certificate']['status']);
		$this->header = 'Detail Sertifikat';

		$this->content = $this->load->view('vendor/admin/certificate/view',$data, TRUE);
		$this->script = $this->load->view('vendor/admin/certificate/view_js', $data, TRUE);
		parent::index();
	}

	public function getDataForm($id, $id_certificate){
		$config['query'] = $this->cm->getDataForm($id, $id_certificate,$this->form);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function approve_temuan($id_temuan,$id_certificate)
	{	
		$auditor = $this->session->userdata('admin')['name'];
		$save_history = array(
			'id_certificate' => $id_certificate,
			'value'			 => date('Y-m-d H:i:s').' '.$auditor.' Approve Temuan.',
			'entry_stamp'	 => date('Y-m-d H:i:s'),
			'del'			 => 0
		);
		$this->db->insert('tr_history',$save_history);
		
		$return = $this->cm->approve_temuan($id_temuan);
		if ($return) {
			redirect('vendor/admin/certificate/view_upload_temuan/'.$id_certificate);
		}
	}

	public function get_temuan($id)
	{
		$config['query'] = $this->cm->get_temuan($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function upload_temuan($id)
	{
		$this->form = $this->form_upload;
		$this->form['url'] = site_url('vendor/admin/certificate/save_temuan/'.$id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	public function detail_temuan($id)
	{
		$this->form = $this->form_upload;
		$modelAlias = $this->modelAlias;
		 $getData   = $this->$modelAlias->selectDataTemuan($id);

        foreach($this->form['form'] as $key => $value){
            $this->form['form'][$key]['readonly'] = TRUE;
            $this->form['form'][$key]['value'] = $getData[$value['field']];
        }
        $addition = array(
		            'field'	=> 	'syarat_file',
		            'type'	=>	'file',
		            'label'	=>	'File Lampiran',
		            'upload_path'=>base_url('assets/lampiran/temuan_file/'),
					'upload_url'=>site_url('vendor/admin/certificate/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 'required',
		            'value'=>$getData['syarat_file']
	         	);
	    $remark = array(
		            'field'	=> 	'remark',
		            'type'	=>	'textarea',
		            'label'	=>	'Catatan',
		            'value'=>$getData['remark']
	         	);
       	$this->form['form'][] = $addition;
       	$this->form['form'][] = $remark;
		$this->form['url'] = site_url('vendor/admin/certificate/process_addition/'.$id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function process_addition($id){
		$modelAlias = $this->modelAlias;
		$getData   = $this->$modelAlias->selectDataTemuan($id);
		 $addition = array('form'=>array(array(
		            'field'	=> 	'remark',
		            'type'	=>	'textarea',
		            'label'	=>	'Catatan'
	         	),array(
		            'field'	=> 	'syarat_file',
		            'type'	=>	'file',
		            'label'	=>	'File Lampiran',
		            'upload_path'=>base_url('assets/lampiran/syarat_file/'),
					'upload_url'=>site_url('vendor/admin/certificate/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 'required',
		            'value'=>$getData['syarat_file']
	         	)));
       	// $this->form['form'][] = $addition;
		$user = $this->session->userdata('user');
	 	
	 	if ($this->validation($addition)) {
	 		$save = $this->input->post();
			$save['edit_stamp'] = date("Y-m-d H:i:s");
			$save['del'] = 0;
	 		if ($this->$modelAlias->updateAddition($id,$save)) {
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 			return true;
	 		}
	 	}
	}
	
	public function save_temuan($id)
	{
		$auditor = $this->session->userdata('admin')['name'];
		$this->form_validation->set_rules($this->form_upload);
		if ($this->validation($this->form_upload)) {
			$save = $this->input->post();
			$save['id_certificate'] = $id;
			$save['entry_stamp'] = date('Y-m-d H:i:s');
			$save['entry_by'] = $this->session->userdata('admin')['name'];
			$save['del'] = 0;

			
		$save_history = array(
			'id_certificate' => $id,
			'value'			 => date('Y-m-d H:i:s').' '.$auditor.' Upload Temuan.',
			'entry_stamp'	 => date('Y-m-d H:i:s'),
			'del'			 => 0
		);
		$this->db->insert('tr_history',$save_history);

			if ($this->cm->save_temuan($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 			return true;
			}
		}
	}

	public function submit_laporan($id)
	{
		$return = $this->cm->submit_laporan($id);
		if ($return) {
			echo '<script>alert("Sukses"); window.location.href="'.site_url('vendor/admin/certificate/view_upload_temuan/'.$id).'";</script>';
		}
	}

	public function viewVerifikasi($id){
    	$data['id'] = $id;
    	$this->load->view('vendor/admin/certificate/view_verifikasi', $data, FALSE);
		$this->load->view('vendor/admin/certificate/view_verifikasi_js', $data, FALSE);
    }

    public function verifikasi_sertifikat($id)
    {
    	$data = $this->session->userdata('user');
    	$data['id'] = $id;
		// if($this->vm->check_pic($data['id_user'])==0){
		// 	redirect(site_url('vendor/pernyataan'));
		// }
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/admin/certificate/'.$id),
			'title' => 'Verifikasi Sertifikat'
		));
		$this->header = 'Verifikasi Sertifikat';
		$this->content = $this->load->view('vendor/admin/certificate/verifikasi',$data, TRUE);
		$this->script = $this->load->view('vendor/admin/certificate/verifikasi_js', $data, TRUE);
		parent::index();
    }

    public function get_data_verifikasi($id)
	{
		$config['query'] = $this->cm->get_data_verifikasi($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function get_laporan_temuan($id)
	{
		$config['query'] = $this->cm->get_laporan_temuan($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

    public function upload_lampiran_admin($id,$id_certificate,$table)
    {
    	$config['upload_path'] 	 = './assets/lampiran/temuan_file/';
		$config['allowed_types'] = 'gif|png|jpg|doc|pdf|xls|txt|php|js|rar|zip|mp4|html|php';

		$this->load->library('upload', $config);
		 $this->upload->initialize($config);
		
		if (! $this->upload->do_upload('lampiran_admin')) {
			//Jika upload gagal
       		// echo "<script>alert('Gagal Upload');</script>";
       		echo $this->upload->display_errors();
		}
		else {
				// upload sukses
			// print_r($this->upload->data());
  			$file = $this->upload->data();
  			$data = array (
  			  'lampiran_admin'  => $file['file_name'],
	          'edit_stamp'		=> date('Y-m-d H:i:s'),
  			);

  			$return = $this->cm->upload_lampiran_admin($id,$data,$table);
  			if ($return) {
  				redirect('vendor/admin/certificate/verifikasi_sertifikat/'.$id_certificate);
  			}
		}
    }

    public function approve($id)
    {
    	if (isset($_POST['terima'])) {
    		$data = array(
    			'reason' => $_POST['reason'],
    			'status' => 4,
    			'edit_stamp' => date('Y-m-d H:i:s')
    		);
    		$return = $this->cm->approve($id,$data);
    		if ($return) {
    			$message = "Selamat kepada ".$return['vendor_name']." ajuan sertifikat anda telah di verifikasi oleh admin Aplikasi Certificate Management System PT. Global Inspeksi Sertifikasi dengan alasan : <br/><br/>
				
				".$_POST['reason']."<br/>
				
				Untuk selanjutnya, anda bisa mencetak sertifikatnya di aplikasi CMS.
				<br/><br/>
				Terima kasih.<br/>
				PT Global Inspeksi Sertifikasi";

				email($return['vendor_email'], $message, 'Verifikasi Ajuan Sertifikat');

  				echo '<script>alert("Berhasil Setujui Data!"); window.location.href="'.site_url('vendor/admin/certificate').'"</script>';
    		}
    	} else{
    		$data = array(
    			'reason' => $_POST['reason'],
    			'status' => 5,
    			'edit_stamp' => date('Y-m-d H:i:s')
    		);
    		$return = $this->cm->approve($id,$data);
    		if ($return) {
    			$message = "Mohon maaf kepada ".$return['vendor_name']." ajuan sertifikat anda telah di tolak oleh admin Aplikasi Certificate Management System PT. Global Inspeksi Sertifikasi dengan alasan : <br/><br/>
				
				".$_POST['reason']."<br/>
				
				Untuk selanjutnya, harap memperbaikinya di aplikasi CMS.
				<br/><br/>
				Terima kasih.<br/>
				PT Global Inspeksi Sertifikasi";
				
				email($return['vendor_email'], $message, 'Verifikasi Ajuan Sertifikat');

    			echo '<script>alert("Berhasil Menolak Data!"); window.location.href="'.site_url('vendor/admin/certificate').'"</script>';	
    		}
    	}
    }

    public function export($id)
    {
    	$config['query'] = $this->cm->getDataExport($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
    }

    public function export_temuan($id)
    {
    	$data = $this->cm->exportTemuan($id);
    	$table = '';
    	foreach ($data as $key => $value) {
    		$table .= '<tr>
    					<td>'.$value['desc'].'</td>
    					<td>'.$value['entry_by'].'</td>
    				   </tr>';
    	}
    	$html = ' <!DOCTYPE html>
				    <html>
				    <head>
				    	<title></title>
				    </head>
				    <body>
				    	<table border = 1>
				    	<tr>
				    	<td>Keterangan</td>
				    	<td>Pelapor</td>
				    	</tr>
				    	'.$table.'
				    	</table>
				    </body>
				    </html>';

    	$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper("A4", "potrait");
        // $dompdf->set_option('isHtml5ParserEnabled', TRUE);
		$dompdf->render();
		$dompdf->stream("Laporan Temuan.pdf", array("Attachment" => 1));	
    }
   
}

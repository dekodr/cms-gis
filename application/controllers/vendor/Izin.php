<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Izin extends MY_Controller {

	public $form;

	public $modelAlias = 'im';

	public $alias = 'ms_ijin_usaha';

	public $module = 'Izin';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/izin_model','im');
		$this->load->model('vendor/vendor_model','vm');
		$this->load->model('get_model','gm');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
						array(
							'field'	=> 	'type',
							'type'	=>	'dropdown',
							'label'	=>	'Surat Izin Usaha',
							'rules' => 	'required',
							'source'=>	array(
											'siujk'=>'SIUJK',
											'sbu'=>'Sertifikat Badan Usaha',
											'siup'=>'SIUP',
											'ijin_lain'=>'Surat Izin Usaha Lainnya',
											'asosiasi'=>'Sertifikat Asosiasi/Lainnya',
										)
						),
						array(
							'field'	=> 	'izin_file',
							'type'	=>	'file',
							'label'	=>	'Lampiran',
							'rules' => 	'required',
							'upload_path'=>base_url('assets/lampiran/izin_file/'),
							'upload_url'=>site_url('vendor/izin/upload_lampiran'),
							'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip',
							'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
            				'rules' => 	'required'
						)
					)
		); 

		$this->getData = $this->im->getData($this->form);
		$this->insertUrl = site_url('vendor/izin/save/');
		$this->updateUrl = 'vendor/izin/update';
		$this->deleteUrl = 'vendor/izin/delete/';
	}
	
	public function index($id=''){
		$data = $this->session->userdata('user');
		// if($this->vm->check_pic($data['id_user'])==0){
		// 	redirect(site_url('vendor/pernyataan'));
		// }
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/izin'),
			'title' => 'Izin Usaha'
		));
		$this->header = 'Izin Usaha';

		$this->content = $this->load->view('vendor/izin/list',$data, TRUE);
		$this->script = $this->load->view('vendor/izin/list_js', $data, TRUE);
		parent::index();
	}

	public function save($data = null)
	 {
	 	$this->load->library('VMS');
	 	$user = $this->session->userdata('user');
	 	$modelAlias = $this->modelAlias;
	 	if ($this->validation()) {
	 		$save = $this->input->post();
	 		$save['id_vendor'] = $user['id_user'];
	 		$save['entry_stamp'] = timestamp();
	 		$id = $this->$modelAlias->insert($save);
	 		if ($id) {
	 			$file_agen = $save['type'];
	 			$data = array(
	 				'id'=>$id,
	 				'expire_date'=>$save['expire_date'],
	 				'doc_type'=>'ms_agen',
	 				'no'=>$save['no'],
	 				'doc'=>$file_agen
	 			);

	 			// $this->vms->setEmailBlast($data);
	 			$this->deleteTemp($save);
	 		}
	 	}
	 }

	 public function izinView($id)
	 {
	 	$this->form = array(
			'form'=>array(
						array(
							'field'	=> 	'type',
							'type'	=>	'text',
							'label'	=>	'Surat Izin Usaha'
						),
						array(
							'field'	=> 	'izin_file',
							'type'	=>	'file',
							'label'	=>	'Lampiran',
							'rules' => 	'required',
							'upload_path'=>base_url('assets/lampiran/izin_file/'),
							'upload_url'=>site_url('vendor/izin/upload_lampiran'),
							'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip',
							'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
            				'rules' => 	'required'
						)
					)
		); 
	 	$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
			$this->form['form'][$key]['readonly'] = true;
		}

		echo json_encode($this->form);
	 }
}

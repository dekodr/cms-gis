<?php defined('BASEPATH') OR exit('No direct script access allowed');
class FPP1 extends MY_Controller {

	public $form;

	public $formData;

	public $modelAlias = 'fpp1m';

	// public $alias = 'news';

	// public $module = 'Upload Jemaat';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/TabsFormulir_permohonan/SISTEM5/FPP1_model','fpp1m');
		$admin = $this->session->userdata('admin');

		$this->form = array( 
			  'form'=>array( 
					 array( 
					  	'field'			=> 	'fpp1_file', 
					  	'type'			=>	'file', 
					  	'label'			=>	'Upload File', 
					  	'upload_path'	=>	base_url('assets/lampiran/fpp1_file/'),
						'upload_url'	=>	site_url('vendor/TabsFormulir_permohonan/SISTEM5/FPP1/upload_lampiran'),
					  	'allowed_types'	=>	'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
						'caption' 		=> 	'<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		              	'rules' 		=> 	'required' 
						) 
			  			
			  	 ) 
			);


		$this->getData = $this->fpp1m->getData($this->form);
		$this->insertUrl = site_url('vendor/TabsFormulir_permohonan/SISTEM5/FPP1/save/');
		$this->updateUrl = 'vendor/TabsFormulir_permohonan/SISTEM5/FPP1/update';
		$this->deleteUrl = 'vendor/TabsFormulir_permohonan/SISTEM5/FPP1/delete/';
		$this->prosesUrl = 'vendor/TabsFormulir_permohonan/SISTEM5/FPP1/proses/';
	}

	public function index($id=''){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/TabsFormulir_permohonan/SISTEM5/FPP1'),
			'title' => 'List Lampiran'
		));
		$this->header = 'List Lampiran';
		$this->load->view('vendor/TabFormulir_permohonan/FPP1/list',$data, FALSE);
		$this->load->view('vendor/TabFormulir_permohonan/FPP1/list_js', $data, FALSE);
	}
	public function edit($id = null)
	{	
		$this->form = $this->form;
		$this->updateUrl = 'vendor/TabsFormulir_permohonan/SISTEM5/FPP1/update';
		parent::edit($id); 
	}
	public function insert()
	{
		$this->form['url'] = site_url('vendor/TabsFormulir_permohonan/SISTEM5/FPP1/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function save($id='')
	{

		$modelAlias = $this->modelAlias;
		// $this->form_validation->set_rules($this->form);
		if ($this->validation()) {
			$save = $this->input->post();
			if ($this->$modelAlias->insert($save)) {
				$this->deleteTemp($save);
				return true;
			}	
		}
	}

	public function update($id)
	{
		$modelAlias = $this->fpp1m;
			$save = $this->input->post();
			$lastData = $this->fpp1m->selectData($id);
			if ($this->fpp1m->update($id, $save)) {
				$this->deleteTemp($save, $lastData);
				echo json_encode(array ('status'=> 'success'));
			}
		// }
	}
	public function delete($id) {
		
		if ($this->fpp1m->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('vendor/TabsFormulir_permohonan/SISTEM5/FPP1/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}
	 public function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form_filter['filter'];
		echo json_encode($return);
	}
}
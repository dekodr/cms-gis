<?php defined('BASEPATH') OR exit('No direct script access allowed');
class SPPTS1 extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Formulir_permohonan';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('vendor/permohonan_sertifikasi_model','psm');
	}
	
	public function index($id='', $process=false)
	{
	 	$data['permohonan'] = $this->psm->getPermohonan($user['id_user']);
		$data['data'] = $this->am->get_data_Formulir_permohonan($id, 'sps_file');
	 	$data['vendor_status'] = $this->am->getVendorStatus($id);
	 	$data['id'] = $id;
	 	$this->header = 'List Formulir Permohonan';
	 	$this->load->view('vendor/TabFormulir_permohonan/view_SPPTS1',$data, FALSE);
	 	$this->load->view('vendor/TabFormulir_permohonan/view_SPPTS1_js', $data, FALSE);
	 	// parent::index();
		
	}

	public function SPS($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonan($id, 'sps_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Surat_permohonan_sertifikasi/list', $data, FALSE);
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Surat_permohonan_sertifikasi/list_js', $data, FALSE);	
	}

	public function SPM($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonan($id, 'spm_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Surat_pelimpahan_merk/list', $data, FALSE);
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Surat_pelimpahan_merk/list_js', $data, FALSE);	
	}

	public function spjps($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonan($id, 'spjps_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Surat_pernyataan_jaminan_proses_sertifikasi/list', $data, FALSE);
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Surat_pernyataan_jaminan_proses_sertifikasi/list_js', $data, FALSE);	
	}

	public function spl($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonan($id, 'spl_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Surat_perjanjian_lisensi/list', $data, FALSE);
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Surat_perjanjian_lisensi/list_js', $data, FALSE);	
	}

	public function spks($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonan($id, 'spks_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Surat_perjanjian_kerjasama_sertifikasi/list', $data, FALSE);
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Surat_perjanjian_kerjasama_sertifikasi/list_js', $data, FALSE);	
	}

	// public function fa($id)
	// {
	// 		$data['data'] = $this->am->get_data_Formulir_permohonan($id, 'fa_file');
	// 		$data['vendor_status'] = $this->am->getVendorStatus($id);
	// 		$data['id'] = $id;
	// 		$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Formulir_aplikasi/list', $data, FALSE);
	// 		$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Formulir_aplikasi/list_js', $data, FALSE);	
	// }

	public function dp($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonan($id, 'dp_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Data_pemohon/list', $data, FALSE);
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Data_pemohon/list_js', $data, FALSE);	
	}

	// public function tps($id)
	// {
	// 		$data['data'] = $this->am->get_data_Formulir_permohonan($id, 'tps_file');
	// 		$data['vendor_status'] = $this->am->getVendorStatus($id);
	// 		$data['id'] = $id;
	// 		$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Tinjauan_permohonan_sertifikasi/list', $data, FALSE);
	// 		$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Tinjauan_permohonan_sertifikasi/list_js', $data, FALSE);	
	// }

	public function ip($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonan($id, 'ip_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Informasi_produsen/list', $data, FALSE);
			$this->load->view('vendor/TabFormulir_permohonan/SPPTS1/Informasi_produsen/list_js', $data, FALSE);	
	}
}

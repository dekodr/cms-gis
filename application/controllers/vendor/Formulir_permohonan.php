<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Formulir_permohonan extends MY_Controller {

	public $form;

	// public $modelAlias = 'pm';

	// public $alias = 'ms_jemaat';

	public $module = 'Formulir Permohonan';

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();
			$this->load->model('vendor/Permohonan_sertifikasi_model','psm');
			// $this->load->model('get_model','gm');
			// $this->load->model('tabs/jemaat_model','um');

			$admin = $this->session->userdata('admin');
	}
	
    public function index($id=''){
	 	$admin = $this->session->userdata('admin');
	 	$user = $this->session->userdata('user');
	 	$this->id_client = $id;

	 	$this->breadcrumb->addlevel(1, array(
	 		'url' => site_url('vendor/Formulir_permohonan'),
	 		'title' => 'List Formulir Permohonan'
	 	));

	 	$data['id'] = $id;
	 	$data['s1'] = $this->psm->getPermohonan($user['id_user'])->result_array();
	 	$this->header = 'List Formulir Permohonan';
	 	$this->content = $this->load->view('vendor/TabFormulir_permohonan/view_tab',$data, TRUE);
	 	$this->script = $this->load->view('vendor/TabFormulir_permohonan/view_tab_js', $data, TRUE);
	 	parent::index();

	 }

	 public function getVal($id)
	 {
	 	echo json_encode($this->psm->getPermohonan($this->input->post('val')));
	 }
}
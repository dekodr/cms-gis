<?php defined('BASEPATH') OR exit('No direct script access allowed');
class DAL extends MY_Controller {

	public $form;

	public $formData;

	public $modelAlias = 'dalm';

	// public $alias = 'news';

	// public $module = 'Upload Jemaat';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/TabsDokumen_mutu/DAL_model','dalm');
		$admin = $this->session->userdata('admin');

		$this->form = array( 
			  'form'=>array( 
					 array( 
					  	'field'			=> 	'dal_file', 
					  	'type'			=>	'file', 
					  	'label'			=>	'Upload File', 
					  	'upload_path'	=>	base_url('assets/lampiran/dal_file/'),
						'upload_url'	=>	site_url('vendor/TabsDokumen_mutu/DAL/upload_lampiran'),
					  	'allowed_types'	=>	'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
						'caption' 		=> 	'<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		              	'rules' 		=> 	'required' 
						) 
			  			
			  	 ) 
			);


		$this->getData = $this->dalm->getData($this->form);
		$this->insertUrl = site_url('vendor/TabsDokumen_mutu/DAL/save/');
		$this->updateUrl = 'vendor/TabsDokumen_mutu/DAL/update';
		$this->deleteUrl = 'vendor/TabsDokumen_mutu/DAL/delete/';
		$this->prosesUrl = 'vendor/TabsDokumen_mutu/DAL/proses/';
	}

	public function index($id=''){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/TabsDokumen_mutu/DAL'),
			'title' => 'List Lampiran'
		));
		$this->header = 'List Lampiran';
		$this->load->view('vendor/TabDokumen_mutu/DAL/list',$data, FALSE);
		$this->load->view('vendor/TabDokumen_mutu/DAL/list_js', $data, FALSE);
	}
	public function edit($id = null)
	{	
		$this->form = $this->form;
		$this->updateUrl = 'vendor/TabsDokumen_mutu/DAL/update';
		parent::edit($id); 
	}
	public function insert()
	{
		$this->form['url'] = site_url('vendor/TabsDokumen_mutu/DAL/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function save($id='')
	{

		$modelAlias = $this->modelAlias;
		// $this->form_validation->set_rules($this->form);
		if ($this->validation()) {
			$save = $this->input->post();
			if ($this->$modelAlias->insert($save)) {
				$this->deleteTemp($save);
				return true;
			}	
		}
	}

	public function update($id)
	{
		$modelAlias = $this->dalm;
			$save = $this->input->post();
			$lastData = $this->dalm->selectData($id);
			if ($this->dalm->update($id, $save)) {
				$this->deleteTemp($save, $lastData);
				echo json_encode(array ('status'=> 'success'));
			}
		// }
	}
	public function delete($id) {
		
		if ($this->dalm->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('vendor/TabsDokumen_mutu/DAL/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Selesai'
			) ,
			array(
				'type' => 'delete',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}
	 public function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form_filter['filter'];
		echo json_encode($return);
	}
}
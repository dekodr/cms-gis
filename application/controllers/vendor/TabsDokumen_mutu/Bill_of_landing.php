<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Bill_of_landing extends MY_Controller {

	public $form;

	public $formData;

	public $modelAlias = 'blm';

	// public $alias = 'news';

	// public $module = 'Upload Jemaat';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/TabsDokumen_mutu/Bill_of_landing_model','blm');
		$admin = $this->session->userdata('admin');

		$this->form = array( 
			  'form'=>array( 
					 array( 
					  	'field'			=> 	'bl_file', 
					  	'type'			=>	'file', 
					  	'label'			=>	'Upload File', 
					  	'upload_path'	=>	base_url('assets/lampiran/bl_file/'),
						'upload_url'	=>	site_url('vendor/TabsDokumen_mutu/Bill_of_landing/upload_lampiran'),
					  	'allowed_types'	=>	'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
						'caption' 		=> 	'<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		              	'rules' 		=> 	'required' 
						) 
			  			
			  	 ) 
			);


		$this->getData = $this->blm->getData($this->form);
		$this->insertUrl = site_url('vendor/TabsDokumen_mutu/Bill_of_landing/save/');
		$this->updateUrl = 'vendor/TabsDokumen_mutu/Bill_of_landing/update';
		$this->deleteUrl = 'vendor/TabsDokumen_mutu/Bill_of_landing/delete/';
		$this->prosesUrl = 'vendor/TabsDokumen_mutu/Bill_of_landing/proses/';
	}

	public function index($id=''){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/TabsDokumen_mutu/Bill_of_landing'),
			'title' => 'List Lampiran'
		));
		$this->header = 'List Lampiran';
		$this->load->view('vendor/TabDokumen_mutu/Bill_of_landing/list',$data, FALSE);
		$this->load->view('vendor/TabDokumen_mutu/Bill_of_landing/list_js', $data, FALSE);
	}
	public function edit($id = null)
	{	
		$this->form = $this->form;
		$this->updateUrl = 'vendor/TabsDokumen_mutu/Bill_of_landing/update';
		parent::edit($id); 
	}
	public function insert()
	{
		$this->form['url'] = site_url('vendor/TabsDokumen_mutu/Bill_of_landing/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function save($id='')
	{

		$modelAlias = $this->modelAlias;
		// $this->form_validation->set_rules($this->form);
		if ($this->validation()) {
			$save = $this->input->post();
			if ($this->$modelAlias->insert($save)) {
				$this->deleteTemp($save);
				return true;
			}	
		}
	}

	public function update($id)
	{
		$modelAlias = $this->blm;
			$save = $this->input->post();
			$lastData = $this->blm->selectData($id);
			if ($this->blm->update($id, $save)) {
				$this->deleteTemp($save, $lastData);
				echo json_encode(array ('status'=> 'success'));
			}
		// }
	}
	public function delete($id) {
		
		if ($this->blm->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('vendor/TabsDokumen_mutu/Bill_of_landing/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Selesai'
			) ,
			array(
				'type' => 'delete',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}
	 public function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form_filter['filter'];
		echo json_encode($return);
	}
}
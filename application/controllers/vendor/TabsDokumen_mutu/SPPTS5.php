<?php defined('BASEPATH') OR exit('No direct script access allowed');
class SPPTS5 extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Dokumen_mutu';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('vendor/Permohonan_sertifikasi_model','psm');
	}
	
	public function index($id='', $process=false)
	{
 		$data['permohonan'] = $this->psm->getPermohonan($user['id_user']);
		$data['data'] = $this->am->get_data_Dokumen_mutu($id, 'pm_file');
 		$data['vendor_status'] = $this->am->getVendorStatus($id);
		$data['id'] = $id;
	 	$this->header = 'List Dokumen Mutu';
		$this->load->view('vendor/TabDokumen_mutu/view_SPPTS5', $data, FALSE);
		$this->load->view('vendor/TabDokumen_mutu/view_SPPTS5_js', $data, FALSE);
		
	}

	public function PM($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutu($id, 'pm_file');
 			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/PM/list', $data, FALSE);
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/PM/list_js', $data, FALSE);	
	}

	public function SSM($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutu($id, 'ssm_file');
 			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/SSM/list', $data, FALSE);
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/SSM/list_js', $data, FALSE);	
	}

	public function RM($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutu($id, 'rm_file');
 			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/RM/list', $data, FALSE);
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/RM/list_js', $data, FALSE);	
	}

	public function DID($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutu($id, 'did_file');
 			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/DID/list', $data, FALSE);
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/DID/list_js', $data, FALSE);	
	}

	public function SO($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutu($id, 'so_file');
 			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/SO/list', $data, FALSE);
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/SO/list_js', $data, FALSE);	
	}

	public function APP($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutu($id, 'app_file');
 			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/APP/list', $data, FALSE);
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/APP/list_js', $data, FALSE);	
	}

	public function PP($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutu($id, 'pp_file');
 			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/PP/list', $data, FALSE);
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/PP/list_js', $data, FALSE);	
	}

	public function PMP($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutu($id, 'pmp_file');
 			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/PMP/list', $data, FALSE);
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/PMP/list_js', $data, FALSE);	
	}

	public function PBB($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutu($id, 'pbb_file');
 			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/PBB/list', $data, FALSE);
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/PBB/list_js', $data, FALSE);	
	}

	public function PL($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutu($id, 'pl_file');
 			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/PL/list', $data, FALSE);
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/PL/list_js', $data, FALSE);	
	}

	public function BL($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutu($id, 'bl_file');
 			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/BL/list', $data, FALSE);
			$this->load->view('vendor/TabDokumen_mutu/SPPTS5/BL/list_js', $data, FALSE);	
	}
}

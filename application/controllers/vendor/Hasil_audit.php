<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Hasil_audit extends MY_Controller {

	public $form;

	public $modelAlias = 'ham';

	// public $alias = 'ms_Hasil_audit';

	public $module = 'Hasil_audit';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/Hasil_audit_model','ham');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'Hasil_audit_file',
		            'type'	=>	'file',
		            'label'	=>	'Jadwal Audit',
		            'upload_path'=>base_url('assets/lampiran/Hasil_audit_file/'),
					'upload_url'=>site_url('vendor/Hasil_audit/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	)
	         )
		);		
		$this->getData = $this->ham->getData($this->form);
		$this->insertUrl = site_url('vendor/Hasil_audit/save/');
		$this->updateUrl = 'vendor/Hasil_audit/update';
		$this->deleteUrl = 'vendor/Hasil_audit/delete/';
		$this->prosesUrl = 'vendor/Hasil_audit/proses/';
	}
	
	public function index($id=''){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/Hasil_audit'),
			'title' => 'Hasil Audit'
		));
		$this->header = 'Hasil Audit';
		$this->content = $this->load->view('vendor/Hasil_audit/list',$data, TRUE);
		$this->script = $this->load->view('vendor/Hasil_audit/list_js', $data, TRUE);
		parent::index();
	}
	public function edit($id = null)
	{
		// $this->form['form'][1]['caption'] = "*Biarkan kosong jika tidak ingin diganti";
		parent::edit($id); 
	}
	public function insert()
	{
		$this->form['url'] = site_url('vendor/Hasil_audit/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();		
			if ($this->$modelAlias->insert($save)) {
				$this->deleteTemp($save);
			}	
		}
	}

	public function update($id)
	{
		$modelAlias = $this->ham;
		if ($this->validation()) {
			$save = $this->input->post();

		$save['jenis_perusahaan'] 		= $save['jenis_perusahaan'];
		$save['nama_perusahaan'] 		= $save['nama_perusahaan'];
		$save['alamat'] 				= $save['alamat'];
		$save['kota'] 					= $save['kota'];
		$save['provinsi'] 				= $save['provinsi'];
		$save['kode_pos'] 				= $save['kode_pos'];
		$save['telepon'] 				= $save['telepon'];
		$save['fax'] 					= $save['fax'];
		$save['email'] 					= $save['email'];
		$save['nama_penanggung_jawab'] 	= $save['nama_penanggung_jawab'];
		$save['jabatan'] 				= $save['jabatan'];
		$save['contact_person'] 		= $save['contact_person'];
		$save['no_hp'] 					= $save['no_hp'];
		$save['edit'] 					= date('Y-m-d H:i:s');

			$lastData = $this->ham->selectData($id);
			if ($this->ham->update($id, $save)) {
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function delete($id) {
		
		if ($this->ham->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('vendor/Hasil_audit/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function proses($id){

	}
	function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form_filter['filter'];
		echo json_encode($return);
	}
}

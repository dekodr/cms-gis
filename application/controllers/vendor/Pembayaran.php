<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pembayaran extends MY_Controller {

	public $form;

	public $modelAlias = 'pm';

	public $alias = 'ms_Pembayaran';

	public $module = 'Pembayaran';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/Pembayaran_model','pm');
		// $this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(	         	
	         	 array(
		            'field'	=> 	'pembayaran_file',
		            'type'	=>	'file',
		            'label'	=>	'Upload Bukti Pembayaran',
		            'upload_path'=>base_url('assets/lampiran/pembayaran_file/'),
					'upload_url'=>site_url('vendor/Pembayaran/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	)
	        )
		);		
		$this->getData = $this->pm->getData($this->form);
		$this->insertUrl = site_url('vendor/Pembayaran/save/');
		$this->updateUrl = 'vendor/Pembayaran/update';
		$this->deleteUrl = 'vendor/Pembayaran/delete/';
		$this->prosesUrl = 'vendor/Pembayaran/proses/';
	}
	
	public function index($id=''){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/Pembayaran'),
			'title' => 'Bukti Pembayaran'
		));
		$this->header = 'Bukti Pembayaran';
		$this->content = $this->load->view('vendor/Pembayaran/list',$data, TRUE);
		$this->script = $this->load->view('vendor/Pembayaran/list_js', $data, TRUE);
		parent::index();
	}
	public function edit($id = null)
	{
		// $this->form['form'][1]['caption'] = "*Biarkan kosong jika tidak ingin diganti";
		parent::edit($id); 
	}
	public function insert()
	{
		$this->form['url'] = site_url('vendor/Pembayaran/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		// if ($this->validation()) {
			$save = $this->input->post();		
			if ($this->$modelAlias->insert($save)) {
				$this->deleteTemp($save);
				echo json_encode(array ('status'=> 'success'));
				// return true;
			}	
		// }
	}

	public function update($id)
	{
		$modelAlias = $this->pm;
		// if ($this->validation()) {
			$save = $this->input->post();
			$lastData = $this->pm->selectData($id);
			if ($this->pm->update($id, $save)) {
				$this->deleteTemp($save, $lastData);
				echo json_encode(array ('status'=> 'success'));
			}
		// }
	}

	public function delete($id) {
		
		if ($this->pm->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('vendor/Pembayaran/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function proses($id){

	}
	function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form_filter['filter'];
		echo json_encode($return);
	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Catatan extends MY_Controller {

	public $form;

	// public $modelAlias = 'pm';

	// public $alias = 'ms_jemaat';

	public $module = 'Formulir Permohonan';

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();
			// $this->load->model('Proses_model','pm');
			// $this->load->model('get_model','gm');
			// $this->load->model('tabs/jemaat_model','um');

			$admin = $this->session->userdata('admin');
	}
	
    public function index($id=''){
	 	$admin = $this->session->userdata('admin');
	 	$this->id_client = $id;

	 	$this->breadcrumb->addlevel(1, array(
	 		'url' => site_url('vendor/Catatan'),
	 		'title' => 'List Catatan'
	 	));

	 	$data['id'] = $id;
	 	$this->header = 'List Catatan';
	 	$this->content = $this->load->view('vendor/TabCatatan/view',$data, TRUE);
	 	$this->script = $this->load->view('vendor/TabCatatan/view_js', $data, TRUE);
	 	parent::index();

	 }
}
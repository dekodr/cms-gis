<?php defined('BASEPATH') or exit('No direct script access allowed');
class Permohonan_sertifikasi extends MY_Controller
{

	public $form;

	public $modelAlias = 'psm';

	// public $alias = 'ms_Permohonan_sertifikasi';

	public $module = 'Permohonan_sertifikasi';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct()
	{

		parent::__construct();

		$this->load->model('vendor/Permohonan_sertifikasi_model', 'psm');
		$this->load->model('get_model', 'gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form' => array(
				array(
					'field'	=> 	'jenis_permohonan_id',
					'type'	=>	'dropdown',
					'label'	=>	'Jenis Permohonan',
					'source' =>	$this->psm->getJenisPermohonan(),
					'rules' =>	'required'
				),
				array(
					'field'	=> 	'sistem_sertifikasi_id',
					'type'	=>	'dropdown',
					'label'	=>	'Sistem Sertifikasi',
					'source' =>  $this->psm->getSistemSertifikasi()
				),
				array(
					'field'	=> 	'lingkup_pemohon_id',
					'type'	=>	'dropdown',
					'label'	=>	'Lingkup Pemohon',
					'source' =>  $this->psm->getLingkupPemohon()
				),
				array(
					'field'	=> 	'id_jenis_sertifikasi',
					'type'	=>	'dropdown',
					'label'	=>	'Produk',
					'source' =>  $this->psm->getLingkupSertifikasi()
				),
				array(
					'field'	=> 	'name',
					'type'	=>	'text',
					'label'	=>	'Lingkup Pemohon'
				),
			)
		);
		$this->getData = $this->psm->getData($this->form);
		$this->insertUrl = site_url('vendor/Permohonan_sertifikasi/save/');
		$this->updateUrl = 'vendor/Permohonan_sertifikasi/update';
		$this->deleteUrl = 'vendor/Permohonan_sertifikasi/delete/';
		$this->prosesUrl = 'vendor/Permohonan_sertifikasi/proses/';
	}

	public function index($id = '')
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/Permohonan_sertifikasi'),
			'title' => 'Data Permohonan Sertifikasi'
		));
		$this->header = 'Data Permohonan Sertifikasi';
		$this->content = $this->load->view('vendor/Permohonan_sertifikasi/list', $data, TRUE);
		$this->script = $this->load->view('vendor/Permohonan_sertifikasi/list_js', $data, TRUE);
		parent::index();
	}
	public function edit($id = null)
	{
		// $this->form['form'][1]['caption'] = "*Biarkan kosong jika tidak ingin diganti";
		parent::edit($id);
	}
	public function insert()
	{
		$this->form['url'] = site_url('vendor/Permohonan_sertifikasi/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			),
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			// print_r($save);die;		
			if ($this->$modelAlias->insert($save)) {
				$this->deleteTemp($save);
			}
		}
	}

	public function update($id)
	{
		$modelAlias = $this->psm;
		if ($this->validation()) {
			$save = $this->input->post();
			$lastData = $this->psm->selectData($id);
			if ($this->psm->update($id, $save)) {
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function delete($id)
	{

		if ($this->psm->delete($id)) {
			$return['status'] = 'success';
		} else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('vendor/Permohonan_sertifikasi/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			),
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function proses($id)
	{
	}
	function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			),
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form_filter['filter'];
		echo json_encode($return);
	}

	public function getLP($id = "")
	{
		echo json_encode($this->psm->getLP($this->input->post('val')));
	}
	public function getJI($id = "")
	{
		echo json_encode($this->psm->getJI($this->input->post('val')));
	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
//include 'admin/certificate_admin.php';

class Certificate extends MY_Controller {

	public $form;

	public $modelAlias = 'cm';

	public $alias = 'ms_certificate';

	public $module = 'Certificate';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){
// error_reporting(E_ALL);
		ini_set("memory_limit","2048M");
		parent::__construct();
		
		require_once(BASEPATH."plugins/dompdf2/dompdf_config.inc.php");
		$this->load->model('vendor/certificate_model','cm');
		$this->load->model('get_model','gm');
		$this->load->model('approval_model','am');
		$user = $this->session->userdata('user');
		
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'id_category',
		            'type'	=> 'dropdown',
		            'label'	=> 'Kategori',
		            'source' => $this->gm->getCategory(),
		            'rules' => 'required'
		        ),
		        array(
		            'field'	=> 'id_certificate',
		            'type'	=> 'dropdown',
		            'label'	=> 'Tipe Sertifikat',
		            'source' => $this->gm->getCertificate(),
		            'rules' => 'required'
		        ),
		       
	         ),
			'filter' => array(
				array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		        ),
	         	array(
		            'field'	=> 	'position',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
	         	)
			)
		);
		$this->formClientUpload = array(
			'form'=>array(
				array(
		            'field'	=> 	'client_upload_file',
		            'type'	=>	'file',
		            'label'	=>	'Dokumen Lampiran',
		            'upload_path'=>base_url('assets/lampiran/client_upload_file/'),
					'upload_url'=>site_url('vendor/certificate/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	)
	         ),
			'filter' => array(
				array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		        ),
	         	array(
		            'field'	=> 	'position',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
	         	)
			)
		);
		$this->getData = $this->cm->getData($this->form);
		$this->insertUrl = site_url('vendor/certificate/save/');
		$this->updateUrl = 'vendor/certificate/update/';
		$this->deleteUrl = 'vendor/certificate/delete/';

	}
	
	public function index($id=''){
		$data = $this->session->userdata('user');
		$data['approval_data'] = $this->am->get_total_data($data['id_user']);
		$data['data_status'] = $this->cm->get_data_status($data['id_user']);
		$data['bar'] = array(
							'rejected'=>array(
								'percentage'=>(count($data['approval_data'][3])+count($data['approval_data'][4]))/$data['approval_data']['total']*100,
								'value'=>(count($data['approval_data'][3])+count($data['approval_data'][4]))
							),
							'pending'=>array(
								'percentage'=>(count($data['approval_data'][0]))/$data['approval_data']['total']*100,
								'value'=>count($data['approval_data'][0])
							),
							'approved'=>array(
								'percentage'=>(count($data['approval_data'][1])+count($data['approval_data'][2]))/$data['approval_data']['total']*100,
								'value'=>(count($data['approval_data'][1])+count($data['approval_data'][2]))
							)
						);
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/certificate'),
			'title' => 'Usulan Sertifikat'
		));
		$this->header = 'Usulan Sertifikat';
		$this->content = $this->load->view('vendor/certificate/list',$data, TRUE);
		$this->script = $this->load->view('vendor/certificate/list_js', $data, TRUE);
		parent::index();
	}

	public function print_certificate_($id){
		$data = $this->session->userdata('user');
	 	$data['id'] = $id;
	 	$data['detail_certificate'] = $this->cm->selectDetailCertificate($id)->row_array();
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/certificate/detail_certificate/'.$id),
			'title' => 'Cetak Sertifikat'
		));
		$data['indicator'] = $this->indicator($data['detail_certificate']['status']);
		$this->header = 'Cetak Sertifikat';

		$this->content = $this->load->view('vendor/certificate/print',$data, TRUE);
		// $this->script = $this->load->view('vendor/admin/certificate/view_js', $data, TRUE);
		parent::index();
	}

	 public function insert()
	 {
	 	$this->form['url'] = site_url('vendor/certificate/save');
	 	$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->form);
	 }

	 public function save($data = null)
	 {
	 	$user = $this->session->userdata('user');
	 	$_POST['id_user'] = $user['id_user'];
		$_POST['status'] = 0;
	 	return parent::save();
	 }

	public function detail_certificate($id,$id_certificate)
	{
	 	$data = $this->session->userdata('user');
	 	$data['id_certificate'] = $id_certificate;
	 	$data['id'] = $id;
	 	$data['detail_certificate'] = $this->cm->selectDetailCertificate($id)->row_array();
	 	$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/certificate'),
			'title' => 'Usulan Sertifikat'
		));
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/certificate/detail_certificate/'.$id),
			'title' => 'Detail Sertifikat'
		));
		$data['indicator'] = $this->indicator($data['detail_certificate']['status']);
		$this->header = 'Detail Sertifikat';

		$this->content = $this->load->view('vendor/certificate/view',$data, TRUE);
		$this->script = $this->load->view('vendor/certificate/view_js', $data, TRUE);
		parent::index();
	}

	public function uploadForm($id_certificate,$id_form){
		$this->formClientUpload['url'] = site_url('vendor/certificate/processUpload/'.$id_certificate.'/'.$id_form);
	 	$this->formClientUpload['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Simpan',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->formClientUpload);
	}

	public function processUpload($id_certificate,$id_form){
		$user = $this->session->userdata('user');
	 	$modelAlias = $this->modelAlias;
	 	if ($this->validation($this->formClientUpload)) {
	 		$save = $this->input->post();
	 		$save['id'] = $id_form;
			$save['edit_stamp'] = date("Y-m-d H:i:s");
			$save['del'] = 0;
	 		if ($this->$modelAlias->insertUploadClient($save,$id_certificate)) {
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 			return true;
	 		}
	 	}
	}

	public function download_form($id)
  	{
	    if (!empty($id)) {

	      $this->load->helper('download');

	      $info = $this->cm->download_form(array('id' => $id));

	      $dir = 'assets/lampiran/file_form/'.$info['template'];

	      force_download($dir, null);
		}
  	}

  	public function getDataForm($id, $id_certificate){
		$config['query'] = $this->cm->getDataForm($id, $id_certificate,$this->form);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function form_print($id_certificate)
	{
		$data = $this->session->userdata('admin');
		$data['id_certificate'] = $id_certificate;
		$this->load->view('vendor/certificate/wisywyg', $data, FALSE);
	}

  	public function print_certificate($id_certificate)
  	{
  		$no_certif = '';
		$user = $this->session->userdata('user');
		
		$fill = $this->cm->print_certificate($id_certificate);

		// print_r($fill);

		
		$no_id = (6 - strlen($fill['id']));

		for($i=0;$i<$no_id;$i++) $no_certif .= "0";
		$no_certif .= $fill['id'];

		$no_certif = $no_certif."/GIS".$sbu."/".date("d/m/Y", strtotime($bsb_date));
		
		$no_telp = $fill['vendor_phone']; 
		if($fill['vendor_fax']) $no_telp .= "/".$fill['vendor_fax'];

		$post = $this->input->post('description');

		if ($post == '') {
			$description = '<tr>
								<td align="center">
									<div style="color : #00aeef; font-size : 20px">SERTIFIKAT '.$fill['certificate_name'].'<br> PT Global Inspeksi Sertifikasi</div>
									<div style="color : #4e5f6e">'.$no_certif.'</div>
								</td>
							</tr>
							<tr>
								<td height="50px"></td>
							</tr>
							<tr>
								<td height="10px" align="center"><b style="font-size : 20px">'.$fill['user'].' '.$fill['name'].'</td>
							</tr>
							<tr>
								<td height="50px"></td>
							</tr>
							<tr>
								<td align="center">
									<div style="margin-left : 70px; border-bottom : 1px solid #000; border-top : 1px solid #000; width : 500px; padding : 20px">
										<table cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="140px" valign="top">NPWP</td>
												<td width="10px" valign="top">:</td>
												<td width="350px" valign="top">'.$fill['npwp_code'].'</td>
											</tr>
											<tr>
												<td valign="top">Alamat</td>
												<td valign="top">:</td>
												<td valign="top">'.nl2br($fill['vendor_address']).'</td>
											</tr>
											<tr>
												<td valign="top">No. Telp/Fax</td>
												<td valign="top">:</td>
												<td valign="top">'.$no_telp.'</td>
											</tr>
											
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td height="100px"></td>
							</tr>';
		}else{
			$description = '<tr>
								<td align="center">
									<div style="color : #00aeef; font-size : 20px">SERTIFIKAT '.$fill['certificate_name'].'<br> PT Global Inspeksi Sertifikasi</div>
									<div style="color : #4e5f6e">'.$no_certif.'</div>
								</td>
							</tr>
							<tr>
								<td height="50px"></td>
							</tr>
							<tr>
								<td height="10px" align="center"><b style="font-size : 20px">'.$post.'</td>
							</tr>
							<tr>
								<td height="50px"></td>
							</tr>
							<tr>
								<td height="100px"></td>
							</tr>';
		}
		
		$return ='<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
					<style type="text/css">
						html, body{
							margin : 0px;
							padding : 0px; 
						 }
						 div.page{
							font-family : "Helvetica";
							margin : 0px;
							padding : 0px;
							font-size : 13px;
						}
						div#first-page{
							background-image : url('.base_url('/assets/images/sertifikat-web.jpg').'); 
							background-repeat : repeat-y;
							background-size: 1351px 793px;
							height : 793px;
							page-break-inside: avoid;
						}
						div.table-separator{
							page-break-inside : avoid;
						}
						table.std-table{
							border-collapse : collapse;
							border : 1px solid #000;
							margin : 15px;
						}
						table.std-table th{
							background : #000;
							color : #fff;
							border : 1px solid #000;
						}
						.qrcode {
							position: absolute;
							top:600px;
							width:150px;
							right:20px;
						}
					</style>
				</head>
				<body>
					<div class="page" id="first-page">
					<img class="qrcode" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='.site_url('vendor/certificate/print_certificate/'.$id_certificate).'%2F&choe=UTF-8" title="Link to Detail Vendor" />
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="220px" valign="top"><font style="font-size : 9px; color : #fff">Dicetak pada tanggal : '.date("d/m/Y, H:i:s").'</font></td>
								<td width="680px">
									<table cellpadding="0" cellspacing="0" border="0" width="680px">
										<tr>
											<td height="30px"></td>
										</tr>
										<tr>
											<td align="center"><img src="'.base_url('assets/images/logo.png').'" width="65"></td>
										</tr>
										<tr>
											<td height="30px"></td>
										</tr>
										'.$description.'
										<tr>
											<td align="center">Jakarta, '.default_date(date('Y-m-d')).'</td>
										</tr>
										<tr>
											<td height="80px"></td>
										</tr>
										
										<tr>
											<td height="10px"></td>
										</tr>
										<tr>
											<td align="center">
												<font style="font-size : 8px">
													dicetak oleh PT Global Inspeksi Sertifikasi .<br/>
													Dokumen ini resmi tanpa stempel
												</font>
											</td>
										</tr>
									</table>
								</td>
								<td width="220px">&nbsp;</td>
							</tr>
						</table>
					</div>
				</body>
			</html>';

		echo $return;die;
		// return;"d/m/Y", strtotime($bsb_date)

		$dompdf = new DOMPDF();  
	    $dompdf->load_html($return);  
	    $dompdf->set_paper('A4','landscape');
	    $dompdf->render();
									
        $dompdf->stream("sertifikat-GIS.pdf",array('Attachment' => false));
	}

  	public function submit_process($id_certificate){
  		$data = $this->cm->submit_process($id_certificate);
  		switch ($data['status']) {
  			case 1:
  				redirect(site_url('vendor/certificate/assessment/'.$id_certificate));
  				break;
  			
  			case 2:
  				redirect(site_url('vendor/certificate/verifikasi/'.$id_certificate));
  				break;

  			case 3:
  				redirect(site_url('vendor/certificate/print/'.$id_certificate));
  				break;
  		}
	}
	
  	public function get_auditor()
  	{
  		echo json_encode($this->cm->get_auditor());
  	}

  	public function assessment($id){
  		$data = $this->session->userdata('user');
	 	$data['id_certificate'] = $id_certificate;
	 	$data['id'] = $id;
	 	$data['detail_certificate'] = $this->cm->selectDetailCertificate($id)->row_array();
	 	$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/certificate'),
			'title' => 'Usulan Sertifikat'
		));
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/certificate/detail_certificate/'.$id),
			'title' => 'Assessment'
		));
		$data['indicator'] = $this->indicator($data['detail_certificate']['status']);
		$this->header = 'Assessment';

		$this->content = $this->load->view('vendor/certificate/assessment',$data, TRUE);
		$this->script = $this->load->view('vendor/certificate/assessment_js', $data, TRUE);
		parent::index();
  	}

  	public function verifikasi($id){
  		$data = $this->session->userdata('user');
	 	$data['id_certificate'] = $id_certificate;
	 	$data['id'] = $id;
	 	$data['detail_certificate'] = $this->cm->selectDetailCertificate($id)->row_array();
	 	$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/certificate'),
			'title' => 'Usulan Sertifikat'
		));
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/certificate/detail_certificate/'.$id),
			'title' => 'Verifikasi'
		));
		$data['indicator'] = $this->indicator($data['detail_certificate']['status']);
		$this->header = 'Verifikasi';

		$this->content = $this->load->view('vendor/certificate/verifikasi',$data, TRUE);
		$this->script = $this->load->view('vendor/certificate/verifikasi_js', $data, TRUE);
		parent::index();
  	}

  	public function getDataTemuan($id){

  		$config['query'] = $this->cm->get_temuan($id,$this->form);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);

  	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Surat_tugas extends MY_Controller {

	public $form;

	public $modelAlias = 'stm';

	// public $alias = 'ms_Surat_tugas';

	public $module = 'Surat_tugas';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/Surat_tugas_model','stm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'Surat_tugas_file',
		            'type'	=>	'file',
		            'label'	=>	'Jadwal Audit',
		            'upload_path'=>base_url('assets/lampiran/Surat_tugas_file/'),
					'upload_url'=>site_url('vendor/Surat_tugas/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	)
	         )
		);		
		$this->getData = $this->stm->getData($this->form);
		$this->insertUrl = site_url('vendor/Surat_tugas/save/');
		$this->updateUrl = 'vendor/Surat_tugas/update';
		$this->deleteUrl = 'vendor/Surat_tugas/delete/';
		$this->prosesUrl = 'vendor/Surat_tugas/proses/';
	}
	
	public function index($id=''){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/Surat_tugas'),
			'title' => 'Surat Tugas'
		));
		$this->header = 'Surat Tugas';
		$this->content = $this->load->view('vendor/Surat_tugas/list',$data, TRUE);
		$this->script = $this->load->view('vendor/Surat_tugas/list_js', $data, TRUE);
		parent::index();
	}
	public function edit($id = null)
	{
		// $this->form['form'][1]['caption'] = "*Biarkan kosong jika tidak ingin diganti";
		parent::edit($id); 
	}
	public function insert()
	{
		$this->form['url'] = site_url('vendor/Surat_tugas/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();		
			if ($this->$modelAlias->insert($save)) {
				$this->deleteTemp($save);
			}	
		}
	}

	public function update($id)
	{
		$modelAlias = $this->stm;
		if ($this->validation()) {
			$save = $this->input->post();

		$save['jenis_perusahaan'] 		= $save['jenis_perusahaan'];
		$save['nama_perusahaan'] 		= $save['nama_perusahaan'];
		$save['alamat'] 				= $save['alamat'];
		$save['kota'] 					= $save['kota'];
		$save['provinsi'] 				= $save['provinsi'];
		$save['kode_pos'] 				= $save['kode_pos'];
		$save['telepon'] 				= $save['telepon'];
		$save['fax'] 					= $save['fax'];
		$save['email'] 					= $save['email'];
		$save['nama_penanggung_jawab'] 	= $save['nama_penanggung_jawab'];
		$save['jabatan'] 				= $save['jabatan'];
		$save['contact_person'] 		= $save['contact_person'];
		$save['no_hp'] 					= $save['no_hp'];
		$save['edit'] 					= date('Y-m-d H:i:s');

			$lastData = $this->stm->selectData($id);
			if ($this->stm->update($id, $save)) {
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function delete($id) {
		
		if ($this->stm->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('vendor/Surat_tugas/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function proses($id){

	}
	function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form_filter['filter'];
		echo json_encode($return);
	}
}

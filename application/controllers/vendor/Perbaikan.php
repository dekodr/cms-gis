<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Perbaikan extends MY_Controller {

	public $form;

	public $modelAlias = 'pm';

	// public $alias = 'ms_jemaat';

	public $module = 'Formulir Perbaikan';

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();
		$this->load->model('vendor/perbaikan_model','pm');
        // $this->load->model('auditor/proses/audit_tahap_model','atm');
		$user = $this->session->userdata('user');

		$this->form = array(
            'form'=>array(
	         	array(
		            'field'	=> 	'jenis_audit',
		            'type'	=>	'text',
		            'label'	=>	'Jenis Audit',
		            'readonly' => true
	         	),
	         	array(
		            'field'	=> 	'standar_audit',
		            'type'	=>	'text',
		            'label'	=>	'Standar Audit',
		            'readonly' => true
	         	),
	         	array(
		            'field'	=> 	'tgl_audit',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Audit',
		            'readonly' => true
	         	),
	         	array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
		            'readonly' => true
	         	),
	         	array(
		            'field'	=> 	'uraian',
		            'type'	=>	'textarea',
		            'label'	=>	'Uraian Ketidaksesuaian',
		            'readonly' => true
	         	),
	         	array(
		            'field'	=> 	'bagian_audit',
		            'type'	=>	'textarea',
		            'label'	=>	'Bagian yang diaudit',
		            'readonly' => true
	         	),
	         	array(
		            'field'	=> 	'klausul',
		            'type'	=>	'textarea',
		            'label'	=>	'Klausul Ketidaksesuaian',
		            'readonly' => true
	         	),
	         	array(
		            'field'	=> 	'kategori',
		            'type'	=>	'text',
		            'label'	=>	'Kategori',
		            'readonly' => true
	         	),
	         	array(
		            'field'	=> 	'tgl_rencana',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Rencana',
		            'readonly' => true
	         	),
	         	array(
		            'field'	=> 	'analisis',
		            'type'	=>	'textarea',
		            'label'	=>	'Analisis Penyebab',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'koreksi',
		            'type'	=>	'textarea',
		            'label'	=>	'Koreksi',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'tindakan',
		            'type'	=>	'textarea',
		            'label'	=>	'Tindakan Korektif',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'tgl_perbaikan',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Perbaikan',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'dokumen_perbaikan_file',
		            'type'	=>	'file',
		            'label'	=>	'Upload Dokumen Perbaikan',
		            'upload_path'=>base_url('assets/lampiran/dokumen_perbaikan_file/'),
					'upload_url'=>site_url('vendor/perbaikan/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
	         )
        ); 

		$this->getData = $this->pm->getData($user['id_user']);
	}
	
    public function index($id=''){
	 	$user = $this->session->userdata('user');
	 	$this->id_client = $id;

	 	$this->breadcrumb->addlevel(1, array(
	 		'url' => site_url('vendor/Perbaikan'),
	 		'title' => 'Tindakan Perbaikan'
	 	));

	 	$data['id'] = $id;
	 	$this->header = 'Tindakan Perbaikan';
	 	$this->content = $this->load->view('vendor/perbaikan/list',$data, TRUE);
	 	$this->script = $this->load->view('vendor/perbaikan/list_js', $data, TRUE);
	 	parent::index();

	}

	public function lihat_perbaikan($id_certificate){
	 	$user = $this->session->userdata('user');
	 	$this->id_client = $id;

	 	$this->breadcrumb->addlevel(1, array(
	 		'url' => site_url('vendor/Perbaikan'),
	 		'title' => 'Tindakan Perbaikan'
	 	));

	 	$data['id_certificate'] = $id_certificate;
	 	$this->header = 'Tindakan Perbaikan';
	 	$this->content = $this->load->view('vendor/perbaikan/view',$data, TRUE);
	 	$this->script = $this->load->view('vendor/perbaikan/view_js', $data, TRUE);
	 	parent::index();

	}

	public function getDataPerbaikan($id_certificate)
	{
		$config['query'] = $this->pm->getDataPerbaikan($id_certificate);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function tindak_lanjut($id,$s)
	{
		$data = $this->pm->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			if ($s == 1) {
				$this->form['form'][$key]['value'] = $data[$element['field']];
			} else {
				$this->form['form'][$key]['readonly'] = true;
				$this->form['form'][$key]['value'] = $data[$element['field']];
			}
		}


		$this->form['url'] = site_url('vendor/perbaikan/saveTindakLanjut/' . $id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Simpan'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function saveTindakLanjut($id)
	{
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = date('Y-m-d H:i:s');
			if ($this->pm->saveTindakLanjut($id,$save)) {
				$this->deleteTemp($save);
				return true;
			}
		}
	}

	 public function edit($id,$type,$id_client,$id_certificate)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectDataPerbaikan($id);
		$data_history = $this->$modelAlias->selectDataHistory($id);
		if ($type == 'history') {
			// $arr = array(
			// 	array(
		 //            'field'	=> 	'analisis',
		 //            'type'	=>	'textarea',
		 //            'label'	=>	'Analisis Penyebab',
		 //            'rules' => 	'required'
	  //        	),
	  //        	array(
		 //            'field'	=> 	'koreksi',
		 //            'type'	=>	'textarea',
		 //            'label'	=>	'Koreksi',
		 //            'rules' => 	'required'
	  //        	),
	  //        	array(
		 //            'field'	=> 	'tindakan',
		 //            'type'	=>	'textarea',
		 //            'label'	=>	'Tindakan Korektif',
		 //            'rules' => 	'required'
	  //        	),
	  //        	array(
		 //            'field'	=> 	'tgl_perbaikan',
		 //            'type'	=>	'date',
		 //            'label'	=>	'Tanggal Perbaikan',
		 //            'rules' => 	'required'
	  //        	),
	  //        	array(
		 //            'field'	=> 	'dokumen_perbaikan_file',
		 //            'type'	=>	'file',
		 //            'label'	=>	'Upload Dokumen Perbaikan',
		 //            'upload_path'=>base_url('assets/lampiran/dokumen_perbaikan_file/'),
			// 		'upload_url'=>site_url('vendor/perbaikan/upload_lampiran'),
			// 		'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	  //        	)
	  //        );
			// $this->form['form'] = array_merge($this->form['form'],$arr);
			foreach($this->form['form'] as $key => $element) {
				$this->form['form'][$key]['readonly'] = true;
            	$this->form['form'][$key]['value'] = $data_history[$element['field']];
			}
        } else {
        	foreach($this->form['form'] as $key => $element) {
				if ($type == 'cek') {
	                $this->form['form'][$key]['readonly'] = true;
	                $this->form['form'][$key]['value'] = $data[$element['field']];
	            } else {
	                $this->form['form'][$key]['value'] = $data[$element['field']];
	            }
			}
        }

		if ($type != 'cek' && $type != 'history') {
            $this->form['url'] = site_url($this->updateUrl.'/'.$id.'/'.$id_client.'/'.$id_certificate);
            $this->form['button'] = array(
                array(
                    'type'  => 'submit',
                    'label' => 'Ubah'
                ) ,
                array(
                    'type'  => 'cancel',
                    'label' => 'Batal'
                )
            );
        }
		echo json_encode($this->form);
    }

	public function history($id_log,$id_client,$id_certificate)
	{
		$data['user'] = $this->session->userdata('user');
		$data['id_log'] = $id_log;
		$data['id_client']  = $id_client;
		$data['id_certificate']  = $id_certificate;
		$this->breadcrumb->addlevel(1, array(
	 		'url' => site_url('vendor/Perbaikan/lihat_perbaikan/'.$id_client),
	 		'title' => 'Tindakan Perbaikan'
	 	));
		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('vendor/perbaikan/history/'.$id_log),
			'title' => 'Riwayat Perbaikan'
		));
		$this->header = 'Riwayat Perbaikan';
		$this->content = $this->load->view('vendor/perbaikan/history/log',$data, TRUE);
		$this->script = $this->load->view('vendor/perbaikan/history/log_js', $data, TRUE);
		parent::index();
	}
	public function getDataHistory($id_log)
	{
		$config['query'] = $this->pm->getDataHistory($id_log);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}
}
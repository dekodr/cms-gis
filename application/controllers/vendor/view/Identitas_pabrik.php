<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Identitas_pabrik extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Identitas_pabrik';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
	         	array(
	         		'field'	=> 	'nama_pabrik',
		            'type'	=>	'text',
		            'label'	=>	'Nama Pabrik',
		            'rules' => 	'required',
	         	),
	         	array(
	         		'field'	=> 	'alamat',
		            'type'	=>	'text',
		            'label'	=>	'Alamat',
		            'rules' => 	'required',
	         	),
	         	array(
	         		'field'	=> 	'provinsi',
		            'type'	=>	'text',
		            'label'	=>	'Provinsi',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'kota',
		            'type'	=>	'text',
		            'label'	=>	'Kota',
		            'rules' => 	'required',
	         	),
	         	array(
	         		'field'	=> 	'kode_pos',
		            'type'	=>	'text',
		            'label'	=>	'Kode Pos',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'telepon',
		            'type'	=>	'textarea',
		            'label'	=>	'Telepon',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'id',
		            'type'	=>  'hidden',
	         	)
	         )
		);
	}
	
	public function index($id=''){
			$data = $this->am->get_data_identitas_pabrik($id);
			$this->load->view('vendor/admin/view/Identitas_pabrik/list', $data, FALSE);
			$this->load->view('vendor/admin/view/Identitas_pabrik/list_js', $data, FALSE);
	}

	public function view($id){

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->get_data_identitas_pabrik($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }
		echo json_encode($this->form);

	 }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Permohonan_sertifikasi extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Permohonan_sertifikasi';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
	         	array(
	         		'field'	=> 	'jenis',
		            'type'	=>	'text',
		            'label'	=>	'Jenis Permohonan',
		            'rules' => 	'required',
	         	),
	         	array(
	         		'field'	=> 	'sistem_sertifikasi_id',
		            'type'	=>	'text',
		            'label'	=>	'Sistem Sertifikasi',
		            'rules' => 	'required',
	         	),
	         	array(
	         		'field'	=> 	'lingkup_pemohon_id',
		            'type'	=>	'text',
		            'label'	=>	'Lingkup Pemohon',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'id',
		            'type'	=>  'hidden',
	         	)
	         )
		);
	}
	
	public function index($id=''){
			$data = $this->am->get_data_Permohonan_sertifikasi($id);
			$this->load->view('vendor/admin/view/Permohonan_sertifikasi/list', $data, FALSE);
			$this->load->view('vendor/admin/view/Permohonan_sertifikasi/list_js', $data, FALSE);
	}

	public function view($id){

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->get_data_Permohonan_sertifikasi($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }
		echo json_encode($this->form);

	 }
}

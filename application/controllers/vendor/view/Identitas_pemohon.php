<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Identitas_pemohon extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Identitas_pemohon';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
	         	array(
	         		'field'	=> 	'jenis_perusahaan',
		            'type'	=>	'text',
		            'label'	=>	'Jenis Perusahaan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Perusahaan',
		            'rules' => 	'required',
	         	),
	         	array(
	         		'field'	=> 	'alamat',
		            'type'	=>	'text',
		            'label'	=>	'Alamat',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'npwp_code',
		            'type'	=>	'npwp',
		            'label'	=>	'NPWP',
		            'rules' => 	'required',
	         	),
	         	array(
	         		'field'	=> 	'provinsi',
		            'type'	=>	'text',
		            'label'	=>	'Provinsi',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'kota',
		            'type'	=>	'text',
		            'label'	=>	'Kota',
		            'rules' => 	'required',
	         	),
	         	array(
	         		'field'	=> 	'kode_pos',
		            'type'	=>	'text',
		            'label'	=>	'Kode Pos',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'telepon',
		            'type'	=>	'textarea',
		            'label'	=>	'Telepon',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'fax',
		            'type'	=>	'text',
		            'label'	=>	'Fax',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'email',
		            'type'	=>	'text',
		            'label'	=>	'E - Mail',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'nama_penanggung_jawab',
		            'type'	=>	'text',
		            'label'	=>	'Nama Penanggung Jawab',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'jabatan',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'contact_person',
		            'type'	=>	'text',
		            'label'	=>	'Contact Person',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'no_hp',
		            'type'	=>	'text',
		            'label'	=>	'Nomor Handphone',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'id',
		            'type'	=>  'hidden',
	         	)
	         )
		);
	}
	
	public function index($id=''){
			$data = $this->am->get_data_administrasi($id);
			$this->load->view('vendor/admin/view/Identitas_pemohon/list', $data, FALSE);
			$this->load->view('vendor/admin/view/Identitas_pemohon/list_js', $data, FALSE);
	}

	public function view($id){

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->get_data_administrasi($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }
		echo json_encode($this->form);

	 }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
class SPPTS5 extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Legalitas_perusahaan';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('vendor/Permohonan_sertifikasi_model','psm');
	}
	
	public function index($id='', $process=false)
	{
		$data['permohonan'] = $this->psm->getPermohonan($user['id_user']);		
		$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'ap_file');
 		$data['vendor_status'] = $this->am->getVendorStatus($id);
		$data['id'] = $id;		
	 	$this->header = 'List Legalitas Perusahaan';
		$this->load->view('vendor/TabLegalitas_perusahaan/view_SPPTS5', $data, FALSE);
		$this->load->view('vendor/TabLegalitas_perusahaan/view_SPPTS5_js', $data, FALSE);
		
	}

	public function AP($id)
	{
			$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'ap_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/Akta_perusahaan/list', $data, FALSE);
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/Akta_perusahaan/list_js', $data, FALSE);	
	}

	public function NIB($id)
	{
			$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'nib_file');
			$data['id'] = $id;
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/NIB/list', $data, FALSE);
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/NIB/list_js', $data, FALSE);	
	}

	public function SKDU($id)
	{
			$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'skdu_file');
			$data['id'] = $id;
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/SKDU/list', $data, FALSE);
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/SKDU/list_js', $data, FALSE);	
	}

	public function IUI($id)
	{
			$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'iui_file');
			$data['id'] = $id;
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/IUI/list', $data, FALSE);
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/IUI/list_js', $data, FALSE);	
	}

	public function SIUP($id)
	{
			$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'siup_file');
			$data['id'] = $id;
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/SIUP/list', $data, FALSE);
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/SIUP/list_js', $data, FALSE);	
	}

	public function TDP($id)
	{
			$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'tdp_file');
			$data['id'] = $id;
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/TDP/list', $data, FALSE);
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/TDP/list_js', $data, FALSE);	
	}

	public function KTP($id)
	{
			$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'ktp_file');
			$data['id'] = $id;
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/KTP/list', $data, FALSE);
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/KTP/list_js', $data, FALSE);	
	}

	public function API($id)
	{
			$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'api_file');
			$data['id'] = $id;
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/API/list', $data, FALSE);
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/API/list_js', $data, FALSE);	
	}

	public function NIK($id)
	{
			$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'nik_file');
			$data['id'] = $id;
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/NIK/list', $data, FALSE);
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/NIK/list_js', $data, FALSE);	
	}

	public function HAKI($id)
	{
			$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'haki_file');
			$data['id'] = $id;
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/HAKI/list', $data, FALSE);
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/HAKI/list_js', $data, FALSE);	
	}

	public function LP($id)
	{
			$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'lp_file');
			$data['id'] = $id;
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/LP/list', $data, FALSE);
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/LP/list_js', $data, FALSE);	
	}

	public function SO($id)
	{
			$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'so_file');
			$data['id'] = $id;
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/SO/list', $data, FALSE);
			$this->load->view('vendor/TabLegalitas_perusahaan/SPPTS5/SO/list_js', $data, FALSE);	
	}
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Identitas_pabrik extends MY_Controller {

	public $form;

	public $modelAlias = 'ipmo';

	public $alias = 'ms_identitas_pabrik';

	public $module = 'Identitas_pabrik';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/Identitas_pabrik_model','ipmo');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(	         	
	         	 array(
		            'field'	=> 	'nama_pabrik',
		            'type'	=>	'text',
		            'label'	=>	'Nama Pabrik'
	         	),
	         	 array(
		            'field'	=> 	'alamat',
		            'type'	=>	'textarea',
		            'label'	=>	'Alamat'
	         	),
	         	 array(
		            'field'	=> 	'kota',
		            'type'	=>	'text',
		            'label'	=>	'Kota'
	         	),
	         	 array(
		            'field'	=> 	'provinsi',
		            'type'	=>	'text',
		            'label'	=>	'Provinsi'
	         	),
	         	 array(
		            'field'	=> 	'kode_pos',
		            'type'	=>	'number',
		            'label'	=>	'Kode Pos'
	         	),
	         	 array(
		            'field'	=> 	'telepon',
		            'type'	=>	'number',
		            'label'	=>	'Telepon'
	         	)
	        )
		);

		// $this->form_filter = array( 
		// 	  'filter'=>array( 
		// 	  	array( 
		// 			'field'	=> 	'a|Identitas_pabrikname',
		// 			'type'	=>	'text', 
		// 			'label'	=>	'Identitas_pabrikname'
		// 			)				  
		// 		) 

		// 	);		
		$this->getData = $this->ipmo->getData($this->form);
		$this->insertUrl = site_url('vendor/Identitas_pabrik/save/');
		$this->updateUrl = 'vendor/Identitas_pabrik/update';
		$this->deleteUrl = 'vendor/Identitas_pabrik/delete/';
		$this->prosesUrl = 'vendor/Identitas_pabrik/proses/';
	}
	
	public function index($id=''){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/Identitas_pabrik'),
			'title' => 'Identitas Pabrik'
		));
		$this->header = 'Identitas Pabrik';
		$this->content = $this->load->view('vendor/Identitas_pabrik/list',$data, TRUE);
		$this->script = $this->load->view('vendor/Identitas_pabrik/list_js', $data, TRUE);
		parent::index();
	}
	public function edit($id = null)
	{
		// $this->form['form'][1]['caption'] = "*Biarkan kosong jika tidak ingin diganti";
		parent::edit($id); 
	}
	public function insert()
	{
		$this->form['url'] = site_url('vendor/Identitas_pabrik/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();		
			if ($this->$modelAlias->insert($save)) {
				$this->deleteTemp($save);
			}	
		}
	}

	public function update($id)
	{
		$modelAlias = $this->ipmo;
		if ($this->validation()) {
			$save = $this->input->post();

		$save['jenis_perusahaan'] 		= $save['jenis_perusahaan'];
		$save['nama_perusahaan'] 		= $save['nama_perusahaan'];
		$save['alamat'] 				= $save['alamat'];
		$save['kota'] 					= $save['kota'];
		$save['provinsi'] 				= $save['provinsi'];
		$save['kode_pos'] 				= $save['kode_pos'];
		$save['telepon'] 				= $save['telepon'];
		$save['fax'] 					= $save['fax'];
		$save['email'] 					= $save['email'];
		$save['nama_penanggung_jawab'] 	= $save['nama_penanggung_jawab'];
		$save['jabatan'] 				= $save['jabatan'];
		$save['contact_person'] 		= $save['contact_person'];
		$save['no_hp'] 					= $save['no_hp'];
		$save['edit'] 			= date('Y-m-d H:i:s');

			$lastData = $this->ipmo->selectData($id);
			if ($this->ipmo->update($id, $save)) {
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function delete($id) {
		
		if ($this->ipmo->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('vendor/Identitas_pabrik/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function proses($id){

	}
	function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form_filter['filter'];
		echo json_encode($return);
	}
}

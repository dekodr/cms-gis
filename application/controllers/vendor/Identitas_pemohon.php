<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Identitas_pemohon extends MY_Controller {

	public $form;

	public $modelAlias = 'ipm';

	public $alias = 'ms_identitas_pemohon';

	public $module = 'Identitas_pemohon';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/Identitas_pemohon_model','ipm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');
		$this->form = array(
			'form'=>array(
				'left'=>array(
	         	array(
		            'field'	=> 	'jenis_perusahaan',
		            'type'	=>	'dropdown',
		            'label'	=>	'Jenis Perusahaan',
		            'source'=> array(
			            			'Local'  => 'Local',
			            			'Import'  => 'Import',
			            ),
			        'rules' =>	'required'
		        ),
	         	 array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Perusahaan',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'alamat',
		            'type'	=>	'textarea',
		            'label'	=>	'Alamat',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'kota',
		            'type'	=>	'text',
		            'label'	=>	'Kota',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'provinsi',
		            'type'	=>	'text',
		            'label'	=>	'Provinsi',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'kode_pos',
		            'type'	=>	'number',
		            'label'	=>	'Kode Pos',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'telepon',
		            'type'	=>	'number',
		            'label'	=>	'Telepon',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'fax',
		            'type'	=>	'text',
		            'label'	=>	'Fax',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'email',
		            'type'	=>	'text',
		            'label'	=>	'E - Mail',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'nama_penanggung_jawab',
		            'type'	=>	'text',
		            'label'	=>	'Nama Penangggung Jawab',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'jabatan',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'contact_person',
		            'type'	=>	'text',
		            'label'	=>	'Contact Person',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'no_hp',
		            'type'	=>	'number',
		            'label'	=>	'Nomot Handphone',
		            'rules' =>	'required'
	         	)
	         )
			)
		);		
		$this->form_edit = array( 
			  'form'=>array( 
			  	array(
		            'field'	=> 	'jenis_perusahaan',
		            'type'	=>	'dropdown',
		            'label'	=>	'Jenis Perusahaan',
		            'source'=> array(
			            			'Local'  => 'Local',
			            			'Import'  => 'Import',
			            ),
			        'rules' =>	'required'
		        ),
	         	 array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Perusahaan',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'alamat',
		            'type'	=>	'textarea',
		            'label'	=>	'Alamat',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'kota',
		            'type'	=>	'text',
		            'label'	=>	'Kota',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'provinsi',
		            'type'	=>	'text',
		            'label'	=>	'Provinsi',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'kode_pos',
		            'type'	=>	'number',
		            'label'	=>	'Kode Pos',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'telepon',
		            'type'	=>	'number',
		            'label'	=>	'Telepon',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'fax',
		            'type'	=>	'text',
		            'label'	=>	'Fax',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'email',
		            'type'	=>	'text',
		            'label'	=>	'E - Mail',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'nama_penanggung_jawab',
		            'type'	=>	'text',
		            'label'	=>	'Nama Penangggung Jawab',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'jabatan',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'contact_person',
		            'type'	=>	'text',
		            'label'	=>	'Contact Person',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'no_hp',
		            'type'	=>	'number',
		            'label'	=>	'Nomot Handphone',
		            'rules' =>	'required'
	         	)
	         )
		);		
		$this->getData = $this->ipm->getData($this->form);
		$this->insertUrl = site_url('vendor/Identitas_pemohon/save/');
		$this->updateUrl = 'vendor/Identitas_pemohon/update';
		$this->deleteUrl = 'vendor/Identitas_pemohon/delete/';
		$this->prosesUrl = 'vendor/Identitas_pemohon/proses/';
	}
	
	public function index($id=''){
		$user = $this->session->userdata('user');
		// print_r($user);die;
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/Identitas_pemohon'),
			'title' => 'Identitas Pemohon'
		));
		// $getHasil = $ip->row_array();
		$data['id'] = $id;
		$ip = $this->ipm->getIdentitasPemohon($user['id_user'])->row_array();
		$this->header = 'Identitas Pemohon';

	if (!empty($ip['name'] == NULL)) {
		$this->content = $this->load->view('vendor/Identitas_pemohon/view',$data, TRUE);
		$this->script = $this->load->view('vendor/Identitas_pemohon/view_js', $data, TRUE);
	}elseif (!empty($ip['name'] != NULL)) {
		$this->content = $this->load->view('vendor/Identitas_pemohon/list',$data, TRUE);
		$this->script = $this->load->view('vendor/Identitas_pemohon/list_js', $data, TRUE);
	}
		parent::index();
	
	}
	
	public function insert()
	{
		$this->form['url'] = site_url('vendor/Identitas_pemohon/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			if ($this->$modelAlias->insert($save)) {
				$this->deleteTemp($save);
			}	
		}
	}
	public function edit($id = null)
	{
		$this->form = $this->form_edit;
		$data = $this->ipm->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}
		$this->form['url'] = site_url('vendor/Identitas_pemohon/update/'. $id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Ubah',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form); 
	}
	public function update($id)
	{
		$modelAlias = $this->ipm;
		if ($this->validation()) {
			$save = $this->input->post();
			$lastData = $this->ipm->selectData($id);
			if ($this->ipm->update($id, $save)) {
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function delete($id) {
		
		if ($this->ipm->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('vendor/Identitas_pemohon/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function proses($id){

	}
	function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form_filter['filter'];
		echo json_encode($return);
	}
	public function lihat_data($id = null)
	{	
		$this->form = array( 
			  'form'=>array( 
			  	 array(
		            'field'	=> 	'nama_penanggung_jawab',
		            'type'	=>	'text',
		            'label'	=>	'Nama Penangggung Jawab',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'jabatan',
		            'type'	=>	'text',
		            'label'	=>	'Jabatan',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'contact_person',
		            'type'	=>	'number',
		            'label'	=>	'Contact Person',
		            'rules' =>	'required'
	         	),
	         	 array(
		            'field'	=> 	'no_hp',
		            'type'	=>	'number',
		            'label'	=>	'Nomot Handphone',
		            'rules' =>	'required'
	         	)
	         )
		);	
		{
		$modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->selectData($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if($this->form['form'][$key]['type']=='date_range'){
		 		$_value = array();

		 		foreach ($this->form['form'][$key]['field'] as $keys => $values) {
		 			$_value[] = $data[$values];

		 		}
		 		$this->form['form'][$key]['value'] = $_value;
		 	}
		 }
		echo json_encode($this->form);
	 }
	}

	public function confirmation(){
		$data = array(
			'header' => 'Confirmation',
			'content' => $this->load->view('vendor/Identitas_pemohon/confirmation', null, TRUE),
			// 'script' => $this->load->view('vendor_regis/view_js', null, TRUE)
		);
		$this->parser->parse('template/blank', $data);
    }
}

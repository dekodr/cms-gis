<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Legalitas_perusahaan extends MY_Controller {

	public $form;

	// public $modelAlias = 'pm';

	// public $alias = 'ms_jemaat';

	public $module = 'Legalitas Perusahaan';

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();
			$this->load->model('vendor/Permohonan_sertifikasi_model','psm');
			// $this->load->model('Proses_model','pm');
			// $this->load->model('get_model','gm');
			// $this->load->model('tabs/jemaat_model','um');

			$admin = $this->session->userdata('admin');
	}
	
    public function index($id=''){
	 	$admin = $this->session->userdata('admin');
	 	$user = $this->session->userdata('user');
	 	$this->id_client = $id;

	 	$this->breadcrumb->addlevel(1, array(
	 		'url' => site_url('vendor/Legalitas_perusahaan'),
	 		'title' => 'List Legalitas Perusahaan'
	 	));

	 	$data['id'] = $id;
	 	$data['s1'] = $this->psm->getPermohonan($user['id_user'])->result_array();
	 	$this->header = 'List Legalitas Perusahaan';
	 	$this->content = $this->load->view('vendor/TabLegalitas_perusahaan/view_tab',$data, TRUE);
	 	$this->script = $this->load->view('vendor/TabLegalitas_perusahaan/view_tab_js', $data, TRUE);
	 	parent::index();

	 }
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Penerbitan_sertifikat_vendor extends MY_Controller {

	public $form;

	public $modelAlias = 'psvm';

	// public $alias = 'ms_Penerbitan_sertifikat_vendor';

	public $module = 'Penerbitan_sertifikat_vendor';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('vendor/Penerbitan_sertifikat_vendor_model','psvm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'Penerbitan_sertifikat_vendor_file',
		            'type'	=>	'file',
		            'label'	=>	'Jadwal Audit',
		            'upload_path'=>base_url('assets/lampiran/Penerbitan_sertifikat_vendor_file/'),
					'upload_url'=>site_url('vendor/Penerbitan_sertifikat_vendor/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	)
	         )
		);		
		$this->getData = $this->psvm->getData($this->form);
		$this->insertUrl = site_url('vendor/Penerbitan_sertifikat_vendor/save/');
		$this->updateUrl = 'vendor/Penerbitan_sertifikat_vendor/update';
		$this->deleteUrl = 'vendor/Penerbitan_sertifikat_vendor/delete/';
		$this->prosesUrl = 'vendor/Penerbitan_sertifikat_vendor/proses/';
	}
	
	public function index($id=''){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('vendor/Penerbitan_sertifikat_vendor'),
			'title' => 'Penerbitan Sertifikat'
		));
		$this->header = 'Penerbitan Sertifikat';
		$this->content = $this->load->view('vendor/Penerbitan_sertifikat_vendor/list',$data, TRUE);
		$this->script = $this->load->view('vendor/Penerbitan_sertifikat_vendor/list_js', $data, TRUE);
		parent::index();
	}
	public function edit($id = null)
	{
		// $this->form['form'][1]['caption'] = "*Biarkan kosong jika tidak ingin diganti";
		parent::edit($id); 
	}
	public function insert()
	{
		$this->form['url'] = site_url('vendor/Penerbitan_sertifikat_vendor/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();		
			if ($this->$modelAlias->insert($save)) {
				$this->deleteTemp($save);
			}	
		}
	}

	public function update($id)
	{
		$modelAlias = $this->psvm;
		if ($this->validation()) {
			$save = $this->input->post();

		$save['jenis_perusahaan'] 		= $save['jenis_perusahaan'];
		$save['nama_perusahaan'] 		= $save['nama_perusahaan'];
		$save['alamat'] 				= $save['alamat'];
		$save['kota'] 					= $save['kota'];
		$save['provinsi'] 				= $save['provinsi'];
		$save['kode_pos'] 				= $save['kode_pos'];
		$save['telepon'] 				= $save['telepon'];
		$save['fax'] 					= $save['fax'];
		$save['email'] 					= $save['email'];
		$save['nama_penanggung_jawab'] 	= $save['nama_penanggung_jawab'];
		$save['jabatan'] 				= $save['jabatan'];
		$save['contact_person'] 		= $save['contact_person'];
		$save['no_hp'] 					= $save['no_hp'];
		$save['edit'] 					= date('Y-m-d H:i:s');

			$lastData = $this->psvm->selectData($id);
			if ($this->psvm->update($id, $save)) {
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function delete($id) {
		
		if ($this->psvm->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('vendor/Penerbitan_sertifikat_vendor/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function proses($id){

	}
	function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form_filter['filter'];
		echo json_encode($return);
	}

	public function approve_draft($id) {
		
		if ($this->psvm->approve($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function approve($id)
	{
		$this->formDelete['url'] = site_url('vendor/Penerbitan_sertifikat_vendor/approve_draft/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function reject_draft($id) {
		
		if ($this->psvm->reject($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function reject($id)
	{
		$this->formReject = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'reason',
		            'type'	=>	'text',
		            'rules' => 	'required'
	         	),
	         )
		);		
		$this->formReject['url'] = site_url('vendor/Penerbitan_sertifikat_vendor/reject_draft/' . $id);
		$this->formReject['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Reject'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formReject);
	}
}

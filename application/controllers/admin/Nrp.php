<?php 
/**
 * 
 */
class Nrp extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/nrp_model','nm');
		$this->getData = $this->nm->getData($this->form);
	}

	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('admin/nrp'),
			'title' => 'NRP/NPB'
		));
		$this->header = 'NRP/NPB';
		$this->content = $this->load->view('admin/nrp/list',$data, TRUE);
		$this->script = $this->load->view('admin/nrp/list_js', $data, TRUE);
		parent::index();
	}
}
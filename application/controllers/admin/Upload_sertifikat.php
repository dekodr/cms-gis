<?php defined('BASEPATH') OR exit('No direct script access allowed');
class upload_sertifikat extends MY_Controller {

	public $form;

	public $modelAlias = 'usm';

	public $alias = 'ms_upload_sertifikat';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('admin/upload_sertifikat_model','usm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'upload_sertifikat_draft_file',
		            'type'	=>	'file',
		            'label'	=>	'Sertifikat Draft',
		            'upload_path'=>base_url('assets/lampiran/upload_sertifikat_draft_file/'),
					'upload_url'=>site_url('admin/Penerbitan_sertifikat/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	),
	         	array(
		            'field'	=> 	'upload_sertifikat_file',
		            'type'	=>	'file',
		            'label'	=>	'Sertifikat Asli',
		            'upload_path'=>base_url('assets/lampiran/upload_sertifikat_file/'),
					'upload_url'=>site_url('admin/Penerbitan_sertifikat/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	)
	         )
		);
		$this->getData = $this->usm->getData($this->form);
		$this->insertUrl = site_url('admin/upload_sertifikat/save/');
		$this->updateUrl = 'admin/upload_sertifikat/update';
		$this->deleteUrl = 'admin/upload_sertifikat/delete/';
	}
	
	public function index($id){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('admin/Penerbitan_sertifikat'),
			'title' => 'Daftar Perusahaan'
		));
		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('admin/upload_sertifikat'),
			'title' => 'Daftar Perusahaan'
		));
		$this->header = 'Daftar Perusahaan';
		$this->content = $this->load->view('admin/Penerbitan_sertifikat/view',$data, TRUE);
		$this->script = $this->load->view('admin/Penerbitan_sertifikat/view_js',$data, TRUE);
		parent::index();
	}

	public function edit($id = null)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . $id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('msg', $this->successMessage);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
	public function insert()
	{
		$this->form['url'] = site_url($this->insertUrl . '/' . $id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
		
			if ($this->$modelAlias->insert($save)) {
				$this->deleteTemp($save);
			}	
		}
	}
}

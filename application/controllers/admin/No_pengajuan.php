<?php defined('BASEPATH') OR exit('No direct script access allowed');
class No_pengajuan extends MY_Controller {

	public $form;

	public $modelAlias = 'npm';

	public $alias = 'ms_no_pengajuan';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('admin/no_pengajuan_model','npm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'no_pengajuan_file',
		            'type'	=>	'file',
		            'label'	=>	'No Pengajuan',
		            'upload_path'=>base_url('assets/lampiran/no_pengajuan_file/'),
					'upload_url'=>site_url('admin/no_pengajuan/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	)
	         )
		);
		$this->getData = $this->npm->getData($this->form);
		$this->insertUrl = site_url('admin/no_pengajuan/save/');
		$this->updateUrl = 'admin/no_pengajuan/update';
		$this->deleteUrl = 'admin/no_pengajuan/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('admin/no_pengajuan'),
			'title' => 'No Pengajuan'
		));
		$this->header = 'No Pengajuan';
		$this->content = $this->load->view('admin/no_pengajuan/list',$data, TRUE);
		$this->script = $this->load->view('admin/no_pengajuan/list_js', $data, TRUE);
		parent::index();
	}

	public function edit($id = null)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . $id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('msg', $this->successMessage);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
}

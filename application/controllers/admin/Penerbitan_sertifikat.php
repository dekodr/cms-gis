<?php defined('BASEPATH') or exit('No direct script access allowed');
class Penerbitan_sertifikat extends MY_Controller
{

	public $form;

	public $modelAlias = 'psm';

	public $alias = 'ms_Penerbitan_sertifikat';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct()
	{

		parent::__construct();

		$this->load->model('admin/Penerbitan_sertifikat_model', 'psm');
		$this->load->model('get_model', 'gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form' => array(
				array(
					'field'	=> 	'Sertifikat_Draft_File',
					'type'	=>	'file',
					'label'	=>	'Sertifikat Draft',
					'upload_path' => base_url('assets/lampiran/Sertifikat_Draft_File/'),
					'upload_url' => site_url('admin/Penerbitan_sertifikat/upload_lampiran'),
					'allowed_types' => 'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
				),
				array(
					'field'	=> 	'Sertifikat_File',
					'type'	=>	'file',
					'label'	=>	'Sertifikat Asli',
					'upload_path' => base_url('assets/lampiran/Sertifikat_File/'),
					'upload_url' => site_url('admin/Penerbitan_sertifikat/upload_lampiran'),
					'allowed_types' => 'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
				)
			)
		);
		$this->getData = $this->psm->getData($this->form);
		$this->insertUrl = site_url('admin/Penerbitan_sertifikat/save/');
		$this->updateUrl = 'admin/Penerbitan_sertifikat/update';
		$this->deleteUrl = 'admin/Penerbitan_sertifikat/delete/';
	}

	public function index($id = "")
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('admin/Penerbitan_sertifikat'),
			'title' => 'Daftar Perusahaan'
		));
		$this->header = 'Daftar Perusahaan';
		$this->content = $this->load->view('admin/Penerbitan_sertifikat/list', $data, TRUE);
		$this->script = $this->load->view('admin/Penerbitan_sertifikat/list_js', $data, TRUE);
		parent::index();
	}

	public function edit($id = null)
	{
		parent::edit($id);
	}

	public function update($id)
	{
		$modelAlias = $this->psm;
		// if ($this->validation()) {
		$save = $this->input->post();
		$save['upload_sertifikat_file']       = $save['upload_sertifikat_file'];
		$save['upload_sertifikat_draft_file'] = $save['upload_sertifikat_draft_file'];
		$save['del']                          = 0;
		$save['edit_stamp'] 				  = timestamp();
		$lastData = $this->psm->selectData($id);
		if ($this->psm->update($id, $save)) {
			$this->session->set_userdata('msg', $this->successMessage);
			$this->deleteTemp($save, $lastData);
			echo json_encode(array('status' => 'success'));
			// return true;
		}
		// }
	}

	public function insert()
	{
		$this->form['url'] = site_url($this->insertUrl . '/' . $id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			),
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function save($data = null)
	{
		$modelAlias = $this->psm;
		if ($this->validation()) {
			$save = $this->input->post();

			if ($this->psm->insert($save)) {
				$this->deleteTemp($save);
			}
		}
	}

	public function Upload_sertifikat($id)
	{
		$data['id'] = $id;
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('admin/Penerbitan_sertifikat'),
			'title' => 'Daftar Perusahaan'
		));
		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('admin/Penerbitan_sertifikat/upload_sertifikat/' . $id),
			'title' => 'Daftar Sertifikat'
		));
		$this->header = 'Daftar Sertifikat';
		$this->content = $this->load->view('admin/Penerbitan_sertifikat/view', $data, TRUE);
		$this->script = $this->load->view('admin/Penerbitan_sertifikat/view_js', $data, TRUE);
		parent::index();
	}

	public function getDataSertifikat($id)
	{
		$config['query'] = $this->psm->get_data_sertifikat($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function upload_sertifikat_asli($id = null)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach ($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if ($this->form['form'][$key]['type'] == 'date_range') {
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];
				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url('admin/Penerbitan_sertifikat/upload_sertifikat_last/' . $id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			),
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function upload_sertifikat_last($id)
	{
		$modelAlias = $this->psm;
		// if ($this->validation()) {
		$save = $this->input->post();
		$save['upload_sertifikat_file']       = $save['upload_sertifikat_file'];
		$save['upload_sertifikat_draft_file'] = $save['upload_sertifikat_draft_file'];
		$save['del']                          = 0;
		$save['edit_stamp'] 				  = timestamp();
		$lastData = $this->psm->selectData($id);
		if ($this->psm->upload_sertifikat_asli($id, $save)) {
			$this->session->set_userdata('msg', $this->successMessage);
			$this->deleteTemp($save, $lastData);
			echo json_encode(array('status' => 'success'));
			// return true;
		}
		// }
	}
}

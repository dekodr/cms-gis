<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Jenis_pengajuan extends MY_Controller {

	public $form;

	public $modelAlias = 'jpm';

	public $alias = 'ms_jenis_permohonan';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('admin/jenis_pengajuan_model','jpm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'jenis',
		            'type'	=>	'text',
		            'label'	=>	'Jenis Permohonan',
		            'rules' => 	'required'
	         	)
	         )
		);
		$this->getData = $this->jpm->getData($this->form);
		$this->insertUrl = site_url('admin/jenis_pengajuan/save/');
		$this->updateUrl = 'admin/jenis_pengajuan/update';
		$this->deleteUrl = 'admin/jenis_pengajuan/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('admin/jenis_pengajuan'),
			'title' => 'Jenis Permohonan'
		));
		$this->header = 'Jenis Permohonan';
		$this->content = $this->load->view('admin/jenis_pengajuan/list',$data, TRUE);
		$this->script = $this->load->view('admin/jenis_pengajuan/list_js', $data, TRUE);
		parent::index();
	}

	public function edit($id = null)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . $id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('msg', $this->successMessage);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
}

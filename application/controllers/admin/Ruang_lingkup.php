<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Ruang_lingkup extends MY_Controller {

	public $form;

	public $modelAlias = 'rlm';

	public $alias = 'ms_lingkup_pemohon';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('admin/ruang_lingkup_model','rlm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'jenis_permohonan_id',
		            'type'	=>	'dropdown',
		            'label'	=>	'Ruang Lingkup',
		            'source' => $this->rlm->getRuangLingkup(),
		            'rules' => 	'required'
	         	)
	         )
		);
		// $this->getData = $this->rlm->getData($this->form);
		$this->insertUrl = site_url('admin/ruang_lingkup/save/');
		$this->updateUrl = 'admin/ruang_lingkup/update';
		$this->deleteUrl = 'admin/ruang_lingkup/delete/';
	}
	
	public function index($id_rl){
		$data['id_rl'] = $id_rl;
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('admin/ruang_lingkup'),
			'title' => 'Ruang Lingkup'
		));
		$this->header = 'Ruang Lingkup';
		$this->content = $this->load->view('admin/ruang_lingkup/list',$data, TRUE);
		$this->script = $this->load->view('admin/ruang_lingkup/list_js', $data, TRUE);
		parent::index();
	}

	public function getData($id = null)
	{
		$config['query'] = $this->rlm->getData($this->form,$id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function insert($id_rl)
	{
		if ($id_rl == 1) {
			$this->form['form'][] = array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Lingkup Pemohon',
		            'rules' => 	'required'
			);
		} else {
			$this->form['form'][] = array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Sistem Sertifikasi',
		            'rules' => 	'required'
			);
		}
		$this->form['url'] = $this->insertUrl .'/'. $id_rl;
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function save($id_rl)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();

			if ($id_rl == 1) {
				$save['is_lingkup_pemohon']	= 1;
			}else{
				$save['is_sistem_sertifikasi']	= 1;
			}

			$save['entry_stamp'] = timestamp();
			if ($this->$modelAlias->insert($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}

	public function edit($id = null,$id_rl)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		if ($id_rl == 1) {
			$this->form['form'][] = array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Lingkup Pemohon',
		            'rules' => 	'required'
			);
		} else {
			$this->form['form'][] = array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Sistem Sertifikasi',
		            'rules' => 	'required'
			);
		}

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . $id . '/' . $id_rl);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id,$id_rl)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();

			if ($id_rl == 1) {
				$save['is_lingkup_pemohon']	= 1;
			}else{
				$save['is_sistem_sertifikasi']	= 1;
			}			
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('msg', $this->successMessage);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
}

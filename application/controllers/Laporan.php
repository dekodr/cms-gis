<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Laporan extends MY_Controller {



	public $form;

	public $module = 'Laporan';

	public $isClientMenu = true;
	public $field = array(
		array(
			"header"=>"PROSES PENGADAAN",
			"field"=>array(
				"user"=>"Pengguna Barang/Jasa",
				"id_mekanisme"=>"Jenis Pengadaan",
				"issue_date"=>"Tanggal Permintaan/Revisi",
				"status"=>"Status Pengadaan",
				"remark"=>"Keterangan",
				"idr_value"=>"Nilai Anggaran",
				"hps"=>"Nilai HPS",
				"jenis_anggaran"=>"Jenis Anggaran",
			)
		),
		array(
			"header"=>"PELAKSANAAN KONTRAK",
			"field"=>array(
				"vendor_name"=>"Penyedia Barang / Jasa / Konsultan",
				"nilai_kontrak"=>array(
					"title"=>"Nilai / Harga Kontrak + PPN",
					"field"=>array(
						"remark"=> "Ket.",
						"contract_price"=>"Rupiah",
						"contract_price_kurs"=>"USD"
					)
				),
				"tanggal_kontrak"=>"Tanggal Kontrak / Amandemen",
				"no_kontrak"=>"Nomor Kontrak / Amandemen",
				"tanggal_kerja"=>"Tanggal Mulai Kerja",
				"tanggal_penyelesaian"=>array(
					"title"=>"Waktu Penyelesaian Pekerjaan",
					"field"=>array(
						array(
							"title"=>"Kontrak / Amandemen",
							"field"=>array(
								"durasi"=>"Durasi",
								"work_end"=>"Tanggal Batas Akhir"
							)
						),
						"realisasi"=>"Tanggal Realisasi",
						"status"=>"Status Pekerjaan"
					)
				),
				"tanggal_pekerjaan"=>array(
					"title"=>"Proses Pembayaran",
					"field"=>array(
						"surat_permohonan"=>"No Surat Permohonan",
						"tanggal_permohonan"=>"Tanggal Surat Permohonan",
						"tanggal_dokumen"=>"Tanggal Terima Dokumen"
					)
				),
				"realisasi_pembayaran"=>array(
					"title"=>"Realisasi Pembayaran",
					"field"=>array(
						"termin"=>"Termijn",
						"%"=>"%",
						"idr_value_bap"=>"Rupiah",
						// "kurs_value_bap"=>"USD",
						"popay"=>"Nomor POPAY",
						"status_pembayaran"=>"Status Pembayaran",//Setelah tanggal dibayar di invoice terisi, maka status jadi paid, kalau belum on progress
						"realisasi_pembayaran_sub"=>"Tanggal Realisasi",//ambil dari field date_dibayar invoice
					)
				),"bap"=>array(
					"title"=>"Berita Acara Pemeriksaan (BAP)",
					"field"=>array(
						"nomor_bap"=>"Nomor",	
						"date_bap"=>"Tanggal"
					)
				),
				"bast"=>array(
					"title"=>"Berta Acara Serah Terima (BAST) ",
					"field"=>array(
						"nomor_bast"=>"Nomor",	
						"date_bast"=>"Tanggal"
					)
				),
			)
		),
		array(
			"field"=>array(
				"fp2b"=>array(
					"title"=>"FP2B to User",
					"field"=>array(
						"nomor_fp2b"=>"Nomor",	
						"date_fp2b"=>"Tanggal"
					)
				),
			)
		),
		array(
			"field"=>array(
				"evaluasi"=>array(
					"title"=>"Evaluasi Kinerja",
					"field"=>array(
						"nomor"=>"Nomor Sertifikat",	
						"date_evaluasi"=>"Tanggal",
						"score"=>"Nilai Kinerja"
					)
				)
			)
		),
		array(
			"field"=>array(
				"remark"=>"Keterangan"
			)
		),
		array(
			"field"=>array(
				"pic"=>"PIC"
			)
		)
	);
	public function __construct(){

		parent::__construct();

		$this->load->model('Baseline_kontrak_model','bkm');
		$this->load->model('Kontrak_model','km');
		$this->load->model('master/Pengguna_model','pm');
		$this->load->model('master/Pusat_biaya_model','pbm');
		$this->load->model('master/Pengguna_model','pm');

		$admin = $this->session->userdata('admin');
		$this->form = array(
				'form' => array(
					array(
						'type'	=>	'date_range',
						'label'	=>	'Periode',
						'name'	=>	'periode',
						'field'	=> 	array(
											'start',
											'end'
										),
					),
				),
				'successAlert'=>'Berhasil mengubah data!'
			);
	}
	
	
	public function index(){

		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('laporan'),
			'title' => 'Laporan Monitoring Kontrak'
		));
		$this->header = 'Laporan Monitoring Kontrak';
		$this->content = $this->load->view('laporan/view',$data, TRUE);
		$this->script = $this->load->view('laporan/view_js', $data, TRUE);
		parent::index();
	}
 	
 	public function getHeader(){
 		echo json_encode($this->field);
 	}

 	public function cetak(){
		$html = '';
		$no  = 1;
		$header = "";
		$mekanisme = array(1=>'Pembelian Langsung', 2=>'Penunjukan Langsung', 3=>'Penunjukan Langsung Kondisi Tertentu', 4=>'Pemilihan Langsung', 5=>'Pelelangan');
		$arrayField = array();
		
		foreach ($this->field as $_key => $_value) {
			foreach($_value['field'] as $sub_key => $sub_value){
				if($_POST['filter'][$sub_key]==1){
					$arrayField[$_key]['field'][$sub_key]=$sub_value;
				}
			}
			if(count($arrayField[$_key]['field'])>0){
				$arrayField[$_key]['header']=$_value['header'];
			}
		}
		$this->field = $arrayField;
		foreach($this->field as $key=>$row){
			$lv1 ="<td colspan={{colspan}}>".$row['header']."</td>"; //LEVEL 1
			$colspan = 0;
			
			foreach ($row['field'] as $keylv1 => $valuelv1) {
				$rowspan = 3;
				// $rowspan--;
				if(is_array($valuelv1)){
					$rowspan--;
					$colspan+=count($valuelv1['field']);
					
					$colspan2=0;

					foreach ($valuelv1['field'] as $keylv2 => $valuelv2) {

						if(is_array($valuelv2)){
							$colspan2+=count($valuelv2['field']);

							$lv3="<td rowspan={{rowspan}} colspan={{colspan2}}>".$valuelv2['title']."</td>";
							
							foreach ($valuelv2['field'] as $keylv3 => $valuelv3) {
								
								
								
								$_lv4.="<td >".$valuelv3."</td>";
								
							}
							$colspan+=1;
							$rs = 1;
						}else{
							$colspan2+=1;
							$lv3="<td rowspan={{rowspan}}>".$valuelv2."</td>";
							$rs = $rowspan;
						}
						
						// $_lv3 .= $lv3;
						$_lv3 .= str_replace(array('{{rowspan}}','{{colspan2}}'), array($rs, $colspan2), $lv3);
						
					}
					
					$lv2="<td rowspan=1 colspan={{colspan2}}>".$valuelv1['title']."</td>";
				}else{
					
					$lv2="<td rowspan={{rowspan}}>".$valuelv1."</td>";
					$colspan+=1;
				}

				
				
				
				// $lv2="<td rowspan={{rowspan}}>".$valuelv1."</td>";
				$_lv2 .= str_replace(array('{{rowspan}}','{{colspan2}}'), array($rowspan,$colspan2), $lv2);

			}

			$lv1 = str_replace('{{colspan}}', $colspan, $lv1);
			$_lv1 .= $lv1;
			$cs +=(($colspan==0) ? 1 : $colspan);

		}
		$data = '';
		
		foreach ($mekanisme as $keyMekanisme => $valueMekanisme) {
			$data .= "<tr ><td colspan =".($cs+2)."><b>".$valueMekanisme."</b></td></tr>";
			$kontrak = $this->km->getKontrak($keyMekanisme);
			// echo print_r($kontrak);
			$id = 1;
			$_data = '';
			
			foreach ($kontrak as $key => $value) {
				$more = array();
				$rowspan = count(max($value));
					$_data .= "<tr><td rowspan={{rowspan}}>".$id."</td><td rowspan={{rowspan}}>".$value['name']."</td>";

					foreach ($this->field as $keys => $values) {
						
						$_data .= $this->recursive($values, $value, $more);

					}
					$_data .="</tr>";
					// echo print_r($more);
					// $moreData = $fetchTable['more'];
					if(count($more)>0){
						foreach($more as $moreKey => $moreValue){
							$_data  .= "<tr>";
							foreach($moreValue as $more){
								$_data .= "<td>".$more."</td>";
							}
							$_data .="</tr>";
						}
						
					}
					$max_colspan = 1;
					$id++;
				
				$_data = str_replace('{{rowspan}}', $rowspan, $_data);
				
			}
			$data.=$_data;
		}

		$header .="	<tr>
						<td rowspan=4><b>No</b></td>
						<td rowspan=4><b>NAMA PAKET PENGADAAN</b></td>
						".$_lv1."
					</tr><tr>".$_lv2."</tr><tr>".$_lv3."</tr><tr>".$_lv4."</tr>";
		
		
		$html .= '<html>
				<head>
					
					<style type="text/css">

						@page{
							size: A4 portrait;
							page-break-after : always;
							margin : 10px;
						}
						
						@media all{
							ol{
								padding-left : 20px;
								padding-top : -15px;
								padding-bottom : -15px;
							}
							
							table { page-break-inside:avoid; }
						    tr    { page-break-inside: avoid; }
						    thead { display:table-header-group; }
					    }
					table{
						width: 100%;
					}
    				</style>
				</head>
				<body>
					<table>
						<tr>
							<td colspan="4" align="right">
								LAPORAN MONITORING PROGRESS DAN KONTRAK UNTUK PENGADAAN TAHUN '.date('Y').'
							</td>
						</tr>
						<tr>
							<td colspan="4" align="right">
								LOGISTIC AND FACILITY MANAGEMENT DIVISION
							</td>
						</tr>
						<tr>
							<td colspan="4" align="right">
								PERIODE '.date('Y-m-d', strtotime($_POST['date_from'])).' S.D '.date('Y-m-d', strtotime($_POST['date_to'])).'
							</td>
						</tr>
					</table>
					<br>
					<table  border=1>
						'.$header.'
						'.$data.'
					</table>
				</body>
				</html>
				';
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=Laporan.xls");
		echo $html;
	}
	function recursive($array,$values, &$more){
		$_return =  array();
		$return='';
		$is_first = 1;
		
		foreach($array['field'] as $key => $value){

			if(is_array($value)){
				$r = $this->recursive($value, $values, $more);
				$return.= $r;
			}else{

				if(is_array($values[$key]) && count($values[$key])>0){
					
					switch ($key) {
						case 'contract_price':
						case 'contract_price_kurs':
						case 'idr_value_bap':
							$return.="<td  >".number_format($values[$key][0])."</td>";
						break;
						case 'tanggal_kontrak':
						case 'tanggal_kerja':
						case 'work_end':
						case 'date_bap':
						case 'date_bast':

							$return.="<td >".$values[$key][0]."</td>";
						break;
						case 'no_kontrak':
						case 'durasi':
						case 'nomor_bap':
						case 'nomor_bast':
						default:
						// echo $values[$key][0];
							$return.="<td >".$values[$key][0]."</td>";
						break;
					}
					
					
					$otherData = $values[$key];

					unset($otherData[0]);
					if(count($values[$key])>0){
						foreach ($otherData as $_key => $_value) {
							switch ($key) {
								case 'contract_price':
								case 'contract_price_kurs':
								case 'idr_value_bap':
									$_value=number_format($_value);
								break;
								case 'tanggal_kontrak':
								case 'tanggal_kerja':
								case 'work_end':
								case 'date_bap':
								case 'date_bast':
									$_value=default_date($_value);
								break;
								case 'no_kontrak':
								case 'durasi':
								case 'nomor_bap':
								case 'nomor_bast':
								default:
								// echo $_value;
									$_value=$_value;
								break;
							}
							$more[$_key][] = $_value;
						}
					}
					

				}else{

					switch ($key) {
						case 'id_mekanisme':
							switch ($values[$key]) {
								case 1 : $mekanisme = 'Pembelian Langsung'; break;
								case 2 : $mekanisme = 'Penunjukan Langsung'; break;
								case 3 : $mekanisme = 'Penunjukan Langsung Kondisi Tertentu'; break;
								case 4 : $mekanisme = 'Pemilihan Langsung'; break;
								case 5 : $mekanisme = 'Pelelangan'; break;
								case 6 : $mekanisme = 'Penunjukkan Langsung Melalui Penugasan'; break;
								
							}
							$return.="<td  rowspan={{rowspan}}>".$mekanisme."</td>";
							break;
						// case 'hps':

						// 	// switch ($values[$key]) {
						// 	// 	case 1:
						// 	// 		$hps = 'Kontrak Lumpsum';
						// 	// 		break;
						// 	// 	case 2:
						// 	// 		$hps = 'Kontrak Harga Satuan';
						// 	// 		break;
						// 	// 	case 3:
						// 	// 		$hps = 'Kontrak Lumpsum & Harga Satuan';
						// 	// 		break;
						// 	// 	case 4:
						// 	// 		$hps = 'Kontrak EPC/C';
						// 	// 		break;
						// 	// 	case 5:
						// 	// 		$hps = 'Kontrak Payung';
						// 	// 		break;
						// 	// 	case 6:
						// 	// 		$hps = 'Kontrak Lainnya';
						// 	// 		break;
						// 	// }
						// 	// $return.="<td rowspan={{rowspan}}>".$hps."</td>";
						// 	break;
						case 'issue_date':
						case 'date_evaluasi':
							if($values[$key]!=''){
								$return.="<td rowspan={{rowspan}}>".default_date($values[$key])."</td>";
							}else{
								$return.="<td rowspan={{rowspan}}></td>";
							}
							
							break;
						case 'hps':

							$return.="<td rowspan={{rowspan}}>".number_format($values[$key])."</td>";
							break;

						default:
							$return.="<td rowspan={{rowspan}}>".$values[$key]."</td>";
							break;
					}
				}
			}
		}

		return $return;
	}
}

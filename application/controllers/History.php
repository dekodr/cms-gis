<?php

/**
 * 
 */
class History extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('History_model','hm');
	}

	public function index($id_certificate)
	{
		$data = $this->session->userdata('user');
		$data['history'] = $this->hm->getData($id_certificate);
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('history/'.$id_certificate),
			'title' => 'History Sertifikat'
		));
		$this->header = 'History Sertifikat';
		$this->content = $this->load->view('history/list',$data, TRUE);
		// $this->script = $this->load->view('history/list_js', $data, TRUE);
		parent::index();
	}
}
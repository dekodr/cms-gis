<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Situ extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_situ';

	public $module = 'Situ';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'type',
		            'type'	=> 'dropdown',
		            'label'	=> 'Nama Surat',
		            'rules' => 'required',
		            'source'=> array(
		            	'Surat Keterangan Domisili Perusahaan (SKDP)' => 'Surat Keterangan Domisili Perusahaan (SKDP)',
		            	'Surat Izin Tempat Usaha (SITU)' => 'Surat Izin Tempat Usaha (SITU)',
		            	'Herregisterasi SKDP' => 'Herregisterasi SKDP' 
		            )
		        ),
	         	array(
		            'field'	=> 	'situ_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/situ_file/'),
					'upload_url'=>site_url('vendor/situ/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
	         )
		);
	}
	
	public function index($id, $process=false){
			$data['data'] = $this->am->get_data_situ($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/situ/list', $data, FALSE);
			$this->load->view('approval/approval_detail/situ/list_js', $data, FALSE);
		
	}

	public function view($id)
    {
        $config['query'] = $this->am->get_data_situ($id);
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
    }
    public function viewVerifikasi($id){
    	$data = $this->am->verifikasi_situ($id);
    	$data['id'] = $id;
    	$this->load->view('approval/approval_detail/situ/view', $data, FALSE);
		$this->load->view('approval/approval_detail/situ/view_js', $data, FALSE);
    }
	public function verifikasi($id){
		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_situ($id);
		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	// if ($this->form['form'][$key]['value'] == 'lifetime') {
		 	// 	$this->form['form'][$key]['value'] = 'Selama Perusahaan Berdiri';
		 	// }
		 }
		echo json_encode($this->form);

	}
	public function commit_approve($id){
		$data = $this->am->verifikasi_situ($id);
		$result = $this->data_process->check($data['id_vendor'], $this->input->post(),$data['id'],'ms_situ','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}
}

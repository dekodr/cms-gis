<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Legalitas_perusahaan extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Legalitas_perusahaan';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('vendor/Permohonan_sertifikasi_model','psm');
	}
	
	public function index($id, $process=false){
		 	$admin = $this->session->userdata('admin');
		 	$user = $this->session->userdata('user');
			$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'ap_file');
	 		$data['s1'] = $this->psm->getPermohonan($id)->result_array();
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Legalitas_perusahaan/view_LP', $data, FALSE);
			$this->load->view('approval/approval_detail/Legalitas_perusahaan/view_LP_js', $data, FALSE);
		
	}

	// public function AP($id)
	// {
	// 		$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'ap_file');
	// 		$data['id'] = $id;
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/ap/list_ap', $data, FALSE);
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/ap/list_ap_js', $data, FALSE);	
	// }

	// public function NIB($id)
	// {
	// 		$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'nib_file');
	// 		$data['id'] = $id;
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/nib/list_nib', $data, FALSE);
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/nib/list_nib_js', $data, FALSE);	
	// }

	// public function SKDU($id)
	// {
	// 		$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'skdu_file');
	// 		$data['id'] = $id;
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/skdu/list_skdu', $data, FALSE);
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/skdu/list_skdu_js', $data, FALSE);	
	// }

	// public function IUI($id)
	// {
	// 		$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'iui_file');
	// 		$data['id'] = $id;
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/iui/list_iui', $data, FALSE);
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/iui/list_iui_js', $data, FALSE);	
	// }

	// public function SIUP($id)
	// {
	// 		$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'siup_file');
	// 		$data['id'] = $id;
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/siup/list_siup', $data, FALSE);
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/siup/list_siup_js', $data, FALSE);	
	// }

	// public function TDP($id)
	// {
	// 		$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'tdp_file');
	// 		$data['id'] = $id;
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/tdp/list_tdp', $data, FALSE);
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/tdp/list_tdp_js', $data, FALSE);	
	// }

	// public function KTP($id)
	// {
	// 		$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'ktp_file');
	// 		$data['id'] = $id;
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/ktp/list_ktp', $data, FALSE);
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/ktp/list_ktp_js', $data, FALSE);	
	// }

	// public function API($id)
	// {
	// 		$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'api_file');
	// 		$data['id'] = $id;
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/api/list_api', $data, FALSE);
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/api/list_api_js', $data, FALSE);	
	// }

	// public function NIK($id)
	// {
	// 		$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'nik_file');
	// 		$data['id'] = $id;
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/nik/list_nik', $data, FALSE);
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/nik/list_nik_js', $data, FALSE);	
	// }

	// public function HAKI($id)
	// {
	// 		$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'haki_file');
	// 		$data['id'] = $id;
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/haki/list_haki', $data, FALSE);
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/haki/list_haki_js', $data, FALSE);	
	// }

	// public function LP($id)
	// {
	// 		$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'lp_file');
	// 		$data['id'] = $id;
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/lp/list_lp', $data, FALSE);
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/lp/list_lp_js', $data, FALSE);	
	// }

	// public function SO($id)
	// {
	// 		$data['data'] = $this->am->get_data_Legalitas_perusahaan($id, 'so_file');
	// 		$data['id'] = $id;
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/so/list_so', $data, FALSE);
	// 		$this->load->view('approval/approval_detail/Legalitas_perusahaan/so/list_so_js', $data, FALSE);	
	// }

	public function view($id = null,$type)
	{
		$config['query'] = $this->am->get_data_Legalitas_perusahaan($id, $type);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function viewVerifikasi($id,$folder,$type){
    	$data = $this->am->verifikasi_Legalitas_perusahaan($id,$type);
    	$data['id'] = $id;
    	$this->load->view('approval/approval_detail/Legalitas_perusahaan/'.$folder.'/view', $data, FALSE);
		$this->load->view('approval/approval_detail/Legalitas_perusahaan/'.$folder.'/view_js', $data, FALSE);
    }

    public function verifikasi($id,$type)
	{
		$this->form = array(
			'form' => array(
				array(
		            'field'	=> 	$type,
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/akta_file/'),
					'upload_url'=>site_url('vendor/akta/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
			)
		);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->verifikasi_Legalitas_perusahaan($id,$type);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['value'] == 'lifetime') {
		 		$this->form['form'][$key]['value'] = 'Seumur Hidup';
		 	}
		 }
		echo json_encode($this->form);
	}

	public function commit_approve($id,$satu,$type){
		$data = $this->am->verifikasi_Legalitas_perusahaan($id,$type);
		$result = $this->data_process->check($data['id_vendor'], $this->input->post(),$data['id'],'ms_legalitas','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}

	public function cekData($id){
    	$modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_Legalitas_perusahaan($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if($this->form['form'][$key]['type']=='date_range'){
		 		$_value = array();

		 		foreach ($this->form['form'][$key]['field'] as $keys => $values) {
		 			$_value[] = $data[$values];

		 		}
		 		$this->form['form'][$key]['value'] = $_value;
		 	}
		 }
		echo json_encode($this->form);
    }
}

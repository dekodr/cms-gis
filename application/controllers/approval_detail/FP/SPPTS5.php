<?php defined('BASEPATH') OR exit('No direct script access allowed');
class SPPTS5 extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Formulir_permohonan';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('vendor/permohonan_sertifikasi_model','psm');
	}
	
	public function index($id, $process=false){
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id,'sps_file');
			$data['permohonan'] = $this->psm->getPermohonan($id)->result_array();
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/view_tab', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/view_tab_js', $data, FALSE);
		
	}

	public function SPS($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'sps_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/sps/list_sps', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/sps/list_sps_js', $data, FALSE);	
	}

	public function SPM($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'spm_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/spm/list_spm', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/spm/list_spm_js', $data, FALSE);	
	}

	public function spjps($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'spjps_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/spjps/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/spjps/list_js', $data, FALSE);	
	}

	public function spl($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'spl_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/spl/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/spl/list_js', $data, FALSE);	
	}

	public function spks($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'spks_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/spks/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/spks/list_js', $data, FALSE);	
	}

	public function fa($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'fa_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/fa/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/fa/list_js', $data, FALSE);	
	}

	public function dp($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'dp_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/dp/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/dp/list_js', $data, FALSE);	
	}

	public function tps($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'tps_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/tps/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/tps/list_js', $data, FALSE);	
	}

	public function ip($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'ip_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/ip/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/ip/list_js', $data, FALSE);	
	}

	public function FMP($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'fmp_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/FMP/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/FMP/list_js', $data, FALSE);	
	}

	public function FPP2($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'fpp2_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/FPP2/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/FPP2/list_js', $data, FALSE);	
	}

	public function FMBB($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'fmbb_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/FMBB/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/FMBB/list_js', $data, FALSE);	
	}

	public function FPP1($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'fpp1_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/FPP1/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/FPP1/list_js', $data, FALSE);	
	}

	public function FPPI($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS5($id, 'fppi_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/FPPI/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/FPPI/list_js', $data, FALSE);	
	}

	public function view($id = null,$type)
	{
		$config['query'] = $this->am->get_data_Formulir_permohonanSPPTS5($id,$type);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function viewVerifikasi($id,$folder,$type){
    	$data = $this->am->verifikasi_Formulir_permohonan($id,$type);
    	$data['vendor_status'] = $this->am->getVendorStatus($id);
    	$data['id'] = $id;
    	$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/'.$folder.'/view', $data, FALSE);
		$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS5/'.$folder.'/view_js', $data, FALSE);
    }

    public function verifikasi($id,$type)
	{
		$this->form = array(
			'form' => array(
				array(
		            'field'	=> 	$type,
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/akta_file/'),
					'upload_url'=>site_url('vendor/akta/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
			)
		);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->verifikasi_Formulir_permohonan($id,$type);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['value'] == 'lifetime') {
		 		$this->form['form'][$key]['value'] = 'Seumur Hidup';
		 	}
		 }
		echo json_encode($this->form);
	}

	public function commit_approve($id,$satu,$type){
		$data = $this->am->verifikasi_Formulir_permohonan($id,$type);
		$result = $this->data_process->check($data['id_vendor'], $this->input->post(),$data['id'],'ms_formulir_permohonan','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}

	public function cekData($id){
    	$modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_Formulir_permohonan($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if($this->form['form'][$key]['type']=='date_range'){
		 		$_value = array();

		 		foreach ($this->form['form'][$key]['field'] as $keys => $values) {
		 			$_value[] = $data[$values];

		 		}
		 		$this->form['form'][$key]['value'] = $_value;
		 	}
		 }
		echo json_encode($this->form);
    }
}

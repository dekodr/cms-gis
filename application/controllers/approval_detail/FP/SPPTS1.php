<?php defined('BASEPATH') OR exit('No direct script access allowed');
class SPPTS1 extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Formulir_permohonan';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('vendor/permohonan_sertifikasi_model','psm');
	}
	
	public function index($id, $process=false){
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS1($id,'sps_file');
			$data['permohonan'] = $this->psm->getPermohonan($id)->result_array();
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/view_tab', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/view_tab_js', $data, FALSE);
		
	}

	public function SPS($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS1($id, 'sps_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/sps/list_sps', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/sps/list_sps_js', $data, FALSE);	
	}

	public function SPM($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS1($id, 'spm_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/spm/list_spm', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/spm/list_spm_js', $data, FALSE);	
	}

	public function spjps($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS1($id, 'spjps_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/spjps/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/spjps/list_js', $data, FALSE);	
	}

	public function spl($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS1($id, 'spl_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/spl/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/spl/list_js', $data, FALSE);	
	}

	public function spks($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS1($id, 'spks_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/spks/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/spks/list_js', $data, FALSE);	
	}

	public function fa($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS1($id, 'fa_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/fa/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/fa/list_js', $data, FALSE);	
	}

	public function dp($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS1($id, 'dp_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/dp/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/dp/list_js', $data, FALSE);	
	}

	public function tps($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS1($id, 'tps_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/tps/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/tps/list_js', $data, FALSE);	
	}

	public function ip($id)
	{
			$data['data'] = $this->am->get_data_Formulir_permohonanSPPTS1($id, 'ip_file');
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/ip/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/ip/list_js', $data, FALSE);	
	}

	public function view($id = null,$type)
	{
		$config['query'] = $this->am->get_data_Formulir_permohonanSPPTS1($id,$type);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function viewVerifikasi($id,$folder,$type){
    	$data = $this->am->verifikasi_Formulir_permohonan($id,$type);
    	$data['vendor_status'] = $this->am->getVendorStatus($id);
    	$data['id'] = $id;
    	$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/'.$folder.'/view', $data, FALSE);
		$this->load->view('approval/approval_detail/Formulir_permohonan/SPPTS1/'.$folder.'/view_js', $data, FALSE);
    }

    public function verifikasi($id,$type)
	{
		$this->form = array(
			'form' => array(
				array(
		            'field'	=> 	$type,
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/akta_file/'),
					'upload_url'=>site_url('vendor/akta/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
			)
		);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->verifikasi_Formulir_permohonan($id,$type);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['value'] == 'lifetime') {
		 		$this->form['form'][$key]['value'] = 'Seumur Hidup';
		 	}
		 }
		echo json_encode($this->form);
	}

	public function commit_approve($id,$satu,$type){
		$data = $this->am->verifikasi_Formulir_permohonan($id,$type);
		$result = $this->data_process->check($data['id_vendor'], $this->input->post(),$data['id'],'ms_formulir_permohonan','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}

	public function cekData($id){
    	$modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_Formulir_permohonan($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if($this->form['form'][$key]['type']=='date_range'){
		 		$_value = array();

		 		foreach ($this->form['form'][$key]['field'] as $keys => $values) {
		 			$_value[] = $data[$values];

		 		}
		 		$this->form['form'][$key]['value'] = $_value;
		 	}
		 }
		echo json_encode($this->form);
    }
}

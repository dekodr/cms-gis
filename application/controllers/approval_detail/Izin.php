<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Izin extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_ijin_usaha';

	public $module = 'Izin';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('vendor/izin_model','im');
	}
	
	public function index($id, $type){
		
			$data['data'] = $this->am->get_data_izin($id,$type);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/izin/view_tab', $data, FALSE);
			$this->load->view('approval/approval_detail/izin/view_tab_js', $data, FALSE);
		

	}

	public function siup($id, $type='siup')
	{
			$data['data'] = $this->am->get_data_izin($id, $type);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/izin/list', $data, FALSE);
			$this->load->view('approval/approval_detail/izin/list_js', $data, FALSE);
	}

	public function siul($id, $type='ijin_lain')
	{
			$data['data'] = $this->am->get_data_izin($id, $type);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/izin/list_siul', $data, FALSE);
			$this->load->view('approval/approval_detail/izin/list_siul_js', $data, FALSE);
	}

	public function asosiasi($id, $type='asosiasi')
	{
			$data['data'] = $this->am->get_data_izin($id,$type);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/izin/list_asosiasi', $data, FALSE);
			$this->load->view('approval/approval_detail/izin/list_asosiasi_js', $data, FALSE);
	}

	public function siujk($id, $type='siujk')
	{
			$data['data'] = $this->am->get_data_izin($id,$type);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/izin/list_siujk', $data, FALSE);
			$this->load->view('approval/approval_detail/izin/list_siujk_js', $data, FALSE);
	}

	public function sbu($id, $type='sbu')
	{
			$data['data'] = $this->am->get_data_izin($id,$type);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/izin/list_sbu', $data, FALSE);
			$this->load->view('approval/approval_detail/izin/list_sbu_js', $data, FALSE);
	}

	public function view($id = null, $type)
	{
		$config['query'] = $this->am->get_data_izin($id, $type);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function viewVerifikasi($id){
    	$data = $this->am->verifikasi_izin($id);
    	$data['id'] = $id;
    	$this->load->view('approval/approval_detail/izin/view', $data, FALSE);
		$this->load->view('approval/approval_detail/izin/view_js', $data, FALSE);
    }

	public function verifikasi($id)
	{
		//$data_ijin = $this->im->get_data_ijin($id);
			// 'id'=>$id,
			// 'type'=>$data_ijin['type'],

		$this->form = array(
			'form'=>array(
				array(
		            'field'	=> 	'type',
		            'type'	=>	'text',
		            'label'	=>	'Tipe',
	         	),
	         	array(
		            'field'	=> 	'izin_file',
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/izin_file/'),
					'upload_url'=>site_url('vendor/izin/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
	         ),
		);
		$modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_izin($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['field'] != 'mandatory' && $this->form['form'][$key]['field'] != 'status' ) {
		 		$this->form['form'][$key]['readonly'] = TRUE;
		 	}
		 	if ($this->form['form'][$key]['value'] == 'siup' && $this->form['form'][$key]['field'] == 'type') {
		 		$this->form['form'][$key]['value'] = 'SIUP';
		 	} else if ($this->form['form'][$key]['value'] == 'ijin_lain' && $this->form['form'][$key]['field'] == 'type') {
		 		$this->form['form'][$key]['value'] = 'Surat Izin/Lainnya';
		 	} else if ($this->form['form'][$key]['value'] == 'asosiasi' && $this->form['form'][$key]['field'] == 'type') {
		 		$this->form['form'][$key]['value'] = 'Sertifikasi Asosiasi/Lainnya';
		 	} else if ($this->form['form'][$key]['value'] == 'siujk' && $this->form['form'][$key]['field'] == 'type') {
		 		$this->form['form'][$key]['value'] = 'SIUJK';
		 	} else if ($this->form['form'][$key]['value'] == 'sbu' && $this->form['form'][$key]['field'] == 'type') {
		 		$this->form['form'][$key]['value'] = 'SBU';
		 	}
		 }
		echo json_encode($this->form);
	}

	public function commit_approve($id){
		$data = $this->am->verifikasi_izin($id);
		$result = $this->data_process->check($data['id_vendor'], $this->input->post(),$data['id'],'ms_ijin_usaha','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}

	public function getCekDataBsb($id){
		// $id_dpt = $this->im->get_data_ijin($id);
		echo json_encode($this->im->getCekDataBsb($id));
	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pembayaran extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Pembayaran';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
		       array(
		            'field'	=> 	'pembayaran_file',
		            'type'	=>	'file',
		            'label'	=>	'Bukti Pembayaran',
		            'upload_path'=>base_url('assets/lampiran/pembayaran/_file'),
					'upload_url'=>site_url('approval_detail/Pembayaran/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	),
	         	array(
		            'field'	=> 	'id',
		            'type'	=>  'hidden',
	         	)
	         )
		);
	}
	
	public function index($id, $process=false){
		// print_r($this->input->post()['vendor_type']);
		if($process==true){
			
				$data = $this->am->get_data_Pembayaran($id);
				$result = $this->data_process->check($id, $this->input->post(),$data['id'],'ms_pembayaran','id_vendor');
				// $this->am->update_ms_vendor_Pembayaran($id,$this->input->post());
				if($result){
					echo json_encode(array('status'=>'success'));
				}
				 else {
					echo json_encode(array('status'=>'fail'));
				}
			
		}else{
			$data = $this->am->get_data_Pembayaran($id);
			$data['id'] = $id;
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$this->load->view('approval/approval_detail/Pembayaran/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Pembayaran/list_js', $data, FALSE);
		}
		
	}

	public function view($id){

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->get_data_Pembayaran($id);

		 foreach($this->form['form'] as $key => $element) {
		 	if($key!=19){
		 		$this->form['form'][$key]['readonly'] = TRUE;
		 	}
		 	
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }
		 $this->form['data'] = $data;
		echo json_encode($this->form);

	}
}

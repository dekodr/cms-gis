<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Permohonan_sertifikasi extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Permohonan_sertifikasi';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('get_model','gm');
		$user = $this->session->userdata('user');

		$this->form = array(
			'form'=>array(
		        array(
	         		'field'	=> 	'jenis',
		            'type'	=>	'text',
		            'label'	=>	'Jenis Permohonan',
		            'rules' => 	'required',
	         	),
	         	array(
	         		'field'	=> 	'name_sistem_sertifikasi',
		            'type'	=>	'text',
		            'label'	=>	'Sistem Sertifikasi',
		            'rules' => 	'required',
	         	),
	         	array(
	         		'field'	=> 	'name_lingkup_pemohon',
		            'type'	=>	'text',
		            'label'	=>	'Lingkup Pemohon',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'name_jenis_sertifikasi',
		            'type'	=>	'text',
		            'label'	=>	'Produk',
		            'rules' => 	'required',
	         	),
	         	array(
		            'field'	=> 	'id',
		            'type'	=>  'hidden',
	         	),
	         	array(
		            'field'	=> 	'name__',
		            'type'	=>	'hidden',
	         	)
	         )
		);
	}
	
	public function index($id, $process=false){
		// print_r($this->input->post()['vendor_type']);
		// if($process==true){
			
		// 		$data = $this->am->get_data_Permohonan_sertifikasi($id);
		// 		$result = $this->data_process->check($id, $this->input->post(),$data['id_vendor'],'ms_permohonan_sertifikasi','id_vendor');
		// 		// $this->am->update_ms_vendor_Permohonan_sertifikasi($id,$this->input->post());
		// 		if($result){
		// 			echo json_encode(array('status'=>'success'));
		// 		}
		// 		 else {
		// 			echo json_encode(array('status'=>'fail'));
		// 		}
			
		// }else{
			$data = $this->am->get_data_Permohonan_sertifikasi($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Permohonan_sertifikasi/list', $data, FALSE);
			$this->load->view('approval/approval_detail/Permohonan_sertifikasi/list_js', $data, FALSE);
		// }
		
	}

	public function getData($id = null)
	{
		$config['query'] = $this->am->get_permohonan_sertifikasi($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function viewVerifikasi($id,$id_vendor){
    	$data = $this->am->verifikasi_Permohonan_sertifikasi($id,$id_vendor);
    	$data['id'] = $id;
    	$data['id_vendor'] = $id_vendor;
    	$this->load->view('approval/approval_detail/Permohonan_sertifikasi/view', $data, FALSE);
		$this->load->view('approval/approval_detail/Permohonan_sertifikasi/view_js', $data, FALSE);
    }
	public function verifikasi($id,$id_vendor){

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_Permohonan_sertifikasi($id);
		 // print_r($data);die;
		 foreach($this->form['form'] as $key => $element) {
		 	if($key!=19){
		 		$this->form['form'][$key]['readonly'] = TRUE;
		 	}
		 	
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }
		 $this->form['data'] = $data;
		echo json_encode($this->form);
	 }

	public function view($id){

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->get_data_Permohonan_sertifikasi($id);
		 // print_r($data);die;
		 foreach($this->form['form'] as $key => $element) {
		 	if($key!=19){
		 		$this->form['form'][$key]['readonly'] = TRUE;
		 	}
		 	
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }
		 $this->form['data'] = $data;
		echo json_encode($this->form);

	}

	public function commit_approve($id,$satu,$type){
		$data = $this->am->verifikasi_Permohonan_sertifikasi($id,$type);
		$result = $this->data_process->check($data['id_vendor'], $this->input->post(),$data['id_doc'],'ms_permohonan_sertifikasi','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}

	public function getDataBank($id)
	{
		$config['query'] = $this->am->get_bank($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}
}

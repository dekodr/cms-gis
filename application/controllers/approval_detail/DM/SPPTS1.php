<?php defined('BASEPATH') OR exit('No direct script access allowed');
class SPPTS1 extends MY_Controller {

	public $form;

	public $modelAlias = 'am';

	public $alias = 'ms_vendor';

	public $module = 'Dokumen_mutu';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('approval_model','am');
		$this->load->model('vendor/Permohonan_sertifikasi_model','psm');
	}
	
	public function index($id, $process=false){
			$data['data'] = $this->am->get_data_Dokumen_mutuSPPTS1($id, 'pm_file');
			$data['permohonan'] = $this->psm->getPermohonan($id)->row_array();
			$data['vendor_status'] = $this->am->getVendorStatus($id);
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/view_tab', $data, FALSE);
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/view_tab_js', $data, FALSE);
		
	}

	public function PM($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutuSPPTS1($id, 'pm_file');
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/pm/list_pm', $data, FALSE);
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/pm/list_pm_js', $data, FALSE);	
	}

	public function SSM($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutuSPPTS1($id, 'ssm_file');
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/ssm/list_ssm', $data, FALSE);
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/ssm/list_ssm_js', $data, FALSE);	
	}

	public function RM($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutuSPPTS1($id, 'rm_file');
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/rm/list_rm', $data, FALSE);
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/rm/list_rm_js', $data, FALSE);	
	}

	public function DID($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutuSPPTS1($id, 'did_file');
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/did/list_did', $data, FALSE);
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/did/list_did_js', $data, FALSE);	
	}

	public function SO($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutuSPPTS1($id, 'so_file');
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/so/list_so', $data, FALSE);
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/so/list_so_js', $data, FALSE);	
	}

	public function APP($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutuSPPTS1($id, 'app_file');
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/app/list_app', $data, FALSE);
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/app/list_app_js', $data, FALSE);	
	}

	public function PP($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutuSPPTS1($id, 'pp_file');
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/pp/list_pp', $data, FALSE);
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/pp/list_pp_js', $data, FALSE);	
	}

	public function PMP($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutuSPPTS1($id, 'pmp_file');
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/pmp/list_pmp', $data, FALSE);
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/pmp/list_pmp_js', $data, FALSE);	
	}

	public function PBB($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutuSPPTS1($id, 'pbb_file');
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/pbb/list_pbb', $data, FALSE);
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/pbb/list_pbb_js', $data, FALSE);	
	}

	public function PL($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutuSPPTS1($id, 'pl_file');
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/pl/list_pl', $data, FALSE);
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/pl/list_pl_js', $data, FALSE);	
	}

	public function BL($id)
	{
			$data['data'] = $this->am->get_data_Dokumen_mutuSPPTS1($id, 'bl_file');
			$data['id'] = $id;
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/bl/list_bl', $data, FALSE);
			$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/bl/list_bl_js', $data, FALSE);	
	}

	public function view($id = null, $type)
	{
		$config['query'] = $this->am->get_data_Dokumen_mutuSPPTS1($id, $type);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function viewVerifikasi($id,$folder,$type){
    	$data = $this->am->verifikasi_Dokumen_mutu($id,$type);
    	$data['id'] = $id;
    	$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/'.$folder.'/view', $data, FALSE);
		$this->load->view('approval/approval_detail/Dokumen_mutu/SPPTS1/'.$folder.'/view_js', $data, FALSE);
    }

    public function verifikasi($id,$type)
	{
		$this->form = array(
			'form' => array(
				array(
		            'field'	=> 	$type,
		            'type'	=>	'file',
		            'label'	=>	'Lampiran',
		            'upload_path'=>base_url('assets/lampiran/akta_file/'),
					'upload_url'=>site_url('vendor/akta/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
			)
		);
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->verifikasi_Dokumen_mutu($id,$type);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if ($this->form['form'][$key]['value'] == 'lifetime') {
		 		$this->form['form'][$key]['value'] = 'Seumur Hidup';
		 	}
		 }
		echo json_encode($this->form);
	}

	public function commit_approve($id,$satu,$type){
		$data = $this->am->verifikasi_Dokumen_mutu($id,$type);
		$result = $this->data_process->check($data['id_vendor'], $this->input->post(),$data['id'],'ms_dokumen_mutu','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}

	public function cekData($id){
    	$modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->verifikasi_Dokumen_mutu($id);

		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 	if($this->form['form'][$key]['type']=='date_range'){
		 		$_value = array();

		 		foreach ($this->form['form'][$key]['field'] as $keys => $values) {
		 			$_value[] = $data[$values];

		 		}
		 		$this->form['form'][$key]['value'] = $_value;
		 	}
		 }
		echo json_encode($this->form);
    }
}

<?php
/**
 * 
 */
class Upload extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('vendor/certificate_model','cm');
		$this->load->model('get_model','gm');

		$this->form_laporan = array(
			'form'=>array(
				array(
		            'field'	=> 'desc',
		            'type'	=> 'textarea',
		            'label'	=> 'Keterangan',
		            'rules' => 'required',
		        ),
		        array(
		            'field'	=> 	'temuan_file',
		            'type'	=>	'file',
		            'label'	=>	'Upload File',
		            'upload_path'=>base_url('assets/lampiran/temuan_file/'),
					'upload_url'=>site_url('upload/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	)
	         )
		);
	}

	public function form_upload_file($id)
  	{
  		$this->form = array(
			'form'=>array(
				array(
		            'field'	=> 	'file_template',
		            'type'	=>	'multiple_file',
		            'label'	=>	'Upload File',
		            'upload_path'=>base_url('assets/lampiran/file_template/'),
					'upload_url'=>site_url('upload/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	)
	         )
		);
		$this->form['url'] = site_url('upload/upload_file/'.$id);
	 	$this->form['button'] = array(
	 		array(
	 			'type' => 'submit',
	 			'label' => 'Upload',
	 		) ,
	 		array(
	 			'type' => 'cancel',
	 			'label' => 'Batal'
	 		)
	 	);
	 	echo json_encode($this->form);
  	}

  	public function upload_file($id)
  	{
  		// if ($this->validation()) {
	 		$save = $this->input->post();
	 		$save['file_template'] = json_encode($save['file_template']);
			$save['edit_stamp'] = date("Y-m-d H:i:s");
	 		if ($this->cm->upload_file($id,$save)) {
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 			echo json_encode(array('status'=>'success'));
	 		}
	 	// }
  	}
  	
  	public function pilihAuditor($id)
	{
		$this->form = array(
			'form'=>array(
				array(
		            'field'	=> 'id_auditor',
		            'type'	=> 'checkboxList',
		            'label'	=> 'Pilih Anggota',
		            'rules' => 'required',
		            'source'=> $this->gm->getAuditorCheckbox()
		        )
	         )
		);
		$this->form['url'] = site_url('upload/save_leader/'.$id);
	 	$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Simpan'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
	 	echo json_encode($this->form);
	}

	public function save_leader($id)
	{
	 	//if ($this->validation()) {
	 		$save = $this->input->post();
	 		$save_['id_sertifikat'] = $id;
			$save_['id_auditor'] = implode(",",$save['id_auditor']);
			$save_['is_leader'] = $save['is_leader'];
			//print_r($save['id_auditor']);
			$save_['entry_stamp'] = date("Y-m-d H:i:s");
			$save_['del'] = 0;
	 		if ($this->cm->save_auditor($save_, $id)) {
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);

	 			$get_sertifikat 	= $this->cm->get_certificate($id)->row_array();
	 			$get_email_leader 	= $this->cm->get_email_leader($save_['is_leader'])->row_array();
	 			$get_email_anggota 	= $this->cm->get_email_anggota($save_['id_auditor'])->result_array();
	 			
	 			$message_leader = 'Anda telah terpilih menjadi leader auditor sertifikat '.$get_sertifikat['nama_sertifikat'].' atas ajuan '.$get_sertifikat['legal_name'].' '.$get_sertifikat['nama_user'].'.<br/>
				PT Global Inspeksi Sertifikasi';

				$message_anggota = 'Anda telah terpilih menjadi anggota auditor sertifikat '.$get_sertifikat['nama_sertifikat'].' atas ajuan '.$get_sertifikat['legal_name'].' '.$get_sertifikat['nama_user'].'.<br/>
				PT Global Inspeksi Sertifikasi';

				$to_leader  = $get_email_leader['email'];
				$to_anggota_ = '';
				foreach ($get_email_anggota as $key => $value) {
					$to_anggota_ .= $value['email'].', ';
				}
				$to_anggota = substr($to_anggota_,substr($to_anggota_),-2);

				$this->send_mail_leader($to_leader,$message_leader);
				$this->send_mail_anggota($to_anggota,$message_anggota);
	 			echo json_encode(array('status' => 'success' ));
	 		}
	 	//}
	}

	public function send_mail_leader($to,$message)
	{
		$this->email->from(EMAIL_HOSTNAME, 'VMS PGN');
		$this->email->to($to); 


		$this->email->subject('Pemilihan Leader Auditor');
		
		$this->email->message($message);	
		$this->email->send();
	}

	public function send_mail_anggota($to,$message)
	{
		$this->email->from(EMAIL_HOSTNAME, 'VMS PGN');
		$this->email->to($to); 


		$this->email->subject('Pemilihan Anggota Auditor');
		
		$this->email->message($message);	
		$this->email->send();
	}

	public function upload_laporan($id)
	{
		$this->form = $this->form_laporan;
		$this->form['url'] = site_url('upload/save_laporan/'.$id);
	 	$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Simpan'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
	 	echo json_encode($this->form);
	}

	public function save_laporan($id)
	{
		
		$auditor = $this->session->userdata('admin')['name'];
		$save_history = array(
			'id_certificate' => $id,
			'value'			 => date('Y-m-d H:i:s').' '.$auditor.' Upload Laporan.',
			'entry_stamp'	 => date('Y-m-d H:i:s'),
			'del'			 => 0
		);
		$this->db->insert('tr_history',$save_history);

		$this->form_validation->set_rules($this->form_laporan);
	 	if ($this->validation($this->form_laporan)) {
	 		$save = $this->input->post();
	 		$save['id_certificate'] = $id;
	 		$save['id_leader']  = $this->session->userdata('admin')['id_user'];
	 		$save['id_pelapor'] = $this->session->userdata('admin')['id_user'];
			//print_r($save['id_auditor']);
			$save['entry_stamp'] = date("Y-m-d H:i:s");
			$save['del'] = 0;
	 		if ($this->cm->save_laporan($save)) {
	 			$this->session->set_flashdata('msg', $this->successMessage);
	 			$this->deleteTemp($save);
	 		}
	 	}
	}

	public function selesai_laporan($id)
	{
		$auditor = $this->session->userdata('admin')['name'];
		$save_history = array(
			'id_certificate' => $id,
			'value'			 => date('Y-m-d H:i:s').' '.$auditor.' Upload Final Laporan.',
			'entry_stamp'	 => date('Y-m-d H:i:s'),
			'del'			 => 0
		);
		$this->db->insert('tr_history',$save_history);

		$return = $this->cm->selesai_laporan($id);
		if ($return) {
			echo '<script>alert("Berhasil!"); window.location.href="'.site_url('dashboard').'"</script>';
		}
	}

	public function upload_verifikasi_admin($id)
	{
		$this->form = array(
			'form' => array(
		            'field'	=> 	'temuan_file',
		            'type'	=>	'file',
		            'label'	=>	'Upload File',
		            'upload_path'=>base_url('assets/lampiran/temuan_file/'),
					'upload_url'=>site_url('upload/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>'
	         	)
		);
		$this->form['url'] = site_url('upload/save_laporan_verifikasi/'.$id);
	 	$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Simpan'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
	 	echo json_encode($this->form);
	}
}
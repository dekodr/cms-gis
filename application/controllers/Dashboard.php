<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public $form;

	public function __construct(){
		parent::__construct();
		$this->load->model('Dashboard_model','dm');
		$this->load->model('vendor/vendor_model','vm');
		$this->load->model('main_model','mm');
		$this->load->model('note_model','nm');
		$this->load->model('approval_model','am');
	}

	public function index($id=null)
	{
		$user = $this->session->userdata('user');
		// if($this->session->userdata('admin')['id_role'] == 7) {
		// 	redirect('auction');
		// }
		if($this->session->userdata('user')) {
			redirect('vendor/dashboard');
		}
		$user = $this->session->userdata('user');
        $admin = $this->session->userdata('admin');
        $data['admin'] = $admin;
        $data['session'] = (($user) ? $user : $admin);
        $data['daftar_tunggu'] = $this->vm->get_total_daftar_tunggu();

		$data['total_dpt'] 	= $this->vm->get_total_dpt();
		$data['notifikasi'] = $this->dm->get_note_admin()->result_array();

		$data['chart'] 		= array(
								'daftar_tunggu_chart' => $this->mm->get_daftar_tunggu_chart(),
								'dpt_chart'			  => $this->mm->dpt_chart()
							  );
		$data['vendor_status'] = $this->am->getVendorStatus($user['id_user']);
       
		$this->load->model('main_model','mm');
		$this->load->model('note_model','nm');        
		$this->header = 'Selamat Datang '.(($user) ? $user['name'] : $admin['name']);
		if($admin){
			$this->content = $this->load->view('dashboard/dashboard_admin',$data, TRUE);
			$this->script  = $this->load->view('dashboard/dashboard_js', $data, TRUE);
		}else if($user){
			$this->content = $this->load->view('dashboard/dashboard_vendor',$data, TRUE);
		}
		
		

		parent::index();
	}

	public function to_waiting_list(){
		$user = $this->session->userdata('user');
		// echo print_r($user);
		$waiting_list = $this->vm->to_waiting_list();
		// echo $waiting_list;die;
		if($waiting_list['msg'] == 'Berhasil'){
				$set_session = array(
					'id_user' 		=> 	$user['id_user'],
					'name'			=>	$user['name'],
					'id_sbu'		=>	$user['id_sbu'],
					'vendor_status'	=>	1,
					'is_active'		=>	$user['is_active'],
					'id_role'		=>	$user['id_role'],
					'npwp_code'		=>  $user['npwp_code']
				);
				
				
				$this->session->set_userdata('user',$set_session);

				redirect(site_url());

		} else {
			echo '<script>alert("'.$waiting_list['msg'].'"); window.location.href="'.site_url('vendor/'.$waiting_list['link']).'"</script>';
		}
	}

	public function get_client($id = null)
	{
		$config['query'] = $this->vm->get_client();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function get_assignment($id = null)
	{
		$config['query'] = $this->dm->get_assignment();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function get_notification($id = null)
	{
		$config['query'] = $this->dm->get_notification();
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function insert_assignment()
	{
		$this->form = array(
			'form'=>array(
		        array(
					'field' => 'name',
					'label' => 'Nama Client',
					'type'  => 'dropdown',
					'source'=>	$this->dm->search_client(),
					'rules' => 'required'
				),
	         	array(
		            'field'	=> 	'id_auditor',
		            'type'	=>	'dropdown',
		            'label'	=>	'Pilih Auditor',
		            'source'=>	$this->dm->getAuditor(),
		            'rules' => 	'required'
		        )
	         ),
			 'button' => array(
			 	array(
					'type' => 'submit',
					'label' => 'Simpan',
				),
				array(
					'type' => 'cancel',
					'label' => 'Batal'
				)
			 )
		);

		echo json_encode($this->form);
	}
	
}

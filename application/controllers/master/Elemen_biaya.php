<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Elemen_biaya extends MY_Controller {
	public $form;
	public $modelAlias = 'ebm';
	public $alias = 'tb_budget_spender';
	public $module = 'Elemen_biaya';
	public $isClientMenu = true;
	public function __construct(){
		parent::__construct();
		$this->load->model('master/Elemen_biaya_model','ebm');

		// $this->load->model('Role_model','rm');
		$user = $this->session->userdata('user');
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'code',
						'type'	=>	'text',
						'label'	=>	'Kode',
						'rules' => 	'required',
					),	
					array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Elemen Biaya',
						'rules' => 	'required',
					),	
				),
				'successAlert'=>'Berhasil mengubah data!'
			);
		$this->insertUrl = site_url('master/elemen_biaya/save/'.$this->id_client);
		$this->updateUrl = 'master/elemen_biaya/update';
		$this->deleteUrl = 'master/elemen_biaya/delete/';
		$this->getData = $this->ebm->getData($this->form);
		$this->form_validation->set_rules($this->form['form']);
	}
	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('elemen_biaya'),
			'title' => 'Elemen Biaya'
		));
		$this->header = 'Elemen Biaya';
		$this->content = $this->load->view('master/elemen_biaya/list',$data, TRUE);
		$this->script = $this->load->view('master/elemen_biaya/list_js', $data, TRUE);
		parent::index();
	}
	public function import(){
		$this->load->library('Excel');
		  $inputFileName = './assets/excel/eb.xlsx';
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objPHPExcel = $objReader->load($inputFileName);
		$sheet = $objPHPExcel->getSheet(0);
		 $highestRow = $sheet->getHighestRow();
		 for ($row = 1; $row <= $highestRow; $row++){
		 	$val = $objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue();
		 	$val = ltrim($val);
		 	$val = explode(' ', $val);
		 	$code = $val[0];
		 	unset($val[0]);
		 	$value = implode(' ', $val);
		 	$code = str_replace("90600.","",$code);
		 	$this->ebm->insert($code, $value);


		 }
	}

}

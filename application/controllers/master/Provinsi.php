<?php
/**
 * 
 */
class Provinsi extends MY_Controller
{
	public $modelAlias = 'pm';
	public $alias = 'tb_province';
	function __construct()
	{
		parent::__construct();
		$this->load->model('master/provinsi_model','pm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
		        array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Provinsi',
		            'rules' =>	'required'
		        )
	         )
		);
		$this->getData = $this->pm->getData($this->form);
		$this->insertUrl = site_url('master/provinsi/save/');
		$this->updateUrl = 'master/provinsi/update/';
		$this->deleteUrl = 'master/provinsi/delete/';
	}

	public function index()
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/provinsi'),
			'title' => 'Provinsi'
		));
		$this->header = 'Provinsi';
		$this->content = $this->load->view('master/provinsi/list',$data, TRUE);
		$this->script = $this->load->view('master/provinsi/list_js', $data, TRUE);
		parent::index();
	}
}
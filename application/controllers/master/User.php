<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User extends MY_Controller {

	public $form;

	public $modelAlias = 'um';

	public $alias = 'ms_admin';

	public $module = 'User';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('master/user_model','um');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama User',
		            'rules' =>	'required'
		        ),
		        array(
		            'field'	=> 	'password',
		            'type'	=>	'password',
		            'label'	=>	'Password',
		            'rules' =>	'required'
	         	),
	         	array(
		            'field'	=> 	'email',
		            'type'	=>	'text',
		            'label'	=>	'Email',
		            'rules' =>	'required'
	         	),
				array(
					'field'=>'id_role',
					'label'=>'Role',
					'type' =>'dropdown',
					'source' =>	$this->gm->getRole(),
		            'rules' =>	'required'
				),
				array(
		            'field'	=> 	'username',
		            'type'	=>	'text',
		            'label'	=>	'Username',
		            'rules' =>	'required'
	         	)
	         )
		);
		$this->getData = $this->um->getData($this->form);
		$this->insertUrl = site_url('master/user/save/');
		$this->updateUrl = 'master/user/update';
		$this->deleteUrl = 'master/user/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/user'),
			'title' => 'User'
		));
		$this->header = 'User';
		$this->content = $this->load->view('master/user/list',$data, TRUE);
		$this->script = $this->load->view('master/user/list_js', $data, TRUE);
		parent::index();
	}
	public function edit($id = null)
	{
		// $this->form['form'][1]['caption'] = "*Biarkan kosong jika tidak ingin diganti";
		parent::edit($id); 
	}
	public function insert()
	{
		$this->form['url'] = site_url('master/user/save/');
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	
	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
		
			if ($this->$modelAlias->insert($save)) {
				$this->deleteTemp($save);
			}	
		}
	}

	public function update($id)
	{
		$modelAlias = $this->um;
		if ($this->validation()) {
			$save = $this->input->post();
		
			$lastData = $this->um->selectData($id);
			if ($this->um->update($id, $save)) {
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function delete($id) {
		
		if ($this->um->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url('master/user/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}
}

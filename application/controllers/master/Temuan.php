<?php 

/**
 * 
 */
class Temuan extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('master/temuan_model','tm');
		$this->getData = $this->tm->getData();
	}

	public function index()
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/temuan'),
			'title' => 'Temuan'
		));
		$this->header = 'Temuan';
		$this->content = $this->load->view('master/temuan/list',$data, TRUE);
		$this->script = $this->load->view('master/temuan/list_js', $data, TRUE);
		parent::index();
	}

	public function view_temuan($id)
	{
		$data['id'] = $id;
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/temuan/view_temuan/'.$id),
			'title' => 'Daftar Temuan'
		));
		$this->header = 'Daftar Temuan';
		$this->content = $this->load->view('master/temuan/view',$data, TRUE);
		$this->script = $this->load->view('master/temuan/view_js', $data, TRUE);
		parent::index();
	}

	public function getDataTemuan($id)
	{
		$config['query'] = $this->tm->get_data_temuan($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}
}
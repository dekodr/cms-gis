<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Tugas extends MY_Controller {

	public $form;

	public $modelAlias = 'tm';

	public $alias = 'ms_tugas';

	public $module = 'tugas';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('master/tugas_model','tm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'tugas_file',
		            'type'	=>	'file',
		            'label'	=>	'tugas File',
		            'upload_path'=>base_url('assets/lampiran/tugas_file/'),
					'upload_url'=>site_url('master/tugas/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	)
	         )
		);
		$this->getData = $this->tm->getData($this->form);
		$this->insertUrl = site_url('master/tugas/save/');
		$this->updateUrl = 'master/tugas/update';
		$this->deleteUrl = 'master/tugas/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/tugas'),
			'title' => 'Surat Tugas'
		));
		$this->header = 'Surat Tugas';
		$this->content = $this->load->view('master/tugas/list',$data, TRUE);
		$this->script = $this->load->view('master/tugas/list_js', $data, TRUE);
		parent::index();
	}

	public function save()
	{
		$_POST['is_master'] = 1;
		parent::save();
	}
}

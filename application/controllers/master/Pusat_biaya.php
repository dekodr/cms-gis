<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pusat_biaya extends MY_Controller {
	public $form;
	public $modelAlias = 'pbm';
	public $alias = 'tb_pengguna';
	public $module = 'Pusat_biaya';
	public $isClientMenu = true;
	public function __construct(){
		parent::__construct();
		$this->load->model('master/Pusat_biaya_model','pbm');

		// $this->load->model('Role_model','rm');
		$user = $this->session->userdata('user');
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'code',
						'type'	=>	'text',
						'label'	=>	'Pusat Biaya',
						'rules' => 	'required',
					),	
					array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Satuan Kerja',
						'rules' => 	'required',
					),	
					array(
						'field'	=> 	'division_head',
						'type'	=>	'text',
						'label'	=>	'Division Head',
						'rules' => 	'required',
					),	
				),
				'successAlert'=>'Berhasil mengubah data!'
			);
		$this->insertUrl = site_url('master/pusat_biaya/save/'.$this->id_client);
		$this->updateUrl = 'master/pusat_biaya/update';
		$this->deleteUrl = 'master/pusat_biaya/delete/';
		$this->getData = $this->pbm->getData($this->form);
		$this->form_validation->set_rules($this->form['form']);
	}
	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('pusat_biaya'),
			'title' => 'Pusat Biaya'
		));
		$this->header = 'Pusat Biaya';
		$this->content = $this->load->view('master/pusat_biaya/list',$data, TRUE);
		$this->script = $this->load->view('master/pusat_biaya/list_js', $data, TRUE);
		parent::index();
	}

}

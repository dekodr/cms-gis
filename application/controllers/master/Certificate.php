<?php
/**
 * 
 */
class Certificate extends MY_Controller
{
	public $form;

	public $modelAlias = 'cm';

	public $alias = 'tb_certificate';

	public $module = 'Certificate';

	public $isClientMenu = true;

	function __construct()
	{
		parent::__construct();
		$this->load->model('master/certificate_model','cm');
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Tipe Sertifikat',
		            'rules' => 'required',
		        )
	         ),
			'filter' => array(
				array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		        )
			)
		);
		$this->formDetail = array(
			'form'=>array(
	         	array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama Form',
		            'rules' => 'required',
		        ),array(
		            'field'	=> 	'template_file',
		            'type'	=>	'file',
		            'label'	=>	'Dokumen Lampiran',
		            'upload_path'=>base_url('assets/lampiran/template_file/'),
					'upload_url'=>site_url('vendor/certificate/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	)
	         ),
			'filter' => array(
				array(
		            'field'	=> 'name',
		            'type'	=> 'text',
		            'label'	=> 'Nama',
		        )
			)
		);
		$this->getData = $this->cm->getData($this->form);
		$this->insertUrl = site_url('master/certificate/save/');
		$this->updateUrl = 'master/certificate/update/';
		$this->deleteUrl = 'master/certificate/delete/';
	}

	public function index(){
		$data = $this->session->userdata('admin');
		// if($this->vm->check_pic($data['id_user'])==0){
		// 	redirect(site_url('vendor/pernyataan'));
		// }
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/certificate'),
			'title' => 'Sertifikat'
		));
		$this->header = 'Sertifikat';
		$this->content = $this->load->view('master/certificate/list',$data, TRUE);
		$this->script = $this->load->view('master/certificate/list_js', $data, TRUE);
		parent::index();
	}
	public function form($id){
		$data = $this->session->userdata('admin');
		// if($this->vm->check_pic($data['id_user'])==0){
		// 	redirect(site_url('vendor/pernyataan'));
		// }
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/certificate/form'),
			'title' => 'Upload Template Form'
		));
		$data['id'] = $id;
		$this->header = 'Upload Template Form';
		$this->content = $this->load->view('master/certificate/form',$data, TRUE);
		$this->script = $this->load->view('master/certificate/form_js', $data, TRUE);
		parent::index();
	}
	public function getDataForm(){
		$config['query'] = $this->cm->getDataForm($this->form);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}
	public function insertFormDetail($id){
		$this->formDetail['url'] = site_url('master/certificate/saveDetail/'.$id);
		$this->formDetail['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			),
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDetail);
	}
	public function saveDetail($id){
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['id_certificate']=$id;
			$save['entry_stamp'] = timestamp();
			if ($this->$modelAlias->insertDetail($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}
	public function deleteDetail($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->$modelAlias->deleteDetail($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function removeDetail($id)
	{
		$this->formDelete['url'] = site_url('master/certificate/deleteDetail/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}
}
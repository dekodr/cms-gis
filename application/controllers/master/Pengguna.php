<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pengguna extends MY_Controller {
	public $form;
	public $modelAlias = 'pm';
	public $alias = 'tb_pengguna';
	public $module = 'Pengguna';
	public $isClientMenu = true;
	public function __construct(){
		parent::__construct();
		$this->load->model('master/Pengguna_model','pm');

		// $this->load->model('Role_model','rm');
		$user = $this->session->userdata('user');
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'name',
						'type'	=>	'text',
						'label'	=>	'Nama Pengguna',
						'rules' => 	'required',
					),	
				),
				'successAlert'=>'Berhasil mengubah data!'
			);
		$this->insertUrl = site_url('master/pengguna/save/'.$this->id_client);
		$this->updateUrl = 'master/pengguna/update';
		$this->deleteUrl = 'master/pengguna/delete/';
		$this->getData = $this->pm->getData($this->form);
		$this->form_validation->set_rules($this->form['form']);
	}
	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('pengguna'),
			'title' => 'Pengguna'
		));
		$this->header = 'Pengguna';
		$this->content = $this->load->view('master/pengguna/list',$data, TRUE);
		$this->script = $this->load->view('master/pengguna/list_js', $data, TRUE);
		parent::index();
	}

}

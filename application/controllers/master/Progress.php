<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Progress extends MY_Controller {
	public $form;
	public $modelAlias = 'pm';
	public $alias = 'tb_progress_pengadaan';
	public $module = 'Tahap Pengadaan';
	public $isClientMenu = true;
	public function __construct(){
		parent::__construct();
		$this->load->model('master/Progress_model','pm');

		// $this->load->model('Role_model','rm');
		$this->id_metode = $this->uri->segment(4);
		$user = $this->session->userdata('user');
		$this->form = array(
				'form' => array(
					array(
						'field'	=> 	'orders',
						'type'	=>	'text',
						'label'	=>	'Urutan',
						'rules' => 	'required',
					),	
					array(
						'field'	=> 	'value',
						'type'	=>	'textarea',
						'label'	=>	'Progress',
						'rules' => 	'required',
					),	
				),
				'successAlert'=>'Berhasil mengubah data!'
			);
		$this->insertUrl = site_url('master/progress/save/'.$this->id_metode);
		$this->updateUrl = 'master/progress/update';
		$this->deleteUrl = 'master/progress/delete/';
		$this->getData = $this->pm->getData($this->form);
		$this->form_validation->set_rules($this->form['form']);
	}
	public function index(){
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('progress'),
			'title' => 'Tahap Pengadaan'
		));
		$this->header = 'Tahap Pengadaan';
		$this->content = $this->load->view('master/progress/list',$data, TRUE);
		$this->script = $this->load->view('master/progress/list_js', $data, TRUE);
		parent::index();
	}
	public function getData(){
		$return = array(
			'data' => array(
				array(
					'id'=>1,
					'name'=>'Pembelian Langsung',
				),array(
					'id'=>2,
					'name'=>'Penunjukan Langsung',
				),array(
					'id'=>3,
					'name'=>'Penunjukan Langsung Kondisi Tertentu',
				),array(
					'id'=>4,
					'name'=>'Pemilihan Langsung',
				),array(
					'id'=>5,
					'name'=>'Pelelangan',
				),array(
					'id'=>6,
					'name'=>'Penunjukkan Langsung Melalui Penugasan',
				)
			)
		);
		echo json_encode($return);
	}
	public function ubahTahapan($id){
		$progress = array(1=>'Pembelian Langsung', 2=>'Penunjukan Langsung', 3=>'Penunjukan Langsung Kondisi Tertentu', 4=>'Pemilihan Langsung', 5=>'Pelelangan', 6=>'Penunjukkan Langsung Melalui Penugasan');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/progress/index/'.$id),
			'title' => $progress[$id]
		));
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('ubahTahapan'),
			'title' => 'Tahapan Pengadaan '.$progress[$id]
		));
		$data['id'] = $id;
		$this->header = 'Tahapan Pengadaan '.$progress[$id];
		$this->content = $this->load->view('master/progress/tahapan',$data, TRUE);
		$this->script = $this->load->view('master/progress/tahapan_js', $data, TRUE);
		parent::index();
	}
	public function insert($id)
	{
		$this->form['url'] = site_url('master/progress/save/'.$id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}
	public function save($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['metode'] = $id;
			$save['entry_stamp'] = timestamp();
			if ($this->$modelAlias->insert($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}
	public function getDataTahapan($id = null)
	{
		$config['query'] = $this->pm->getDataTahapan($id);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

}

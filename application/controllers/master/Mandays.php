<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Mandays extends MY_Controller {

	public $form;

	public $modelAlias = 'mm';

	public $alias = 'ms_mandays';

	public $module = 'Mandays';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('master/mandays_model','mm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'mandays_file',
		            'type'	=>	'file',
		            'label'	=>	'Mandays File',
		            'upload_path'=>base_url('assets/lampiran/mandays_file/'),
					'upload_url'=>site_url('master/mandays/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	),
		        array(
		            'field'	=> 	'jumlah_mandays',
		            'type'	=>	'number',
		            'label'	=>	'Jumlah Mandays',
		            'rules' => 	'required',
	         	)
	         )
		);
		$this->getData = $this->mm->getData($this->form);
		$this->insertUrl = site_url('master/mandays/save/');
		$this->updateUrl = 'master/mandays/update';
		$this->deleteUrl = 'master/mandays/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/mandays'),
			'title' => 'Perhitungan Mandays'
		));
		$this->header = 'Perhitungan Mandays';
		$this->content = $this->load->view('master/mandays/list',$data, TRUE);
		$this->script = $this->load->view('master/mandays/list_js', $data, TRUE);
		parent::index();
	}

	public function save()
	{
		$_POST['is_master'] = 1;
		parent::save();
	}
}

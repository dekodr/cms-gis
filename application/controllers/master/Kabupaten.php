<?php 

/**
 * 
 */
class Kabupaten extends MY_Controller
{
	public $modelAlias = 'km';
	public $alias = 'tb_kabupaten';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('master/kabupaten_model','km');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
		        array(
		            'field'	=> 	'id_province',
		            'type'	=>	'dropdown',
		            'label'	=>	'Provinsi',
		            'source'=>	$this->gm->getProvince(),
		            'rules' =>	'required'
		        ),
	         	array(
		            'field'	=> 	'name',
		            'type'	=>	'text',
		            'label'	=>	'Nama Kabupaten/Kota',
		            'rules' =>	'required'
		        )
	         )
		);
		$this->getData = $this->km->getData($this->form);
		$this->insertUrl = site_url('master/kabupaten/save/');
		$this->updateUrl = 'master/kabupaten/update/';
		$this->deleteUrl = 'master/kabupaten/delete/';
	}

	public function index()
	{
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('master/kabupaten'),
			'title' => 'Kabupaten'
		));
		$this->header = 'Kabupaten';
		$this->content = $this->load->view('master/kabupaten/list',$data, TRUE);
		$this->script = $this->load->view('master/kabupaten/list_js', $data, TRUE);
		parent::index();
	}
}
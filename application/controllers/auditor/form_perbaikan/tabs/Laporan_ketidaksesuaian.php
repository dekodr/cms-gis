<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Laporan_ketidaksesuaian extends MY_Controller {

	public $form;

	public $modelAlias = 'lkm';

	public $alias = 'ms_laporan_ketidaksesuaian';

	public $module = 'Form Perbaikan';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('auditor/form_perbaikan/laporan_ketidaksesuaian_model','lkm');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'type',
		            'type'	=> 'dropdown',
		            'label'	=> 'Jenis',
		            'rules' => 'required',
		            'source'=> array(
		            	'pendirian' => 'Akta Pendirian',
		            	'perubahan' => 'Akta Perubahan'
		            )
		        ),
	         	array(
		            'field'	=> 	'akta_file',
		            'type'	=>	'file',
		            'label'	=>	'Akta File',
		            'upload_path'=>base_url('assets/lampiran/akta_file/'),
					'upload_url'=>site_url('vendor/akta/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
	         ),
		);
	}
	
	public function index(){
		$this->load->view('auditor/form_perbaikan/tabs/laporan_ketidaksesuaian/list', $data, FALSE);
		$this->load->view('auditor/form_perbaikan/tabs/laporan_ketidaksesuaian/list_js', $data, FALSE);
	}

	public function getData()
	{
		$config['query'] = $this->lkm->getData($this->form);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Form_perbaikan extends MY_Controller {

	public $form;

	public $modelAlias = 'fpm';

	public $module = 'Form Perbaikan';

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();
			
			if (! $this->session->userdata('admin')) {
				redirect(site_url());
			} 
	}

	 public function index(){

	 	$admin = $this->session->userdata('admin');

	 	$this->breadcrumb->addlevel(1, array(
	 		'url' => site_url('vendor/admin/daftar_tunggu'),
	 		'title' => 'Form Perbaikan'
	 	));

	 	$data['admin'] = $admin;
	 	$this->header = 'Form Perbaikan';
	 	$this->content = $this->load->view('auditor/form_perbaikan/view',$data, TRUE);
	 	$this->script = $this->load->view('auditor/form_perbaikan/view_js', $data, TRUE);
	 	parent::index();

	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Catatan_audit extends MY_Controller {

	public $form;

	public $modelAlias = 'cam';

	public $alias = 'ms_catatan_audit';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('auditor/pda/catatan_audit_model','cam');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'catatan_audit_file',
		            'type'	=>	'file',
		            'label'	=>	'Catatan Audit',
		            'upload_path'=>base_url('assets/lampiran/catatan_audit_file/'),
					'upload_url'=>site_url('auditor/pda/catatan_audit/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	)
	         )
		);
		$this->getData = $this->cam->getData($this->form);
		$this->insertUrl = site_url('auditor/pda/catatan_audit/save/');
		$this->updateUrl = 'auditor/pda/catatan_audit/update';
		$this->deleteUrl = 'auditor/pda/catatan_audit/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auditor/pda/catatan_audit'),
			'title' => 'Catatan Audit'
		));
		$this->header = 'Catatan Audit';
		$this->content = $this->load->view('auditor/pda/catatan_audit/list',$data, TRUE);
		$this->script = $this->load->view('auditor/pda/catatan_audit/list_js', $data, TRUE);
		parent::index();
	}

	public function edit($id = null)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . $id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('msg', $this->successMessage);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
}

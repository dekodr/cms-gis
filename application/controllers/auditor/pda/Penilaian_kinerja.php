<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Penilaian_kinerja extends MY_Controller {

	public $form;

	public $modelAlias = 'pkm';

	public $alias = 'ms_penilaian_kinerja';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('auditor/pda/penilaian_kinerja_model','pkm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'penilaian_kinerja_file',
		            'type'	=>	'file',
		            'label'	=>	'Penilaian Kinerja',
		            'upload_path'=>base_url('assets/lampiran/penilaian_kinerja_file/'),
					'upload_url'=>site_url('auditor/pda/penilaian_kinerja/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	)
	         )
		);
		$this->getData = $this->pkm->getData($this->form);
		$this->insertUrl = site_url('auditor/pda/penilaian_kinerja/save/');
		$this->updateUrl = 'auditor/pda/penilaian_kinerja/update';
		$this->deleteUrl = 'auditor/pda/penilaian_kinerja/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auditor/pda/penilaian_kinerja'),
			'title' => 'Peniliaian Kinerja'
		));
		$this->header = 'Peniliaian Kinerja';
		$this->content = $this->load->view('auditor/pda/penilaian_kinerja/list',$data, TRUE);
		$this->script = $this->load->view('auditor/pda/penilaian_kinerja/list_js', $data, TRUE);
		parent::index();
	}

	public function edit($id = null)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . $id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('msg', $this->successMessage);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
}

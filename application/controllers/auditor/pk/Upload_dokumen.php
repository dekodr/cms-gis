<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Upload_dokumen extends MY_Controller {

	public $form;

	public $modelAlias = 'udm';

	public $alias = 'ms_upload_dokumen';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('auditor/pk/upload_dokumen_model','udm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
				array(
		            'field'	=> 	'jenis',
		            'type'	=>	'dropdown',
		            'label'	=>	'Jenis Upload Dokumen',
		            'source'=>	array(
		            	'pendidikan' 		=> 'Pendidikan',
		            	'pelatihan'	 		=> 'Pelatihan',
		            	'pengalaman_kerja' 	=> 'Pengalaman Kerja'
		            ),
		            'rules' =>	'required'
		        ),
	         	array(
		            'field'	=> 	'upload_dokumen_file',
		            'type'	=>	'file',
		            'label'	=>	'Upload Dokumen',
		            'upload_path'=>base_url('assets/lampiran/upload_dokumen_file/'),
					'upload_url'=>site_url('auditor/pk/upload_dokumen/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	)
	         )
		);
		$this->getData = $this->udm->getData($this->form);
		$this->insertUrl = site_url('auditor/pk/upload_dokumen/save/');
		$this->updateUrl = 'auditor/pk/upload_dokumen/update';
		$this->deleteUrl = 'auditor/pk/upload_dokumen/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auditor/pk/upload_dokumen'),
			'title' => 'Upload Dokumen'
		));
		$this->header = 'Upload Dokumen';
		$this->content = $this->load->view('auditor/pk/upload_dokumen/list',$data, TRUE);
		$this->script = $this->load->view('auditor/pk/upload_dokumen/list_js', $data, TRUE);
		parent::index();
	}

	public function edit($id = null)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . $id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('msg', $this->successMessage);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
}

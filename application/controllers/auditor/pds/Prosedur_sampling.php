<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Prosedur_sampling extends MY_Controller {

	public $form;

	public $modelAlias = 'psm';

	public $alias = 'ms_prosedur_sampling';

	public $isClientMenu = true;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public function __construct(){

		parent::__construct();

		$this->load->model('auditor/pds/prosedur_sampling_model','psm');
		$this->load->model('get_model','gm');
		$admin = $this->session->userdata('admin');

		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 	'prosedur_sampling_file',
		            'type'	=>	'file',
		            'label'	=>	'Prosedur Sampling',
		            'upload_path'=>base_url('assets/lampiran/prosedur_sampling_file/'),
					'upload_url'=>site_url('auditor/pds/prosedur_sampling/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	)
	         )
		);
		$this->getData = $this->psm->getData($this->form);
		$this->insertUrl = site_url('auditor/pds/prosedur_sampling/save/');
		$this->updateUrl = 'auditor/pds/prosedur_sampling/update';
		$this->deleteUrl = 'auditor/pds/prosedur_sampling/delete/';
	}
	
	public function index(){
		$data['admin'] = $this->session->userdata('admin');
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auditor/pds/prosedur_sampling'),
			'title' => 'Prosedur Sampling'
		));
		$this->header = 'Prosedur Sampling';
		$this->content = $this->load->view('auditor/pds/prosedur_sampling/list',$data, TRUE);
		$this->script = $this->load->view('auditor/pds/prosedur_sampling/list_js', $data, TRUE);
		parent::index();
	}

	public function edit($id = null)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . $id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('msg', $this->successMessage);
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function get_lampiran($id)
	{
		echo json_encode($this->psm->get_lampiran($id));
	}
}

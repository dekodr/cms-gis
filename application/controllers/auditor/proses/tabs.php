<?php

class Tabs extends MY_Controller
{
    function __construct()
    {
        parent::__construct();   
        $this->load->model('auditor/proses/pemilihan_auditor_model','pam');
    }   

    public function index($id,$id_certificate)
    {
        $admin = $this->session->userdata('admin');
         
	 	$this->breadcrumb->addlevel(1, array(
	 		'url' => site_url('auditor/proses/proses/index/'.$id),
	 		'title' => 'Proses Sertifikasi'
         ));
         
	 	$data['id_client']       = $id;
        $data['id_certificate']  = $id_certificate;
        $data['admin']           = $admin;
        $data['role']           = $this->pam->getRoleByUser($admin['id_user'],$id,$id_certificate);
         
	 	$this->header = 'Proses Sertifikasi';
	 	$this->content = $this->load->view('auditor/proses/tab',$data, TRUE);
	 	$this->script = $this->load->view('auditor/proses/tab_js', $data, TRUE);
	 	parent::index();
    }
}

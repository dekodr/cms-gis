<?php

class Proses extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('auditor/proses/pemilihan_auditor_model','pam');
    }   

    public function index($id)
    {
        $admin = $this->session->userdata('admin');
         
	 	$this->breadcrumb->addlevel(1, array(
	 		'url' => site_url('auditor/proses/proses/index/'.$id),
	 		'title' => 'Proses Sertifikasi'
         ));
         
	 	$data['id_client']  = $id;
        $data['admin']      = $admin;
         
	 	$this->header = 'Proses Sertifikasi';
	 	$this->content = $this->load->view('auditor/proses/view',$data, TRUE);
	 	$this->script = $this->load->view('auditor/proses/view_js', $data, TRUE);
	 	parent::index();
    }

    public function sertifikat($id)
    {
        $admin = $this->session->userdata('admin');
         
        $this->breadcrumb->addlevel(1, array(
            'url' => site_url('auditor/proses/proses/sertifikat/'.$id),
            'title' => 'Proses Sertifikasi'
         ));
         
        $data['id_client']  = $id;
        $data['admin']      = $admin;
         
        $this->header = 'Proses Sertifikasi';
        $this->content = $this->load->view('auditor/proses/auditor',$data, TRUE);
        $this->script = $this->load->view('auditor/proses/auditor_js', $data, TRUE);
        parent::index();
    }

    public function getDataSertifikat($id = null)
    {
        $config['query'] = $this->pam->getCertificateAssignment($id);
        $return = $this->tablegenerator->initialize($config);
        echo json_encode($return);
    }
}

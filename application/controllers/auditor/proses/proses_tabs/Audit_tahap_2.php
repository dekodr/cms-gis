<?php
class Audit_tahap_2 extends MY_Controller
{
    public $modelAlias = 'atm';
    function __construct()
    {
        parent::__construct();  
        $this->load->model('auditor/proses/audit_tahap_model','atm');
        $this->form = array(
            'form'=>array(
	         	array(
		            'field'	=> 	'jenis_audit',
		            'type'	=>	'text',
		            'label'	=>	'Jenis Audit',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'standar_audit',
		            'type'	=>	'text',
		            'label'	=>	'Standar Audit',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'tgl_audit',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Audit',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'no',
		            'type'	=>	'text',
		            'label'	=>	'Nomor',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'uraian',
		            'type'	=>	'textarea',
		            'label'	=>	'Uraian Ketidaksesuaian',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'bagian_audit',
		            'type'	=>	'textarea',
		            'label'	=>	'Bagian yang diaudit',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'klausul',
		            'type'	=>	'textarea',
		            'label'	=>	'Klausul Ketidaksesuaian',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'kategori',
		            'type'	=>	'text',
		            'label'	=>	'Kategori',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'tgl_rencana',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Rencana',
		            'rules' => 	'required'
	         	)
	         )
        ); 
        $this->insertUrl = site_url('auditor/proses/proses_tabs/audit_tahap_2/save');
        $this->updateUrl = 'auditor/proses/proses_tabs/audit_tahap_2/update';
        $this->deleteUrl = 'auditor/proses/proses_tabs/audit_tahap_2/delete/';
    }   

    public function index($id, $id_certificate,$process=false){
			$data['data']       = $this->atm->getData($id);
			$data['id_client']  = $id;
			$data['id_certificate']  = $id_certificate;
			$this->load->view('auditor/proses/proses_tabs/audit_tahap_2/list', $data, FALSE);
			$this->load->view('auditor/proses/proses_tabs/audit_tahap_2/list_js', $data, FALSE);
		
    }
    
   	public function getData($id_client, $id_certificate,$tahap)
	{
		$config['query'] = $this->atm->getData($id_client, $id_certificate,$tahap);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
    }
    
    public function insert($id_client,$id_certificate)
	{
		$this->form['url'] = $this->insertUrl.'/'.$id_client.'/'.$id_certificate;
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function save($id_client,$id_certificate)
	{
		$admin = $this->session->userdata('admin');
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
            $save = $this->input->post();
            $save['id_auditor'] = $admin['id_user'];
            $save['id_client'] 		= $id_client;
            $save['id_certificate'] = $id_certificate;
            $save['tahap'] = 2;
			$save['entry_stamp'] = timestamp();
			if ($this->$modelAlias->insert($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
    }
    
    public function edit($id,$type,$id_client,$id_certificate)
	{
		$modelAlias = $this->modelAlias;
		$data 			= $this->$modelAlias->selectData($id);
		$data_history 	= $this->$modelAlias->selectDataHistory($id);
		if ($type == 'history') {
			$arr = array(
				array(
		            'field'	=> 	'analisis',
		            'type'	=>	'textarea',
		            'label'	=>	'Analisis Penyebab',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'koreksi',
		            'type'	=>	'textarea',
		            'label'	=>	'Koreksi',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'tindakan',
		            'type'	=>	'textarea',
		            'label'	=>	'Tindakan Korektif',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'tgl_perbaikan',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Perbaikan',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'dokumen_perbaikan_file',
		            'type'	=>	'file',
		            'label'	=>	'Upload Dokumen Perbaikan',
		            'upload_path'=>base_url('assets/lampiran/dokumen_perbaikan_file/'),
					'upload_url'=>site_url('vendor/perbaikan/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
	         );
			$this->form['form'] = array_merge($this->form['form'],$arr);
			foreach($this->form['form'] as $key => $element) {
				$this->form['form'][$key]['readonly'] = true;
            	$this->form['form'][$key]['value'] = $data_history[$element['field']];
			}
        } else {
        	foreach($this->form['form'] as $key => $element) {
				if ($type == 'cek') {
	                $this->form['form'][$key]['readonly'] = true;
	                $this->form['form'][$key]['value'] = $data[$element['field']];
	            } else {
	                $this->form['form'][$key]['value'] = $data[$element['field']];
	            }
			}
        }

		if ($type != 'cek' && $type != 'history') {
            $this->form['url'] = site_url($this->updateUrl.'/'.$id.'/'.$id_client.'/'.$id_certificate);
            $this->form['button'] = array(
                array(
                    'type'  => 'submit',
                    'label' => 'Ubah'
                ) ,
                array(
                    'type'  => 'cancel',
                    'label' => 'Batal'
                )
            );
        }
		echo json_encode($this->form);
    }
    
    public function update($id,$id_client,$id_certificate)
	{
		$admin = $this->session->userdata('admin');
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['id_auditor'] 	= $admin['id_user'];
            $save['id_client'] 		= $id_client;
            $save['id_certificate'] = $id_certificate;
            $save['tahap'] 			= 2;
			$save['edit_stamp'] 	= timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}

	public function log($id_log,$id_client,$id_certificate)
	{
		$data['admin'] = $this->session->userdata('admin');
		$data['id_log'] = $id_log;
		$data['id_client']  = $id_client;
		$data['id_certificate']  = $id_certificate;
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('auditor/proses/tabs/index/'.$id_client.'/'.$id_certificate.'#tabTahap2'),
			'title' => 'Audit Tahap 2'
		));
		$this->breadcrumb->addlevel(2, array(
			'url' => site_url('auditor/proses/proses_tabs/audit_tahap_2/log/'.$id_log),
			'title' => 'Riwayat Perbaikan'
		));
		$this->header = 'Riwayat Perbaikan';
		$this->content = $this->load->view('auditor/proses/proses_tabs/audit_tahap_2/log',$data, TRUE);
		$this->script = $this->load->view('auditor/proses/proses_tabs/audit_tahap_2/log_js', $data, TRUE);
		parent::index();
	}

	public function getDataHistory($id_log)
	{
		$config['query'] = $this->atm->getDataHistory($id_log);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function viewVerifikasi($id,$id_vendor){
    	$data = $this->atm->selectData($id,$id_vendor);
    	$data['id'] = $id;
    	$data['id_vendor'] = $id_vendor;
    	$this->load->view('auditor/proses/proses_tabs/audit_tahap_2/view', $data, FALSE);
		$this->load->view('auditor/proses/proses_tabs/audit_tahap_2/view_js', $data, FALSE);
    }
	public function verifikasi($id,$id_vendor){

		$arr = array(
				array(
		            'field'	=> 	'analisis',
		            'type'	=>	'textarea',
		            'label'	=>	'Analisis Penyebab',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'koreksi',
		            'type'	=>	'textarea',
		            'label'	=>	'Koreksi',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'tindakan',
		            'type'	=>	'textarea',
		            'label'	=>	'Tindakan Korektif',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'tgl_perbaikan',
		            'type'	=>	'date',
		            'label'	=>	'Tanggal Perbaikan',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'dokumen_perbaikan_file',
		            'type'	=>	'file',
		            'label'	=>	'Upload Dokumen Perbaikan',
		            'upload_path'=>base_url('assets/lampiran/dokumen_perbaikan_file/'),
					'upload_url'=>site_url('vendor/perbaikan/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
	         	)
	         );
			$this->form['form'] = array_merge($this->form['form'],$arr);

		 $modelAlias = $this->modelAlias;
		 $data = $this->$modelAlias->selectData($id);
		 
		 foreach($this->form['form'] as $key => $element) {
		 	$this->form['form'][$key]['readonly'] = TRUE;
		 	$this->form['form'][$key]['value'] = $data[$element['field']];
		 }
		echo json_encode($this->form);
	 }
    
    public function commit_approve($id){
		$data = $this->atm->selectData($id);
		$result = $this->data_process->check($data['id_vendor'], $this->input->post(),$data['id'],'ms_tahap_audit','id');
		if($result){
			echo json_encode(array('status'=>'success'));
		}
	}
}

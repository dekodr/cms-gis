<?php
class Perhitungan_mandays extends MY_Controller
{
    public $modelAlias = 'pmm';
    function __construct()
    {
        parent::__construct();  
        $this->load->model('auditor/proses/perhitungan_mandays_model','pmm');
        $this->form = array(
            'form'=>array(
	         	array(
		            'field'	=> 	'mandays_file',
		            'type'	=>	'file',
		            'label'	=>	'Mandays File',
		            'upload_path'=>base_url('assets/lampiran/mandays_file/'),
					'upload_url'=>site_url('auditor/proses/proses_tabs/perhitungan_mandays/upload_lampiran'),
					'allowed_types'=>'pdf|jpeg|jpg|png|gif|rar|zip|doc|docx',
					'caption' => '<span class="english-caption">*Format data harus PDF, JPEG, JPG, PNG, GIF ,ZIP dan RAR.</span>',
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'jumlah_mandays',
		            'type'	=>	'number',
                    'label'	=>	'Jumlah Mandays',
		            'rules' => 	'required'
	         	)
	         )
        ); 
        $this->insertUrl = site_url('auditor/proses/proses_tabs/perhitungan_mandays/save');
        $this->updateUrl = 'auditor/proses/proses_tabs/perhitungan_mandays/update';
        $this->deleteUrl = 'auditor/proses/proses_tabs/perhitungan_mandays/delete/';
    }   

    public function index($id, $id_certificate,$process=false){
			$data['data']       = $this->pmm->getData($id);
			$data['id_client']  = $id;
			$data['id_certificate']  = $id_certificate;
			$data['mandays_file']	= $this->pmm->getMandaysFile();
			$this->load->view('auditor/proses/proses_tabs/perhitungan_mandays/list', $data, FALSE);
			$this->load->view('auditor/proses/proses_tabs/perhitungan_mandays/list_js', $data, FALSE);
		
    }
    
    public function getData($id_client, $id_certificate)
	{
		$config['query'] = $this->pmm->getData($id_client, $id_certificate);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
    }
    
    public function insert($id_client,$id_certificate)
	{
		$this->form['url'] = $this->insertUrl.'/'.$id_client.'/'.$id_certificate;
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function save($id_client,$id_certificate)
	{
		$admin = $this->session->userdata('admin');
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
            $save = $this->input->post();
            $save['id_auditor'] = $admin['id_user'];
            $save['id_client'] 		= $id_client;
            $save['id_certificate'] = $id_certificate;
			$save['entry_stamp'] = timestamp();
			if ($this->$modelAlias->insert($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
    }
    
    public function edit($id,$type)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			if ($type == 'cek') {
                $this->form['form'][$key]['readonly'] = true;
                $this->form['form'][$key]['value'] = $data[$element['field']];
            } else {
                $this->form['form'][$key]['value'] = $data[$element['field']];
            }
		}


		if ($type != 'cek') {
            $this->form['url'] = site_url($this->updateUrl . '/' . $id);
            $this->form['button'] = array(
                array(
                    'type'  => 'submit',
                    'label' => 'Ubah'
                ) ,
                array(
                    'type'  => 'cancel',
                    'label' => 'Batal'
                )
            );
        }
		echo json_encode($this->form);
    }
    
    public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
}

<?php
class Pemilihan_auditor extends MY_Controller
{
    public $modelAlias = 'pam';
    function __construct()
    {
        parent::__construct();  
        $this->load->model('auditor/proses/pemilihan_auditor_model','pam');
        
        $this->form = array(
            'form'=>array(
	         	array(
		            'field'	=> 	'id_lead_auditor',
		            'type'	=>	'multiple',
                    'label'	=>	'Pilih Lead Auditor',
                    'source'=>  $this->pam->getTeam(3),
		            'rules' => 	'required'
                 ),
                 array(
		            'field'	=> 	'id_auditor',
		            'type'	=>	'multiple',
                    'label'	=>	'Auditor',
                    'source'=>  $this->pam->getTeam(5),
		            'rules' => 	'required'
                 ),
                 array(
		            'field'	=> 	'id_ppc',
		            'type'	=>	'multiple',
                    'label'	=>	'Pilih PPC',
                    'source'=>  $this->pam->getTeam(6),
		            'rules' => 	'required'
                 ),
                 array(
		            'field'	=> 	'id_panel',
		            'type'	=>	'multiple',
                    'label'	=>	'Pilih Panel',
                    'source'=>  $this->pam->getTeam(7),
		            'rules' => 	'required'
                 ),
                 array(
		            'field'	=> 	'id_expertise',
		            'type'	=>	'multiple',
                    'label'	=>	'Pilih Expertise',
                    'source'=>  $this->pam->getTeam(8),
		            'rules' => 	'required'
	         	),
                array(
		            'field'	=> 	array('date_start','date_end'),
		            'type'	=>	'date_range',
                    'label'	=>	'Tanggal Audit',
		            'rules' => 	'required'
	         	)
	         )
        );

        $this->formRole = array(
            'form'=>array(
	         	array(
		            'field'	=> 	'role_auditor',
		            'type'	=>	'checkboxList',
                    'label'	=>	'Pilih Role',
                    'source'=>  $this->pam->getRole(),
		            'rules' => 	'required'
                 ),
	         )
        );

        $this->insertUrl = site_url('auditor/proses/proses_tabs/pemilihan_auditor/save');
        $this->updateUrl = 'auditor/proses/proses_tabs/pemilihan_auditor/update';
        $this->deleteUrl = 'auditor/proses/proses_tabs/pemilihan_auditor/delete/';
    }   

    public function index($id,$id_certificate, $process=false){
    		$admin = $this->session->userdata('admin');
			$data['data']       = $this->pam->getData($id);
			$data['admin']		= $admin;
			$data['id_client']  = $id;
			$data['id_certificate']  = $id_certificate;
			$this->load->view('auditor/proses/proses_tabs/pemilihan_auditor/list', $data, FALSE);
			$this->load->view('auditor/proses/proses_tabs/pemilihan_auditor/list_js', $data, FALSE);
		
    }
    
    public function getData($id_client,$id_certificate)
	{
		$config['query'] = $this->pam->getData($id_client,$id_certificate);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
    }

    public function getDataAuditor($id_certificate,$id_client)
	{
		$config['query'] = $this->pam->getDataAuditor($id_certificate,$id_client);
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
    }
    
    public function insert($id_certificate,$id_client)
	{
		$this->form['url'] = $this->insertUrl.'/'.$id_certificate.'/'.$id_client;
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function pilihRole($id)
	{
		$this->form = $this->formRole;

		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
            $this->form['form'][$key]['value'] = $data[$element['field']];
		}

		$this->form['url'] = site_url('auditor/proses/proses_tabs/pemilihan_auditor/saveRole/'.$id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function saveRole($id)
	{
			$save = $this->input->post();

			$role_ = '';
			foreach ($save['role_auditor'] as $key => $value) {
				$role_  .= $value.',';
			}
			$role = substr($role_, substr($role_), -1);
			// echo "asdas".$role;die;
			$this->pam->saveRole($id,$role);
			
	        echo json_encode(array('status'=>'success'));
    }

	public function save($id_certificate,$id_client)
	{
		$save = $this->input->post();
		// print_r($save);die;
		$arr = array(
				'id_certificate' 	=> $id_certificate,
				'id_client' 		=> $id_client,
				'entry_stamp' 		=> date('Y-m-d H:i:s'),
				'date_start'		=> $save['date_start'],
				'date_end'			=> $save['date_end']
			);

		foreach ($save['id_lead_auditor'] as $key => $value) {
			// echo "string".$value;die;
			$arr['id_auditor'] = $value;
			$arr['is_leader']  = 1;
			// print_r($arr);die;
			$this->pam->insert($arr);
		}

		foreach ($save['id_auditor'] as $key => $value) {
			$arr['id_auditor'] = $value;
			$arr['is_leader']  = 0;
			$this->pam->insert($arr);
		}

		foreach ($save['id_ppc'] as $key => $value) {
			$arr['id_auditor'] = $value;
			$arr['is_leader']  = 0;
			$this->pam->insert($arr);
		}

		foreach ($save['id_panel'] as $key => $value) {
			$arr['id_auditor'] = $value;
			$arr['is_leader']  = 0;
			$this->pam->insert($arr);
		}

		foreach ($save['id_expertise'] as $key => $value) {
			$arr['id_auditor'] = $value;
			$arr['is_leader']  = 0;
			$this->pam->insert($arr);
		}
		
        echo json_encode(array('status'=>'success'));
    }
    
    public function edit($id,$type)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			if ($type == 'cek') {
                $this->form['form'][$key]['readonly'] = true;
                $this->form['form'][$key]['value'] = $data[$element['field']];
            } else {
                $this->form['form'][$key]['value'] = $data[$element['field']];
            }
		}


		if ($type != 'cek') {
            $this->form['url'] = site_url($this->updateUrl . '/' . $id);
            $this->form['button'] = array(
                array(
                    'type'  => 'submit',
                    'label' => 'Ubah'
                ) ,
                array(
                    'type'  => 'cancel',
                    'label' => 'Batal'
                )
            );
        }
		echo json_encode($this->form);
    }
    
    public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
}

<?php
class Verifikasi extends MY_Controller
{
    public $modelAlias = 'ver';
    function __construct()
    {
        parent::__construct();  
        $this->load->model('auditor/proses/verifikasi_model','ver');
        $this->form = array(
            'form'=>array(
	         	array(
		            'field'	=> 	'perhitungan_mandays',
		            'type'	=>	'radioList',
		            'label'	=>	'Perhitungan Mandays',
		            'source'=>	array(
		            	'1' => '<label class="nephritisAtt"><i class="fa fa-check"></i>&nbsp;OK</label>',
		            	'2' => '<label class="pomegranateAtt"><i class="fa fa-times"></i>&nbsp;NOT OK</label>'
		            ),
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'surat_tugas',
		            'type'	=>	'radioList',
		            'label'	=>	'Surat Tugas',
		            'source'=>	array(
		            	'1' => '<label class="nephritisAtt"><i class="fa fa-check"></i>&nbsp;OK</label>',
		            	'2' => '<label class="pomegranateAtt"><i class="fa fa-times"></i>&nbsp;NOT OK</label>'
		            ),
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'pemilihan_auditor',
		            'type'	=>	'radioList',
		            'label'	=>	'Pemilihan Auditor dan Panelis',
		            'source'=>	array(
		            	'1' => '<label class="nephritisAtt"><i class="fa fa-check"></i>&nbsp;OK</label>',
		            	'2' => '<label class="pomegranateAtt"><i class="fa fa-times"></i>&nbsp;NOT OK</label>'
		            ),
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'jadwal_audit',
		            'type'	=>	'radioList',
		            'label'	=>	'Jadwal Audit',
		            'source'=>	array(
		            	'1' => '<label class="nephritisAtt"><i class="fa fa-check"></i>&nbsp;OK</label>',
		            	'2' => '<label class="pomegranateAtt"><i class="fa fa-times"></i>&nbsp;NOT OK</label>'
		            ),
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'audit_tahap_1',
		            'type'	=>	'radioList',
		            'label'	=>	'Audit Tahap 1',
		            'source'=>	array(
		            	'1' => '<label class="nephritisAtt"><i class="fa fa-check"></i>&nbsp;OK</label>',
		            	'2' => '<label class="pomegranateAtt"><i class="fa fa-times"></i>&nbsp;NOT OK</label>'
		            ),
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'audit_tahap_2',
		            'type'	=>	'radioList',
		            'label'	=>	'Audit Tahap 2',
		            'source'=>	array(
		            	'1' => '<label class="nephritisAtt"><i class="fa fa-check"></i>&nbsp;OK</label>',
		            	'2' => '<label class="pomegranateAtt"><i class="fa fa-times"></i>&nbsp;NOT OK</label>'
		            ),
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'dokumen_sampling',
		            'type'	=>	'radioList',
		            'label'	=>	'Dokumen Sampling',
		            'source'=>	array(
		            	'1' => '<label class="nephritisAtt"><i class="fa fa-check"></i>&nbsp;OK</label>',
		            	'2' => '<label class="pomegranateAtt"><i class="fa fa-times"></i>&nbsp;NOT OK</label>'
		            ),
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'hasil_uji',
		            'type'	=>	'radioList',
		            'label'	=>	'Hasil Uji',
		            'source'=>	array(
		            	'1' => '<label class="nephritisAtt"><i class="fa fa-check"></i>&nbsp;OK</label>',
		            	'2' => '<label class="pomegranateAtt"><i class="fa fa-times"></i>&nbsp;NOT OK</label>'
		            ),
		            'rules' => 	'required'
	         	),
	         	array(
		            'field'	=> 	'panel_sertifikasi',
		            'type'	=>	'radioList',
		            'label'	=>	'Panel Sertifikasi',
		            'source'=>	array(
		            	'1' => '<label class="nephritisAtt"><i class="fa fa-check"></i>&nbsp;OK</label>',
		            	'2' => '<label class="pomegranateAtt"><i class="fa fa-times"></i>&nbsp;NOT OK</label>'
		            ),
		            'rules' => 	'required'
	         	)
	         )
        ); 
        $this->insertUrl = site_url('auditor/proses/proses_tabs/verifikasi/save');
        $this->updateUrl = 'auditor/proses/proses_tabs/verifikasi/update';
        $this->deleteUrl = 'auditor/proses/proses_tabs/verifikasi/delete/';
    }   

    public function index($id, $id_certificate,$process=false){
		$data	= $this->ver->getDataVerifikasi($id,$id_certificate);
		$data['id_client']  		= $id;
		$data['id_certificate'] 	= $id_certificate;
		$this->load->view('auditor/proses/proses_tabs/verifikasi/list', $data, FALSE);
		$this->load->view('auditor/proses/proses_tabs/verifikasi/list_js', $data, FALSE);	
    }

    public function view($id_client,$id_certificate)
    {
    	$data = $this->ver->getDataVerifikasi($id_client,$id_certificate);
    	// print_r($data);die;
    	if (!empty($data)) {
    		foreach ($this->form['form'] as $key => $value) {
	    		$this->form['form'][$key]['value'] = $data[$value['field']];
	    	}
    	}

    	$this->form['data'] = $data;

    	$this->form['url'] = $this->insertUrl.'/'.$id_client.'/'.$id_certificate;
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			)
		);
		echo json_encode($this->form);
    }

	public function save($id_client,$id_certificate)
	{
		$admin = $this->session->userdata('admin');
		$modelAlias = $this->modelAlias;
		// if ($this->validation()) {
            $save = $this->input->post();
            
            if (!empty($save)) {
            	$save['id_client'] 		= $id_client;
	            $save['id_certificate'] = $id_certificate;
				$save['entry_stamp'] = timestamp();

				$cek_action = $this->ver->cek_action($id_client,$id_certificate);

				if ($cek_action == 'insert') {
					$action = $this->ver->insert($save);
				} else {
					$action = $this->ver->update_data($id_client,$id_certificate,$save);
				}
				if ($action) {
					$this->session->set_flashdata('msg', $this->successMessage);
					$this->deleteTemp($save);
					echo json_encode(array('status' => 'success'));
					// return true;
				}
            } else {
            	echo json_encode(array('status' => 'fail'));
            }
		// }
    }
    
   	public function verifikasi_sertifikat($id_client)
   	{
   		$this->formDelete['url'] = site_url('auditor/proses/proses_tabs/verifikasi/proses_verifikasi/'.$id_client);
		$this->formDelete['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Verifikasi',
			)
		);
		echo json_encode($this->formDelete);
   	}

   	public function proses_verifikasi($id_client)
   	{
   		if ($this->ver->verifikasi_sertifikat($id_client)) {
   			echo json_encode(array('status' => 'success'));
   		}
   	}
}

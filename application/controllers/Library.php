<?php
/**
 * 
 */
class Library extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->library('treeview');
		$this->form = array(
			'form'=>array(
	         	array(
		            'field'	=> 'folder_name',
		            'type'	=> 'text',
		            'label'	=> 'Nama Folder',
		            'rules' => 'required',
		        ),
	         )
		);
		$this->load->model('library_model','lm');
		$this->insertUrl = site_url('library/save');
	}

	public function index(){
		$data = $this->session->userdata('user');
		// if($this->vm->check_pic($data['id_user'])==0){
		// 	redirect(site_url('vendor/pernyataan'));
		// }
		$this->breadcrumb->addlevel(1, array(
			'url' => site_url('library'),
			'title' => 'Library'
		));
		$this->header = 'Library';
		$this->content = $this->load->view('library/list',$data, TRUE);
		$this->script = $this->load->view('library/list_js', $data, TRUE);
		parent::index();
	}

	public function save()
	{
		if ($this->validation()) {
			$save = $this->input->post();
			$save['name'] = $save['folder_name'];
			$save = array(
				'name' 			=> $save['folder_name'],
				'type' 			=> 'folder',
				'entry_stamp' 	=> date('Y-m-d H:i:s'),
				'del'			=> 0
			);
			if ($this->lm->insert($save)) {
				return true;
			}
		}
	}

	public function getMainFolder()
	{
		$getData = $this->lm->getData();

		foreach ($getData as $key => $value) {
			$sub_folder['id'] = $value['id'];
			$sub_folder['name'] = $value['name'];
			//$sub_folder['type'] = $value['type'];
			$sub_folder['parent_id'] = $value['parent_id'];
			$dataFolder[]=$sub_folder;
		}

		foreach ($dataFolder as $key => &$value) {
			$html[$value['id']] = &$value;
		}

		foreach ($dataFolder as $key => &$value) {
			if ($value['parent_id'] && isset($html[$value['parent_id']])) {
				$html[$value['parent_id']]['nodes'][] = &$value;
			}
		}

		foreach ($dataFolder as $key => &$value) {
			if ($value['parent_id'] && isset($html[$value['parent_id']])) {
				unset($dataFolder[$key]);
			}
		}
		// print_r($dataFolder);die;
		echo $this->folderGenerator($dataFolder);
	}

	public function generateFolder()
	{
		$path = urldecode( $_REQUEST['dir'] );
		$tree = new treeview( $path );
		echo $tree->create_tree();
	}

	public function folderGenerator($dataFolder)
	{
		// print_r($dataFolder);die;
		$files = $dataFolder;
			if( count($files) > 0 ) { /* The 2 accounts for . and .. */
				// All dirs
				echo '<ul class="tree">';
				foreach( $files as $file => $value) {
					
						echo '<li class="linetree"><span class="icon tree"><i class="fas fa-angle-down"></i></span><span class="caret"><a href="#">' . htmlentities($value['name']) . '</a></span><ul class="nested">';
						foreach ($value['nodes'] as $key_ => $value_) {
							echo '<li><a href="#"><i class="fas fa-file"></i>&nbsp;'.$value_['name'].'</a></li>';
						}

						echo '</ul></li>';
				}
				// All files
				foreach( $files as $file => $value) {
					
						echo "<li ><a href='#'>" . htmlentities($value['name']) . "</a></li>";
				}	
				echo '</ul>';
			}
	}

	public function make_folder()
	{
		$path = base_url('assets/lampiran/test');
		mkdir('assets/lampiran/test');
	}

	// public function getMainFolder()
	// {
	// 	$html = '';
	// 	$directory = $this->lm->getData();

	// 	foreach ($directory as $name => $value) {
	// 		$html .='<li class="linetree"><span class="icon tree"><i class="fas fa-angle-down"></i></span><span class="caret"><i class="fas fa-folder"></i> '.$value['name'].'</span>
	// 						<ul class="nested">';

	// 		  $subFolder = $this->lm->getSubFolder($value['id']);
	//     	  foreach ($subFolder as $key_folder => $isiFolder) {

	//     	  		$getSubFolder_2 = $this->lm->getSubFolder_2($isiFolder['id']);
	//     	  		// print_r($getSubFolder_2);die;
	//     	  		if ($isiFolder['type'] != 'folder') {
	//     	  				$html .= '<li class="linetree"><span class="caret"><a href="'.site_url('assets/library/'.$isiFolder['name']).'" target="blank">'.$isiFolder['name'].'</a></span>';
	//     	  		} else{
	//     	  				$html .= '<li class="linetree"><span class="icon tree"><i class="fas fa-angle-down"></i></span><span class="caret"><i class="fas fa-folder"></i> '.$isiFolder['name'].'</span><ul class="nested">';
	//     	  			foreach ($getSubFolder_2 as $sub2 => $value_sub2) {

	//     	  				$html .= '<li class="linetree"><span class="icon tree"><i class="fas fa-angle-down"></i></span><span class="caret"><i class="fas fa-folder"></i> '.$value_sub2['name_sub'].'</span>';
	//     	  			}
	//     	  			$html .= '</ul>';
	//     	  		}
	// 				// if (count($isiFolder) > 0) {
	// 				// 	foreach ($isiFolder as $keyFile => $valueFile) {
	// 	   //  				$html .= '<li><a href="'.site_url('vendor/bukti_pajak/download/'.$bulan.$key_folder.$valueFile).'"><i class="fas fa-file"></i>&nbsp;'.$valueFile.'</a></li>';
	// 				// 	}
	// 				// }
	// 				$html .= '</li>';
	//     	  	}
	//     	  	$html .= '</ul></li>';
	//     	 }
	//     	 echo $html;
	// 	}

	// 	public function download($folder,$secFolder,$final_file)
	// 	{
	// 		$loc = 'assets/lampiran/library/'.$folder.'/'.$secFolder;
	// 		// echo $loc;die;
	// 		if (!empty($loc)) {
	// 			$this->load->helper('download');
	// 			force_download($loc, null);
	// 		}
	// 		else {
	// 			echo "Kosong";
	// 		}
	// 	}
	// }
}
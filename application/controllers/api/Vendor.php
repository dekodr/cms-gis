<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Vendor extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function login_get()
    {
        // Users from a data store e.g. database
        $dataLoginVendor = $this    ->db
                                    ->where('username', $this->input->get('username'))
                                    ->where('password', do_hash($this->input->get('password')))
                                    ->get('ms_login');

        if($dataLoginVendor->num_rows() > 0){
            $dataResponse = $this   ->db
                                    ->select('c.id, c.name, c.npwp_code, b.name legal_name')
                                    ->join('ms_vendor_admistrasi a','a.id_vendor = c.id')
                                    ->join('tb_legal b','a.id_legal = b.id')
                                    ->get('ms_vendor c');

            $this->response($dataResponse->row_array(), REST_Controller::HTTP_OK);
        }else{
             $this->response([
                    'status' => FALSE,
                    'message' => 'Username atau Password Salah'
                ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function search_vendor_get()
    {
        $query = " SELECT
                        id,
                        name
                    FROM ms_vendor
                    WHERE del = 0 AND name LIKE ? ";

        $query = $this->db->query($query, array('%'.$this->input->post('search').'%',));
        if ($query->num_rows() > 0) {
             $this->response($query->result_array(), REST_Controller::HTTP_OK);
        }else{
             $this->response([
                    'status' => FALSE,
                    'message' => 'Vendor tidak ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND);
        }
        
    }

    public function vendor_get()
    {
        $query = "SELECT
                    a.id_sbu,
                    a.id_role,
                    a.npwp_code vendor_npwp,
                    a.vendor_code,
                    a.name vendor_name,
                    b.id_legal,
                    b.npwp_code adm_npwp,
                    b.npwp_date,
                    b.npwp_file,
                    b.npwp_code,
                    b.nppkp_date,
                    b.nppkp_code,
                    b.nppkp_file,
                    b.vendor_office_status,
                    b.vendor_address,
                    b.vendor_country,
                    b.vendor_province,
                    b.vendor_city,
                    b.vendor_phone,
                    b.vendor_fax,
                    b.vendor_email,
                    b.vendor_postal,
                    b.vendor_website,
                    b.email_pic,
                    b.vendor_type
                FROM 
                    ms_vendor_admistrasi b
                INNER JOIN
                    ms_vendor a ON a.id=b.id_vendor
                WHERE
                 a.del = 0 AND b.del = 0";
        $query = $this->db->query($query);
        if ($query->num_rows() > 0) {
             $this->response($query->result_array(), REST_Controller::HTTP_OK);
        }else{
             $this->response([
                    'status' => FALSE,
                    'message' => 'Vendor tidak ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
}

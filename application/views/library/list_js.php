<script type="text/javascript" charset="utf8" src="<?php echo base_url('assets/js/treefile.js') ?>"></script>
<script type="text/javascript">
	$(function() {
		var add = $('.buttonAdd').modal({
			render: function(el, data) {
				data.onSuccess = function() {
					$(add).data('modal').close();
					// folder.data('plugin_folder').fetchData();
					location.reload();
				}
				$(el).form(data);
			}
		});

		getfilelist($('#treeview'), './assets/library/');

		function getfilelist(cont, root) {

			$(cont).addClass('wait');

			$.post('<?php echo site_url('library/generateFolder') ?>', {
				dir: root
			}, function(data) {

				$(cont).find('.start').html('');
				$(cont).removeClass('wait').append(data);
				if ('Sample' == root)
					$(cont).find('UL:hidden').show();
				else
					$(cont).find('UL:hidden').slideDown({
						duration: 500,
						easing: null
					});

			});
		}

		$('#treeview').on('click', 'LI A', function() {
			var entry = $(this).parent();

			if (entry.hasClass('folder')) {
				if (entry.hasClass('collapsed')) {

					entry.find('UL').remove();
					getfilelist(entry, escape($(this).attr('rel')));
					entry.removeClass('collapsed').addClass('expanded');
				} else {

					entry.find('UL').slideUp({
						duration: 500,
						easing: null
					});
					entry.removeClass('expanded').addClass('collapsed');
				}
			} else {
				$('#selected_file').text("File:  " + $(this).attr('rel'));
			}
			return false;
		});
	});
</script>
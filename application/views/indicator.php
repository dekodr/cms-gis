<div class="col col-12">        
        <div class="content" id="content">
          <div class="wrapper">
            <div class="col col-12" style="margin-top: -50px;">
              <div class="panel">
                <div class="container-title" style="width: 100%;">
                  <h3>Tahap Sertifikasi</h3>
                </div>

                <!-- S T E P  S T A R T -->
                <div class="step-wrapper">
                  <ul>
                    <?php foreach ($indicator as $key => $value) { ?>
                      <li class="<?php echo $value['status'] ?>">
                        <div class="step">
                          <div class="step-icon">
                            <img src="<?php echo base_url("assets/images/".$value['image'].".png") ?>" alt="">
                          </div>
                          <div class="step-title">
                           <?php echo $value['label'];?>
                          </div>
                        </div>
                      </li>
                    <?php } ?>
                    
                    
                  </ul>
                </div>
                <!-- S T E P  E N D -->

              </div>
            </div>
          </div>
        </div>
  </div>
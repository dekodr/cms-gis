<!DOCTYPE html>
<html lang="en">

<head>
  <title>Dashboard</title>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="stylesheet" href="<?php echo base_url('assets/styles/scss/main.min.css'); ?>" />
  <!-- <link rel="stylesheet" href="assets/css/vendors/jquery-ui.css" /> -->
  <link rel="stylesheet" href="<?php echo base_url('assets/styles/fontawesome5.6.3/css/all.css'); ?>" type="text/css" media="screen" />
</head>

<body>
  <section class="main-content">
    <div class="wrapper">
      <div class="col col-2">
        <?php include "_sidebar.php" ?>
      </div>
      <div class="col col-10">
        <div class="content" id="content" oncontextmenu="return false;">
          <div class="wrapper">
            <div class="col col-12">
              <div class="panel">
                <div class="container-title" style="width: 100%;">
                  <h3>PO #001</h3>
                </div>

                <!-- S T E P  S T A R T -->
                <div class="step-wrapper">
                  <ul>
                    <li class="active">
                      <div class="step">
                        <div class="step-icon">
                          <img src="<?php echo base_url("assets/images/003.svg") ?>" alt="">
                        </div>
                        <div class="step-title">
                          Pesananan <br> Diterima
                        </div>
                      </div>
                    </li>
                    <li class="active">
                      <div class="step">
                        <div class="step-icon">
                          <img src="<?php echo base_url("assets/images/004.svg") ?>" alt="">
                        </div>
                        <div class="step-title">
                          Purchased <br> Order
                        </div>
                      </div>
                    </li>
                    <li class="denied">
                      <div class="step">
                        <div class="step-icon">
                          <img src="<?php echo base_url("assets/images/001.svg") ?>" alt="">
                        </div>
                        <div class="step-title">
                          Delivery <br> Order
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="step">
                        <div class="step-icon">
                          <img src="<?php echo base_url("assets/images/002.svg") ?>" alt="">
                        </div>
                        <div class="step-title">
                          Pesanan <br> Sampai
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
                <!-- S T E P  E N D -->

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="bg-shape">
      <img src="../source/img/bg-shape.png" alt="">
    </div>

  </section>

  <?php include "_js.php" ?>

</body>

</html>
<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'a.id',
		sort: 'desc'
	};

	var _xhr;
	// $.ajax({
	// 	url: '<?php echo site_url('vendor/Pembayaran/formFilter')?>',
	// 	async: false,
	// 	dataType: 'json',
	// 	success:function(xhr){
	// 		_xhr = xhr;
	// 	}
	// })
	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/Pembayaran/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "pembayaran_file",
				"value"	: "File Bukti Pembayaran"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=editButton(site_url+"vendor/Pembayaran/edit/"+data[1].value, data[1].value);
				html +=deleteButton(site_url+"vendor/Pembayaran/remove/"+data[1].value, data[1].value);
				return html;
			},
			target : [1]

		},
		{
			renderCell: function(data, row, key, el){
				var link = '';
				if (data[0].value == null ) {
				link +='-';	
				}else{				
					link += '<a href="'+base_url+"assets/lampiran/pembayaran_file/"+data[0].value+'" target="blank">'+data[0].value+'</a>'
				}
				return link;
			},
			target : [0]
		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"vendor/Pembayaran/insert/<?php echo $id;?>"));
		},
		finish: function(){
		      var edit = $('.buttonEdit').modal({
		        render : function(el, data){

		          data.onSuccess = function(){
		          	$(edit).data('modal').close();
		            table.data('plugin_tableGenerator').fetchData();
		          };
		          data.isReset = false;

		          $(el).form(data).data('form');
				}
		        
		      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
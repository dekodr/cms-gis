<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'a.id',
		sort: 'desc'
	};

	var _xhr;
	// $.ajax({
	// 	url: '<?php echo site_url('vendor/Penerbitan_sertifikat_vendor/formFilter')?>',
	// 	async: false,
	// 	dataType: 'json',
	// 	success:function(xhr){
	// 		_xhr = xhr;
	// 	}
	// })
	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/Penerbitan_sertifikat_vendor/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Permohonan Sertifikasi"
			},
			{
				"key"	: "name",
				"value"	: "Sistem Sertifikasi"
			},
			{
				"key"	: "symbol",
				"value"	: "Sertifikat Draft"
			},
			{
				"key"	: "action",
				"value"	: "Sertifikat Asli"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				if (data[5].value == 2 && data[2].value != null ) {
				html += '<a href="'+site_url+"vendor/Penerbitan_sertifikat_vendor/approve/"+data[4].value+'" class="button is-success buttonApprove"><span class="icon"><i class="fas fa-check"></i></span>Approve Sertifikat Draft</a>';
				html += '<a href="'+site_url+"vendor/Penerbitan_sertifikat_vendor/reject/"+data[4].value+'" class="button is-danger buttonReject"><span class="icon"><i class="fas fa-times"></i></span>Reject Sertifikat Draft</a>';
				html +='<a class="button is-primary buttonDownload" href="'+site_url+'vendor/Penerbitan_sertifikat_vendor/download/Sertifikat_Draft_File/'+data[2].value+'"><span class="icon"><i class="fas fa-download"></i></span> Download Draft Sertifikat</a>';
			}else if (data[5].value == 3) {
				html += '<p><span class="icon"><i class="fas fa-clock"></i></span>&nbsp;Menunggu Sertifikat Asli</p>';
			}else if (data[5].value == 5) {
				html +='<a class="button is-warning buttonDownload" href="'+site_url+'vendor/Penerbitan_sertifikat_vendor/download/Sertifikat_File/'+data[3].value+'"><span class="icon"><i class="fas fa-download"></i></span> Download Sertifikat Asli</a>';
			}else if (data[5].value == 1 && data[2].value == null) {
				html += '<p><span class="icon"><i class="fas fa-clock"></i></span>&nbsp;Menunggu Admin Upload Sertifikat Draft</p>';
			}else if (data[5].value == 1 && data[2].value != null) {
				html += '<p><span class="icon"><i class="fas fa-clock"></i></span>&nbsp;Menunggu Perbaikan Sertifikat</p>';
				// html +=editButton(site_url+"vendor/Penerbitan_sertifikat_vendor/edit/"+data[4].value, data[4].value);
				// html +=deleteButton(site_url+"vendor/Penerbitan_sertifikat_vendor/remove/"+data[4].value, data[4].value);
			}
				return html;
			},
			target : [4]

		},
		{
			renderCell: function(data, row, key, el){
				var html = '';
			if (data[1].value == 'Pilih Salah Satu' ) { 
				html +='-';
				}else{
					html+= data[1].value 
				}
				return html;
			},
			target : [1]
		},
		{
			renderCell: function(data, row, key, el){
				var link = '';
				if (data[2].value == null ) {
				link +='-';	
				}else{				
					link += '<a href="'+base_url+"assets/lampiran/Sertifikat_Draft_File/"+data[2].value+'" target="blank">'+data[2].value+'</a>'
				}
				return link;
			},
			target : [2]
		},
		{
			renderCell: function(data, row, key, el){
				var link = '';
				if (data[3].value == null || data[3].value == '' ) {
				link +='-';	
				}else{					
					link += '<a href="'+base_url+"assets/lampiran/Sertifikat_File/"+data[3].value+'" target="blank">'+data[3].value+'</a>'
				}
				return link;
			},
			target : [3]
		}
		],
		additionFeature: function(el){
			// el.append(insertButton(site_url+"vendor/Penerbitan_sertifikat_vendor/insert/<?php echo $id;?>"));
		},
		finish: function(){
		      var edit = $('.buttonEdit').modal({
               render : function(el, data){
 
                data.onSuccess = function(){
                   $(edit).data('modal').close();
                   table.data('plugin_tableGenerator').fetchData();
                 };
                 data.isReset = false;
 
               $(el).form(data).data('form');
               }
               
             });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});
			var app = $('.buttonApprove').modal({
				header: 'Approve Sertifikat',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin data anda sudah benar?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(app).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});
			var rjt = $('.buttonReject').modal({
				header: 'Reject Sertifikat',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Alasan Anda Reject Sertifikat?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(rjt).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
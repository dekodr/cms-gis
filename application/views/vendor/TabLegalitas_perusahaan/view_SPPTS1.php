<div id="SPPTS1" class="tabs">

  <ul>
    <li id="tabAktaPerusahaan">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SPPTS1/AP/index/'.$id) ?>">Akta Perusahaan</a>
    </li>
    <li id="tabNIB">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SPPTS1/NIB/index/'.$id) ?>">NIB</a>
    </li>
    <li id="tabSKDU">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SPPTS1/SKDU/index/'.$id) ?>">SKDU</a>
    </li>
    <li id="tabIUI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SPPTS1/IUI/index/'.$id) ?>">IUI</a>
    </li>
    <li id="tabSIUP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SPPTS1/SIUP/index/'.$id) ?>">SIUP</a>
    </li>
    <li id="tabTDP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SPPTS1/TDP/index/'.$id) ?>">TDP</a>
    </li>
    <li id="tabKTP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SPPTS1/KTP/index/'.$id) ?>">KTP</a>
    </li>
    <li id="tabAPI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SPPTS1/API/index/'.$id) ?>">API</a>
    </li>
    <li id="tabNIK">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SPPTS1/NIK/index/'.$id) ?>">NIK</a>
    </li>
    <li id="tabHAKI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SPPTS1/HAKI/index/'.$id) ?>">HAKI</a>
    </li>
    <li id="tabLP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SPPTS1/LP/index/'.$id) ?>">List Produk</a>
    </li>
  </ul>
  
</div>
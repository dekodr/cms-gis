<?php 
$getData = $permohonan->row_array();
 ?>
<div id="tabs" class="tabs">

  <ul>
    <?php  if(!empty($getData['sistem_sertifikasi_id'] == 15)) { ?>
    <li id="tabAktaPerusahaan">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/Akta_perusahaan/index/'.$id) ?>">Akta Perusahaan</a>
    </li>
    <li id="tabNIB">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/NIB/index/'.$id) ?>">NIB</a>
    </li>
    <li id="tabSKDU">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SKDU/index/'.$id) ?>">SKDU</a>
    </li>
    <li id="tabIUI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/IUI/index/'.$id) ?>">IUI</a>
    </li>
    <li id="tabSIUP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SIUP/index/'.$id) ?>">SIUP</a>
    </li>
    <li id="tabTDP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/TDP/index/'.$id) ?>">TDP</a>
    </li>  
    <?php }if (!empty($getData['sistem_sertifikasi_id'] == 16)) { ?>
    <li id="tabAktaPerusahaan">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/Akta_perusahaan/index/'.$id) ?>">Akta Perusahaan</a>
    </li>
    <li id="tabNIB">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/NIB/index/'.$id) ?>">NIB</a>
    </li>
    <li id="tabSKDU">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SKDU/index/'.$id) ?>">SKDU</a>
    </li>
    <li id="tabIUI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/IUI/index/'.$id) ?>">IUI</a>
    </li>
    <li id="tabSIUP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SIUP/index/'.$id) ?>">SIUP</a>
    </li>
  <?php }if (!empty($getData['jenis_permohonan_id'] == 2)) { ?>
    <li id="tabAktaPerusahaan">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/Akta_perusahaan/index/'.$id) ?>">Akta Perusahaan</a>
    </li>
    <li id="tabNIB">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/NIB/index/'.$id) ?>">NIB</a>
    </li>
    <li id="tabSKDU">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SKDU/index/'.$id) ?>">SKDU</a>
    </li>
    <li id="tabIUI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/IUI/index/'.$id) ?>">IUI</a>
    </li>
    <li id="tabSIUP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SIUP/index/'.$id) ?>">SIUP</a>
    </li>
    <li id="tabTDP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/TDP/index/'.$id) ?>">TDP</a>
    </li>
    <li id="tabKTP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/KTP/index/'.$id) ?>">KTP</a>
    </li>
<?php }if (!empty($getData['jenis_permohonan_id'] == 1)) { ?>
    <li id="tabAktaPerusahaan">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/Akta_perusahaan/index/'.$id) ?>">Akta Perusahaan</a>
    </li>
    <li id="tabNIB">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/NIB/index/'.$id) ?>">NIB</a>
    </li>
    <li id="tabSKDU">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SKDU/index/'.$id) ?>">SKDU</a>
    </li>
    <li id="tabIUI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/IUI/index/'.$id) ?>">IUI</a>
    </li>
    <li id="tabSIUP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SIUP/index/'.$id) ?>">SIUP</a>
    </li>
    <li id="tabTDP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/TDP/index/'.$id) ?>">TDP</a>
    </li>
    <li id="tabKTP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/KTP/index/'.$id) ?>">KTP</a>
    </li>
    <li id="tabAPI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/API/index/'.$id) ?>">API</a>
    </li>
  <?php }elseif (!empty($getData['sistem_sertifikasi_id'] != 15) && !empty($getData['sistem_sertifikasi_id'] != 16) && !empty($getData['jenis_permohonan_id'] != 2) && !empty($getData['jenis_permohonan_id'] != 1)) { ?>
      <li id="tabAktaPerusahaan">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/Akta_perusahaan/index/'.$id) ?>">Akta Perusahaan</a>
    </li>
    <li id="tabNIB">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/NIB/index/'.$id) ?>">NIB</a>
    </li>
    <li id="tabSKDU">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SKDU/index/'.$id) ?>">SKDU</a>
    </li>
    <li id="tabIUI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/IUI/index/'.$id) ?>">IUI</a>
    </li>
    <li id="tabSIUP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/SIUP/index/'.$id) ?>">SIUP</a>
    </li>
    <li id="tabTDP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/TDP/index/'.$id) ?>">TDP</a>
    </li>
    <li id="tabKTP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/KTP/index/'.$id) ?>">KTP</a>
    </li>
    <li id="tabAPI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/API/index/'.$id) ?>">API</a>
    </li>
    <li id="tabNIK">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/NIK/index/'.$id) ?>">NIK</a>
    </li>
    <li id="tabHAKI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/HAKI/index/'.$id) ?>">HAKI</a>
    </li>
    <?php } ?>
  </ul>
  
</div>
<div id="tabs9001" class="tabs">

  <ul>
    <li id="tabAktaPerusahan">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/ISO9001/AP/index/'.$id) ?>">Akta Perusahaan</a>
    </li>
    <li id="tabNIB">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/ISO9001/NIB/index/'.$id) ?>">NIB</a>
    </li>
    <li id="tabSKDU">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/ISO9001/SKDU/index/'.$id) ?>">SKDU</a>
    </li>
    <li id="tabIUI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/ISO9001/IUI/index/'.$id) ?>">IUI</a>
    </li>
    <li id="tabSIUP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/ISO9001/SIUP/index/'.$id) ?>">SIUP</a>
    </li>
    <li id="tabTDP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/ISO9001/TDP/index/'.$id) ?>">TDP</a>
    </li>
    <li id="tabKTP">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/ISO9001/KTP/index/'.$id) ?>">KTP</a>
    </li>
    <li id="tabNIK">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/ISO9001/NIK/index/'.$id) ?>">NIK</a>
    </li>
    <!-- <li id="tabAPI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/ISO9001/API/index/'.$id) ?>">API</a>
    </li>
    <li id="tabHAKI">
      <a href="<?php echo site_url('vendor/TabsLegalitas_perusahaan/ISO9001/HAKI/index/'.$id) ?>">HAKI</a>
    </li> -->
  </ul>
  
</div>
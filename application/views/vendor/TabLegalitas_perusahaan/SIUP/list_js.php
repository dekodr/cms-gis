<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'a.id',
		sort: 'desc'
	};

	var _xhr;
	// $.ajax({
	// 	url: '<?php echo site_url('vendor/TabsLegalitas_perusahaan/SIUP/formFilter')?>',
	// 	async: false,
	// 	dataType: 'json',
	// 	success:function(xhr){
	// 		_xhr = xhr;
	// 	}
	// })
	var table = $('#tableGeneratorSIUP').tableGenerator({
		url: '<?php echo site_url('vendor/TabsLegalitas_perusahaan/SIUP/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "SIUP_file",
				"value"	: "Lampiran"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		// {
		// 	renderCell: function(data, row, key, el){
		// 		var html = '';	
		// 		if (data[0].value == null || data[0].value == '') 
		// 		{
		// 			html += 'Tidak Ada Data';
		// 		} 
		// 		else 
		// 		{
		// 			html += '<a href="'+base_url+"assets/lampiran/Formulir_permohonan_file/sps_file/"+data[0].value+'" target="blank">'+data[0].value+'</a>';
		// 		}	
		// 		return html;
		// 	},
		// 	target : [0]

		// },
		// {
		// 	renderCell: function(data, row, key, el){
		// 		var html = '';	
		// 		if (data[0].value == null || data[0].value == '') 
		// 		{
		// 			html += 'Tidak Ada Data';
		// 		} 
		// 		else 
		// 		{
		// 			html += '<a href="'+base_url+"assets/lampiran/temp/"+data[0].value+'" target="blank">'+data[0].value+'</a>';
		// 		}	
		// 		return html;
		// 	},
		// 	target : [0]

		// },
		{
			renderCell: function(data, row, key, el){
				var html = '';			
					html +=editButton(site_url+"vendor/TabsLegalitas_perusahaan/SIUP/edit/"+data[1].value, data[1].value);
					html +=deleteButton(site_url+"vendor/TabsLegalitas_perusahaan/SIUP/remove/"+data[1].value, data[1].value);
				return html;
			},
			target : [1]

		},{
			renderCell: function(data, row, key, el){
				var link = '';	
					return link += '<a href="'+base_url+"assets/lampiran/siup_file/"+data[0].value+'" target="blank">'+data[0].value+'</a>' 		
			},
			target : [0]

		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"vendor/TabsLegalitas_perusahaan/SIUP/insert/<?php echo $id;?>"));
		},
		finish: function(){
			var edit = $('.buttonEdit').modal({
		        render : function(el, data){

		          data.onSuccess = function(){
		          	$(edit).data('modal').close();
		            table.data('plugin_tableGenerator').fetchData();
		          };
		          data.isReset = false;

		          $(el).form(data).data('form');
				}
		        
		      });
			var del = $('.buttonDelete').modal({
				header: 'Verifikasi Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menyelesaikan data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}

			
			$(el).form(data);
		}
	});
});
</script>
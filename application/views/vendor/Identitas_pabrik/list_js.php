<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'a.id',
		sort: 'desc'
	};

	var _xhr;
	// $.ajax({
	// 	url: '<?php echo site_url('vendor/Identitas_pabrik/formFilter')?>',
	// 	async: false,
	// 	dataType: 'json',
	// 	success:function(xhr){
	// 		_xhr = xhr;
	// 	}
	// })
	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/Identitas_pabrik/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "nama_pabrik",
				"value"	: "Nama Pabrik"
			},
			{
				"key"	: "alamat",
				"value"	: "Alamat"
			},
			{
				"key"	: "kota",
				"value"	: "Kota"
			},
			{
				"key"	: "provinsi",
				"value"	: "Provinsi"
			},
			{
				"key"	: "kode_pos",
				"value"	: "Kode Pos"
			},
			{
				"key"	: "telepon",
				"value"	: "Telepon"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=editButton(site_url+"vendor/Identitas_pabrik/edit/"+data[6].value, data[6].value);
				html +=deleteButton(site_url+"vendor/Identitas_pabrik/remove/"+data[6].value, data[6].value);
				return html;
			},
			target : [6]

		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"vendor/Identitas_pabrik/insert/<?php echo $id;?>"));
		},
		finish: function(){
		      var edit = $('.buttonEdit').modal({
		        render : function(el, data){

		          data.onSuccess = function(){
		          	$(edit).data('modal').close();
		            table.data('plugin_tableGenerator').fetchData();
		          };
		          data.isReset = false;

		          $(el).form(data).data('form');
				}
		        
		      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
<div id="tabsSPPTS5" class="tabs">

  <ul>

    <li id="tabSPS">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/SPPTS5/SPS/index/'.$id) ?>">Surat Permohonan Sertifikasi</a>
    </li>
    <li id="tabSPM">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/SPPTS5/SPM/index/'.$id) ?>">Surat Pelimpahan Merk</a>
    </li>
    <li id="tabSPJPS">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/SPPTS5/SPJPS/index/'.$id) ?>">Surat Pernyataan Jaminan Proses Sertifikasi</a>
    </li>
    <!-- <li id="tabSPL">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/SPPTS5/SPL/index/'.$id) ?>">Surat Perjanjian Lisensi</a>
    </li>
    <li id="tabSPKS">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/SPPTS5/SPKS/index/'.$id) ?>">Surat Perjanjian Kerjasama Sertifikasi</a>
    </li> -->
    <!-- <li id="tabFA">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/SPPTS5/FA/index/'.$id) ?>">Formulir Aplikasi</a>
    </li> -->
    <li id="tabDP">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/SPPTS5/DP/index/'.$id) ?>">Data Pemohon</a>
    </li>
    <li id="tabIP">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/SPPTS5/IP/index/'.$id) ?>">Informasi Produsen</a>
    </li>
    <li id="tabFMP">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/SPPTS5/FMP/index/'.$id) ?>">Formulir Mutu Produk</a>
    </li>
    <li id="tabFMBB">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/SPPTS5/FMBB/index/'.$id) ?>">Formulir Mutu Bahan Baku</a>
    </li>
    <li id="tabFPP1">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/SPPTS5/FPP1/index/'.$id) ?>">Formulir Pengendalian Produk</a>
    </li>
    <li id="tabFPPI">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/SPPTS5/FPPI/index/'.$id) ?>">Formulir Peralatan Pengujian Inspeksi</a>
    </li>
    <li id="tabFPP2">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/SPPTS5/FPP2/index/'.$id) ?>">Formulir Peralatan Produksi</a>
    </li>

  </ul>
  
</div>
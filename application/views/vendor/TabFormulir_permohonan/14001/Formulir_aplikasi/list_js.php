<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'a.id',
		sort: 'desc'
	};

	var _xhr;
	// $.ajax({
	// 	url: '<?php echo site_url('vendor/TabsFormulir_permohonan/14001/Formulir_aplikasi/formFilter')?>',
	// 	async: false,
	// 	dataType: 'json',
	// 	success:function(xhr){
	// 		_xhr = xhr;
	// 	}
	// })
	var table = $('#tableGeneratorFA14001').tableGenerator({
		url: '<?php echo site_url('vendor/TabsFormulir_permohonan/14001/Formulir_aplikasi/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "spm_file",
				"value"	: "Lampiran"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';			
					html +=editButton(site_url+"vendor/TabsFormulir_permohonan/14001/Formulir_aplikasi/edit/"+data[1].value, data[1].value);  
					html +=deleteButton(site_url+"vendor/TabsFormulir_permohonan/14001/Formulir_aplikasi/remove/"+data[1].value, data[1].value);
					html +='<a class="button is-warning buttonDownload" href="'+site_url+'vendor/TabsFormulir_permohonan/14001/Surat_permohonan_sertifikasi/download/fa_file/'+data[0].value+'"><span class="icon"><i class="fas fa-download"></i></span> Download File</a>';
				return html;
			},
			target : [1]

		},{
			renderCell: function(data, row, key, el){
				var link = '';	
					return link += '<a href="'+base_url+"assets/lampiran/fa_file/"+data[0].value+'" target="blank">'+data[0].value+'</a>' 		
			},
			target : [0]
		}],
		additionFeature: function(el){
			el.append('<a href="'+base_url+'vendor/TabsFormulir_permohonan/9001/Surat_permohonan_sertifikasi/download/template_file/akta_file_120219_032247_065.jpg" target="blank" class="button is-warning"><span class="icon"><i class="fas fa-download"></i></span> Download File</a>');
			
			el.append(insertButton(site_url+"vendor/TabsFormulir_permohonan/14001/Formulir_aplikasi/insert/<?php echo $id;?>"));
		},
		finish: function(){
			var edit = $('.buttonEdit').modal({
		        render : function(el, data){

		          data.onSuccess = function(){
		          	$(edit).data('modal').close();
		            table.data('plugin_tableGenerator').fetchData();
		          };
		          data.isReset = false;

		          $(el).form(data).data('form');
				}
		        
		      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}

			
			$(el).form(data);
		}
	});
});
</script>
<?php 
$getData = $permohonan->row_array();
 ?>
<div id="tabs" class="tabs">

  <ul>

    <?php if (!empty($getData['jenis_permohonan_id'] == 1)) { ?>
    <li id="tabSuratPermohonanSertifikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_permohonan_sertifikasi/index/'.$id) ?>">Surat Permohonan Sertifikasi</a>
    </li>
   <li id="tabFormulirAplikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Formulir_aplikasi/index/'.$id) ?>">Formulir Aplikasi</a>
    </li>
    <li id="tabSuratPerjanjianKerjasamaSertifikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_perjanjian_kerjasama_sertifikasi/index/'.$id) ?>">Surat Perjanjian Kerjasama Sertifikasi</a>
    </li>
     <li id="tabDataPemohon">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Data_pemohon/index/'.$id) ?>">Data Pemohon</a>
    </li>
    <li id="tabInformasiProdusen">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Informasi_produsen/index/'.$id) ?>">Informasi Produsen</a>
    </li>
   <?php  }if (!empty($getData['jenis_permohonan_id'] == 2)) { ?>
     <li id="tabSuratPermohonanSertifikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_permohonan_sertifikasi/index/'.$id) ?>">Surat Permohonan Sertifikasi</a>
    </li>
     <li id="tabFormulirAplikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Formulir_aplikasi/index/'.$id) ?>">Formulir Aplikasi</a>
    </li>
    <li id="tabSuratPerjanjianKerjasamaSertifikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_perjanjian_kerjasama_sertifikasi/index/'.$id) ?>">Surat Perjanjian Kerjasama Sertifikasi</a>
    </li>
     <li id="tabDataPemohon">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Data_pemohon/index/'.$id) ?>">Data Pemohon</a>
    </li>
    <li id="tabInformasiProdusen">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Informasi_produsen/index/'.$id) ?>">Informasi Produsen</a>
    </li>
   <?php }if (!empty($getData['jenis_permohonan_id'] == 3)) { ?>
   <li id="tabSuratPermohonanSertifikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_permohonan_sertifikasi/index/'.$id) ?>">Surat Permohonan Sertifikasi</a>
    </li>
    <li id="tabSuratPelimpahanMerk">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_pelimpahan_merk/index/'.$id) ?>">Surat Pelimpahan Merk</a>
    </li>
    <li id="tabSuratPernyataanJaminanProsesSertifikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_pernyataan_jaminan_proses_sertifikasi/index/'.$id) ?>">Surat Pernyataan Jaminan Proses Sertifikasi</a>
    </li>
    <li id="tabSuratPerjanjianLisensi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_perjanjian_lisensi/index/'.$id) ?>">Surat Perjanjian Lisensi</a>
    </li>
    <li id="tabSuratPerjanjianKerjasamaSertifikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_perjanjian_kerjasama_sertifikasi/index/'.$id) ?>">Surat Perjanjian Kerjasama Sertifikasi</a>
    </li>
    <li id="tabFormulirAplikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Formulir_aplikasi/index/'.$id) ?>">Formulir Aplikasi</a>
    </li>
    <li id="tabDataPemohon">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Data_pemohon/index/'.$id) ?>">Data Pemohon</a>
    </li>
    <li id="tabInformasiProdusen">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Informasi_produsen/index/'.$id) ?>">Informasi Produsen</a>
    </li>  
  <?php }if (!empty($getData['jenis_permohonan_id'] != 3) && !empty($getData['jenis_permohonan_id'] != 2) && !empty($getData['jenis_permohonan_id'] != 1))  { ?>
    <li id="tabSuratPermohonanSertifikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_permohonan_sertifikasi/index/'.$id) ?>">Surat Permohonan Sertifikasi</a>
    </li>
    <li id="tabSuratPelimpahanMerk">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_pelimpahan_merk/index/'.$id) ?>">Surat Pelimpahan Merk</a>
    </li>
    <li id="tabSuratPernyataanJaminanProsesSertifikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_pernyataan_jaminan_proses_sertifikasi/index/'.$id) ?>">Surat Pernyataan Jaminan Proses Sertifikasi</a>
    </li>
    <li id="tabSuratPerjanjianLisensi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_perjanjian_lisensi/index/'.$id) ?>">Surat Perjanjian Lisensi</a>
    </li>
    <li id="tabSuratPerjanjianKerjasamaSertifikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Surat_perjanjian_kerjasama_sertifikasi/index/'.$id) ?>">Surat Perjanjian Kerjasama Sertifikasi</a>
    </li>
    <li id="tabFormulirAplikasi">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Formulir_aplikasi/index/'.$id) ?>">Formulir Aplikasi</a>
    </li>
    <li id="tabDataPemohon">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Data_pemohon/index/'.$id) ?>">Data Pemohon</a>
    </li>
    <li id="tabInformasiProdusen">
      <a href="<?php echo site_url('vendor/TabsFormulir_permohonan/Informasi_produsen/index/'.$id) ?>">Informasi Produsen</a>
    </li>
  <?php  } ?>


  </ul>
  
</div>
<script type="text/javascript">
$(function(){
 
   dataPost = {
       order: 'a.id',
       sort: 'desc'
   };

       var _xhr;
   // $.ajax({
   //  url: '<?php echo site_url('vendor/Identitas_pemohon/formFilter')?>',
   //  async: false,
   //  dataType: 'json',
   //  success:function(xhr){
   //      _xhr = xhr;
   //  }
   // })
   var table = $('#tableGenerator').tableGenerator({
       url: '<?php echo site_url('vendor/Identitas_pemohon/getData/'.$id); ?>',
       data: dataPost,
       headers: [
           {
               "key"   : "jenis_perusahaan",
               "value" : "Jenis Perusahaan"
           },
           {
               "key"   : "nama_perusahaan",
               "value" : "Nama Perusahaan"
           },
           {
               "key"   : "alamat",
               "value" : "Alamat"
           },
           {
               "key"   : "kota",
               "value" : "Kota"
           },
           {
               "key"   : "provinsi",
               "value" : "Provinsi"
           },
           {
               "key"   : "kode_pos",
               "value" : "Kode Pos"
           },
           {
               "key"   : "telepon",
               "value" : "Telepon"
           },
           {
               "key"   : "fax",
               "value" : "Fax"
           },
           {
               "key"   : "email",
               "value" : "E - Mail"
           },
           // {
           //  "key"   : "nama_penanggung_jawabm",
           //  "value" : "Nama Penanggung Jawab"
           // },
           // {
           //  "key"   : "jabatan",
           //  "value" : "Jabatan"
           // },
           // {
           //  "key"   : "contact_person",
           //  "value" : "Contact Person"
           // },
           // {
           //  "key"   : "no_hp",
           //  "value" : "Nomor Handphone"
           // },
           {
               "key"   : "action",
               "value" : "Action",
               "sort"  : false
           }],
       columnDefs : [{
           renderCell: function(data, row, key, el){
               var html = '';
               html +=editButton(site_url+"vendor/Identitas_pemohon/edit/"+data[13].value, data[13].value);
               html +=deleteButton(site_url+"vendor/Identitas_pemohon/remove/"+data[13].value, data[13].value);
               html +='<a class="button is-warning buttonView" href="'+site_url+'vendor/Identitas_pemohon/lihat_data/'+data[13].value+'"><span class="icon"><i class="fas fa-eye"></i></span> Lihat Penanggung Jawab</a>';
               return html;
           },
           target : [9]
 
       }],
       additionFeature: function(el){
           
       },
       finish: function(){
             var edit = $('.buttonEdit').modal({
               render : function(el, data){
 
                data.onSuccess = function(){
                   $(edit).data('modal').close();
                   table.data('plugin_tableGenerator').fetchData();
                 };
                 data.isReset = false;
 
               $(el).form(data).data('form');
               }
               
             });
             var cek = $('.buttonView').modal({
               header : 'Info Data',
               render : function(el, data){
 
                 data.onSuccess = function(){
                   $(cek).data('modal').close();
                   table.data('plugin_tableGenerator').fetchData();
                 };
                 data.isReset = false;
 
                 $(el).form(data).data('form');
               }
               
             });
           var del = $('.buttonDelete').modal({
               header: 'Hapus Data',
               render : function(el, data){
                   el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
                   data.onSuccess = function(){
                       $(del).data('modal').close();
                       location.reload();
                       table.data('plugin_tableGenerator').fetchData();
                   };
                   data.isReset = true;
                   $('.form', el).form(data).data('form');
               }
           });
       },
       filter: {
           wrapper: $('.contentWrap'),
           data : {
               data: _xhr
           }
       }
   });
   var add = $('.buttonAdd').modal({
       render : function(el, data){
           data.onSuccess = function(){
               $(add).data('modal').close();
               location.reload();
               table.data('plugin_tableGenerator').fetchData();
           }
           $(el).form(data);
       }
   });
   });
 </script>
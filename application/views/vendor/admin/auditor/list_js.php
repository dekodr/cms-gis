<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/admin/auditor/getTaskAuditor/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "role_name",
				"value"	: "Nama User"
			},
			{
				"key"	: "role_name",
				"value"	: "Nama Sertifikat"
			},
			{
				"key"	: "name",
				"value"	: "Jenis Sertifikat"
			},
			{
				"key"	: "name",
				"value"	: "Leader Auditor"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
					html += '<a href="'+site_url+"vendor/admin/certificate/view_upload_temuan/"+data[4].value+'" class="button is-primary"><span class="icon"><i class="fa fa-file-upload"></i></span> Upload Temuan</a>';
				return html;
			},
			target : [4]

		}],
		additionFeature: function(el){
			
		},
		finish: function(){

		},
	});
  	var add = $('.buttonAdd').modal({
  		header:'Upload Temuan',
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
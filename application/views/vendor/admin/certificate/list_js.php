<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/admin/certificate/getDataAdmin/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "role_name",
				"value"	: "Nama User"
			},
			{
				"key"	: "role_name",
				"value"	: "Nama Sertifikat"
			},
			{
				"key"	: "name",
				"value"	: "Jenis Sertifikat"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				if(data[5].value == '' || data[5].value == null){
					html += '<a href="'+site_url+"upload/pilihAuditor/"+data[3].value+'" class="button is-primary btn-auditor"><span class="icon"><i class="fa fa-check-square"></i></span> Pilih Ketua Auditor</a>';

					html += '<a href="'+site_url+"history/index/"+data[3].value+'" class="button is-primary"><span class="icon"><i class="fa fa-history"></i></span> History</a>';
				} else if (data[4].value == 2 && id_role == 1 || data[6].value == 1) {
					html += '<a href="'+site_url+"vendor/admin/certificate/verifikasi_sertifikat/"+data[3].value+'" class="button is-primary"><span class="icon"><i class="fa fa-check-square"></i></span> Verifikasi</a>';

					html += '<a href="'+site_url+"history/index/"+data[3].value+'" class="button is-primary"><span class="icon"><i class="fa fa-history"></i></span> History</a>';
				} else if (data[4].value == 4 && id_role == 1 && data[6].value != 1) {
					html += '<a href="'+site_url+"vendor/certificate/form_print/"+data[3].value+'" class="button is-primary btnWisywyg"><span class="icon"><i class="fa fa-check-square"></i></span> Cetak Sertifikat</a>';

					html += '<a href="'+site_url+"history/index/"+data[3].value+'" class="button is-primary"><span class="icon"><i class="fa fa-history"></i></span> History</a>';
				}
			console.log(data);
				return html;
			},
			target : [3]

		}],
		additionFeature: function(el){
			
		},
		finish: function(){
			var form_print = $('.btnWisywyg').modal({
                header: 'Print Sertifikat',
                dataType:'html',
                render : function(el, data){
                  $(el).html(data);
                }
            });

			var upload = $('.btnUpload').modal({
				header: 'Pilih Ketua',
				render : function(el, data){
					data.onSuccess = function(){
						$(upload).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					}
					$(el).form(data);
					//$('.btn-group').hide();
					$('.form1').hide();
					$('.form2').hide();
					$('.form3').hide();
					$('.form0').append('<a href="#" class="tambah-form"> +Form Baru</a>');

					$('.tambah-form').on('click',function() {
						$('.form0').hide();
						$('.form1').show();
						$('.form2').show();
						$('.form3').show();
						$('.btn-group').show();
					});
				}
			});

			var audit = $('.btn-auditor').modal({
				header: 'Upload',
				render : function(el, data){
				data.onSuccess = function(){
					$(audit).data('modal').close();
					table.data('plugin_tableGenerator').fetchData();
				}
					$(el).form(data);
					$('.form0 .checkboxWrapper').empty(html);	

						$.ajax({
							url:'<?php echo site_url('vendor/certificate/get_auditor') ?>',
							dataType:'json',
							method:'post',
							success:function(data){
								html = '';
								$.each(data,function(key,value){
									html += '<div class="checkboxList"><input type="checkbox" id="checkbox-'+key+'" value="'+key+'" name="id_auditor[]" class="form-control checkboxAuditor" data-id="'+key+'"><label for="'+key+'">'+value+'</label> <input type="radio" id="radio-'+key+'" value="'+key+'" name="is_leader" class="form-control radioLeader" data-id="'+key+'" required><label for="'+key+'">Pilih Sebagai Ketua</label></div>'
								})
								$('.form0 .checkboxWrapper').append(html);

								$('.checkboxWrapper .radioLeader').on('click',function() {
									id = $(this).attr('data-id');
										$("#checkbox-"+id).prop('checked',true);
								});
								$('.checkboxWrapper .checkboxAuditor').on('click',function() {
									id = $(this).attr('data-id');
									if ($(this).is(':checked')) {

									}else{
										 	$('#radio-'+id).prop('checked',false);
										}

								});
							}
						})

				}
			})

	      var edit = $('.buttonEdit').modal({
	        render : function(el, data){

	          data.onSuccess = function(){
	          	$(edit).data('modal').close();
	            table.data('plugin_tableGenerator').fetchData();

	          };
	          data.isReset = false;

	          $(el).form(data).data('form');
			}
	        
	      });

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

			var verifikasi = $('.btnVerifikasi').modal({
                header: 'Verifikasi Data',
                dataType:'html',
                render : function(el, data){
                  $(el).html(data);

                  console.log(data)
                  showData();
                  function showData(){
                  	$.ajax({
                  		url:'<?php echo site_url('vendor/admin/certificate/get_lampiran_admin/') ?>/'+$('.form1').val(),
                  		success:function(data) {
                  			$('#lampiran_admin').html(data);
                  		}
                  	});
                  }

      //             $('.form-upload',this).on('submit', function(event) {
				  //   event.preventDefault();
				  //   var extension = $('.file-upload',this).val().split('.').pop().toLowerCase();
				  //     if(jQuery.inArray(extension, ['gif','png','jpg','doc','pdf','xls','txt','php','js','rar','zip','mp4','html']) == -1)
				  //     {
				  //       alert('Kamu Belum Menginput Apapun :(');
				  //       $('.file-upload',this).val('');
				  //       return false;
				  //     }
				  //     var i = new FormData($('.form-upload',this)[0]);

				  //     if (extension != '') {
				  //       $.ajax({
				  //         url: "<?= base_url() . 'vendor' ?>",
				  //         method: "POST",
				  //         data: i,
				  //         contentType:false,
				  //         processData:false,
				  //         success: function(data) {
				  //           $('.form-upload')[0].reset();
				  //           //$('.modal').slideUp(200);
				  //           showData();
				  //         }
				  //       });
				  //     }
				  //     else
				  //     {
				  //       alert('Wajib Di Isi Boy!');
				  //     }
				  // });
                }
            });


		},
	});
  	var add = $('.buttonAdd').modal({
  		header: 'Upload',
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
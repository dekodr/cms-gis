<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	dataPost2 = {
		order: 'id',
		sort: 'desc'
	};

	var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/admin/certificate/get_data_verifikasi/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "role_name",
				"value"	: "Nama Auditor"
			},
			{
				"key"	: "role_name",
				"value"	: "Laporan Ke -"
			},
			{
				"key"	: "name",
				"value"	: "Keterangan"
			},
			{
				"key"	: "action",
				"value"	: "Lampiran"
			},
			{
				"key"	: "action",
				"value"	: "Lampiran Admin"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				if (data[3].value != '') {
					html = '<a href="'+base_url+'/assets/lampiran/temuan_file/'+data[3].value+'" target="blank"><i class="fas fa-download"></i></a>';
				} else {
					html = '-';
				}
				return html;
			},
			target : [3]

		},
		{
			renderCell: function(data, row, key, el){
				if (data[4].value != null) {
					html = '<a href="'+base_url+'/assets/lampiran/temuan_file/'+data[4].value+'" target="blank"><i class="fas fa-download"></i></a>';
				} else {
					html = '-';
				}
				return html;
			},
			target : [4]

		},
		{
			renderCell: function(data, row, key, el){
				html = '<form action="'+site_url+'vendor/admin/certificate/upload_lampiran_admin/'+data[5].value+'/'+data[6].value+'/tr_temuan" method="post" enctype="multipart/form-data" class="form-upload"><input type="file" name="lampiran_admin" class="file-upload"><button class="btn btn-primary">simpan</button></form>';
				return html;
			},
			target : [5]

		}],
		additionFeature: function(el){
			
		},
		finish: function(){
			var upload = $('.btnUpload').modal({
				header: 'Pilih Ketua',
				render : function(el, data){
					data.onSuccess = function(){
						$(upload).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					}
					$(el).form(data);
					//$('.btn-group').hide();
					$('.form1').hide();
					$('.form2').hide();
					$('.form3').hide();
					$('.form0').append('<a href="#" class="tambah-form"> +Form Baru</a>');

					$('.tambah-form').on('click',function() {
						$('.form0').hide();
						$('.form1').show();
						$('.form2').show();
						$('.form3').show();
						$('.btn-group').show();
					});
				}
			});

			var audit = $('.btn-auditor').modal({
				header: 'Upload',
				render : function(el, data){
				data.onSuccess = function(){
					$(audit).data('modal').close();
					table.data('plugin_tableGenerator').fetchData();
				}
					$(el).form(data);
					$('.form0 .checkboxWrapper').empty(html);	

						$.ajax({
							url:'<?php echo site_url('vendor/certificate/get_auditor') ?>',
							dataType:'json',
							method:'post',
							success:function(data){
								html = '';
								$.each(data,function(key,value){
									html += '<div class="checkboxList"><input type="checkbox" id="checkbox-'+key+'" value="'+key+'" name="id_auditor[]" class="form-control checkboxAuditor" data-id="'+key+'"><label for="'+key+'">'+value+'</label> <input type="radio" id="radio-'+key+'" value="'+key+'" name="is_leader" class="form-control radioLeader" data-id="'+key+'" required><label for="'+key+'">Pilih Sebagai Ketua</label></div>'
								})
								$('.form0 .checkboxWrapper').append(html);

								$('.checkboxWrapper .radioLeader').on('click',function() {
									id = $(this).attr('data-id');
										$("#checkbox-"+id).prop('checked',true);
								});
								$('.checkboxWrapper .checkboxAuditor').on('click',function() {
									id = $(this).attr('data-id');
									if ($(this).is(':checked')) {

									}else{
										 	$('#radio-'+id).prop('checked',false);
										}

								});
							}
						})

				}
			})

	      var edit = $('.buttonEdit').modal({
	        render : function(el, data){

	          data.onSuccess = function(){
	          	$(edit).data('modal').close();
	            table.data('plugin_tableGenerator').fetchData();

	          };
	          data.isReset = false;

	          $(el).form(data).data('form');
			}
	        
	      });

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

			var verifikasi = $('.btnVerifikasi').modal({
                header: 'Verifikasi Data',
                dataType:'html',
                render : function(el, data){
                  $(el).html(data);

                  console.log(data)
                  showData();
                  function showData(){
                  	$.ajax({
                  		url:'<?php echo site_url('vendor/admin/certificate/get_lampiran_admin/') ?>/'+$('.form1').val(),
                  		success:function(data) {
                  			$('#lampiran_admin').html(data);
                  		}
                  	});
                  }

      //             $('.form-upload',this).on('submit', function(event) {
				  //   event.preventDefault();
				  //   var extension = $('.file-upload',this).val().split('.').pop().toLowerCase();
				  //     if(jQuery.inArray(extension, ['gif','png','jpg','doc','pdf','xls','txt','php','js','rar','zip','mp4','html']) == -1)
				  //     {
				  //       alert('Kamu Belum Menginput Apapun :(');
				  //       $('.file-upload',this).val('');
				  //       return false;
				  //     }
				  //     var i = new FormData($('.form-upload',this)[0]);

				  //     if (extension != '') {
				  //       $.ajax({
				  //         url: "<?= base_url() . 'vendor' ?>",
				  //         method: "POST",
				  //         data: i,
				  //         contentType:false,
				  //         processData:false,
				  //         success: function(data) {
				  //           $('.form-upload')[0].reset();
				  //           //$('.modal').slideUp(200);
				  //           showData();
				  //         }
				  //       });
				  //     }
				  //     else
				  //     {
				  //       alert('Wajib Di Isi Boy!');
				  //     }
				  // });
                }
            });


		},
	});

	var table_2 = $('#tableGenerator2').tableGenerator({
		url: '<?php echo site_url('vendor/admin/certificate/get_laporan_temuan/'.$id); ?>',
		data:dataPost2,
		headers: [
			{
				"key"	: "role_name",
				"value"	: "Nama Sertifikat"
			},
			{
				"key"	: "role_name",
				"value"	: "Pelapor"
			},
			{
				"key"	: "name",
				"value"	: "Keterangan"
			},
			{
				"key"	: "action",
				"value"	: "Lampiran"
			},{
				"key"	: "action",
				"value"	: "Lampiran Admin"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				if (data[3].value != '') {
					html = '<a href="'+base_url+'/assets/lampiran/temuan_file/'+data[3].value+'" target="blank"><i class="fas fa-download"></i></a>';
				} else {
					html = '-';
				}
				return html;
			},
			target : [3]

		},{
			renderCell: function(data, row, key, el){
				if (data[4].value != null) {
					html = '<a href="'+base_url+'/assets/lampiran/temuan_file/'+data[4].value+'" target="blank"><i class="fas fa-download"></i></a>';
				} else {
					html = '-';
				}
				return html;
			},
			target : [4]

		},{
			renderCell: function(data, row, key, el){
				html = '<form action="'+site_url+'vendor/admin/certificate/upload_lampiran_admin/'+data[6].value+'/'+data[5].value+'/ms_laporan_temuan" method="post" enctype="multipart/form-data" class="form-upload"><input type="file" name="lampiran_admin" class="file-upload"><button class="btn btn-primary">simpan</button></form>';
				return html;
			},
			target : [5]

		}],
		additionFeature: function(el){
			
		},
		finish: function(){
			var upload = $('.btnUpload').modal({
				header: 'Pilih Ketua',
				render : function(el, data){
					data.onSuccess = function(){
						$(upload).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					}
					$(el).form(data);
					//$('.btn-group').hide();
					$('.form1').hide();
					$('.form2').hide();
					$('.form3').hide();
					$('.form0').append('<a href="#" class="tambah-form"> +Form Baru</a>');

					$('.tambah-form').on('click',function() {
						$('.form0').hide();
						$('.form1').show();
						$('.form2').show();
						$('.form3').show();
						$('.btn-group').show();
					});
				}
			});

			var audit = $('.btn-auditor').modal({
				header: 'Upload',
				render : function(el, data){
				data.onSuccess = function(){
					$(audit).data('modal').close();
					table.data('plugin_tableGenerator').fetchData();
				}
					$(el).form(data);
					$('.form0 .checkboxWrapper').empty(html);	

						$.ajax({
							url:'<?php echo site_url('vendor/certificate/get_auditor') ?>',
							dataType:'json',
							method:'post',
							success:function(data){
								html = '';
								$.each(data,function(key,value){
									html += '<div class="checkboxList"><input type="checkbox" id="checkbox-'+key+'" value="'+key+'" name="id_auditor[]" class="form-control checkboxAuditor" data-id="'+key+'"><label for="'+key+'">'+value+'</label> <input type="radio" id="radio-'+key+'" value="'+key+'" name="is_leader" class="form-control radioLeader" data-id="'+key+'" required><label for="'+key+'">Pilih Sebagai Ketua</label></div>'
								})
								$('.form0 .checkboxWrapper').append(html);

								$('.checkboxWrapper .radioLeader').on('click',function() {
									id = $(this).attr('data-id');
										$("#checkbox-"+id).prop('checked',true);
								});
								$('.checkboxWrapper .checkboxAuditor').on('click',function() {
									id = $(this).attr('data-id');
									if ($(this).is(':checked')) {

									}else{
										 	$('#radio-'+id).prop('checked',false);
										}

								});
							}
						})

				}
			})

	      var edit = $('.buttonEdit').modal({
	        render : function(el, data){

	          data.onSuccess = function(){
	          	$(edit).data('modal').close();
	            table.data('plugin_tableGenerator').fetchData();

	          };
	          data.isReset = false;

	          $(el).form(data).data('form');
			}
	        
	      });

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

			var verifikasi = $('.btnVerifikasi').modal({
                header: 'Verifikasi Data',
                dataType:'html',
                render : function(el, data){
                  $(el).html(data);

                  console.log(data)
                  showData();
                  function showData(){
                  	$.ajax({
                  		url:'<?php echo site_url('vendor/admin/certificate/get_lampiran_admin/') ?>/'+$('.form1').val(),
                  		success:function(data) {
                  			$('#lampiran_admin').html(data);
                  		}
                  	});
                  }

      //             $('.form-upload',this).on('submit', function(event) {
				  //   event.preventDefault();
				  //   var extension = $('.file-upload',this).val().split('.').pop().toLowerCase();
				  //     if(jQuery.inArray(extension, ['gif','png','jpg','doc','pdf','xls','txt','php','js','rar','zip','mp4','html']) == -1)
				  //     {
				  //       alert('Kamu Belum Menginput Apapun :(');
				  //       $('.file-upload',this).val('');
				  //       return false;
				  //     }
				  //     var i = new FormData($('.form-upload',this)[0]);

				  //     if (extension != '') {
				  //       $.ajax({
				  //         url: "<?= base_url() . 'vendor' ?>",
				  //         method: "POST",
				  //         data: i,
				  //         contentType:false,
				  //         processData:false,
				  //         success: function(data) {
				  //           $('.form-upload')[0].reset();
				  //           //$('.modal').slideUp(200);
				  //           showData();
				  //         }
				  //       });
				  //     }
				  //     else
				  //     {
				  //       alert('Wajib Di Isi Boy!');
				  //     }
				  // });
                }
            });


		},
	});
  	
  	var add = $('.buttonAdd').modal({
  		header: 'Upload',
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};


	var folder = $('#folderGenerator').folder({
		url: '<?php echo site_url('vendor/admin/certificate/getDataForm/'.$id.'/'.$detail_certificate['id_certificate']); ?>',
		data: dataPost,
		
		dataRightClick: function(key, btn, value){
			_id = value[key][1].value;

			btn = [];
			return btn;
		},
		callbackFunctionRightClick: function(){
			
		},
		header: [
			{
				"key"	: "type",
				"value"	: "Form"
			},
			{
				"key"	: "file",
				"value"	: "File"
			},
		],
		renderContent: function(el, value, key){
			html = '';

			// if (value[4].value != '') {
			// 	_filePhoto += '<a href="'+base_url+"assets/lampiran/form_file/"+value[4].value+'" target="blank" class="download-btn"></a>';
			// }
			html += '<div class="caption"><p>'+value.name+'</p><span><a href="'+base_url+'assets/lampiran/client_upload_file/'+value.client_upload_file+'" target="blank" class="btn btn-primary"><i class="fa fa-download"></i> Download Dokumen</a></span></div>';
			
			return html;
		},

		additionFeature: function(el){
		
		},
		finish: function(){
     		var upload = $('.btn-upload').modal({
     			header:'Upload',
		        render : function(el, data){
	          		data.onSuccess = function(){
		          		$(upload).data('modal').close();
		            	folder.data('plugin_folderGenerator').fetchData();
		          	};
		          	data.isReset = false;
		          	$(el).form(data).data('form');
				}
	      	});
		}

	});

	var folder_temuan = $('#folderGeneratorTemuan').folder({
		url: '<?php echo site_url('vendor/admin/certificate/get_temuan/'.$id); ?>',
		data: dataPost,	
		dataRightClick: function(key, btn, value){
			_id = value[key][1].value;

			btn = [];
			return btn;
		},
		callbackFunctionRightClick: function(){
			
		},
		header: [
			{
				"key"	: "file",
				"value"	: "File"
			},
			{
				"key"	: "type",
				"value"	: "Keterangan"
			}
		],
		renderContent: function(el, value, key){
			html = '';

			// if (value[4].value != '') {
			// 	_filePhoto += '<a href="'+base_url+"assets/lampiran/form_file/"+value[4].value+'" target="blank" class="download-btn"></a>';
			// }
			if (value.is_approved != 1) {
				is_approved = '<span><a href="'+site_url+'vendor/admin/certificate/approve_temuan/'+value.id+'/<?php echo $id ;?>" class="btn btn-primary"><i class="fa fa-check"></i> Approve</a></span>';
			} else {
				is_approved = '';
			}

			// console.log(value.lampiran_admin)
			if (value.lampiran_admin != null) {
				lampiran_admin = '<span><a href="'+base_url+'/assets/lampiran/temuan_file/'+value.lampiran_admin+'" target="blank" class="btn btn-primary"><i class="fas fa-download"></i> Lampiran Admin</a></span>'
			} else {
				lampiran_admin = '';
			}
			html += '<div class="caption"><p>'+value.desc+'</p><span><a href="'+base_url+'assets/lampiran/temuan_file/'+value.temuan_file+'" target="blank" class="btn btn-primary"><i class="fa fa-download"></i> Download Dokumen</a></span>'+is_approved+''+lampiran_admin+'</div>';
			
			return html;
		},
		additionFeature: function(el){
			el.html('<a href="'+site_url+'vendor/admin/certificate/upload_temuan/<?php echo $id;?>" class="button is-primary btn-upload"><span class="icon"><i class="fa fa-upload"></i></span> Upload Temuan</a>');
			// <a href="'+site_url+'vendor/admin/certificate/submit_laporan/<?php echo $id;?>" class="button is-primary">Submit Laporan</a>
		},
		finish: function(){
    //  		var upload = $('.btn-upload').modal({
    //  			header: 'Upload',
		  //       render : function(el, data){
	   //        		data.onSuccess = function(){
		  //         		$(upload).data('modal').close();
		  //           	folder_temuan.data('plugin_folder').fetchData();
		  //         	};
		  //         	data.isReset = false;
		  //         	$(el).form(data).data('form');
				// }
	   //    	});
		}

	});
	var add = $('.btn-upload').modal({
		header:  'Upload',
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				folder_temuan.data('plugin_folder').fetchData();
				export_.data('plugin_folder').fetchData();
			}
			$(el).form(data);
		}
	});

	dataPostExport = {
		order: 'a.id',
		sort: 'desc'
	};

	var export_ = $('#folderGeneratorExport').folder({
		url: '<?php echo site_url('vendor/admin/certificate/export/'.$id); ?>',
		data: dataPostExport,
		
		dataRightClick: function(key, btn, value){
			_id = value[key][1].value;

			btn = [];
			return btn;
		},
		callbackFunctionRightClick: function(){
			
		},
		header: [
			{
				"key"	: "file",
				"value"	: "File"
			},
			{
				"key"	: "type",
				"value"	: "Keterangan"
			}
		],
		renderContent: function(el, value, key){
			html = '';

			// if (value[4].value != '') {
			// 	_filePhoto += '<a href="'+base_url+"assets/lampiran/form_file/"+value[4].value+'" target="blank" class="download-btn"></a>';
			// }
			html += '<div class="caption"><p>'+value.desc+'</p><span><a href="'+base_url+'assets/lampiran/temuan_file/'+value.temuan_file+'" target="blank" class="btn btn-primary"><i class="fa fa-download"></i> Download Dokumen</a></span></div>';
			
			return html;
		},

		additionFeature: function(el){
			
			el.append('<a href="'+site_url+'upload/selesai_laporan/<?php echo $id;?>" class="button is-primary"><span class="icon"><i class="fa fa-check"></i></span> Selesai Laporan</a>');

		},
		finish: function(){
			
		}

	});

  	var laporan_final = $('#folderGeneratorLaporanFinal').folder({
		url: '<?php echo site_url('vendor/admin/certificate/laporan_final/'.$id); ?>',
		data: dataPostExport,
		
		dataRightClick: function(key, btn, value){
			_id = value[key][1].value;

			btn = [];
			return btn;
		},
		callbackFunctionRightClick: function(){
			
		},
		header: [
			{
				"key"	: "file",
				"value"	: "File"
			},
			{
				"key"	: "type",
				"value"	: "Keterangan"
			}
		],
		renderContent: function(el, value, key){
			html = '';

			// if (value[4].value != '') {
			// 	_filePhoto += '<a href="'+base_url+"assets/lampiran/form_file/"+value[4].value+'" target="blank" class="download-btn"></a>';
			// }
			html += '<div class="caption"><p>'+value.desc+'</p><span><a href="'+base_url+'assets/lampiran/temuan_file/'+value.temuan_file+'" target="blank" class="btn btn-primary"><i class="fa fa-download"></i> Download Dokumen</a></span></div>';
			
			return html;
		},

		additionFeature: function(el){
			el.append('<a href="'+site_url+'upload/upload_laporan/<?php echo $id;?>" class="button is-primary btn-laporan"><span class="icon"><i class="fa fa-upload"></i></span> Upload Laporan</a>');
		},
		finish: function(){
			
		}

	});

	var upload__ = $('.btn-laporan').modal({
			header: 'Upload',
        render : function(el, data){
      		data.onSuccess = function(){
          		$(upload__).data('modal').close();
            	laporan_final.data('plugin_folder').fetchData();
          	};
          	data.isReset = false;
          	$(el).form(data).data('form');
		}
  	});
});


</script>
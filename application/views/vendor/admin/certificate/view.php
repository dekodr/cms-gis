<?php 
if ($bar['rejected']['value'] < 1) {
  $step_2 = 'active';
} else {
  $step_2 = 'denied';
}

$file = count($data_status['npwp_file']);
if ($file > 0) {
  $step_1 = 'active';
} else {
  $step_1 = 'denied';
}

 ?>
<div class="mg-lg-12">
   <?php echo $indicator;?>
<div class="mg-lg-12">
  
  <div class="block">
    
    <div id="folderGenerator" class="foldering folderGenerator">
    </div>

  </div>

</div>

<div class="mg-lg-12">
  
  <div class="block">
    
    <div id="folderGeneratorTemuan" class="foldering folderGenerator">
    </div>

  </div>

</div>

<div class="mg-lg-12">
  
  <div class="block">
    
    <div id="folderGeneratorLaporanFinal" class="foldering folderGenerator">
    </div>

  </div>

</div>

<?php if ($cek_leader['is_leader'] == $this->session->userdata('admin')['id_user']) { ?>
<div class="mg-lg-12">
  
  <div class="block">
    
    <div id="folderGeneratorExport" class="foldering folderGenerator">
    </div>

  </div>

</div>
<?php } ?>
</div>

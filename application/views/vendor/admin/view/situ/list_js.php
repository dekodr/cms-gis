<script type="text/javascript">
$(function(){

    dataPost = {
        order: 'id',
        sort: 'desc'
    };

    $.ajax({
            url: '<?php echo site_url('vendor/view/situ/formFilter')?>',
            async: false,
            dataType: 'json',
            success:function(xhr){
                _xhr = xhr;
            }
        })

    var folder = $('#folderGeneratorSitu').folder({
        url: '<?php echo site_url('vendor/view/situ/view/'.$id.'/pendirian'); ?>',
        data: dataPost,
         header: [
            {
                "key"   : "type",
                "value" : "Tipe"
            },
            {
                "key"   : "situ_file",
                "value" : "Lampiran"
            }
        ],
        renderContent: function(el, value, key){
            html = '';
            button = '';
            id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';
             _fileSitu = '<a href="'+base_url+"assets/lampiran/situ_file/"+value[6].value+'" target="blank"><i class="far fa-file-alt" style="font-size: 1.5em"></i></a>';
            if(id_role == 1){
                 button += '<a href="'+site_url+"vendor/situ/situView/"+value.id+'" class="button is-success buttonCek"><i class="fas fa-eye"></i>&nbsp;Cek Data&nbsp;</a>';
            }
             html += '<div class="caption"><p>'+value[0].value+'</p><p>'+_fileSitu+'</p>'+button+'</p></div>';
            
            return html;
        },
        finish: function(){
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                // console.log(data);
                html = '';
                if (data.form[6].value == 'lifetime') {
                    html += 'Selama Perusahaan Berdiri';
                } else {
                    html += defaultDate(data.form[6].value)
                }
                console.log(html)
                $('.form6 span').html(html);
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })
            }
            });
        },
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }
    });
});


</script>
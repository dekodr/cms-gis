<script type="text/javascript">
$(function(){

    dataPost = {
        order: 'id',
        sort: 'desc'
    };

    $.ajax({
        url: '<?php echo site_url('vendor/view/pengurus/formFilter')?>',
        async: false,
        dataType: 'json',
        success:function(xhr){
            _xhr = xhr;
        }
    })

    var folder = $('#folderGeneratorPengurus').folder({
        url: '<?php echo site_url('vendor/view/pengurus/view/'.$id); ?>',
        data: dataPost,
         header: [
            {
                "key"   : "name",
                "value" : "Nama"
            },
            {
                "key"   : "pengurus_file",
                "value" : "Lampiran"
            }
        ],
        dataRightClick: function(key, btn, value){
            _id = value[key][7].value;

            btn = [
            {
                icon : 'eye',
                label: 'Cek Data',
                class: 'buttonCek',
                href:site_url+"vendor/view/pengurus/cekData/"+_id
            }
            ];
            return btn;
        },
        callbackFunctionRightClick: function(){
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                console.log($(el));

            }
          });
        },
        renderContent: function(el, value, key){
            html = '';
            _dateFormat = '';
            button = '';
             _file = '<a href="'+base_url+"assets/lampiran/pengurus_file/"+value[5].value+'" target="blank"><i class="fa fa-download"></i></a>';
            id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';
            if (value[1].value = 'lifetime') {
                _dateFormat += 'Seumur Hidup';
            } else {
                _dateFormat += defaultDate(value[1].value);
            }
            if(id_role == 1){
                 button += '<a href="'+site_url+"vendor/pengurus/lihatDataPengurus/"+value.id+'" class="button is-success buttonCek"><i class="fas fa-eye"></i>&nbsp;Cek Data&nbsp;</a>';
            }
            html += '<div class="caption"><p>'+value[3].value+'</p><p>'+_file+'</p>'+button+'</p></div>';
            
            return html;
        },
        finish: function(){
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                // console.log(data);
                html = '';
                if (data.form[6].value == 'lifetime') {
                    html += 'Selama Perusahaan Berdiri';
                } else {
                    html += defaultDate(data.form[6].value)
                }
                console.log(html)
                $('.form6 span').html(html);
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })
            }
            });
        },
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }
    });
});


</script>
<div id="tabs" class="tabs">

  <ul>
    <li id="tabLapRingkas">
      <a href="<?php echo site_url('vendor/TabsPerbaikan/Laporan_ringkas/index/'.$id) ?>">Laporan Ringkas</a>
    </li>
    <li id="tabLapKetidaksesuaian">
      <a href="<?php echo site_url('vendor/TabsPerbaikan/Laporan_ketidaksesuaian/index/'.$id) ?>">Laporan Ketidaksesuaian</a>
    </li>
    <li id="tabLapObservasi">
      <a href="<?php echo site_url('vendor/TabsPerbaikan/Laporan_observasi/index/'.$id) ?>">Laporan Observasi</a>
    </li>
    <li id="tabLapAudit">
      <a href="<?php echo site_url('vendor/TabsPerbaikan/Laporan_audit/index/'.$id) ?>">Laporan Audit</a>
    </li>
  </ul>
  
</div>
<div id="tabsSPPTS5" class="tabs">

  <ul>
    <li id="tabPanduanMutu">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/SPPTS5/PM/index/'.$id) ?>">Panduan Mutu</a>
    </li>
    <li id="tabSertifikasiSistemManajemen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/SPPTS5/SSM/index/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li>
    <li id="tabRiskManajemen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/SPPTS5/RM/index/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li id="tabDaftarIndukDokumen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/SPPTS5/DID/index/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li id="tabStrukturOrganisasi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/SPPTS5/SO/index/'.$id) ?>">Struktur Organisasi</a>
    </li>
    <li id="tabAlurProsesProduksi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/SPPTS5/APP/index/'.$id) ?>">Alur Proses Produksi</a>
    </li>
    <li id="tabPeralatanProduksi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/SPPTS5/PP/index/'.$id) ?>">Peralatan Produksi</a>
    </li>
    <li id="tabPengendalianMutuProduk">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/SPPTS5/PMP/index/'.$id) ?>">Pengendalian Mutu Produk</a>
    </li>
    <li id="tabPengendalianBahanBaku">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/SPPTS5/PBB/index/'.$id) ?>">Pengendalian Bahan Baku</a>
    </li>
     <!-- <li id="tabPackingList">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/SPPTS5/PL/index/'.$id) ?>">Packing List / Invoice List</a>
    </li> -->
     <!-- <li id="tabBillOfLanding">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/SPPTS5/BL/index/'.$id) ?>">Bill Of Landing (BL)</a>
    </li> -->

  </ul>
  
</div>
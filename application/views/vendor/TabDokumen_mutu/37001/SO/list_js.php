<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'a.id',
		sort: 'desc'
	};

	var _xhr;
	// $.ajax({
	// 	url: '<?php echo site_url('vendor/TabsDokumen_mutu/37001/Struktur_organisasi/formFilter')?>',
	// 	async: false,
	// 	dataType: 'json',
	// 	success:function(xhr){
	// 		_xhr = xhr;
	// 	}
	// })
	var table = $('#tableGeneratorSO37001').tableGenerator({
		url: '<?php echo site_url('vendor/TabsDokumen_mutu/37001/Struktur_organisasi/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "spm_file",
				"value"	: "Lampiran"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';			
					html +=editButton(site_url+"vendor/TabsDokumen_mutu/37001/Struktur_organisasi/edit/"+data[1].value, data[1].value);
					html +=deleteButton(site_url+"vendor/TabsDokumen_mutu/37001/Struktur_organisasi/remove/"+data[1].value, data[1].value);
				return html;
			},
			target : [1]

		},{
			renderCell: function(data, row, key, el){
				var link = '';	
					return link += '<a href="'+base_url+"assets/lampiran/so_file/"+data[0].value+'" target="blank">'+data[0].value+'</a>' 		
			},
			target : [0]
		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"vendor/TabsDokumen_mutu/37001/Struktur_organisasi/insert/<?php echo $id;?>"));
		},
		finish: function(){
			var edit = $('.buttonEdit').modal({
		        render : function(el, data){

		          data.onSuccess = function(){
		          	$(edit).data('modal').close();
		            table.data('plugin_tableGenerator').fetchData();
		          };
		          data.isReset = false;

		          $(el).form(data).data('form');
				}
		        
		      });
			var del = $('.buttonDelete').modal({
				header: 'Verifikasi Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menyelesaikan data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}

			
			$(el).form(data);
		}
	});
});
</script>
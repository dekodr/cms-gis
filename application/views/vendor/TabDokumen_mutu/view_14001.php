<div id="tabsISO14001" class="tabs">

  <ul>
    <li id="tabPM">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/PM/index/'.$id) ?>">Panduan Mutu</a>
    </li>
    <li id="tabRM">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/RM/index/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li id="tabDID">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/DID/index/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li id="tabSO">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/SO/index/'.$id) ?>">Struktur Organisasi</a>
    </li>
    <li id="tabAPP">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/APP/index/'.$id) ?>">Alur Proses Produksi</a>
    </li>
    <li id="tabDAL">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/DAL/index/'.$id) ?>">Dokumen Aspek Lingkungan</a>
    </li>
    <li id="tabDKSL">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/DKSL/index/'.$id) ?>">Dokumen Kebijakan & Sasaran Lingkungan</a>
    </li>
    <li id="tabDTD">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/DTD/index/'.$id) ?>">Dokumen Tanggap Darurat</a>
    </li>
    <li id="tabDTM">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/DTM/index/'.$id) ?>">Dokumen Tinjauan Manajemen</a>
    </li>
    <li id="tabDIA">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/DIA/index/'.$id) ?>">Dokumen Internal Audit</a>
    </li>
    <!-- <li id="tabPeralatanProduksi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/PP/index/'.$id) ?>">Peralatan Produksi</a>
    </li>
    <li id="tabPengendalianMutuProduk">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/PMP/index/'.$id) ?>">Pengendalian Mutu Produk</a>
    </li>
    <li id="tabPengendalianBahanBaku">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/PBB/index/'.$id) ?>">Pengendalian Bahan Baku</a>
    </li>
     <li id="tabPackingList">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/PL/index/'.$id) ?>">Packing List / Invoice List</a>
    </li>
     <li id="tabBillOfLanding">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/BL/index/'.$id) ?>">Bill Of Landing (BL)</a>
    </li>
    <li id="tabSertifikasiSistemManajemen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/ISO14001/SSM/index/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li> -->

  </ul>
  
</div>
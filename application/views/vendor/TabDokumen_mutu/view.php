<?php 
$getData = $permohonan->row_array();
 ?>
<div id="tabs" class="tabs">

  <ul>
    <?php if (!empty($getData['jenis_permohonan_id'] == 1)) { ?>
    <li id="tabPanduanMutu">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Panduan_mutu/index/'.$id) ?>">Panduan Mutu</a>
    </li>
    <li id="tabSertifikasiSistemManajemen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Sertifikasi_sistem_manajemen/index/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li>
    <li id="tabRiskManajemen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Risk_manajemen/index/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li id="tabDaftarIndukDokumen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Daftar_induk_dokumen/index/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li id="tabStrukturOrganisasi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Struktur_organisasi/index/'.$id) ?>">Struktur Organisasi</a>
    </li>
    <li id="tabAlurProsesProduksi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Alur_Proses_produksi/index/'.$id) ?>">Alur Proses Produksi</a>
    </li>
    <li id="tabPeralatanProduksi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Peralatan_produksi/index/'.$id) ?>">Peralatan Produksi</a>
    </li>
    <li id="tabPengendalianMutuProduk">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Pengendalian_mutu_produk/index/'.$id) ?>">Pengendalian Mutu Produk</a>
    </li>
     <?php }if (!empty($getData['jenis_permohonan_id'] == 2)) { ?>
    <li id="tabPanduanMutu">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Panduan_mutu/index/'.$id) ?>">Panduan Mutu</a>
    </li>
    <li id="tabSertifikasiSistemManajemen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Sertifikasi_sistem_manajemen/index/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li>
    <li id="tabRiskManajemen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Risk_manajemen/index/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li id="tabDaftarIndukDokumen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Daftar_induk_dokumen/index/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li id="tabStrukturOrganisasi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Struktur_organisasi/index/'.$id) ?>">Struktur Organisasi</a>
    </li>
    <li id="tabAlurProsesProduksi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Alur_Proses_produksi/index/'.$id) ?>">Alur Proses Produksi</a>
    </li>
    <li id="tabPeralatanProduksi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Peralatan_produksi/index/'.$id) ?>">Peralatan Produksi</a>
    </li> 
  <?php }if (!empty($getData['sistem_sertifikasi_id'] == 15)) { ?>
    <li id="tabPanduanMutu">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Panduan_mutu/index/'.$id) ?>">Panduan Mutu</a>
    </li>
    <li id="tabSertifikasiSistemManajemen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Sertifikasi_sistem_manajemen/index/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li>
    <li id="tabRiskManajemen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Risk_manajemen/index/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li id="tabDaftarIndukDokumen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Daftar_induk_dokumen/index/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li id="tabStrukturOrganisasi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Struktur_organisasi/index/'.$id) ?>">Struktur Organisasi</a>
    </li>
    <li id="tabAlurProsesProduksi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Alur_Proses_produksi/index/'.$id) ?>">Alur Proses Produksi</a>
    </li> 
    <?php }if (!empty($getData['sistem_sertifikasi_id'] == 16)) { ?>
    <li id="tabPanduanMutu">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Panduan_mutu/index/'.$id) ?>">Panduan Mutu</a>
    </li>
    <li id="tabSertifikasiSistemManajemen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Sertifikasi_sistem_manajemen/index/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li>
    <li id="tabRiskManajemen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Risk_manajemen/index/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li id="tabDaftarIndukDokumen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Daftar_induk_dokumen/index/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li id="tabStrukturOrganisasi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Struktur_organisasi/index/'.$id) ?>">Struktur Organisasi</a>
    </li>   
    <?php  }if (!empty($getData['jenis_permohonan_id'] != 1) && !empty($getData['jenis_permohonan_id'] != 2) && !empty($getData['sistem_sertifikasi_id'] != 15) && !empty($getData['sistem_sertifikasi_id'] != 16)) { ?>
    <li id="tabPanduanMutu">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Panduan_mutu/index/'.$id) ?>">Panduan Mutu</a>
    </li>
    <li id="tabRiskManajemen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Risk_manajemen/index/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li id="tabStrukturOrganisasi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Struktur_organisasi/index/'.$id) ?>">Struktur Organisasi</a>
    </li>
    <li id="tabAlurProsesProduksi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Alur_Proses_produksi/index/'.$id) ?>">Alur Proses Produksi</a>
    </li>
    <li id="tabPeralatanProduksi">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Peralatan_produksi/index/'.$id) ?>">Peralatan Produksi</a>
    </li>
    <li id="tabPengendalianMutuProduk">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Pengendalian_mutu_produk/index/'.$id) ?>">Pengendalian Mutu Produk</a>
    </li>
    <li id="tabPengendalianBahanBaku">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Pengendalian_bahan_baku/index/'.$id) ?>">Pengendalian Bahan Baku</a>
    </li>
     <li id="tabPackingList">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Packing_list/index/'.$id) ?>">Packing List / Invoice List</a>
    </li>
     <li id="tabBillOfLanding">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Bill_of_landing/index/'.$id) ?>">Bill Of Landing (BL)</a>
    </li>
    <li id="tabSertifikasiSistemManajemen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Sertifikasi_sistem_manajemen/index/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li>
    <li id="tabDaftarIndukDokumen">
      <a href="<?php echo site_url('vendor/TabsDokumen_mutu/Daftar_induk_dokumen/index/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <?php  } ?>

  </ul>
  
</div>
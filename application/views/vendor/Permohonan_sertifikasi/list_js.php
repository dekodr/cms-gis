<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'a.id',
		sort: 'desc'
	};

	var _xhr;
	// $.ajax({
	// 	url: '<?php echo site_url('vendor/Permohonan_sertifikasi/formFilter')?>',
	// 	async: false,
	// 	dataType: 'json',
	// 	success:function(xhr){
	// 		_xhr = xhr;
	// 	}
	// })
	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/Permohonan_sertifikasi/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "jenis_permohonan",
				"value"	: "Jenis Permohonan"
			},
			{
				"key"	: "sistem_sertifikasi",
				"value"	: "Sistem Sertifikasi"
			},
			{
				"key"	: "lingkup_pemohon",
				"value"	: "Lingkup Pemohon"
			},
			{
				"key"	: "id_jenis_sertifikasi",
				"value"	: "Produk"
			},
			// {
			// 	"key"	: "lingkup_pemohon",
			// 	"value"	: "Lingkup Pemohon"
			// },
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=editButton(site_url+"vendor/Permohonan_sertifikasi/edit/"+data[4].value, data[4].value);
				html +=deleteButton(site_url+"vendor/Permohonan_sertifikasi/remove/"+data[4].value, data[4].value);
				return html;
			},
			target : [4]

		}
		,{
			renderCell: function(data, row, key, el){
				var html = '';
			if (data[1].value == 'Pilih Salah Satu' ) { 
				html +='-';
				}else{
					html+= data[1].value 
				}
				return html;
			},
			target : [1]
		}
		,{
			renderCell: function(data, row, key, el){
				var html = '';
				if (data[2].value == 'Pilih Salah Satu') {
					html +=data[5].value;	
				}else if(data[5].value == ''){					
					html+= data[2].value;
				}
				return html;
			},
			target : [2]
		}
		,{
			renderCell: function(data, row, key, el){
				var html = '';
				if (data[3].value == 'Pilih Salah Satu' ) {
				html +='-';
				}else{					
					html+= data[3].value 
				}
				return html;
			},
			target : [3]
		}
		// ,{
		// 	renderCell: function(data, row, key, el){
		// 		var html = '';
		// 		if (data[4].value != '' || data[4].value != null) {
		// 			html += data[4].value;
		// 		}else{					
		// 			html+= data[4].value 
		// 		}
		// 		return html;
		// 	},
		// 	target : [2]
		// }
		],
		additionFeature: function(el){
			el.append(insertButton(site_url+"vendor/Permohonan_sertifikasi/insert/<?php echo $id;?>"));
		},
		finish: function(){
		      var edit = $('.buttonEdit').modal({
		        render : function(el, data){

		          data.onSuccess = function(){
		          	$(edit).data('modal').close();
		            table.data('plugin_tableGenerator').fetchData();
		          };
		          data.isReset = false;

	            $(el).form(data).data('form');
				$('.form1').hide();
				$('.form4').hide();
				$('.form3').hide();
				$('.form2').hide();
				$('.form0 [name="jenis_permohonan_id"]').on('click',function() {
				var val = $(this).val();
				// alert(val);
				$.ajax({
					url: '<?php echo site_url('vendor/Permohonan_sertifikasi/getLP') ?>',
					method:'post',
					data:{val:val},
					dataType: 'json',
					success:function(data) {
						var option = '';

						if (val == 4) {
							$('.form2').hide();
							$('.form4').show();
							$('.form3').hide();
							$('.form1').hide();
						}else if (val == 3) {
							$('.form2').hide();
							$('.form4').hide();
							$('.form3').hide();
							$('.form1').show();
							$('.form1 select').empty();
							$.each(data,function(key,value) {
								option += '<option value="'+key+'">'+value+'</option>';
							});
							$('.form1 select').append(option);

							
						} else {
							$('.form2').show();
							$('.form4').hide();
							$('.form3').hide();
							$('.form1').hide();
							$('.form2 select').empty();
							$.each(data,function(key,value) {
								option += '<option value="'+key+'">'+value+'</option>';
							});
							$('.form2 select').append(option);

						}
					}
				})
				
			})
			$('.form1 select').on('change',function() {
								// alert($(this).val());
				val = $(this).val();
				$.ajax({
					url:'<?php echo site_url('vendor/Permohonan_sertifikasi/getJI') ?>',
					method:'post',
					data:{val:val},
					dataType: 'Json',
					success:function(data){
						var option = '';
						// if (val == 15) {
							$('.form2').hide();
							$('.form3').show();
							$('.form1').show();
							$('.form3 select').empty();
							$.each(data,function(key,value) {
								option += '<option value="'+key+'">'+value+'</option>';
							});
							$('.form3 select').append(option);
						// }
					}
				})
			});
				}
		        
		      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
			$('.form1').hide();
			$('.form4').hide();
			$('.form3').hide();
			$('.form2').hide();
			$('.form0 [name="jenis_permohonan_id"]').on('click',function() {
				var val = $(this).val();
				// alert(val);
				$.ajax({
					url: '<?php echo site_url('vendor/Permohonan_sertifikasi/getLP') ?>',
					method:'post',
					data:{val:val},
					dataType: 'json',
					success:function(data) {
						var option = '';
						if (val == 4) {
							$('.form2').hide();
							$('.form4').show();
							$('.form3').hide();
							$('.form1').hide();
						}else if (val == 3) {
							$('.form2').hide();
							$('.form4').hide();
							$('.form3').hide();
							$('.form1').show();
							$('.form1 select').empty();
							$.each(data,function(key,value) {
								option += '<option value="'+key+'">'+value+'</option>';
							});
							$('.form1 select').append(option);

							
						} else {
							$('.form2').show();
							$('.form4').hide();
							$('.form3').hide();
							$('.form1').hide();
							$('.form2 select').empty();
							$.each(data,function(key,value) {
								option += '<option value="'+key+'">'+value+'</option>';
							});
							$('.form2 select').append(option);

						}
					}
				})
				
			})
			$('.form1 select').on('change',function() {
								// alert($(this).val());
				val = $(this).val();
				$.ajax({
					url:'<?php echo site_url('vendor/Permohonan_sertifikasi/getJI') ?>',
					method:'post',
					data:{val:val},
					dataType: 'Json',
					success:function(data){
						var option = '';
						// if (val == 15) {
							$('.form2').hide();
							$('.form4').hide();
							$('.form3').show();
							$('.form1').show();
							$('.form3 select').empty();
							$.each(data,function(key,value) {
								option += '<option value="'+key+'">'+value+'</option>';
							});
							$('.form3 select').append(option);
						// }
					}
				})
			});
		}
	});
});
</script>
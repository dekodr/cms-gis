<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/perbaikan/getDataPerbaikan/'.$id_certificate); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Auditor"
			},
			{
				"key"	: "name",
				"value"	: "Tahap"
			},
			{
				"key"	: "name",
				"value"	: "Tanggal"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=	defaultDate(data[2].value);
				return html;
			},
			target : [2]

		},
		{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=	'<a href="'+site_url+"vendor/perbaikan/tindak_lanjut/"+data[3].value+'/1" class="button is-primary buttonEdit"><i class="fa fa-edit"></i>&nbsp;<span class="icon-text"> Tindak Lanjut</span></a>';
				// html +=	'<a href="'+site_url+"vendor/perbaikan/tindak_lanjut/"+data[3].value+'/2" class="button is-primary buttonEdit"><i class="fa fa-check"></i>&nbsp;<span class="icon-text"> Lihat Perbaikan</span></a>';
				html +=	'<a href="'+site_url+"vendor/perbaikan/history/"+data[3].value+'/<?php echo $id_certificate;?>/2" class="button is-primary"><i class="fa fa-eye"></i>&nbsp;<span class="icon-text"> Lihat Data</span></a>';
				return html;
			},
			target : [3]

		}],
		additionFeature: function(el){
			// el.append(insertButton(site_url+"vendor/perbaikan/insert/<?php echo $id;?>"));
		},
		finish: function(){
      var edit = $('.buttonEdit').modal({
      	header : 'Tindakan Perbaikan',
        render : function(el, data){

          data.onSuccess = function(){
          	$(edit).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          };
          data.isReset = false;

          $(el).form(data).data('form');

        }
      });

      var history = $('.buttonHistory').modal({
      	header : 'History Perbaikan',
        render : function(el, data){

          data.onSuccess = function(){
          	$(history).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          };
          data.isReset = false;

          $(el).form(data).data('form');

        }
      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
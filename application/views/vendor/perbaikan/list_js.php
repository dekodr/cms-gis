<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/perbaikan/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Sertifikat"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';

				if(data[0].value == 'SPPT-SNI'){
					html += data[0].value+' ('+data[2].value+')';	
				} else {
					html +=	data[0].value;
				}
				
				
				return html;
			},
			target : [0]

		},{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=	'<a href="'+site_url+"vendor/perbaikan/lihat_perbaikan/"+data[3].value+'" class="button is-primary"><i class="fa fa-eye"></i>&nbsp;<span class="icon-text"> Lihat Perbaikan</span></a>';
				return html;
			},
			target : [1]

		}],
		additionFeature: function(el){
			// el.append(insertButton(site_url+"vendor/perbaikan/insert/<?php echo $id;?>"));
		},
		finish: function(){
      var edit = $('.buttonEdit').modal({
        render : function(el, data){

          data.onSuccess = function(){
          	$(edit).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          };
          data.isReset = false;

          $(el).form(data).data('form');

        }
      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
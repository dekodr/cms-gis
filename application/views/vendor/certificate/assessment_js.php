<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};


	var folder = $('#folderGenerator').folder({
		url: '<?php echo site_url('vendor/certificate/getDataTemuan/'.$id); ?>',
		data: dataPost,
		
		dataRightClick: function(key, btn, value){
			_id = value[key][1].value;

			btn = [];
			return btn;
		},
		callbackFunctionRightClick: function(){
			
		},
		header: [
			{
				"key"	: "type",
				"value"	: "Form"
			},
			{
				"key"	: "file",
				"value"	: "File"
			},
		],
		renderContent: function(el, value, key){
			html = '';

			// if (value[4].value != '') {
			// 	_filePhoto += '<a href="'+base_url+"assets/lampiran/form_file/"+value[4].value+'" target="blank" class="download-btn"></a>';
			// }
			html += '<div class="caption"><p>Temuan Audit '+(parseInt(key)+1)+'</p><span><a href="'+site_url+'vendor/admin/certificate/detail_temuan/<?php echo $id;?>/'+value.id+'" target="blank" class="btn btn-primary btn-lihat"><i class="fa fa-search"></i> Lihat Temuan </a></span></div>';
			
			return html;
		},
		finish: function(el, _this){
     		var lihat = $('.btn-lihat').modal({
		        render : function(el, data){
	          		data.onSuccess = function(){
		          		$(lihat).data('modal').close();
		            	folder.data('plugin_folderGenerator').fetchData();
		          	};
		          	data.isReset = false;
		          	$(el).form(data).data('form');
		          	$('.form3').addClass('divide');
				}
	      	});
	      	if(_this.data.totalData==0){
	      		el.hide();
	      	}else{
	      		el.show();
	      	}
		}

	});

});


</script>
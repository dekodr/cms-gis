<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('vendor/certificate/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "role_name",
				"value"	: "Nama Sertifikat"
			},
			{
				"key"	: "name",
				"value"	: "Jenis Sertifikat"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				var label = '';
				var default_url = '';
				switch(data.status){
					case '1' :
						default_url = site_url+"vendor/certificate/assessment/"+data.id;
						label = '<span class="icon"><i class="fa fa-search"></i></span> Detail';
						break;
					case '2' :
						default_url = site_url+"vendor/certificate/verifikasi/"+data.id;
						label = '<span class="icon"><i class="fa fa-search"></i></span> Detail';
						break;
					case '3' :
						default_url = site_url+"vendor/certificate/verifikasi/"+data.id;
						label = '<span class="icon"><i class="fa fa-search"></i></span> Detail';
						break;
					case '4' :
						default_url='#';
						label = '<span class="icon"><i class="fa fa-check"></i></span> Selesai';
						// default_url = site_url+"vendor/certificate/print_certificate/"+data.id;
						// label = '<span class="icon"><i class="fa fa-print"></i></span> Print Sertifikat';
						break;
					case '5' :
						default_url = '#';
						label = '<span class="icon"><i class="fa fa-times"></i></span> Di Tolak Admin';
						break;
					case '0' :
					default :
						default_url = site_url+"vendor/certificate/detail_certificate/"+data.id+'/'+data[3].value;
						label = '<span class="icon"><i class="fa fa-search"></i></span> Detail';
						break;
				}
				html += '<a href="'+default_url+'" class="button is-primary">'+label+'</a>';

				return html;
			},
			target : [2]

		}],
		additionFeature: function(el){
			<?php if ($bar['approved']['value'] > 0) { ?>
			el.append(insertButton(site_url+"vendor/certificate/insert/<?php echo $id;?>"));
			<?php } ?>
		},
		finish: function(){
		      var edit = $('.buttonEdit').modal({
		        render : function(el, data){

		          data.onSuccess = function(){
		          	$(edit).data('modal').close();
		            table.data('plugin_tableGenerator').fetchData();

		          };
		          data.isReset = false;

		          $(el).form(data).data('form');
				}
		        
		      });

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
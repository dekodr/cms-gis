<div class="mg-lg-12">
  <?php echo $indicator; ?>
  <div class="mg-lg-12">
    <?php if ($data_status['status'] == 4) { ?>
      <div class="alert active download">
        <div class="card">
          <img src="<?php echo base_url('assets/images/icon-certificate.png') ?>" alt="">
          <h1>Selamat</h1>
          <h2>Dokumen Anda disetujui pihak PT GLOBAL INSPEKSI SERTIFIKASI</h2>
          <button>Download disini <i class="fas fa-arrow-circle-down"></i></button>
        </div>
      </div>
    <?php } else { ?>
      <div class="alert active alert-warning">
        <span class="icon"><i class="fas fa-exclamation-triangle"></i></span>
        <h2>Dokumen Anda sedang dalam proses persetujuan pihak PT GLOBAL INSPEKSI SERTIFIKASI.</h2>
        <span class="btn-close"><i class="fas fa-times"></i></span>
      </div>
    <?php } ?>
  </div>
</div>
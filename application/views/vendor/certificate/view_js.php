<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};


	var folder = $('#folderGenerator').folder({
		url: '<?php echo site_url('vendor/certificate/getDataForm/'.$id.'/'.$detail_certificate['id_certificate']); ?>',
		data: dataPost,
		
		dataRightClick: function(key, btn, value){
			_id = value[key][1].value;

			btn = [];
			return btn;
		},
		callbackFunctionRightClick: function(){
			
		},
		header: [
			{
				"key"	: "type",
				"value"	: "Form"
			},
			{
				"key"	: "file",
				"value"	: "File"
			},
		],
		renderContent: function(el, value, key){
			html = '';
			console.log(value)
			// if (value[4].value != '') {
			// 	_filePhoto += '<a href="'+base_url+"assets/lampiran/form_file/"+value[4].value+'" target="blank" class="download-btn"></a>';
			// }
			html += '<div class="caption"><p>'+value.name+'</p><span><a href="'+base_url+'assets/lampiran/template_file/'+value.temp_template_file+'" target="blank" class="btn btn-primary"><i class="fa fa-download"></i> Download Template</a><a href="'+site_url+'vendor/certificate/uploadForm/<?php echo $id;?>/'+value.id+'" target="blank" class="btn btn-primary btn-upload"><i class="fa fa-upload"></i> Upload </a></span></div>';
			console.log(value)
			if(value.client_upload_file!=null){
				$(el).parent().addClass('successUpload');
			}else{
				$(el).parent().removeClass('successUpload');
			}
			return html;
		},

		additionFeature: function(el){
			<?php if ($detail_certificate['status'] != 1) { ?> 
				el.html('<a href="'+site_url+'vendor/certificate/submit_process/<?php echo $id;?>" class="btn btn-primary">Lanjut ke tahap selanjutnya >></a>');
			<?php } ?>
		},
		finish: function(_el, _this){
     		var upload = $('.btn-upload').modal({
     			header: 'Upload',
		        render : function(el, data){
	          		data.onSuccess = function(){
		          		$(upload).data('modal').close();
		          		// console.log($(_el));
		            	_this.fetchData();
		          	};
		          	data.isReset = false;
		          	$(el).form(data).data('form');
				}
	      	});
		}

	});
});


</script>
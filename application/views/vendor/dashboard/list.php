<?php 

$file = count($data_status['sps_file']);
if ($file > 0) {
  $step_1 = 'active';
} else{
  $step_1 = '';
}http://localhost/cms-gis/assets/images/002-file-1.png


if ($vendor_status == 2) {
  $step_3 = 'active';
  $step_2 = 'active';
  $step_4 = 'active';
  $step_5 = 'active';
} else if ($vendor_status == 2 && $bar['rejected']['value'] > 0) {
  $step_3 = 'denied';
  $step_2 = 'denied';
} else if ($vendor_status == 2 && $bar['pending']['value'] > 0) {
  $step_3 = '';
  $step_2 = '';
} else if ($vendor_status == 3) {
  $step_3 = 'active';
  $step_2 = 'active';
  $step_4 = 'active';
  $step_5 = 'active';  
  $step_6 = 'active';
}
 ?>
<div class="mg-lg-12">
   <div class="col col-12">        
        <div class="content" id="content">
          <div class="wrapper">
            <div class="col col-12" style="margin-top: -50px;">
              <div class="panel">
                <div class="container-title" style="width: 100%;">
                  <h3>Tahap Sertifikasi</h3>
                </div>

                <!-- S T E P  S T A R T -->
                <div class="step-wrapper">
                  <ul>
                    <li class="<?php echo $step_1 ?>">
                      <div class="step">
                        <div class="step-icon">
                          <img src="<?php echo base_url("assets/images/001.png") ?>" alt="">
                        </div>
                        <div class="step-title">
                          Upload <br> File
                        </div>
                      </div>
                    </li>
                    <li class="<?php echo $step_2 ?>">
                      <div class="step">
                        <div class="step-icon">
                          <img src="<?php echo base_url("assets/images/003.png") ?>" alt="">
                        </div>
                        <div class="step-title">
                          Verifikasi <br> Admin
                        </div>
                      </div>
                    </li>
                    <li class="<?php echo $step_3 ?>">
                      <div class="step">
                        <div class="step-icon">
                          <img src="<?php echo base_url("assets/images/002.png") ?>" alt="">
                        </div>
                        <div class="step-title">
                          Terverifikasi
                        </div>
                      </div>
                    </li>
                    <li class="<?php echo $step_4 ?>">
                      <div class="step">
                        <div class="step-icon">
                          <img src="<?php echo base_url("assets/images/003-time-and-date.png") ?>" alt="">
                        </div>
                        <div class="step-title">
                          Proses Audit
                        </div>
                      </div>
                    </li>
                    <li class="<?php echo $step_5 ?>">
                      <div class="step">
                        <div class="step-icon">
                          <img src="<?php echo base_url("assets/images/001-file.png") ?>" alt="">
                        </div>
                        <div class="step-title">
                          Proses <br> Perbaikan
                        </div>
                      </div>
                    </li>
                    <li class="<?php echo $step_6 ?>">
                      <div class="step">
                        <div class="step-icon">
                          <img src="<?php echo base_url("assets/images/002-file-1.png") ?>" alt="">
                        </div>
                        <div class="step-title">
                          Penerbitan <br> Sertifikasi
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
                <!-- S T E P  E N D -->

              </div>
            </div>
          </div>
        </div>
      </div>
<div class="wrapper">
<div class="col col-6">
    <div class="panel">
      
      	<div class="container-title">
        	<h3>Summary Data</h3>
      	</div>

      	<div class="summary">
	        	<div class="summary-title">
	          		Approved
	          	<span><?php echo $bar['approved']['value']?>/<?php echo $approval_data['total']?></span>
	        </div>
	        <div class="summary-bars">
	          	<span class="bar-top is-success" style="width:<?php echo $bar['approved']['percentage']?>%"></span>
	          	<span class="bar-bottom"></span>
	        </div>
	    </div>

      	<div class="summary">
        	<div class="summary-title">
          		Pending
				<span><?php echo $bar['pending']['value']?>/<?php echo $approval_data['total']?></span>
        	</div>
	        <div class="summary-bars">
	          	<span class="bar-top is-warning" style="width:<?php echo $bar['pending']['percentage']?>%"></span>
	          	<span class="bar-bottom"></span>
	        </div>
      	</div>

      	<div class="summary">
        	<div class="summary-title">
          		Rejected
          		<span><?php echo $bar['rejected']['value']?>/<?php echo $approval_data['total']?></span>
        	</div>
        	<div class="summary-bars">
          		<span class="bar-top is-danger" style="width:<?php echo $bar['rejected']['percentage']?>%"></span>
          		<span class="bar-bottom"></span>
       		</div>
      	</div>

      	<div class="container-title">
        	<h3>Overview</h3>
      	</div>

      	<div class="is-block">
       		<button class="accordion-header" for="approved_content">Approved <span class="badge is-success"><?php echo $bar['approved']['value']?></span><span class="icon"><i class="fas fa-angle-down"></i></span></button>
	        <ul class="accordion-panel" id="approved_content">
            <?php echo generateAccordionVerifikasi(array_merge($approval_data[1], $approval_data[2]))?>	
	        </ul>
        	<button class="accordion-header" for="pending_content">Pending <span class="badge is-warning"><?php echo $bar['pending']['value']?></span><span class="icon"><i class="fas fa-angle-down"></i></span></button>
        	<ul class="accordion-panel" id="pending_content">
          		<?php echo generateAccordionVerifikasi($approval_data[0])?> 
        	</ul>
        	<button class="accordion-header" for="reject_content">Rejected <span class="badge is-danger"><?php echo $bar['rejected']['value']?></span><span class="icon"><i class="fas fa-angle-down"></i></span></button>
        	<ul class="accordion-panel" id="reject_content">
          	 <?php echo generateAccordionVerifikasi(array_merge($approval_data[3], $approval_data[4]))?> 
        	</ul>
      	</div>

    </div>

</div>
 <div class="col col-6">

            <div class="panel">

              <div class="container-title">
                <h3>Note</h3>
                <div class="badge is-primary is-noticable"><?php echo count($notifikasi)?></div>
              </div>

              <div class="scrollbar" id="custom-scroll" style="height: 470px; overflow-x: auto;">

              <?php foreach ($notifikasi as $key => $value) { ?>
                 <div class="notification is-warning listNotification">
                  <p>   
                    <b>Dokumen</b> : <?php echo $value['document']?>
                  </p>
                  <?php if ($value['sub_type'] != '' || $value['sub_type'] != null) { ?>
                  <p>   
                    <b>Tipe</b> : <?php echo str_replace('%20', ' ', $value['sub_type']); ?>
                  </p>
                  <?php } ?>
                  <p>   
                    <b>Note</b> : <?php echo $value['value']?>
                  </p>
                  <p>   
                    <b>Tanggal Note</b> : <?php echo date('d M Y',strtotime($value['entry_stamp'])); ?>
                  </p>

                  <a class="delete" href="<?php echo site_url('note/close/'.$value['id'])?>">X</a>

                </div>
               
              <?php } ?>

             

             

              </div>

            </div>
                      
          </div>
 </div>


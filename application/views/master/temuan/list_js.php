<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'a.id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('master/temuan/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Sertifikat"
			},
			{
				"key"	: "symbol",
				"value"	: "Pelapor"
			},
			{
				"key"	: "action",
				"value"	: "Keterangan"
			},
			{
				"key"	: "action",
				"value"	: "Lampiran"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				html += '<a href="'+base_url+"assets/lampiran/temuan_file/"+data[3].value+'" target="blank">'+data[3].value+'</a>';
				return html;
			},
			target : [3]

		},
		{
			renderCell: function(data, row, key, el){
				var html = '';
				html += '<a href="'+site_url+"master/temuan/view_temuan/"+data[4].value+'" class="button is-primary"><span class="icon"><i class="fa fa-search"></i></span> Lihat Temuan</a>';
				return html;
			},
			target : [4]

		}],
		additionFeature: function(el){
		},
		finish: function(){

		},
	});
});
</script>
<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'a.id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('master/temuan/getDataTemuan/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Nama Auditor"
			},
			{
				"key"	: "symbol",
				"value"	: "Laporan Ke -"
			},
			{
				"key"	: "action",
				"value"	: "Keterangan"
			},
			{
				"key"	: "action",
				"value"	: "Lampiran"
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				html += '<a href="'+base_url+"assets/lampiran/temuan_file/"+data[3].value+'" target="blank">'+data[3].value+'</a>';
				return html;
			},
			target : [3]

		}],
		additionFeature: function(el){
		},
		finish: function(){

		},
	});
});
</script>
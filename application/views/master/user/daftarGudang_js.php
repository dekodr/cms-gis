<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('gudang/getDataUser/'.$id); ?>',
		data: dataPost,
		headers: [
		  	{
				"key"	: "name",
				"value"	: "Lokasi Gudang"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				if(data[2].value!=1){
					html +='<a href="'+site_url+"user/tambahGudangByUser/"+data[1].value+'/<?php echo $id;?>" class="btn btn-primary btnTambah"><i class="fa fa-plus"></i>&nbsp;Tambah Gudang</a>';
				}else{
					html +='<a href="'+site_url+"user/hapusGudangByUser/"+data[1].value+'/<?php echo $id;?>" class="btn btn-danger btnKurang"><i class="fa fa-minus"></i>&nbsp;Hapus Gudang</a>';
				}
				return html;
			},
			target : [1]

		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"gudang/insert/<?php echo $id;?>"));
			
		},
		finish: function(){
     		
		},
	});
	$('#tableGenerator').on('click', '.btnTambah', function(e){
		e.preventDefault();
		var _this = $(this);
		_isTrue = confirm('Apakah anda ingin menambahkan gudang?');
		if(_isTrue){
			$.ajax({
			url : _this.attr('href'),
			method : 'POST',
			
			success : function(xhr){
				table.data('plugin_tableGenerator').fetchData();
			}	
		});
		}
		
	})

		$('#tableGenerator').on('click', '.btnKurang', function(e){
			e.preventDefault();
			var _this = $(this);
			_isTrue = confirm('Apakah anda ingin mengurangi gudang?');
			if(_isTrue){
				$.ajax({
				url : _this.attr('href'),
				method : 'POST',
				
				success : function(xhr){
					table.data('plugin_tableGenerator').fetchData();
				}	
			});
			}
			
		})
});
</script>

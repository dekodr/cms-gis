<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var _xhr;

	var folder = $('#folderGenerator').folder({
		url: '<?php echo site_url('master/certificate/getDataForm/'.$id); ?>',
		data: dataPost,
		
		dataRightClick: function(key, btn, value){
			_id = value[key][1].value;

			btn = [{
				icon : 'trash',
				label: 'Hapus',
				class: 'buttonDelete',
				href:site_url+"master/certificate/removeDetail/"+_id
			}/*,{
				icon : 'cog',
				label: 'Edit',
				class: 'buttonEdit',
				href:site_url+"master/certificate/editDetail/"+_id
			}*/];
			return btn;
		},
		callbackFunctionRightClick: function(){
			var edit = $('.buttonEdit').modal({
				header: 'Ubah Data',
        		render : function(el, data){

          		data.onSuccess = function(){
	          		$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();

          		};
          		data.isReset = false;

          		$(el).form(data).data('form');

          		$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})

        	}
      		});
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						folder.data('plugin_folder').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');

					$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
				}
			});
		},
		header: [
			{
				"key"	: "type",
				"value"	: "Form"
			},
			{
				"key"	: "file",
				"value"	: "File"
			},
		],
		renderContent: function(el, value, key){
			html = '';

			// if (value[4].value != '') {
			// 	_filePhoto += '<a href="'+base_url+"assets/lampiran/form_file/"+value[4].value+'" target="blank" class="download-btn"></a>';
			// }
			html += '<div class="caption"><p>'+value.name+'</p><span><a href="'+base_url+'assets/lampiran/template_file/'+value.template_file+'" target="blank" class="btn btn-primary"><i class="fa fa-download"></i> Download Template</a></span></div>';
			
			return html;
		},

		additionFeature: function(el){
			el.prepend(insertButton(site_url+"master/certificate/insertFormDetail/<?php echo $id;?>"));
		},
		finish: function(){
     		

		},

		filter: {
			wrapper: $('.contentWrap'),
			data : {
				data: _xhr
			}
		}

	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				folder.data('plugin_folder').fetchData();
			}
			$(el).form(data);
		}
	});
});


</script>
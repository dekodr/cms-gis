<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'a.id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('admin/Penerbitan_sertifikat/getDataSertifikat/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Permohonan Sertifikasi"
			},
			{
				"key"	: "name",
				"value"	: "Sistem Sertifikasi"
			},
			{
				"key"	: "symbol",
				"value"	: "Sertifikat Draft"
			},
			{
				"key"	: "asd",
				"value"	: "Sertifikat Asli"
			},
			{
				"key"	: "ertgfd",
				"value"	: "Alasan Reject"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				if ( data[7].value  == 1) {
		 			html += '<a href="'+site_url+"admin/Penerbitan_sertifikat/edit/"+data[5].value+'" class="button is-primary buttonEdit"><span class="icon"><i class="fas fa-upload"></i></span>Upload Sertifikat Draft</a>';
				}else if( data[7].value  == 2){
					html += '<p><span class="icon"><i class="fas fa-clock"></i></span>&nbsp;Menunggu Approve</p>';
				}else if( data[7].value  == 3){
					html += '<a href="'+site_url+"admin/Penerbitan_sertifikat/upload_sertifikat_asli/"+data[5].value+'" class="button is-primary buttonUpload"><span class="icon"><i class="fas fa-upload"></i></span>Upload Sertifikat Asli</a>';
				}else if( data[7].value  == 5){
					html += '<p><span class="icon"><i class="fas fa-check"></i></span>&nbsp;Sertifikat Asli Telah Diupload</p>';
				}
		 		// html +=insertButton(site_url+"admin/Penerbitan_sertifikat/insert/"+data[4].value, data[4].value);
				// html +=editButton(site_url+"admin/Penerbitan_sertifikat/edit/"+data[4].value, data[4].value);
				// html +=deleteButton(site_url+"admin/upload_sertifikat/remove/"+data[4].value, data[4].value);
				return html;
			},
			target : [5]

		}
		,{
			renderCell: function(data, row, key, el){
				var html = '';
			if (data[1].value == 'Pilih Salah Satu' ) { 
				html +='-';
				}else{
					html+= data[1].value 
				}
				return html;
			},
			target : [1]
		},
		{
			renderCell: function(data, row, key, el){
				var link = '';
				if (data[2].value == null ) {
				link +='-';	
				}else{				
					link += '<a href="'+base_url+"assets/lampiran/Sertifikat_Draft_File/"+data[2].value+'" target="blank">'+data[2].value+'</a>'
				}
				return link;
			},
			target : [2]
		},
		{
			renderCell: function(data, row, key, el){
				var link = '';
				if (data[3].value == null || data[3].value == '' ) {
				link +='-';	
				}else{					
					link += '<a href="'+base_url+"assets/lampiran/Sertifikat_File/"+data[3].value+'" target="blank">'+data[3].value+'</a>'
				}
				return link;
			},
			target : [3]
		},
		{
			renderCell: function(data, row, key, el){
				var html = '';
				if (data[4].value == null || data[4].value == '' ) {
				html +='-';	
				}else{					
					html += data[4].value
				}
				return html;
			},
			target : [4]
		}
		],
		additionFeature: function(el){
		},
		finish: function(){

		      var edit = $('.buttonEdit').modal({
		      	header: 'Apakah anda yakin ingin meng-Upload Sertifikat Draft?',
		        render : function(el, data){
		        	el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin mengupload draft sertifikat?<span><div class="form"></div><div>');
		          data.onSuccess = function(){
		          	$(edit).data('modal').close();
		            table.data('plugin_tableGenerator').fetchData();

		          };
		          data.isReset = false;

		          $(el).form(data).data('form');

		        }
		      });
		      var upl = $('.buttonUpload').modal({
		      	header: 'Apakah anda yakin ingin meng-Upload Sertifikat Asli?',
		        render : function(el, data){
		        	el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin mengupload draft sertifikat?<span><div class="form"></div><div>');
		          data.onSuccess = function(){
		          	$(upl).data('modal').close();
		            table.data('plugin_tableGenerator').fetchData();

		          };
		          data.isReset = false;

		          $(el).form(data).data('form');

		        }
		      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
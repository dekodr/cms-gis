<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('admin/penerbitan_sertifikat/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Nama Perusahaan"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
		 		html += '<a href="'+site_url+"admin/Penerbitan_sertifikat/upload_sertifikat/"+data[1].value+'" class="button is-primary"><span class="icon"><i class="fas fa-sticky-note"></i></span>Lihat Sertifikat</a>';
				// html +=editButton(site_url+"admin/penerbitan_sertifikat/edit/"+data[1].value, data[1].value);
				// html +=deleteButton(site_url+"admin/penerbitan_sertifikat/remove/"+data[1].value, data[1].value);
				return html;
			},
			target : [1]

		}],
		additionFeature: function(el){
			// el.append(insertButton(site_url+"admin/penerbitan_sertifikat/insert/<?php echo $id;?>"));
		},
		finish: function(){
      var edit = $('.buttonEdit').modal({
        render : function(el, data){

          data.onSuccess = function(){
          	$(edit).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          };
          data.isReset = false;

          $(el).form(data).data('form');

        }
      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
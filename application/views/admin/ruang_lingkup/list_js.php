<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('admin/ruang_lingkup/getData/'.$id_rl); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Jenis Permohonan"
			},
			<?php if ($id_rl == 1) { ?>
			{
				"key"	: "name",
				"value"	: "Lingkup Pemohon"
			},
			<?php } else { ?>
			{
				"key"	: "name",
				"value"	: "Sistem Sertifikasi"
			},	
			<?php } ?>
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				html +=editButton(site_url+"admin/ruang_lingkup/edit/"+data[2].value, data[2].value+"/<?php echo $id_rl ?>");
				html +=deleteButton(site_url+"admin/ruang_lingkup/remove/"+data[2].value, data[2].value+"/<?php echo $id_rl ?>");
				return html;
			},
			target : [2]

		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"admin/ruang_lingkup/insert/<?php echo $id_rl;?>"));
		},
		finish: function(){
      var edit = $('.buttonEdit').modal({
        render : function(el, data){

          data.onSuccess = function(){
          	$(edit).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          };
          data.isReset = false;

          $(el).form(data).data('form');

        }
      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
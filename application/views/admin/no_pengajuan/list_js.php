<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('admin/no_pengajuan/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Perusahaan"
			},
			{
				"key"	: "name",
				"value"	: "No Pengajuan"
			},
			{
				"key"	: "name",
				"value"	: "Jenis Permohonan"
			},
			// {
			// 	"key"	: "action",
			// 	"value"	: "Action",
			// 	"sort"	: false
			// }
			],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				if (data[2].value == 'SPPT-SNI') {
					html += data[2].value+' ('+data[4].value+')';
				}else{
					html += data[2].value;
				}
				return html;
			},
			target : [2]

		},
		// {
		// 	renderCell: function(data, row, key, el){
		// 		var html = '';
		// 		html +=editButton(site_url+"admin/no_pengajuan/edit/"+data[3].value, data[3].value);
		// 		html +=deleteButton(site_url+"admin/no_pengajuan/remove/"+data[3].value, data[3].value);
		// 		return html;
		// 	},
		// 	target : [3]

		// }
		],
		// additionFeature: function(el){
		// 	el.append(insertButton(site_url+"admin/no_pengajuan/insert/<?php echo $id;?>"));
		// },
		finish: function(){
      var edit = $('.buttonEdit').modal({
        render : function(el, data){

          data.onSuccess = function(){
          	$(edit).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          };
          data.isReset = false;

          $(el).form(data).data('form');

        }
      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
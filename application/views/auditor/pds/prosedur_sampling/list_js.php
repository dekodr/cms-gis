<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('auditor/pds/prosedur_sampling/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "File"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				if (data[0].value == null || data[0].value == '') {
					html += '';
				}else{
					html += '<a href="'+site_url+"auditor/pds/prosedur_sampling/get_lampiran/"+data[1].value+'" class="buttonPdf" target="blank">'+data[0].value+'</a>';
				}
				return html;
			},
			target : [0]

		},
		{
			renderCell: function(data, row, key, el){
				var html = '';
				<?php if ($this->session->userdata('admin')['id_role'] == 4) { ?>
					html +=editButton(site_url+"auditor/pds/prosedur_sampling/edit/"+data[1].value, data[1].value);
					html +=deleteButton(site_url+"auditor/pds/prosedur_sampling/remove/"+data[1].value, data[1].value);
				<?php } ?>
				html += '<a href="'+site_url+"auditor/akd/akd/download/prosedur_sampling_file/"+data[0].value+'" class="button is-primary" target="blank"><span class="icon"><i class="fa fa-download"></i></span> Download</a>';
				
				return html;
			},
			target : [1]

		}],
		additionFeature: function(el){
			<?php if ($this->session->userdata('admin')['id_role'] == 4) { ?>
				el.append(insertButton(site_url+"auditor/pds/prosedur_sampling/insert/<?php echo $id;?>"));
			<?php } ?>
		},
		finish: function(){
      var edit = $('.buttonEdit').modal({
        render : function(el, data){

          data.onSuccess = function(){
          	$(edit).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          };
          data.isReset = false;

          $(el).form(data).data('form');

        }
      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

			var pdf = $('.buttonPdf').modal({
				header: 'Lihat Dokumen',
				render : function(el, data){
					
					var array = data.prosedur_sampling_file.split('.');
					if(array[1]=='pdf'){
						el.html(' <embed src="'+site_url+'/assets/lampiran/prosedur_sampling_file/'+data.prosedur_sampling_file+'" width="100%" height="500" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">');
					
					}else if(array[1]=='jpg'||array[1]=='png'||array[1]=='jpeg'){
						el.html(' <img src="'+site_url+'/assets/lampiran/prosedur_sampling_file/'+data.prosedur_sampling_file+'" width="100%" height="500" >');
					}
					
					
				}
			});
		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
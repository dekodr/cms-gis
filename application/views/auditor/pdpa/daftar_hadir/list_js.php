<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('auditor/pdpa/daftar_hadir/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "File"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				if (data[0].value == null || data[0].value == '') {
					html += '';
				}else{
					html += '<a href="'+base_url+"assets/lampiran/daftar_hadir_file/"+data[0].value+'" target="blank">'+data[0].value+'</a>';
				}
				return html;
			},
			target : [0]

		},
		{
			renderCell: function(data, row, key, el){
				var html = '';
				<?php if ($this->session->userdata('admin')['id_role'] == 4) { ?>
					html +=editButton(site_url+"auditor/pdpa/daftar_hadir/edit/"+data[1].value, data[1].value);
					html +=deleteButton(site_url+"auditor/pdpa/daftar_hadir/remove/"+data[1].value, data[1].value);
				<?php } ?>
				html += '<a href="'+site_url+"auditor/akd/akd/download/daftar_hadir_file/"+data[0].value+'" class="button is-primary" target="blank"><span class="icon"><i class="fa fa-download"></i></span> Download</a>';
				return html;
			},
			target : [1]

		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"auditor/pdpa/daftar_hadir/insert/<?php echo $id;?>"));
		},
		finish: function(){
      var edit = $('.buttonEdit').modal({
        render : function(el, data){

          data.onSuccess = function(){
          	$(edit).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          };
          data.isReset = false;

          $(el).form(data).data('form');

        }
      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('auditor/panel/dps/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "File"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				if (data[0].value == null || data[0].value == '') {
					html += '';
				}else{
					html += '<a href="'+base_url+"assets/lampiran/dps_file/"+data[0].value+'" target="blank">'+data[0].value+'</a>';
				}
				return html;
			},
			target : [0]

		},
		{
			renderCell: function(data, row, key, el){
				var html = '';
				<?php if ($this->session->userdata('admin')['id_role'] == 4) { ?>
					html +=editButton(site_url+"auditor/panel/dps/edit/"+data[1].value, data[1].value);
					html +=deleteButton(site_url+"auditor/panel/dps/remove/"+data[1].value, data[1].value);
				<?php } ?>
				return html;
			},
			target : [1]

		}],
		additionFeature: function(el){
			<?php if ($this->session->userdata('admin')['id_role'] == 4) { ?>
				el.append(insertButton(site_url+"auditor/panel/dps/insert/<?php echo $id;?>"));
			<?php } ?>
		},
		finish: function(){
      var edit = $('.buttonEdit').modal({
        render : function(el, data){

          data.onSuccess = function(){
          	$(edit).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          };
          data.isReset = false;

          $(el).form(data).data('form');

        }
      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('auditor/pk/upload_dokumen/getData/'.$id); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Jenis"
			},
			{
				"key"	: "name",
				"value"	: "File"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				
				if (data[0].value == 'pendidikan') {
					html += 'Pendidikan';
				} else if (data[0].value == 'pelatihan') {
					html += 'Pelatihan';
				} else if (data[0].value == 'pengalaman_kerja') {
					html += 'Pengalaman Kerja';
				} else {
					html += '-';
				}

				return html;
			},
			target : [0]

		},
		{
			renderCell: function(data, row, key, el){
				var html = '';
				if (data[1].value == null || data[1].value == '') {
					html += '';
				}else{
					html += '<a href="'+base_url+"assets/lampiran/upload_dokumen_file/"+data[1].value+'" target="blank">'+data[1].value+'</a>';
				}
				return html;
			},
			target : [1]

		},
		{
			renderCell: function(data, row, key, el){
				var html = '';
				<?php //if ($this->session->userdata('admin')['id_role'] == 4) { ?>
					html +=editButton(site_url+"auditor/pk/upload_dokumen/edit/"+data[2].value, data[2].value);
					html +=deleteButton(site_url+"auditor/pk/upload_dokumen/remove/"+data[2].value, data[2].value);

				<?php //} ?>
				//html += '<a href="'+site_url+"auditor/akd/akd/download/upload_dokumen_file/"+data[1].value+'" class="button is-primary" target="blank"><span class="icon"><i class="fa fa-download"></i></span> Download</a>';
				return html;
			},
			target : [2]

		}],
		additionFeature: function(el){
			<?php //if ($this->session->userdata('admin')['id_role'] == 4) { ?>
				el.append(insertButton(site_url+"auditor/pk/upload_dokumen/insert/<?php echo $id;?>"));
			<?php //} ?>
			
		},
		finish: function(){
      var edit = $('.buttonEdit').modal({
        render : function(el, data){

          data.onSuccess = function(){
          	$(edit).data('modal').close();
            table.data('plugin_tableGenerator').fetchData();

          };
          data.isReset = false;

          $(el).form(data).data('form');

        }
      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
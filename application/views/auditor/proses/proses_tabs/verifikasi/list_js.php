<script type="text/javascript">

$(function(){

    $.ajax({
        url : '<?php echo site_url('auditor/proses/proses_tabs/verifikasi/view/'.$id_client.'/'.$id_certificate)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            console.log(xhr);
            xhr.onSuccess = function(data){
                alert('Sukses');
            }
            xhr.successMessage = 'Berhasil !!';
            xhr.formWrap = false;

            $('#verifikasiForm').form(xhr);

            $('.form-group.btn-group').append('<a href="'+site_url+"note/insert/ms_verifikasi/"+xhr.data.id+'" class="button is-primary btnNote"><i class="fas fa-sticky-note"></i>&nbsp;Note&nbsp;<span>'+xhr.data.total_note+'</span></a>');

            <?php 
            $field = array(
                '1' => 'perhitungan_mandays',
                '2' => 'surat_tugas',
                '3' => 'pemilihan_auditor',
                '4' => 'jadwal_audit',
                '5' => 'audit_tahap_1',
                '6' => 'audit_tahap_2',
                '7' => 'dokumen_sampling',
                '8' => 'hasil_uji',
                '9' => 'panel_sertifikasi'
            );

                    if ($perhitungan_mandays == 1 && $surat_tugas == 1 && $pemilihan_auditor == 1 && $jadwal_audit == 1 && $audit_tahap_1 == 1 && $audit_tahap_2 == 1 && $dokumen_sampling == 1 && $hasil_uji == 1 && $panel_sertifikasi == 1 && $status != 1) {
            ?>
                $('.form-group.btn-group').append('<a href="'+site_url+"auditor/proses/proses_tabs/verifikasi/verifikasi_sertifikat/"+'<?php echo $id_client ?>'+'" class="button is-primary btnVerifikasi"><i class="fas fa-check"></i>&nbsp;Verifikasi&nbsp;</a>');
            <?php } ?>

            var note = $('.btnNote').modal({
                header: 'Note Verifikasi',
                render : function(el, data){

                    data.onSuccess = function(){
                        $(note).data('modal').close();
                        var current_index = $("#tabs").tabs("option","active");
                        $('#tabs').tabs('load',current_index);
                    }

                    $(el).form(data);
                    $(el).prepend('<div class="dots-list"><ol></ol></div>');
                    $.each(data.note, function(key, value){
                        $('.dots-list ol', el).append('<li><span class="date">'+moment(value.entry_stamp).format('DD MMM YYYY')+'</span>'+value.value+' </li>');
                    });
                }

            });

            var verifikasi = $('.btnVerifikasi').modal({
                header: 'Verifikasi Data',
                render : function(el, data){
                    el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin verifikasi data ini?<span><div class="form"></div><div>');
                    data.onSuccess = function(){
                        $(verifikasi).data('modal').close();
                        location.reload();
                    };
                    data.isReset = true;
                    $('.form', el).form(data).data('form');
                }
            });
        }
    });

    $('#formVerifikasi').on('submit', function(e){
        e.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url : '<?php echo site_url('auditor/proses/proses_tabs/verifikasi/save/'.$id_client.'/'.$id_certificate) ?>',
            method: 'POST',
            dataType: 'json',
            data : form_data
        }).done(function(response){ //
            console.log(response.status)
             if(response.status!='fail'){
                alert('Sukses!!');
                var current_index = $("#tabs").tabs("option","active");
                $('#tabs').tabs('load',current_index);
            } else {
                alert('Gagal!, Terjadi kesalahan!');
            }
        });
    })
});


</script>
<script type="text/javascript">
$(function(){

    dataPost = {
        order: 'a.id',
        sort: 'desc'
    };

    var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

    var _xhr;
    // $.ajax({
    //         url: '<?php echo site_url('vendor/view/Formulir_permohonan/formFilter')?>',
    //         async: false,
    //         dataType: 'json',
    //         success:function(xhr){
    //             _xhr = xhr;
    //         }
    //     })

    var folder = $('#folderGeneratorPa').folder({
        url: '<?php echo site_url('auditor/proses/proses_tabs/pemilihan_auditor/getData/'.$id_client); ?>',
        data: dataPost,
        header: [
           {
                "key"   : "authorize_date",
                "value" : "Lampiran File"
            },
        ],
        dataRightClick: function(key, btn, value){
            _id = value[key][4].value;

            btn = [
            {
                icon : 'plus',
                label: 'Tambah',
                class: 'buttonAdd',
                href:site_url+"auditor/proses/proses_tabs/pemilihan_auditor/insert/"+_id+"/<?php echo $id_client;?>"
            },
            {
                icon : 'eye',
                label: 'Cek Data',
                class: 'buttonCek',
                href:site_url+"auditor/proses/proses_tabs/pemilihan_auditor/edit/"+_id+"/cek"
            },
            {
                icon : 'edit',
                label: 'Edit',
                class: 'buttonEdit',
                href:site_url+"auditor/proses/proses_tabs/pemilihan_auditor/edit/"+_id+"/update"
            },
            {
                icon : 'trash',
                label: 'Hapus',
                class: 'buttonDelete',
                href:site_url+"auditor/proses/proses_tabs/pemilihan_auditor/remove/"+_id
            }
            ];
            return btn;
        },
        callbackFunctionRightClick: function(){
             var add = $('.buttonAdd').modal({
                header : 'Tambah Data',
                render : function(el, data){
                    data.onSuccess = function(){
                        $(add).data('modal').close();
                        folder.data('plugin_folder').fetchData();
                    }
                    $(el).form(data);
                }
            });

            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })

            }
            });

            var edit = $('.buttonEdit').modal({
                header: 'Edit Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })
                }
            });

            var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						folder.data('plugin_folder').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');

					$('.close').on('click', function() {
          			$(edit).data('modal').close();
	            	folder.data('plugin_folder').fetchData();
          		})
				}
			});
        },
        header: [
            {
                "key"   : "authorize_date",
                "value" : "Lampiran File"
            },
        ],
        renderContent: function(el, value, key){
            html = '';

            return html += '<div class="caption"><p><a href="'+site_url+'auditor/proses/proses_tabs/pemilihan_auditor/tambah/'+value[4].value+'/<?php echo $id_client ?>" title="">'+value[0].value+'</a></p></div>';;
        },
        additionFeature: function(el){
			<?php //if ($this->session->userdata('admin')['id_role'] == 4) { ?>
				// el.append(insertButton(site_url+"auditor/proses/proses_tabs/pemilihan_auditor/insert/<?php echo $id_client;?>"));
			<?php //} ?>
		},
        finish: function(){
            
        },
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }
    });
});


</script>
<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#folderGeneratorPa').tableGenerator({
		url: '<?php echo site_url('auditor/proses/proses_tabs/pemilihan_auditor/getDataAuditor/'.$id_certificate.'/'.$id_client); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Nama Auditor"
			},
			{
				"key"	: "symbol",
				"value"	: "Status"
			},
			{
				"key"	: "symbol",
				"value"	: "Tanggal Audit"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
            renderCell: function(data, row, key, el){
                var html = '';
                if (data[3].value == '' || data[3].value == null) {
                    html += "-";
                } else {
                    html += defaultDate(data[3].value)+' sampai '+defaultDate(data[4].value);
                }
                return html;
            },
            target : [2]

        },{
			renderCell: function(data, row, key, el){
				var html = '';
				// html +=editButton(site_url+"auditor/proses/proses_tabs/pemilihan_auditor/edit/"+data[2].value, data[2].value);
				// html += '<a href="'+site_url+"auditor/proses/proses_tabs/pemilihan_auditor/pilihRole/"+data[2].value+'" class="button is-primary tableButton buttonRole"><i class="fa fa-users"></i>&nbsp;<span class="icon-text"> Pilih Role</span></a>';
				html +=deleteButton(site_url+"auditor/proses/proses_tabs/pemilihan_auditor/remove/"+data[2].value, data[2].value);
				return html;
			},
			target : [3]

		}],
		additionFeature: function(el){
			el.append(insertButton(site_url+"auditor/proses/proses_tabs/pemilihan_auditor/insert/<?php echo $id_certificate;?>/<?php echo $id_client ?>"));
		},
		finish: function(){
			var role = $('.buttonRole').modal({
			header : 'Pilih Role',
	        render : function(el, data){

	          data.onSuccess = function(){
	          	$(role).data('modal').close();
	            table.data('plugin_tableGenerator').fetchData();

	          };
	          data.isReset = false;

	          $(el).form(data).data('form');

	        }
	      });

	      var edit = $('.buttonEdit').modal({
	        render : function(el, data){

	          data.onSuccess = function(){
	          	$(edit).data('modal').close();
	            table.data('plugin_tableGenerator').fetchData();

	          };
	          data.isReset = false;

	          $(el).form(data).data('form');

	        }
	      });
			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
</script>
<script type="text/javascript">
$(function(){

    dataPost = {
        order: 'a.id',
        sort: 'desc'
    };

    var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

    var _xhr;
    // $.ajax({
    //         url: '<?php echo site_url('vendor/view/Formulir_permohonan/formFilter')?>',
    //         async: false,
    //         dataType: 'json',
    //         success:function(xhr){
    //             _xhr = xhr;
    //         }
    //     })

    var folder = $('#folderGeneratorPs').folder({
        url: '<?php echo site_url('auditor/proses/proses_tabs/panel_sertifikasi/getData/'.$id_client.'/'.$id_certificate); ?>',
        data: dataPost,
        header: [
           {
                "key"   : "authorize_date",
                "value" : "Lampiran File"
            },
        ],
        dataRightClick: function(key, btn, value){
            _id = value[key][2].value;

            btn = [
            {
                icon : 'eye',
                label: 'Cek Data',
                class: 'buttonCek',
                href:site_url+"auditor/proses/proses_tabs/panel_sertifikasi/edit/"+_id+"/cek"
            },
            {
                icon : 'edit',
                label: 'Edit',
                class: 'buttonEdit',
                href:site_url+"auditor/proses/proses_tabs/panel_sertifikasi/edit/"+_id+"/update"
            },
            {
                icon : 'trash',
                label: 'Hapus',
                class: 'buttonDelete',
                href:site_url+"auditor/proses/proses_tabs/panel_sertifikasi/remove/"+_id
            }
            ];
            // return btn;
        },
        callbackFunctionRightClick: function(){
             var add = $('.buttonAdd').modal({
                header : 'Tambah Data',
                render : function(el, data){
                    data.onSuccess = function(){
                        $(add).data('modal').close();
                        folder.data('plugin_folder').fetchData();
                    }
                    $(el).form(data);
                }
            });

            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })

            }
            });

            var edit = $('.buttonEdit').modal({
                header: 'Edit Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })
                }
            });

            var del = $('.buttonDelete').modal({
                header: 'Hapus Data',
                render : function(el, data){
                    el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
                    data.onSuccess = function(){
                        $(del).data('modal').close();
                        folder.data('plugin_folder').fetchData();
                    };
                    data.isReset = true;
                    $('.form', el).form(data).data('form');

                    $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })
                }
            });
        },
        header: [
            {
                "key"   : "authorize_date",
                "value" : "Lampiran File"
            },
        ],
        renderContent: function(el, value, key){
            html = '';
            _file = '';
            _id = value[2].value;

            if(value[1].value != ''){
                _file += '<a href="'+base_url+"auditor/proses/proses_tabs/perhitungan_mandays/download/panel_sertifikasi/"+value[1].value+'" target="blank"><i class="fa fa-download"></i></a>' 
            }
            
            btnView = '<a href="'+site_url+'auditor/proses/proses_tabs/panel_sertifikasi/edit/'+_id+'/cek" class="button is-primary buttonCek"><i class="fa fa-eye"></i>&nbsp;<span class="icon-text"> Lihat Data</span></a>';

            <?php if ($this->session->userdata('admin')['id_role'] != 4) { ?>
                btnEdit = '<a href="'+site_url+'auditor/proses/proses_tabs/hasil_uji/edit/'+_id+'/update" class="button is-success buttonEdit"><i class="fa fa-edit"></i>&nbsp;<span class="icon-text"> Edit</span></a>';

                btnHapus = '<a href="'+site_url+'auditor/proses/proses_tabs/hasil_uji/remove/'+_id+'/update" class="button is-danger buttonDelete"><i class="fa fa-trash"></i>&nbsp;<span class="icon-text"> Hapus</span></a>';
            <?php } else { ?>
                btnEdit = '';
                btnHapus = '';
            <?php } ?>

            return html += '<div class="caption"><p>'+value[0].value+'</p><p>'+btnView+'</p><p>'+btnEdit+'</p><p>'+btnHapus+'</p></div>';
        },
        additionFeature: function(el){
            <?php if ($this->session->userdata('admin')['id_role'] != 4) { ?>
                el.append(insertButton(site_url+"auditor/proses/proses_tabs/panel_sertifikasi/insert/<?php echo $id_client;?>/<?php echo $id_certificate;?>"));
            <?php } ?>
        },
        finish: function(){
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                
                $('.close').on('click', function() {
                    $(cek).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })

            }
            });

            var edit = $('.buttonEdit').modal({
                header: 'Edit Data',
                render : function(el, data){

                data.onSuccess = function(){
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();

                };
                data.isReset = false;

                $(el).form(data).data('form');
                
                $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })
                }
            });

            var del = $('.buttonDelete').modal({
                header: 'Hapus Data',
                render : function(el, data){
                    el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
                    data.onSuccess = function(){
                        $(del).data('modal').close();
                        folder.data('plugin_folder').fetchData();
                    };
                    data.isReset = true;
                    $('.form', el).form(data).data('form');

                    $('.close').on('click', function() {
                    $(edit).data('modal').close();
                    folder.data('plugin_folder').fetchData();
                })
                }
            });
        },
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }
    });
    var add = $('.buttonAdd').modal({
        render : function(el, data){
            data.onSuccess = function(){
                $(add).data('modal').close();
                folder.data('plugin_folder').fetchData();
            }
            $(el).form(data);
        }
    });
});


</script>
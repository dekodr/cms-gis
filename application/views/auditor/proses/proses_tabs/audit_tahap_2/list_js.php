<script type="text/javascript">
$(function(){

    dataPost = {
        order: 'a.id',
        sort: 'desc'
    };

    var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

    var _xhr;

    var table = $('#tableGeneratorTa2').tableGenerator({
        url: '<?php echo site_url('auditor/proses/proses_tabs/audit_tahap_2/getData/'.$id_client.'/'.$id_certificate.'/2'); ?>',
        data: dataPost,
        headers: [
            {
                "key"   : "name",
                "value" : "Auditor"
            },
            {
                "key"   : "action",
                "value" : "Action",
                "sort"  : false
            }],
        columnDefs : [{
            renderCell: function(data, row, key, el){
                var html = '';
                _id = data[2].value;

                switch(data[3].value){
                    case '0' :
                        html += '<a href="'+site_url+"auditor/proses/proses_tabs/audit_tahap_2/viewVerifikasi/"+_id+'/<?php echo $id_client;?>" class="button is-verification btnVerifikasi"><i class="far fa-clock"></i>&nbsp;Verification</a>'
                        break;
                    case '1' :
                    case '2' :
                        html += '<a href="'+site_url+"auditor/proses/proses_tabs/audit_tahap_2/viewVerifikasi/"+_id+'/<?php echo $id_client;?>" class="button is-verified btnVerifikasi"><i class="fa fa-check"></i>&nbsp;Verified</a>'
                        break;
                    case '3' :
                    case '4' :
                        html += '<a href="'+site_url+"auditor/proses/proses_tabs/audit_tahap_2/viewVerifikasi/"+_id+'/<?php echo $id_client;?>" class="button is-rejected btnVerifikasi"><i class="fa fa-close"></i>&nbsp;Rejected</a>'
                        break;
                }
                html += '<a href="'+site_url+"note/insert/ms_tahap_audit/"+_id+'" class="button is-warning btnNote"><i class="fas fa-sticky-note"></i>&nbsp;Note&nbsp;<span>'+data[4].value+'</span></a>';

                html += '<a href="'+site_url+'auditor/proses/proses_tabs/audit_tahap_2/log/'+_id+'/<?php echo $id_client;?>/<?php echo $id_certificate;?>" class="button is-primary"><i class="fa fa-eye"></i>&nbsp;<span class="icon-text"> Lihat Data</span></a>';

                html +=editButton(site_url+"auditor/proses/proses_tabs/audit_tahap_2/edit/"+_id+"/update");
                html +=deleteButton(site_url+"auditor/proses/proses_tabs/audit_tahap_2/remove/"+_id);

                return html;
            },
            target : [1]

        }],
        additionFeature: function(el){
            <?php if ($this->session->userdata('admin')['id_role'] != 4) { ?>
                el.append(insertButton(site_url+"auditor/proses/proses_tabs/audit_tahap_2/insert/<?php echo $id_client;?>/<?php echo $id_certificate;?>"));
            <?php } ?>
        },
        finish: function(){
            var edit = $('.buttonEdit').modal({
                render : function(el, data){

                  data.onSuccess = function(){
                    $(edit).data('modal').close();
                    table.data('plugin_tableGenerator').fetchData();

                  };
                  data.isReset = false;

                  $(el).form(data).data('form');

                }
            });
            var del = $('.buttonDelete').modal({
                header: 'Hapus Data',
                render : function(el, data){
                    el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
                    data.onSuccess = function(){
                        $(del).data('modal').close();
                        table.data('plugin_tableGenerator').fetchData();
                    };
                    data.isReset = true;
                    $('.form', el).form(data).data('form');
                }
            });
            var cek = $('.buttonCek').modal({
                header: 'Info Data',
                render : function(el, data){

                    data.onSuccess = function(){
                        $(cek).data('modal').close();
                        folder.data('plugin_folder').fetchData();

                    };
                    data.isReset = false;

                    $(el).form(data).data('form');
                    
                    $('.close').on('click', function() {
                        $(edit).data('modal').close();
                        folder.data('plugin_folder').fetchData();
                    })

                }
            });
            var verifikasi = $('.btnVerifikasi').modal({
                header: 'Verifikasi Data',
                dataType:'html',
                render : function(el, data){
                  $(el).html(data);
                }
            });
            var note = $('.btnNote').modal({
                header: 'Note Verifikasi',
                render : function(el, data){

                    data.onSuccess = function(){
                        $(note).data('modal').close();
                        location.reload();
                        folder.data('plugin_folder').fetchData();
                    }

                    $(el).form(data);
                    $(el).prepend('<div class="dots-list"><ol></ol></div>');
                    $.each(data.note, function(key, value){
                        $('.dots-list ol', el).append('<li><span class="date">'+moment(value.entry_stamp).format('DD MMM YYYY')+'</span>'+value.value+' </li>');
                    });
                }

            });
        },
    });
   
    var add = $('.buttonAdd').modal({
        render : function(el, data){
            data.onSuccess = function(){
                $(add).data('modal').close();
                folder.data('plugin_folder').fetchData();
            }
            $(el).form(data);
        }
    });
});

 // var folder = $('#folderGeneratorTa2').folder({
    //     url: '<?php echo site_url('auditor/proses/proses_tabs/audit_tahap_2/getData/'.$id_client.'/'.$id_certificate.'/2'); ?>',
    //     data: dataPost,
    //     header: [
    //        {
    //             "key"   : "authorize_date",
    //             "value" : "Lampiran File"
    //         },
    //     ],
    //     dataRightClick: function(key, btn, value){
    //         _id = value[key][2].value;

    //         btn = [
    //         {
    //             icon : 'eye',
    //             label: 'Cek Data',
    //             class: 'buttonCek',
    //             href:site_url+"auditor/proses/proses_tabs/audit_tahap_2/edit/"+_id+"/cek"
    //         },
    //         {
    //             icon : 'edit',
    //             label: 'Edit',
    //             class: 'buttonEdit',
    //             href:site_url+"auditor/proses/proses_tabs/audit_tahap_2/edit/"+_id+"/update"
    //         },
    //         {
    //             icon : 'trash',
    //             label: 'Hapus',
    //             class: 'buttonDelete',
    //             href:site_url+"auditor/proses/proses_tabs/audit_tahap_2/remove/"+_id
    //         }
    //         ];
    //         // return btn;
    //     },
    //     callbackFunctionRightClick: function(){
    //          var add = $('.buttonAdd').modal({
    //             header : 'Tambah Data',
    //             render : function(el, data){
    //                 data.onSuccess = function(){
    //                     $(add).data('modal').close();
    //                     folder.data('plugin_folder').fetchData();
    //                 }
    //                 $(el).form(data);
    //             }
    //         });

    //         var cek = $('.buttonCek').modal({
    //             header: 'Info Data',
    //             render : function(el, data){

    //             data.onSuccess = function(){
    //                 $(cek).data('modal').close();
    //                 folder.data('plugin_folder').fetchData();

    //             };
    //             data.isReset = false;

    //             $(el).form(data).data('form');
                
    //             $('.close').on('click', function() {
    //                 $(edit).data('modal').close();
    //                 folder.data('plugin_folder').fetchData();
    //             })

    //         }
    //         });

    //         var edit = $('.buttonEdit').modal({
    //             header: 'Edit Data',
    //             render : function(el, data){

    //             data.onSuccess = function(){
    //                 $(edit).data('modal').close();
    //                 folder.data('plugin_folder').fetchData();

    //             };
    //             data.isReset = false;

    //             $(el).form(data).data('form');
                
    //             $('.close').on('click', function() {
    //                 $(edit).data('modal').close();
    //                 folder.data('plugin_folder').fetchData();
    //             })
    //             }
    //         });

    //         var del = $('.buttonDelete').modal({
    //             header: 'Hapus Data',
    //             render : function(el, data){
    //                 el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
    //                 data.onSuccess = function(){
    //                     $(del).data('modal').close();
    //                     folder.data('plugin_folder').fetchData();
    //                 };
    //                 data.isReset = true;
    //                 $('.form', el).form(data).data('form');

    //                 $('.close').on('click', function() {
    //                 $(edit).data('modal').close();
    //                 folder.data('plugin_folder').fetchData();
    //             })
    //             }
    //         });
    //     },
    //     header: [
    //         {
    //             "key"   : "authorize_date",
    //             "value" : "Lampiran File"
    //         },
    //     ],
    //     renderContent: function(el, value, key){
    //         html = '';
    //         _id = value[2].value;
            
    //         btnView = '<a href="'+site_url+'auditor/proses/proses_tabs/audit_tahap_2/log/'+_id+'/<?php echo $id_client;?>/<?php echo $id_certificate;?>" class="button is-primary"><i class="fa fa-eye"></i>&nbsp;<span class="icon-text"> Lihat Data</span></a>';

    //         <?php if ($this->session->userdata('admin')['id_role'] != 4) { ?>
    //             btnEdit = '<a href="'+site_url+'auditor/proses/proses_tabs/audit_tahap_2/edit/'+_id+'/update/<?php echo $id_client;?>/<?php echo $id_certificate;?>" class="button is-success buttonEdit"><i class="fa fa-edit"></i>&nbsp;<span class="icon-text"> Edit</span></a>';

    //             btnHapus = '<a href="'+site_url+'auditor/proses/proses_tabs/audit_tahap_2/remove/'+_id+'/update" class="button is-danger buttonDelete"><i class="fa fa-trash"></i>&nbsp;<span class="icon-text"> Hapus</span></a>';
    //         <?php } else { ?>
    //             btnEdit = '';
    //             btnHapus = '';
    //         <?php } ?>

    //         return html += '<div class="caption"><p>'+value[0].value+'</p><p>'+btnView+'</p><p>'+btnEdit+'</p><p>'+btnHapus+'</p></div>';
    //     },
    //     additionFeature: function(el){
    //         <?php if ($this->session->userdata('admin')['id_role'] != 4) { ?>
    //             el.append(insertButton(site_url+"auditor/proses/proses_tabs/audit_tahap_2/insert/<?php echo $id_client;?>/<?php echo $id_certificate;?>"));
    //         <?php } ?>
    //     },
    //     finish: function(){
    //         var cek = $('.buttonCek').modal({
    //             header: 'Info Data',
    //             render : function(el, data){

    //             data.onSuccess = function(){
    //                 $(cek).data('modal').close();
    //                 folder.data('plugin_folder').fetchData();

    //             };
    //             data.isReset = false;

    //             $(el).form(data).data('form');
                
    //             $('.close').on('click', function() {
    //                 $(edit).data('modal').close();
    //                 folder.data('plugin_folder').fetchData();
    //             })

    //         }
    //         });

    //         var edit = $('.buttonEdit').modal({
    //             header: 'Edit Data',
    //             render : function(el, data){

    //             data.onSuccess = function(){
    //                 $(edit).data('modal').close();
    //                 folder.data('plugin_folder').fetchData();

    //             };
    //             data.isReset = false;

    //             $(el).form(data).data('form');
                
    //             $('.close').on('click', function() {
    //                 $(edit).data('modal').close();
    //                 folder.data('plugin_folder').fetchData();
    //             })
    //             }
    //         });

    //         var del = $('.buttonDelete').modal({
    //             header: 'Hapus Data',
    //             render : function(el, data){
    //                 el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
    //                 data.onSuccess = function(){
    //                     $(del).data('modal').close();
    //                     folder.data('plugin_folder').fetchData();
    //                 };
    //                 data.isReset = true;
    //                 $('.form', el).form(data).data('form');

    //                 $('.close').on('click', function() {
    //                 $(edit).data('modal').close();
    //                 folder.data('plugin_folder').fetchData();
    //             })
    //             }
    //         });
    //     },
    //     filter: {
    //         wrapper: $('.contentWrap'),
    //         data : {
    //             data: _xhr
    //         }
    //     }
    // });


</script>
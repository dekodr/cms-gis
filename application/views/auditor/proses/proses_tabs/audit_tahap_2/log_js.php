<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('auditor/proses/proses_tabs/audit_tahap_2/getDataHistory/'.$id_log); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Nama Pemohon"
			},
			{
				"key"	: "symbol",
				"value"	: "Nama Auditor"
			},
			{
				"key"	: "symbol",
				"value"	: "Tanggal Audit"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [
		{
			renderCell: function(data, row, key, el){
				var html = '';
				if (data[2].value == '' || data[2].value == null) {
					html += '-';
				} else {
					html += defaultDate(data[2].value);
				}
				return html;
			},
			target : [2]

		},{
			renderCell: function(data, row, key, el){
				var html = '';
				_id = data[3].value;
				html += '<a href="'+site_url+'auditor/proses/proses_tabs/audit_tahap_2/edit/'+_id+'/history" class="button is-primary buttonCek"><i class="fa fa-eye"></i>&nbsp;<span class="icon-text"> Lihat Perbaikan</span></a>';
				return html;
			},
			target : [3]

		}],
		additionFeature: function(el){
			// el.append(insertButton(site_url+"auditor/proses/proses_tabs/audit_tahap_2/insert/<?php echo $id;?>"));
		},
		finish: function(){
		      	var edit = $('.buttonEdit').modal({
			        render : function(el, data){

			          data.onSuccess = function(){
			          	$(edit).data('modal').close();
			            table.data('plugin_tableGenerator').fetchData();

			          };
			          data.isReset = false;

			          $(el).form(data).data('form');

			        }
			    });
			  	var del = $('.buttonDelete').modal({
					header: 'Hapus Data',
					render : function(el, data){
						el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
						data.onSuccess = function(){
							$(del).data('modal').close();
							table.data('plugin_tableGenerator').fetchData();
						};
						data.isReset = true;
						$('.form', el).form(data).data('form');
					}
				});
			  	var cek = $('.buttonCek').modal({
	                header: 'Riwayat Perbaikan',
	                render : function(el, data){

	                data.onSuccess = function(){
	                    $(cek).data('modal').close();
	                    folder.data('plugin_folder').fetchData();

	                };
	                data.isReset = false;

	                $(el).form(data).data('form');
	                
	                $('.close').on('click', function() {
	                    $(edit).data('modal').close();
	                    folder.data('plugin_folder').fetchData();
	                })

	            }
	            });
		},
	});
  	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			$(el).form(data);
		}
	});
});
// ,{
// 			renderCell: function(data, row, key, el){
// 				var html = '';
// 				if (data[4].value == '' || data[4].value == null) {
// 					html += '<span style="background-color:red; color:white;">Belum di perbaiki</span>';
// 				} else {
// 					html += '<span style="background-color:green; color:white;">Sudah di perbaiki</span>';
// 				}
// 				return html;
// 			},
// 			target : [3]

// 		}
</script>
<script type="text/javascript">
$(function(){

	dataPost = {
		order: 'a.id',
		sort: 'desc'
	};

	var table = $('#tableGenerator').tableGenerator({
		url: '<?php echo site_url('auditor/proses/proses_tabs/pemilihan_auditor/getData/'.$id_client); ?>',
		data: dataPost,
		headers: [
			{
				"key"	: "name",
				"value"	: "Sertifikat"
			},
			{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				var html = '';
				if (data[1].value != null) {
					html += data[0].value+" ("+data[1].value+")";
				}else {
					html += data[0].value;
				}
				return html;
			},
			target : [0]

		},{
			renderCell: function(data, row, key, el){
				var html = '';
				html += '<a href="'+site_url+"auditor/proses/tabs/index/<?php echo $id_client; ?>/"+data[4].value+'" class="button is-primary"><span class="icon"><i class="fas fa-edit"></i></span>Proses Sertifikat</a>';
				return html;
			},
			target : [1]

		}],
		additionFeature: function(el){
			// el.append(insertButton(site_url+"master/badan_hukum/insert/<?php echo $id;?>"));
		},
		finish: function(){
		},
	});
});
</script>
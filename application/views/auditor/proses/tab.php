<div id="tabs" class="tabs">

  <ul>
    <?php if ($role['role_auditor'] == '') { ?>
      <li id="tabPermohonan">
          <a href="<?php echo site_url('approval_detail/permohonan_sertifikasi/index/'.$id_client) ?>">Permohonan Sertifikasi</a>
        </li>
        <li id="tabFormulir">
          <a href="<?php echo site_url('approval_detail/formulir_permohonan/index/'.$id_client) ?>">Formulir Permohonan</a>
        </li>
        <li id="tabLegalitas">
          <a href="<?php echo site_url('approval_detail/legalitas_perusahaan/index/'.$id_client) ?>">Legalitas Perusahaan</a>
        </li>
        <li id="tabDokumen">
          <a href="<?php echo site_url('approval_detail/dokumen_mutu/index/'.$id_client) ?>">Dokumen Mutu</a>
      </li>
      <?php if ($admin['id_role'] == 4 || $admin['id_role'] == 7) { ?>
      <li id="tabMandays">
        <a href="<?php echo site_url('auditor/proses/proses_tabs/perhitungan_mandays/index/'.$id_client.'/'.$id_certificate) ?>" >Perhitungan Mandays</a>
      </li>
      <li id="tabSuratTugas">
        <a href="<?php echo site_url('auditor/proses/proses_tabs/tugas/index/'.$id_client.'/'.$id_certificate) ?>" >Surat Tugas</a>
      </li>
      <li id="tabPemilihan">
        <a href="<?php echo site_url('auditor/proses/proses_tabs/pemilihan_auditor/index/'.$id_client.'/'.$id_certificate) ?>"  >Pemilihan Auditor dan Panelis</a>
      </li>
    <?php } ?>
    
    <?php if ($admin['id_role'] == 3 || $admin['id_role'] == 5 || $admin['id_role'] == 6 || $admin['id_role'] == 4 || $admin['id_role'] == 7 || $admin['id_role'] == 8) { ?>
      <li id="tabJadwal">
        <a href="<?php echo site_url('auditor/proses/proses_tabs/jadwal_audit/index/'.$id_client.'/'.$id_certificate) ?>" >Jadwal Audit</a>
      </li>
      <li id="tabTahap1">
        <a href="<?php echo site_url('auditor/proses/proses_tabs/audit_tahap_1/index/'.$id_client.'/'.$id_certificate) ?>" >Audit Tahap 1</a>
      </li>
      <li id="tabTahap2">
        <a href="<?php echo site_url('auditor/proses/proses_tabs/audit_tahap_2/index/'.$id_client.'/'.$id_certificate) ?>" >Audit Tahap 2</a>
      </li>
      <li id="tabDokumen">
        <a href="<?php echo site_url('auditor/proses/proses_tabs/dokumen_sampling/index/'.$id_client.'/'.$id_certificate) ?>" >Dokumen Sampling</a>
      </li>
      <?php if ($admin['id_role'] != 8 && $admin['id_role'] != 3 && $admin['id_role'] != 5) { ?>
        <li id="tabHasil">
          <a href="<?php echo site_url('auditor/proses/proses_tabs/hasil_uji/index/'.$id_client.'/'.$id_certificate) ?>">Hasil Uji</a>
        </li>
      <?php } ?>
    <?php } ?>

    <?php if ($admin['id_role'] == 4 || $admin['id_role'] == 7) { ?>
      <li id="tabPanel">
        <a href="<?php echo site_url('auditor/proses/proses_tabs/panel_sertifikasi/index/'.$id_client.'/'.$id_certificate) ?>">Panel Sertifikasi</a>
      </li>
      <li id="tabVerifikasi">
        <a href="<?php echo site_url('auditor/proses/proses_tabs/verifikasi/index/'.$id_client.'/'.$id_certificate) ?>">Hasil Verifikasi</a>
      </li>
    <?php } ?>      
        
    <?php } else {
      // echo "asdasdsa123 ";print_r($role);die;
      $per_role = explode(',', $role['role_auditor']);
      // print_r($per_role);die;
    ?>
        <li id="tabPermohonan">
              <a href="<?php echo site_url('approval_detail/permohonan_sertifikasi/index/'.$id_client) ?>">Permohonan Sertifikasi</a>
            </li>
            <li id="tabFormulir">
              <a href="<?php echo site_url('approval_detail/formulir_permohonan/index/'.$id_client) ?>">Formulir Permohonan</a>
            </li>
            <li id="tabLegalitas">
              <a href="<?php echo site_url('approval_detail/legalitas_perusahaan/index/'.$id_client) ?>">Legalitas Perusahaan</a>
            </li>
            <li id="tabDokumen">
              <a href="<?php echo site_url('approval_detail/dokumen_mutu/index/'.$id_client) ?>">Dokumen Mutu</a>
            </li>
         <?php if (in_array(4,$per_role) || in_array(7,$per_role)) { ?>
          <li id="tabMandays">
            <a href="<?php echo site_url('auditor/proses/proses_tabs/perhitungan_mandays/index/'.$id_client.'/'.$id_certificate) ?>" >Perhitungan Mandays</a>
          </li>
          <li id="tabSuratTugas">
            <a href="<?php echo site_url('auditor/proses/proses_tabs/tugas/index/'.$id_client.'/'.$id_certificate) ?>" >Surat Tugas</a>
          </li>
          <li id="tabPemilihan">
            <a href="<?php echo site_url('auditor/proses/proses_tabs/pemilihan_auditor/index/'.$id_client.'/'.$id_certificate) ?>"  >Pemilihan Auditor dan Panelis</a>
          </li>
        <?php } ?>
        
        <?php if (in_array(3,$per_role) || in_array(5,$per_role) || in_array(6,$per_role) || in_array(4,$per_role) || in_array(7,$per_role) || in_array(8,$per_role)) { ?>
          <li id="tabJadwal">
            <a href="<?php echo site_url('auditor/proses/proses_tabs/jadwal_audit/index/'.$id_client.'/'.$id_certificate) ?>" >Jadwal Audit</a>
          </li>
          <li id="tabTahap1">
            <a href="<?php echo site_url('auditor/proses/proses_tabs/audit_tahap_1/index/'.$id_client.'/'.$id_certificate) ?>" >Audit Tahap 1</a>
          </li>
          <li id="tabTahap2">
            <a href="<?php echo site_url('auditor/proses/proses_tabs/audit_tahap_2/index/'.$id_client.'/'.$id_certificate) ?>" >Audit Tahap 2</a>
          </li>
          <li id="tabDokumen">
            <a href="<?php echo site_url('auditor/proses/proses_tabs/dokumen_sampling/index/'.$id_client.'/'.$id_certificate) ?>" >Dokumen Sampling</a>
          </li>
          <?php if ($value != 8 && $value != 3 && $value != 5) { ?>
            <li id="tabHasil">
              <a href="<?php echo site_url('auditor/proses/proses_tabs/hasil_uji/index/'.$id_client.'/'.$id_certificate) ?>">Hasil Uji</a>
            </li>
          <?php } ?>
        <?php } ?>

        <?php if (in_array(4,$per_role) || in_array(7,$per_role)) { ?>
          <li id="tabPanel">
            <a href="<?php echo site_url('auditor/proses/proses_tabs/panel_sertifikasi/index/'.$id_client.'/'.$id_certificate) ?>">Panel Sertifikasi</a>
          </li>
          <?php if (in_array(3,$per_role) || in_array(5,$per_role) || in_array(6,$per_role) || in_array(4,$per_role) || in_array(7,$per_role) || in_array(8,$per_role)) { ?>
            <li id="tabVerifikasi">
              <a href="<?php echo site_url('auditor/proses/proses_tabs/verifikasi/index/'.$id_client.'/'.$id_certificate) ?>">Hasil Verifikasi</a>
            </li>
            
          <?php } ?>
        <?php } ?>

    <?php } ?>
  </ul>
  
</div>
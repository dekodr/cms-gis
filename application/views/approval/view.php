<div id="tabs" class="tabs">

  <ul>

    <li id="tabIdentitasPemohon">
      <a href="<?php echo site_url('approval_detail/Identitas_pemohon/index/'.$id) ?>" >Identitas Pemohon</a>
    </li>
    <li id="tabIdentitasPabrik">
      <a href="<?php echo site_url('approval_detail/Identitas_pabrik/index/'.$id) ?>"  >Identitas Pabrik</a>
    </li>
    <li id="tabPermohonanSertifikasi">
      <a href="<?php echo site_url('approval_detail/Permohonan_sertifikasi/index/'.$id) ?>" >Permohonan Sertifikasi</a>
    </li>
    <?php if ($this->session->userdata('admin')['id_role'] != 4) { ?>
      <li id="tabFormulirPermohonan">
        <a href="<?php echo site_url('approval_detail/Formulir_permohonan/index/'.$id) ?>" >Formulir Permohonan</a>
      </li>
      <li id="tabLegalitasPerusahaan">
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/index/'.$id) ?>" >Legalitas Perusahaan</a>
      </li>
    <?php } ?>
    <li id="tabDokumenMutu">
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/index/'.$id) ?>" >Dokumen Mutu</a>
    </li>
     <li id="tabPembayaran">
      <a href="<?php echo site_url('approval_detail/Pembayaran/index/'.$id) ?>">Pembayaran</a>
    </li>
    <?php if ($this->session->userdata('admin')['id_role'] != 12) { ?>
    <li id="tabVerifikasi">
      <a href="<?php echo site_url('approval_detail/verifikasi/index/'.$id) ?>">Hasil Verifikasi</a>
    </li>
    <?php } ?>

  </ul>
  
</div>
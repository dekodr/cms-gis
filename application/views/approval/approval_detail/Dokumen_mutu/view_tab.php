<div id="tabsDokumenMutu" class="tabs">

  <ul>
    <?php if (!empty($permohonan['sistem_sertifikasi_id'] == 15)) { ?>

    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/PM/'.$id) ?>">Panduan Mutu</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/SSM/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/RM/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/DID/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/SO/'.$id) ?>">Struktur Organisasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/APP/'.$id) ?>">Alur Proses Produksi</a>
    </li>
      
    <?php } if (!empty($permohonan['sistem_sertifikasi_id'] == 16)) { ?>

    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/PM/'.$id) ?>">Panduan Mutu</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/SSM/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/RM/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/DID/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/SO/'.$id) ?>">Struktur Organisasi</a>
    </li>

    <?php } if (!empty($permohonan['jenis_permohonan_id'] == 1)) { ?>

    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/PM/'.$id) ?>">Panduan Mutu</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/SSM/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/RM/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/DID/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/SO/'.$id) ?>">Struktur Organisasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/APP/'.$id) ?>">Alur Proses Produksi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/PP/'.$id) ?>">Peralatan Produksi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/PMP/'.$id) ?>">Pengendalian Mutu Produk</a>
    </li>
      
    <?php } if (!empty($permohonan['jenis_permohonan_id'] == 2)) { ?>

    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/PM/'.$id) ?>">Panduan Mutu</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/SSM/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/RM/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/DID/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/SO/'.$id) ?>">Struktur Organisasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/APP/'.$id) ?>">Alur Proses Produksi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/PP/'.$id) ?>">Peralatan Produksi</a>
    </li>
     
    <?php } if (!empty($permohonan['jenis_permohonan_id'] != 1) && !empty($permohonan['jenis_permohonan_id'] != 2) && !empty($permohonan['sistem_sertifikasi_id'] != 15) && !empty($permohonan['sistem_sertifikasi_id'] != 16)) { ?>

    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/PM/'.$id) ?>">Panduan Mutu</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/SSM/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/RM/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/DID/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/SO/'.$id) ?>">Struktur Organisasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/APP/'.$id) ?>">Alur Proses Produksi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/PP/'.$id) ?>">Peralatan Produksi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/PMP/'.$id) ?>">Pengendalian Mutu Produk</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/PBB/'.$id) ?>">Pengendalian Bahan Baku</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/PL/'.$id) ?>">Packing List / Invoice List</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Dokumen_mutu/BL/'.$id) ?>">Bill Of Landing (BL)</a>
    </li>
      
    <?php } ?>   

  </ul>
  
</div>
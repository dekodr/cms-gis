<div id="tabsDokumenMutuSPPTS5" class="tabs">

  <ul>

    <li>
      <a href="<?php echo site_url('approval_detail/DM/SPPTS5/PM/'.$id) ?>">Panduan Mutu</a>
    </li>
   <li>
      <a href="<?php echo site_url('approval_detail/DM/SPPTS5/SSM/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li> 
   <li>
      <a href="<?php echo site_url('approval_detail/DM/SPPTS5/RM/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/SPPTS5/DID/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/SPPTS5/SO/'.$id) ?>">Struktur Organisasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/SPPTS5/APP/'.$id) ?>">Alur Proses Produksi</a>
    </li> 
    <li>
      <a href="<?php echo site_url('approval_detail/DM/SPPTS5/PP/'.$id) ?>">Peralatan Produksi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/SPPTS5/PMP/'.$id) ?>">Pengendalian Mutu Produk</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/SPPTS5/PBB/'.$id) ?>">Pengendalian Bahan Baku</a>
    </li> 
    <!-- <li>
      <a href="<?php echo site_url('approval_detail/DM/SPPTS5/PL/'.$id) ?>">Packing List / Invoice List</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/SPPTS5/BL/'.$id) ?>">Bill Of Landing (BL)</a>
    </li> -->

  </ul>
  
</div>
<div id="tabsDokumenMutu9001" class="tabs">

  <ul>

    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO9001/PM/'.$id) ?>">Panduan Mutu</a>
    </li>
   <!--  <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO9001/SSM/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li> -->
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO9001/RM/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO9001/DID/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO9001/SO/'.$id) ?>">Struktur Organisasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO9001/APP/'.$id) ?>">Alur Proses Produksi</a>
    </li>
   <!--  <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO9001/PP/'.$id) ?>">Peralatan Produksi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO9001/PMP/'.$id) ?>">Pengendalian Mutu Produk</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO9001/PBB/'.$id) ?>">Pengendalian Bahan Baku</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO9001/PL/'.$id) ?>">Packing List / Invoice List</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO9001/BL/'.$id) ?>">Bill Of Landing (BL)</a>
    </li> -->
     <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO9001/DTM/'.$id) ?>">Dokumen Tinjauan Manajemen</a>
    </li>
     <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO9001/DIA/'.$id) ?>">Dokumen Internal Audit</a>
    </li>

  </ul>
  
</div>
<div id="tabsDokumenMutu14001" class="tabs">

  <ul>

    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/PM/'.$id) ?>">Panduan Mutu</a>
    </li>
   <!--  <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/SSM/'.$id) ?>">Sertifikasi Sistem Manajemen</a>
    </li> -->
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/RM/'.$id) ?>">Risk Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/DID/'.$id) ?>">Daftar Induk Dokumen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/SO/'.$id) ?>">Struktur Organisasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/APP/'.$id) ?>">Alur Proses Produksi</a>
    </li>
   <!--  <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/PP/'.$id) ?>">Peralatan Produksi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/PMP/'.$id) ?>">Pengendalian Mutu Produk</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/PBB/'.$id) ?>">Pengendalian Bahan Baku</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/PL/'.$id) ?>">Packing List / Invoice List</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/BL/'.$id) ?>">Bill Of Landing (BL)</a>
    </li> -->
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/DAL/'.$id) ?>">Dokumen Aspek Lingkungan</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/DKSL/'.$id) ?>">Dokumen Kebijaksanaan & Sasaran Lingkungan</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/DTD/'.$id) ?>">Dokumen Tanggap Darurat</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/DTM/'.$id) ?>">Dokumen Tinjauan Manajemen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/DM/ISO14001/DIA/'.$id) ?>">Dokumen Internal Audit</a>
    </li>

  </ul>
  
</div>
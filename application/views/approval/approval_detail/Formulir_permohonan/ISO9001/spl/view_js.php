<script type="text/javascript">

$(function(){

    $.ajax({
        url : '<?php echo site_url('approval_detail/formulir_permohonan/verifikasi/'.$id.'/spl_file')?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                alert('Sukses');
            }
            xhr.successMessage = 'Berhasil !!';

            $('#formSpl .form').form(xhr);
        }
    });
    $('#formSpl').on('submit', function(e){
        e.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url : '<?php echo site_url('approval_detail/formulir_permohonan/commit_approve/'.$id.'/1/spl_file') ?>',
            method: 'POST',
            dataType: 'json',
            data : form_data
        }).done(function(response){ 

             if(response.status){
                var current_index = $("#tabs").tabs("option","active");
                $('#tabs').tabs('load',current_index);
                $('.btnVerifikasi').data('modal').close();
            } else {
                alert('Gagal!!');
            }
        });
    })
});


</script>
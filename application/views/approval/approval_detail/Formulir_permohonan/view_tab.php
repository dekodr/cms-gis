<div id="tabsFormulirPermohonan" class="tabs">

  <ul>
    <?php if (!empty($permohonan['jenis_permohonan_id'] == 1)) { ?>
      
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/SPS/'.$id) ?>">Surat Permohonan Sertifikasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/FA/'.$id) ?>">Formulir Aplikasi</a>
    </li>
    
    <?php } if(!empty($permohonan['jenis_permohonan_id'] == 2)) { ?>

    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/SPS/'.$id) ?>">Surat Permohonan Sertifikasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/FA/'.$id) ?>">Formulir Aplikasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/SPKS/'.$id) ?>">Surat Perjanjian Kerjasama Sertifkasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/DP/'.$id) ?>">Data Pemohon</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/IP/'.$id) ?>">Informasi Produsen</a>
    </li>

    <?php } if(!empty($permohonan['jenis_permohonan_id'] == 3)) { ?>

    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/SPS/'.$id) ?>">Surat Permohonan Sertifikasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/SPM/'.$id) ?>">Surat Pelimpahan Merk  </a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/SPJPS/'.$id) ?>">Surat Pernyataan Jaminan Proses Sertifikat</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/SPL/'.$id) ?>">Surat Perjanjian Lisensi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/SPKS/'.$id) ?>">Surat Perjanjian Kerjasama Sertifkasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/FA/'.$id) ?>">Formulir Aplikasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/DP/'.$id) ?>">Data Pemohon</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/IP/'.$id) ?>">Informasi Produsen</a>
    </li>

    <?php } if (!empty($permohonan['jenis_permohonan_id'] != 3) && !empty($permohonan['jenis_permohonan_id'] != 2) && !empty($permohonan['jenis_permohonan_id'] != 1))  { ?>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/SPS/'.$id) ?>">Surat Permohonan Sertifikasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/SPM/'.$id) ?>">Surat Pelimpahan Merk	</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/SPJPS/'.$id) ?>">Surat Pernyataan Jaminan Proses Sertifikat</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/SPL/'.$id) ?>">Surat Perjanjian Lisensi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/SPKS/'.$id) ?>">Surat Perjanjian Kerjasama Sertifkasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/FA/'.$id) ?>">Formulir Aplikasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/DP/'.$id) ?>">Data Pemohon</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/TPS/'.$id) ?>">Tinjauan Permohonan Sertifikasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/Formulir_permohonan/IP/'.$id) ?>">Informasi Produsen</a>
    </li>
    <?php } ?>

  </ul>
  
</div>
<div id="tabsFormulirPermohonanSPPTS5" class="tabs">

  <ul>
    <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/SPS/'.$id) ?>">Surat Permohonan Sertifikasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/SPM/'.$id) ?>">Surat Pelimpahan Merk</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/SPJPS/'.$id) ?>">Surat Pernyataan Jaminan Proses Sertifikat</a>
    </li>
    <!-- <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/SPL/'.$id) ?>">Surat Perjanjian Lisensi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/SPKS/'.$id) ?>">Surat Perjanjian Kerjasama Sertifkasi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/FA/'.$id) ?>">Formulir Aplikasi</a>
    </li> -->
    <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/DP/'.$id) ?>">Data Pemohon</a>
    </li>
   <!--  <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/TPS/'.$id) ?>">Tinjauan Permohonan Sertifikasi</a>
    </li> -->
    <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/IP/'.$id) ?>">Informasi Produsen</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/FMP/'.$id) ?>">Formulir Mutu Produk</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/FMBB/'.$id) ?>">Formulir Mutu Bahan Baku</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/FPP1/'.$id) ?>">Formulir Pengendalian Produk</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/FPPI/'.$id) ?>">Formulir Peralatan Pengujian Inspeksi</a>
    </li>
    <li>
      <a href="<?php echo site_url('approval_detail/FP/SPPTS5/FPP2/'.$id) ?>">Formulir Peralatan Produksi</a>
    </li>

  </ul>
  
</div>
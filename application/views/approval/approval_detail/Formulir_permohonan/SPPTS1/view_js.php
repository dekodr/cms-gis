<script type="text/javascript">

$(function(){

    $.ajax({
        url : '<?php echo site_url('approval_detail/akta/verifikasi/'.$id)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                alert('Sukses');
            }
            xhr.successMessage = 'Berhasil !!';

            $('#formFormulirPermohonanSPPTS1 .form').form(xhr);
        }
    });
    $('#formFormulirPermohonanSPPTS1').on('submit', function(e){
        e.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url : '<?php echo site_url('approval_detail/akta/commit_approve/'.$id.'/1') ?>',
            method: 'POST',
            dataType: 'json',
            data : form_data
        }).done(function(response){ 

             if(response.status){
                var current_index = $("#tabs").tabs("option","active");
                $('#tabs').tabs('load',current_index);
                $('.btnVerifikasi').data('modal').close();
            } else {
                alert('Gagal!!');
            }
        });
    })
});


</script>
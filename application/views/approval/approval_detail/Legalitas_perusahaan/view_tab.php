<div id="tabsLegalitasPerusahaan" class="tabs">

  <ul>
    <?php if (!empty($permohonan['sistem_sertifikasi_id'] == 15)) { ?>
    
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/AP/'.$id) ?>">Akte Perusahaan</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/NIB/'.$id) ?>">NIB</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/SKDU/'.$id) ?>">SKDU</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/IUI/'.$id) ?>">IUI</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/SIUP/'.$id) ?>">SIUP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/TDP/'.$id) ?>">TDP</a>
      </li>

    <?php } if (!empty($permohonan['sistem_sertifikasi_id'] == 16)) { ?>

      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/AP/'.$id) ?>">Akte Perusahaan</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/NIB/'.$id) ?>">NIB</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/SKDU/'.$id) ?>">SKDU</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/IUI/'.$id) ?>">IUI</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/SIUP/'.$id) ?>">SIUP</a>
      </li>

    <?php } if (!empty($permohonan['jenis_permohonan_id'] == 1)) { ?>
      
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/AP/'.$id) ?>">Akte Perusahaan</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/NIB/'.$id) ?>">NIB</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/SKDU/'.$id) ?>">SKDU</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/IUI/'.$id) ?>">IUI</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/SIUP/'.$id) ?>">SIUP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/TDP/'.$id) ?>">TDP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/KTP/'.$id) ?>">KTP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/API/'.$id) ?>">API</a>
      </li>

    <?php } if (!empty($permohonan['jenis_permohonan_id'] == 2)) { ?>

      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/AP/'.$id) ?>">Akte Perusahaan</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/NIB/'.$id) ?>">NIB</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/SKDU/'.$id) ?>">SKDU</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/IUI/'.$id) ?>">IUI</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/SIUP/'.$id) ?>">SIUP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/TDP/'.$id) ?>">TDP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/KTP/'.$id) ?>">KTP</a>
      </li>

    <?php } if (!empty($permohonan['sistem_sertifikasi_id'] != 15) && !empty($permohonan['sistem_sertifikasi_id'] != 16) && !empty($permohonan['jenis_permohonan_id'] != 2) && !empty($permohonan['jenis_permohonan_id'] != 1)) { ?>
      
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/AP/'.$id) ?>">Akte Perusahaan</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/NIB/'.$id) ?>">NIB</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/SKDU/'.$id) ?>">SKDU</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/IUI/'.$id) ?>">IUI</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/SIUP/'.$id) ?>">SIUP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/TDP/'.$id) ?>">TDP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/KTP/'.$id) ?>">KTP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/API/'.$id) ?>">API</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/NIK/'.$id) ?>">NIK</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/HAKI/'.$id) ?>">HAKI</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/LP/'.$id) ?>">List Produk</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/Legalitas_perusahaan/SO/'.$id) ?>">Struktur Organisasi</a>
      </li>

    <?php }?>
  </ul>
  
</div>
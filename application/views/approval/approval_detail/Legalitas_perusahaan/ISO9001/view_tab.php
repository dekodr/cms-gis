<div id="tabsLegalitasPerusahaan9001" class="tabs">

  <ul>      
      <li>
        <a href="<?php echo site_url('approval_detail/LP/ISO9001/AP/'.$id) ?>">Akta Perusahaan</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/ISO9001/NIB/'.$id) ?>">NIB</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/ISO9001/SKDU/'.$id) ?>">SKDU</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/ISO9001/IUI/'.$id) ?>">IUI</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/ISO9001/SIUP/'.$id) ?>">SIUP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/ISO9001/TDP/'.$id) ?>">TDP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/ISO9001/KTP/'.$id) ?>">KTP</a>
      </li>
      <!-- <li>
        <a href="<?php echo site_url('approval_detail/LP/ISO9001/API/'.$id) ?>">API</a>
      </li> -->
      <li>
        <a href="<?php echo site_url('approval_detail/LP/ISO9001/NIK/'.$id) ?>">NIK</a>
      </li>
      <!-- <li>
        <a href="<?php echo site_url('approval_detail/LP/ISO9001/HAKI/'.$id) ?>">HAKI</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/ISO9001/LP/'.$id) ?>">List Produk</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/ISO9001/SO/'.$id) ?>">Struktur Organisasi</a>
      </li> -->
  </ul>
  
</div>
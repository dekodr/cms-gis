<div id="tabsLegalitasPerusahaanSPPTS1" class="tabs">

  <ul>      
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS1/AP/'.$id) ?>">Akta Perusahaan</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS1/NIB/'.$id) ?>">NIB</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS1/SKDU/'.$id) ?>">SKDU</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS1/IUI/'.$id) ?>">IUI</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS1/SIUP/'.$id) ?>">SIUP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS1/TDP/'.$id) ?>">TDP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS1/KTP/'.$id) ?>">KTP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS1/API/'.$id) ?>">API</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS1/NIK/'.$id) ?>">NIK</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS1/HAKI/'.$id) ?>">HAKI</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS1/LP/'.$id) ?>">List Produk</a>
      </li>
      <!--<li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS1/SO/'.$id) ?>">Struktur Organisasi</a>
      </li> -->
  </ul>
  
</div>
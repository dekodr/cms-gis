<div id="tabsLegalitasPerusahaanSPPTS5" class="tabs">

  <ul>      
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS5/AP/'.$id) ?>">Akta Perusahaan</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS5/NIB/'.$id) ?>">NIB</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS5/SKDU/'.$id) ?>">SKDU</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS5/IUI/'.$id) ?>">IUI</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS5/SIUP/'.$id) ?>">SIUP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS5/TDP/'.$id) ?>">TDP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS5/KTP/'.$id) ?>">KTP</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS5/API/'.$id) ?>">API</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS5/NIK/'.$id) ?>">NIK</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS5/HAKI/'.$id) ?>">HAKI</a>
      </li>
      <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS5/LP/'.$id) ?>">List Produk</a>
      </li>
      <!-- <li>
        <a href="<?php echo site_url('approval_detail/LP/SPPTS5/SO/'.$id) ?>">Struktur Organisasi</a>
      </li> -->
  </ul>
  
</div>
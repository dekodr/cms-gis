<script type="text/javascript">

$(function(){

    $.ajax({
        url : '<?php echo site_url('approval_detail/Identitas_pemohon/view/'.$id)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                alert('Sukses');
            }
            xhr.successMessage = 'Berhasil !!';
            xhr.formWrap = false;
            $('.form').form(xhr);
            
            <?php if ($vendor_status != 0 || $vendor_status != 2) { ?>
                $('#formformIdentitasPemohon .buttonRegBox').append('<a href="'+site_url+"note/insert/ms_identitas_pemohon/"+xhr.data.id_doc+'" class="button is-primary btnNote"><i class="fas fa-sticky-note"></i>&nbsp;Note&nbsp;<span>'+xhr.data.total_note+'</span></a>')
            <?php } ?>

            var note = $('.btnNote').modal({
                header: 'Note Verifikasi',
                render : function(el, data){

                    data.onSuccess = function(){
                        $(note).data('modal').close();
                        location.reload();
                        var current_index = $("#tabs").tabs("option","active");
                        $('#tabs').tabs('load',current_index);
                    }

                    $(el).form(data);
                    $(el).prepend('<div class="dots-list"><ol></ol></div>');
                    $.each(data.note, function(key, value){
                        $('.dots-list ol', el).append('<li><span class="date">'+moment(value.entry_stamp).format('DD MMM YYYY')+'</span>'+value.value+' </li>');
                    });
                }

            });
        }
    });
    $('#formformIdentitasPemohon').on('submit', function(e){
        e.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url : '<?php echo site_url('approval_detail/Identitas_pemohon/index/'.$id.'/1') ?>',
            method: 'POST',
            dataType: 'json',
            data : form_data
        }).done(function(response){ //
            console.log(response.status)
             if(response.status!='fail'){
                alert('Sukses!!');
                var current_index = $("#tabs").tabs("option","active");
                $('#tabs').tabs('load',current_index);
            } else {
                alert('Gagal!, Harap lengkapi data status perusahaan!');
            }
        });
    });
});


</script>
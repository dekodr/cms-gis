<script type="text/javascript">

$(function(){

    $.ajax({
        url : '<?php echo site_url('approval_detail/pengurus/verifikasi/'.$id.'/'.$id_vendor)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                alert('Sukses');
            }
            xhr.successMessage = 'Berhasil !!';
            console.log(xhr)

            $('#formPengurus .form').form(xhr);
            html = '';
            if (xhr.form[2].value == 'lifetime') {
                html += 'Seumur Hidup';
            } else {
                html += defaultDate(xhr.form[2].value)
            }
            console.log(html)
            $('.form2 span').html(html);
        }
    });
    $('#formPengurus').on('submit', function(e){
        e.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url : '<?php echo site_url('approval_detail/pengurus/commit_approve/'.$id.'/1') ?>',
            method: 'POST',
            dataType: 'json',
            data : form_data
        }).done(function(response){ 

             if(response.status){
                var current_index = $("#tabs").tabs("option","active");
                $('#tabs').tabs('load',current_index);
                console.log(current_index)
                $('.btnVerifikasi').data('modal').close();
            } else {
                alert('Gagal!!');
            }
        });
    })
});


</script>
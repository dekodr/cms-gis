<div class="mg-lg-12">

	<div class="block">
	 <div id="tabsIzin" class="tabs">

	  <ul>
	    	<li class="is-active">
	      		<a href="<?php echo site_url('approval_detail/izin/siup/'.$id) ?>">SIUP</a>
	    	</li>
	    	<li>
	      		<a href="<?php echo site_url('approval_detail/izin/siul/'.$id) ?>">Surat Izin Usaha Lainnya</a>
	    	</li>
	    	<li>
	      		<a href="<?php echo site_url('approval_detail/izin/asosiasi/'.$id) ?>">Sertifikat Asosiasi/Lainnya</a>
	    	</li>
	    	<li>
	      		<a href="<?php echo site_url('approval_detail/izin/siujk/'.$id) ?>">SIUJK</a>
	    	</li>
	    	<li>
	      		<a href="<?php echo site_url('approval_detail/izin/sbu/'.$id) ?>">SBU</a>
	    	</li>
		</ul>
	 </div>

		<div id="folderGenerator" class="foldering">

		</div>



	</div>

</div>

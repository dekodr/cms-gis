<script type="text/javascript">

$(function(){

    $.ajax({
        url : '<?php echo site_url('approval_detail/tdp/verifikasi/'.$id)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                alert('Sukses');
            }
            xhr.successMessage = 'Berhasil !!';
            console.log(xhr)

            $('#formTdp .form').form(xhr);
            html = '';
            if (xhr.form[2].value == 'lifetime') {
                html += 'Selama Perusahaan Berdiri';
            } else {
                html += defaultDate(xhr.form[2].value)
            }
            console.log(html)
            $('.form2 span').html(html);
        }
    });
    $('#formTdp').on('submit', function(e){
        e.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url : '<?php echo site_url('approval_detail/tdp/commit_approve/'.$id.'/1') ?>',
            method: 'POST',
            dataType: 'json',
            data : form_data
        }).done(function(response){ 

             if(response.status){
                var current_index = $("#tabs").tabs("option","active");
                $('#tabs').tabs('load',current_index);
                $('.btnVerifikasi').data('modal').close();
            } else {
                alert('Gagal!!');
            }
        });
    })
});


</script>
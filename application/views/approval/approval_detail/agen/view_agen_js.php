<script type="text/javascript">

$(function(){

    $.ajax({
        url : '<?php echo site_url('approval_detail/agen/verifikasiProduk/'.$id)?>',
        method: 'POST',
        async : false,
        dataType : 'json',
        success: function(xhr){
            xhr.onSuccess = function(data){
                alert('Sukses');
            }
            xhr.successMessage = 'Berhasil !!';

            $('#formAgenProduk .form').form(xhr);
             html = '';
            if (xhr.form[3].value == 'lifetime') {
                html += 'Selama Perusahaan Berdiri';
            } else {
                html += defaultDate(xhr.form[6].value)
            }
            console.log(html)
            $('.form3 span').html(html);
        }
    });
    var table = $('#formAgenProduk .table').tableGenerator({
        url: '<?php echo site_url('approval_detail/agen/getProduk/'.$id); ?>',
        data: dataPost,
        headers: [
            {
                "key"   : "produk",
                "value" : "Produk"
            },
            {
                "key"   : "merk",
                "value" : "Merk"
            }
            ],
        columnDefs : [{
            renderCell: function(data, row, key, el){
                // var html = '<label class="orangeAtt"><input type="checkbox" name="mandatory" value="1">&nbsp;<i class="fa fa-exclamation-triangle"></i>&nbsp;Mandatory</label>'+
                // '<label class="nephritisAtt"><input type="radio" name="status" value="1">&nbsp;<i class="fa fa-check"></i>&nbsp;OK</label>'+
                // '<label class="pomegranateAtt"><input type="radio" name="status" value="0">&nbsp;<i class="fa fa-times"></i>&nbsp;Not OK</label>';
                // return html;
            },
            target : [2]

        }],
    });
    $('#formAgenProduk').on('submit', function(e){
        e.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url : '<?php echo site_url('approval_detail/agen/commit_approve_agen/'.$id.'/1') ?>',
            method: 'POST',
            dataType: 'json',
            data : form_data
        }).done(function(response){ 

             if(response.status){
                var current_index = $("#tabs").tabs("option","active");
                $('#tabs').tabs('load',current_index);
                console.log(current_index)
                $('.btnVerifikasi').data('modal').close();
            } else {
                alert('Gagal!!');
            }
        });
    })
});


</script>
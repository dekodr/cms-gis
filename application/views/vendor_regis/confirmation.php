 <section class="main-content">

 	<div class="wrapper ">

 		<div class="col col-12 is-centered" style="padding: 0 0 35px;">

 			<div class="col col-4">
 				<div class="card" style="padding: 30px; border-radius: 8px;">

 					<div class="card-header">
 						<div class="icons" style="position: relative;">
 							<img src="<?php echo base_url('assets/images/002-portfolio.png') ?>" alt="" height="100px">
 							<img class="pulse icon__check" src="<?php echo base_url('assets/images/001-confirmation.png') ?>" alt="" height="50px">
 						</div>
 					</div>

 					<div class="card-title" style="margin-top: 45px">
 						Tahap Registrasi Berhasil!
 					</div>

 					<div class="card-footer">
 						<p>Tahap registrasi telah berhasil, <br> silahkan hubungi admin.</p>
 						<div class="form-group btn-group" style="text-align: center;">
 							<a href="<?php echo site_url() ?>" class="button btn is-primary">Lanjutkan</a>
 						</div>
 					</div>

 				</div>
 			</div>

 		</div>

 	</div>

 </section>
<!-- <script src="https://code.highcharts.com/highcharts.js"></script> -->
<!-- <script src="https://code.highcharts.com/modules/variable-pie.js"></script> -->
<script type="text/javascript">

$(function(){

    var id_role = '<?php echo $this->session->userdata('admin')['id_role'] ?>';

    dataPost = {
        order: 'id',
        sort: 'desc'
    };

    dataPostAss = {
        order: 'c.id',
        sort: 'desc'
    };
    
    var table = $('#tableClient').tableGenerator({
        url: '<?php echo site_url('dashboard/get_assignment/'.$id); ?>',
        data: dataPostAss,
        headers: [
            {
                "key"   : "name",
                "value" : "Nama PT"
            },
            {
                "key"   : "name",
                "value" : "Tanggal Audit"
            },
            {
                "key"   : "name",
                "value" : "Status"
            },
            {
                "key"   : "action",
                "value" : "Action",
                "sort"  : false
            }],
        columnDefs : [{
            renderCell: function(data, row, key, el){
                var html = '';
                if (data[1].value == '' || data[1].value == null) {
                    html += "-";
                } else {
                    html += defaultDate(data[1].value)+' sampai '+defaultDate(data[5].value);
                }
                return html;
            },
            target : [1]

        },{
            renderCell: function(data, row, key, el){
               
                return data[2].value+' ('+data[6].value+')';
            },
            target : [2]

        },{
            renderCell: function(data, row, key, el){
                var html = '';
                html += '<a href="'+site_url+"auditor/proses/proses/sertifikat/"+data[4].value+'" class="button is-primary"><span class="icon"><i class="fas fa-user"></i></span>Proses</a>';
                return html;
            },
            target : [3]

        }],
        additionFeature: function(el){
            <?php if ($admin['id_role'] == 4) { ?>
                // el.append(insertButton(site_url+"dashboard/insert_assignment/<?php echo $id;?>"));
            <?php } ?>
        },
        finish: function(){

        }
    });
    var add = $('.buttonAdd').modal({
        render : function(el, data){
            data.onSuccess = function(){
                $(add).data('modal').close();
                table.data('plugin_tableGenerator').fetchData();
            }
            $(el).form(data);
        }
    });

    var tableNotif = $('#tableNotification').tableGenerator({
        url: '<?php echo site_url('dashboard/get_notification/'.$id); ?>',
        data: dataPost,
        headers: [
            {
                "key"   : "name",
                "value" : "Nama User"
            },
            {
                "key"   : "name",
                "value" : "Keterangan"
            },
            {
                "key"   : "action",
                "value" : "Action",
                "sort"  : false
            }],
        columnDefs : [{
            renderCell: function(data, row, key, el){
                var html = '';
                html += '<a href="'+site_url+"approval/index/"+data[2].value+'" class="button is-primary"><span class="icon"><i class="fas fa-edit"></i></span>Cek Data</a>'
                return html;
            },
            target : [2]

        }],
        additionFeature: function(el){
            
        },
        finish: function(){

        }
    });

    var table = $('#daftar_tunggu').tableGenerator({
        url: '<?php echo site_url('dashboard/get_client/'.$id); ?>',
        data: dataPost,
        headers: [
            {
                "key"   : "name",
                "value" : "Nama PT"
            },
            {
                "key"   : "name",
                "value" : "Status"
            },
            {
                "key"   : "action",
                "value" : "Action",
                "sort"  : false
            }],
        columnDefs : [
        {
            renderCell: function(data, row, key, el){
                var html = '';
                if (data[1].value == 0 && (data[3].value == '' || data[3].value == null)) {
                    html += 'Baru Daftar'; 
                }else if (data[1].value == 1 && (data[3].value == '' || data[3].value == null || data[3].value == '0') ) {
                    html += 'Menunggu verifikasi admin'; 
                }else if (data[1].value == 1 && data[3].value == 1) {
                    html += 'Proses Pengajuan'; 
                }else if (data[1].value == 2) {
                    html += 'Proses Sertifikasi'; 
                }else if (data[1].value == 3) {
                    html += 'Penerbitan sertifikat';
                }
                return html;
            },
            target : [1]

        },{
            renderCell: function(data, row, key, el){
                var html = '';
                if (id_role == 1) {
                    if (data[1].value != 0 && data[1].value != 2) {
                        html += '<a href="'+site_url+"approval/index/"+data[2].value+'" class="button is-primary"><span class="icon"><i class="fas fa-check"></i></span>Verifikasi</a>';
                    }
                }
                if (id_role == 4) {
                    if (data[1].value == 2) {
                        html += '<a href="'+site_url+"auditor/proses/proses/index/"+data[2].value+'" class="button is-primary"><span class="icon"><i class="fas fa-user"></i></span>Proses</a>';
                    } else if (data[1].value == 3) {
                        html += '<a href="'+site_url+"auditor/proses/proses/index/"+data[2].value+'" class="button is-primary"><span class="icon"><i class="fas fa-edit"></i></span>Cek Data</a>';
                    }
                }
                return html;
            },
            target : [2]

        }],
        additionFeature: function(el){
            
        },
        finish: function(){

        }
    });
});

</script>

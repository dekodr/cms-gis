<?php
/*
 Folder Tree with PHP and jQuery.

 R. Savoul Pelister
 http://techlister.com

*/

class treeview {

	private $files;
	private $folder;
	
	function __construct( $path ) {
		// echo $path;die;
		
		$files = array();	
		
		if( file_exists( $path)) {
			if( $path[ strlen( $path ) - 1 ] ==  '/' )
				$this->folder = $path;
			else
				$this->folder = $path . '/';
			
			$this->dir = opendir( $path );
			while(( $file = readdir( $this->dir ) ) != false )
				$this->files[] = $file;
			closedir( $this->dir );
		}
	}

	function create_tree() {
		// echo $path;die;
		// print_r($this->files);die;
			
		if( count( $this->files ) > 2 ) { /* First 2 entries are . and ..  -skip them */
			natcasesort( $this->files );
			// print_r($this->files);die;
			$list = '<ul class="tree">';
			// Group folders first
			foreach( $this->files as $file ) {
				// print_r($file);die;
				// print_r($file);die;
				if( file_exists( $this->folder . $file ) && $file != '.' && $file != '..' && is_dir( $this->folder . $file )) {
					$list .= '<li class="linetree"><span class="icon tree"><i class="fas fa-angle-down"></i></span><span class="caret folder collapsed"><a href="#" rel="' . htmlentities( $this->folder . $file ) . '/">' . htmlentities( $file ) . '</a></span>';
				}
			}
			// Group all files
			foreach( $this->files as $file ) {
				if( file_exists( $this->folder . $file ) && $file != '.' && $file != '..' && !is_dir( $this->folder . $file )) {
					$ext = preg_replace('/^.*\./', '', $file);
					if ($ext != '') {
						$link = '<a href="'.base_url('assets/library/'.htmlentities( $file )).'" rel="' . htmlentities( $this->folder . $file ) . '">' . htmlentities( $file ) . '</a>';
					}
					else {
						$link = '<a href="#" rel="' . htmlentities( $this->folder . $file ) . '">' . htmlentities( $file ) . '</a>';
					}
					$list .= '<li class="file ext_' . $ext . '">'.$link.'</li>';
				}
			}
			$list .= '</ul>';	
			return $list;
		}
	}
}

?>
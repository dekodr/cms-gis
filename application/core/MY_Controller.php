<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
	public $id_client;

	public $_sideMenu;

	public $breadcrumb;

	public $header;

	public $content;

	public $script;

	public $form;

	public $activeMenu;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public $isClientMenu;

	public $needLogin = true;

	function __construct()
	{
		parent::__construct();
		$this->_sideMenu = array();
		$this->load->library('vms');
		$this->load->library('breadcrumb', array());
		$this->load->library('tablegenerator', array());
		$this->form_validation->set_error_delimiters('', '');
		if ($this->uri->segment(1) != '' && $this->needLogin) {
			if (!$this->session->userdata('admin')&&!$this->session->userdata('user')) {
				redirect(site_url());
			}
		}
	}

	function index($id = null)
	{
		$this->isAdmin();
		$this->breadcrumb = $this->breadcrumb->generate();
		$this->load->library('sideMenu', $this->_sideMenu);
		$user = $this->session->userdata('user');
		$admin = $this->session->userdata('admin');
		$data = array(
			'user' => ($user) ? $user['name'] : $admin['name'],
			'sideMenu' => $this->sidemenu->generate($this->activeMenu) ,
			'breadcrumb' => $this->breadcrumb,
			'header' => $this->header,
			'content' => $this->content,
			'script' => $this->script
		);
		$this->parser->parse('template/base', $data);
	}

	function formFilter()
	{
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form['filter'];
		echo json_encode($return);
	}

	function isAdmin()
	{
		if ($this->session->userdata('admin')) {
			$this->_sideMenu = array(
				array(
					'group' => 'dashboard',
					'title' => 'Dashboard',
					'url' => site_url() ,
					'role' => array(
						1,
						2,
						3,
						4,
						5,
						6,
						7,
						8
					)
				) ,
				array(
					'group' => 'dashboard',
					'title' => 'No. Pengajuan',
					'url' => site_url('admin/no_pengajuan') ,
					'role' => array(
						1
					)
				) ,
				array(
					'group' => 'dashboard',
					'title' => 'Data Perusahaan',
					'url' => site_url('vendor/admin/daftar_vendor'),
					'role' => array(
						1
					)
				) ,
				array(
					'group' => 'dashboard',
					'title' => 'Penerbitan Sertifikat',
					'url' => site_url('admin/penerbitan_sertifikat'),
					'role' => array(
						1
					)
				) ,
				// array(
				// 	'group' => 'dashboard',
				// 	'title' => 'NRP / NPB',
				// 	'url' => site_url('admin/nrp'),
				// 	'role' => array(
				// 		1
				// 	)
				// ) ,
				array(
					'title' => 'Master',
					'url' => '#',
					'role' => array(
						2,4
					) ,
					'list' => array(
						array(
							'url' => site_url('master/mandays') ,
							'title' => 'Perhitungan Mandays',
							'role' => array(
								4
							) ,
						),
						array(
							'url' => site_url('master/tugas') ,
							'title' => 'Surat Tugas',
							'role' => array(
								4
							) ,
						),
						array(
							'url' => site_url('master/user') ,
							'title' => 'User',
							'role' => array(
								2
							) ,
						),
						array(
							'group' => 'dashboard',
							'title' => 'Jenis Permohonan',
							'url' => site_url('admin/jenis_pengajuan') ,
							'role' => array(
								2
							)
						) ,
						array(
							'group' => 'dashboard',
							'title' => 'Lingkup Pemohon',
							'url' => site_url('admin/ruang_lingkup/index/1') ,
							'role' => array(
								2
							)
						),
						array(
							'group' => 'dashboard',
							'title' => 'Sistem Sertifikasi',
							'url' => site_url('admin/ruang_lingkup/index/2') ,
							'role' => array(
								2
							)
						),
						array(
							'group' => 'dashboard',
							'title' => 'Produk',
							'url' => site_url('admin/jenis_industri') ,
							'role' => array(
								2	
							)
						)
					)
				),
				array(
					'title' => 'Prosedur Dokumen Audit ',
					'url' => '#',
					'role' => array(
						3,
						4,
						5,
						6,
						7,
						8
					) ,
					'list' => array(
						array(
							'url' => site_url('auditor/pda/prosedur_audit') ,
							'title' => 'Prosedur Audit',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						) ,
						array(
							'url' => site_url('auditor/pda/kecukupan_dokumen') ,
							'title' => 'Kecukupan Dokumen',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						) ,
						array(
							'url' => site_url('auditor/pda/jadwal_audit') ,
							'title' => 'Jadwal Audit',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						),
						array(
							'url' => site_url('auditor/pda/daftar_hadir') ,
							'title' => 'Daftar Hadir',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						),
						array(
							'url' => site_url('auditor/pda/catatan_audit') ,
							'title' => 'Catatan Audit',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						),
						array(
							'url' => site_url('auditor/pda/logback_audit') ,
							'title' => 'Logbook Audit',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						),
						array(
							'url' => site_url('auditor/pda/penilaian_kinerja') ,
							'title' => 'Penilaian Kinerja',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						)
					)
				) ,
				array(
					'title' => 'Prosedur Dokumen Panel Audit',
					'url' => '#',
					'role' => array(
						3,
						4,
						5,
						6,
						7,
						8
					) ,
					'list' => array(
						array(
							'url' => site_url('auditor/pdpa/ppks') ,
							'title' => ' Prosedur Pengambilan Keputusan Sertifikasi',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						) ,
						// array(
						// 	'url' => site_url('auditor/pdpa/daftar_hadir') ,
						// 	'title' => 'Daftar Hadir',
						// 	'role' => array(
						// 		3,
						// 		4
						// 	) ,
						// ) ,
						array(
							'url' => site_url('auditor/pdpa/panel_sertifikasi') ,
							'title' => 'Panel Sertifikasi',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						)
					)
				) ,
				array(
					'title' => 'Prosedur Dokumen Sampling',
					'url' => '#',
					'role' => array(
						3,
						4,
						5,
						6,
						7,
						8
					) ,
					'list' => array(
						array(
							'url' => site_url('auditor/pds/prosedur_sampling') ,
							'title' => 'Prosedur Sampling',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						) ,
						array(
							'url' => site_url('auditor/pds/sampling_plan') ,
							'title' => 'Sampling Plan',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						) ,
						array(
							'url' => site_url('auditor/pds/bapc') ,
							'title' => 'BAPC',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						),
						array(
							'url' => site_url('auditor/pds/label') ,
							'title' => 'Label',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						),
						array(
							'url' => site_url('auditor/pds/lpk') ,
							'title' => 'Laporan pengambilan sampel',
							'role' => array(
								3,
								4,
								5,
								6,
								7,
								8
							) ,
						)
					)
				) ,
				array(
					'title' => 'Pembaharuan kompetensi',
					'url' => site_url('auditor/pk/upload_dokumen'),
					'role' => array(
						3,
						4,
						5,
						6,
						7,
						8
					) ,
					// 'list' => array(
					// 	array(
					// 		'url' => site_url('auditor/pk/jenis_pembaharuan') ,
					// 		'title' => 'Jenis Pembaharuan',
					// 		'role' => array(
					// 			3,
					// 			4
					// 		) ,
					// 	) ,
					// 	array(
					// 		'url' => site_url('auditor/pk/upload_dokumen') ,
					// 		'title' => 'Upload dokumen',
					// 		'role' => array(
					// 			3,
					// 			4
					// 		) ,
					// 	)
					// )
				) ,
				// array(
				// 	'group' => 'Audit kecukupan dokumen',
				// 	'title' => 'Audit kecukupan dokumen',
				// 	'url' => site_url('auditor/akd/akd') ,
				// 	'role' => array(
				// 		3,
				// 		4
				// 	)
				// ) ,
				// array(
				// 	'title' => 'Sampling LHU',
				// 	'url' => '#',
				// 	'role' => array(
				// 		3,
				// 		4
				// 	) ,
				// 	'list' => array(
				// 		// array(
				// 		// 	'url' => site_url('auditor/pds/sampling_plan') ,
				// 		// 	'title' => 'Sampling Plan',
				// 		// 	'role' => array(
				// 		// 		3,
				// 		// 		4
				// 		// 	) ,
				// 		// ) ,
				// 		// array(
				// 		// 	'url' => site_url('auditor/pds/label') ,
				// 		// 	'title' => 'Label',
				// 		// 	'role' => array(
				// 		// 		3,
				// 		// 		4
				// 		// 	) ,
				// 		// ) ,
				// 		// array(
				// 		// 	'url' => site_url('auditor/pds/bapc') ,
				// 		// 	'title' => 'BAPC',
				// 		// 	'role' => array(
				// 		// 		3,
				// 		// 		4
				// 		// 	) ,
				// 		// ),
				// 		array(
				// 			'url' => site_url('auditor/slhu/lpc') ,
				// 			'title' => 'Laporan pengambilan catatan',
				// 			'role' => array(
				// 				3,
				// 				4
				// 			) ,
				// 		)
				// 	)
				// ) ,
				// array(
				// 	'title' => 'Panel',
				// 	'url' => '#',
				// 	'role' => array(
				// 		3,
				// 		4
				// 	) ,
				// 	'list' => array(
				// 		// array(
				// 		// 	'url' => site_url('auditor/pdpa/daftar_hadir') ,
				// 		// 	'title' => 'Daftar Hadir',
				// 		// 	'role' => array(
				// 		// 		3,
				// 		// 		4
				// 		// 	) ,
				// 		// ) ,
				// 		array(
				// 			'url' => site_url('auditor/panel/dps') ,
				// 			'title' => 'Dokumen pengambilan keputusan',
				// 			'role' => array(
				// 				3,
				// 				4
				// 			) ,
				// 		)
				// 	)
				// ) ,
				array(
					'url' => site_url('auditor/form_perbaikan/form_perbaikan') ,
					'title' => 'Form Perbaikan',
					'role' => array(
						3
					) ,
				),
				// array(
				// 	'group' => 'dashboard',
				// 	'title' => 'Dokumen Mutu',
				// 	'url' => site_url('vendor/admin/daftar_vendor'),
				// 	'role' => array(
				// 		4,
				// 	)
				// )
				// array(
				// 	'group' => 'dashboard',
				// 	'title' => 'Form Permohonan',
				// 	'url' => site_url('admin/form_permohonan') ,
				// 	'role' => array(
				// 		1,
				// 	)
				// ) ,
				// array(
				// 	'group' => 'dashboard',
				// 	'title' => 'Legalitas Perusahaan',
				// 	'url' => site_url('admin/legalitas_perusahaan') ,
				// 	'role' => array(
				// 		1,
				// 	)
				// ) ,
				// array(
				// 	'group' => 'dashboard',
				// 	'title' => 'Dokumen Mutu',
				// 	'url' => site_url('admin/dokumen_mutu') ,
				// 	'role' => array(
				// 		1,
				// 	)
				// ) ,
				// array(
				// 	'group' => 'dashboard',
				// 	'title' => 'Note',
				// 	'url' => site_url('admin/note') ,
				// 	'role' => array(
				// 		1,
				// 	)
				// ) ,
				// array(
				// 	'group' => 'library',
				// 	'title' => 'Library',
				// 	'url' => site_url('library') ,
				// 	'role' => array(
				// 		1
				// 	)
				// ) ,
				// array(
				// 	'group' => 'master',
				// 	'title' => 'Master',
				// 	'url' => '#' ,
				// 	'role' => array(
				// 		1
				// 	),
				// 	'list' => array(
				// 		array(
				// 			'url' => site_url('master/user') ,
				// 			'title' => 'User',
				// 			'role' => array(
				// 				1
				// 			) ,
				// 		),
				// 		array(
				// 			'url' => site_url('master/certificate') ,
				// 			'title' => 'Sertifikat',
				// 			'role' => array(
				// 				1
				// 			) ,
				// 		),
				// 		array(
				// 			'url' => site_url('master/temuan') ,
				// 			'title' => 'Laporan Temuan',
				// 			'role' => array(
				// 				1
				// 			) ,
				// 		),
				// 		array(
				// 			'url' => site_url('master/badan_hukum') ,
				// 			'title' => 'Badan Hukum',
				// 			'role' => array(
				// 				1
				// 			) ,
				// 		),
				// 		array(
				// 			'url' => site_url('master/bidang') ,
				// 			'title' => 'Bidang',
				// 			'role' => array(
				// 				1
				// 			) ,
				// 		),
				// 		array(
				// 			'url' => site_url('master/sub_bidang') ,
				// 			'title' => 'Sub Bidang',
				// 			'role' => array(
				// 				1
				// 			) ,
				// 		),
				// 		array(
				// 			'url' => site_url('master/kurs') ,
				// 			'title' => 'Mata Uang',
				// 			'role' => array(
				// 				1
								
				// 			) ,
				// 		),
				// 	)
				// ) ,
				// array(
				// 	'title' => 'Client',
				// 	'url' => '#',
				// 	'role' => array(
				// 		1,
				// 		3,
				// 		4,
				// 		5,
				// 		6,
				// 		8,
				// 		12
				// 	) ,
				// 	'list' => array(
				// 		array(
				// 			'url' => site_url('vendor/admin/daftar_vendor') ,
				// 			'title' => 'Daftar Client',
				// 			'role' => array(
				// 				1,
				// 				4,
				// 				6,
				// 				8,
				// 				12
				// 			) ,
				// 		) ,
				// 		array(
				// 			'url' => site_url('vendor/admin/daftar_tunggu') ,
				// 			'title' => 'Verifikasi File',
				// 			'role' => array(
				// 				1,
				// 				4,
				// 				6,
				// 				8,
				// 				12
				// 			) ,
				// 		) ,
				// 		array(
				// 			'url' => site_url('vendor/admin/certificate') ,
				// 			'title' => 'Verifikasi Sertifikat',
				// 			'role' => array(
				// 				1,
				// 				4,
				// 				6,
				// 				8,
				// 				12
				// 			) ,
				// 		),
				// 		array(
				// 			'url' => site_url('vendor/admin/auditor') ,
				// 			'title' => 'Auditor Task',
				// 			'role' => array(
				// 				3
				// 			) ,
				// 		)
				// 	)
				// ) ,
			);
		}else if($this->session->userdata('user')) {
			$this->_sideMenu = array(
				array(
					'title' => 'Beranda',
					'url' => site_url('vendor/dashboard') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Identitas Pemohon',
					'url' => site_url('vendor/identitas_pemohon') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Identitas Pabrik',
					'url' => site_url('vendor/identitas_pabrik') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Permohonan Sertifikasi',
					'url' => site_url('vendor/permohonan_sertifikasi') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Formulir Permohonan',
					'url' => site_url('vendor/formulir_permohonan') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Legalitas Perusahaan',
					'url' => site_url('vendor/legalitas_perusahaan') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Dokumen Mutu',
					'url' => site_url('vendor/dokumen_mutu') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Pembayaran',
					'url' => site_url('vendor/Pembayaran') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Jadwal Audit',
					'url' => site_url('vendor/Jadwal_audit') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Audit Tahap 1',
					'url' => site_url('vendor/Hasil_audit') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Surat Tugas',
					'url' => site_url('vendor/Surat_tugas') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Tindakan Perbaikan',
					'url' => site_url('vendor/Perbaikan') ,
					'role' => array(
						11
					),
				),
				array(
					'title' => 'Penerbitan Sertifikat',
					'url' => site_url('vendor/Penerbitan_sertifikat_vendor') ,
					'role' => array(
						11
					),
				),
				// array(
				// 	'title' => 'NRP / NPB',
				// 	'url' => site_url('vendor/nrp') ,
				// 	'role' => array(
				// 		11
				// 	),
				// ),
				// array(
				// 	'title' => 'Administrasi',
				// 	'url' => site_url('vendor/administrasi') ,
				// 	'role' => array(
				// 		11
				// 	),
				// ),
				// array(
				// 	'title' => 'Akta Perusahaan',
				// 	'url' => site_url('vendor/akta') ,
				// 	'role' => array(
				// 		11
				// 	),
				// ),
				// array(
				// 	'title' => 'Domisili Perusahaan',
				// 	'url' => site_url('vendor/situ') ,
				// 	'role' => array(
				// 		11
				// 	),
				// ),
				// array(
				// 	'title' => 'Tanda Daftar Perusahaan',
				// 	'url' => site_url('vendor/tdp') ,
				// 	'role' => array(
				// 		11
				// 	),
				// ),
				// array(
				// 	'title' => 'Pengurus Perusahaan',
				// 	'url' => site_url('vendor/pengurus') ,
				// 	'role' => array(
				// 		11
				// 	),
				// ),
				// array(
				// 	'title' => 'Izin Usaha',
				// 	'url' => site_url('vendor/izin') ,
				// 	'role' => array(
				// 		11
				// 	),
				// ),
				// array(
				// 	'title' => 'Pabrikan/Keagenan/Distributor',
				// 	'url' => site_url('vendor/agen') ,
				// 	'role' => array(
				// 		11
				// 	),
				// ),
				// array(
				// 	'title' => 'Usulkan Sertifikat',
				// 	'url' => site_url('vendor/certificate') ,
				// 	'role' => array(
				// 		11
				// 	),
				// )
			);
		}
	}

	public	function validation($form = null)
	{

		ob_start();

		$_r = false;
		if ($form == null) {
			$form = $this->form['form'];
		}else{
			$this->form = $form;
		}
			foreach($form['form'] as $key =>  $value) {
				if($value['type'] == 'file'||$value['type'] == 'multiple_file'){
					unset($form['form'][$key]);
				}
			}
			// print_r($this->form);

			if($this->form['form'][0]==NULL){
				$_form = array();
				foreach ($this->form['form'] as $key => $value) {
					foreach ($value as $keys => $values) {
						$v = explode('|', $values['label']);
						$_form[$keys]['label'] = $v[0];
						$_form[$keys]['field'] = $values['field'];  
						$_form[$keys]['rules'] = $values['rules']; 
					}
 
				}
				$this->form_validation->set_rules($_form);
			}else{

				foreach($this->form['form'] as $key =>  $value) {
					$v = explode('|', $value['label']);

					$this->form['form'][$key]['label'] = $v[0];
				}

				$this->form_validation->set_rules($this->form['form']);

			}
		// }else{
		// 	$this->form_validation->set_rules($form);
		// 	$this->form =$form;
		// }

		if ($this->form_validation->run() == FALSE) {
			
			$return['status'] = 'error';
			if($this->form['form'][0]==NULL){
				foreach($form as $key=>$value) {
					foreach ($value as $keys => $values) {
						if ($values['type'] == 'file'||$values['type'] == 'multiple_file') {
							$return['file'][$key][$values['field']] = $this->session->userdata($values['field']);
						}

						if ($values['type'] == 'date_range') {
							$return['form'][$key][$values['field'][0]] = form_error($values['field'][0] . '_start');
							$return['form'][$key][$values['field'][1]] = form_error($values['field'][1] . '_start');
						}
						else {
							$return['form'][$key][$values['field']] = form_error($values['field']);
						}
					}
					
				}
			}else{
				//echo print_r($form);
				foreach($form as $value) {
					if ($value['type'] == 'file'||$value['type'] == 'multiple_file') {
						$return['file'][$value['field']] = $this->session->userdata($value['field']);
					}

					if ($value['type'] == 'date_range') {
						$return['form'][$value['field'][0]] = form_error($value['field'][0] . '_start');
						$return['form'][$value['field'][1]] = form_error($value['field'][1] . '_start');
					}
					else {
						$return['form'][$value['field']] = form_error($value['field']);
					}
				}
			}
			

			$_r = false;
		}
		else {
			$return['status'] = 'success';
			$_r = true;
		}

		echo json_encode($return);
		return $_r;
	}

	public function getData($id = null)
	{
		$config['query'] = $this->getData;
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function insert()
	{
		$this->form['url'] = $this->insertUrl;
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['entry_stamp'] = timestamp();
			if ($this->$modelAlias->insert($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}

	public function edit($id = null)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);

		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();

				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];

				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . $id);
		$this->form['button'] = array(
			array(
				'type'  => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type'  => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['edit_stamp'] = timestamp();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
	 public function getSingleData($id){
        $user  = $this->session->userdata('user');
        $modelAlias = $this->modelAlias;
        $getData   = $this->$modelAlias->selectData($id);

        foreach($this->form['form'] as $key => $value){
            $this->form['form'][$key]['readonly'] = TRUE;
            $this->form['form'][$key]['value'] = $getData[$value['field']];

            if($value['type']=='date_range'){
                foreach($value['field'] as $keyField =>$rowField){
                    $this->form['form'][$key]['value'][] = $getData[$rowField];
                }

            }
            if($value['type']=='money'){
                    $this->form['form'][$key]['value'] = number_format($getData[$value['field']]);
                }
            if($value['type']=='money_asing'){
                $this->form['form'][$key]['value'][] = $getData[$value['field'][0]];
                $this->form['form'][$key]['value'][] = number_format($getData[$value['field'][1]]);
            }
        }

        echo json_encode($this->form);
    }
	public function approveOvertimeUser($id)
	{
		$modelAlias = $this->modelAlias;
		$save = $this->input->post();
		$save['edit_stamp'] = timestamp();
		return $this->$modelAlias->update($id, $save);
	}

	public function delete($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->$modelAlias->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url($this->deleteUrl . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function upload_lampiran()
	{

		foreach($_FILES as $key => $row) {
			if(is_array($row['name'])){
				foreach ($row['name'] as $keys => $values) {
					$file_name = $row['name'] = $key . '_' . name_generator($_FILES[$key]['name'][$keys]);
					 $_FILES['files']['name']= $file_name;
			        $_FILES['files']['type']= $_FILES[$key]['type'][$keys];
			        $_FILES['files']['tmp_name']= $_FILES[$key]['tmp_name'][$keys];
			        $_FILES['files']['error']= $_FILES[$key]['error'][$keys];
			        $_FILES['files']['size']= $_FILES[$key]['size'][$keys];

					$config['upload_path'] = './assets/lampiran/temp/';
					$config['allowed_types'] = $_POST['allowed_types'];
					$this->load->library('upload');
					$this->upload->initialize($config);

					if (!$this->upload->do_upload('files')) {
						$return['status'] = 'error';
						$return['message'] = $this->upload->display_errors('', '');
					}
					else {
						$return['status'] = 'success';
						$return['upload_path'] = base_url('assets/lampiran/temp/' . $file_name);
						$return['file_name'] = $file_name;
					}

					echo json_encode($return);
				}

			}else{
				$file_name = $_FILES[$key]['name'] = $key . '_' . name_generator($_FILES[$key]['name']);
				$config['upload_path'] = './assets/lampiran/temp/';
				$config['allowed_types'] = $_POST['allowed_types'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if (!$this->upload->do_upload($key)) {
					$return['status'] = 'error';
					$return['message'] = $this->upload->display_errors('', '');
				}
				else {
					$return['status'] = 'success';
					$return['upload_path'] = base_url('assets/lampiran/temp/' . $file_name);
					$return['file_name'] = $file_name;
				}

				echo json_encode($return);
			}

		}
	}

	public function do_upload($field, $db_name = '')
	{
		$file_name = $_FILES[$db_name]['name'] = $db_name . '_' . name_generator($_FILES[$db_name]['name']);
		$config['upload_path'] = './assets/lampiran/' . $db_name . '/';
		$config['allowed_types'] = 'pdf|jpeg|jpg|png|gif';
		$this->load->library('upload');
		$this->upload->initialize($config);
		if (!$this->upload->do_upload($db_name)) {
			$_POST[$db_name] = $file_name;
			$this->form_validation->set_message('do_upload', $this->upload->display_errors('', ''));
			return false;
		}
		else {
			$this->session->set_userdata($db_name, $file_name);
			$_POST[$db_name] = $file_name;
			return true;
		}
	}

	public function deleteTemp($save, $lastData = null)
	{
		if($this->form['form'][0]==NULL){
			$_form = array();
			$i = 0;
			foreach ($this->form['form'] as $key => $value) {
				foreach ($value as $keys => $values) {
					$_form[$i] = $values;  
					$i++;
				}

			}
			$this->form['form'] = $_form;
		}

		foreach($this->form['form'] as $key => $value) {
			if ($value['type'] == 'file') {
				if ($lastData != null && ($save[$value['field']] != $lastData[$value['field']])) {
					if ($lastData[$value['field']] != '') {
						unlink('./assets/lampiran/' . $value['field'] . '/' . $lastData[$value['field']]);
					}
				}

				if ($save[$value['field']] != '') {
					if (file_exists('./assets/lampiran/temp/' . $save[$value['field']])) {
						
						rename('./assets/lampiran/temp/' . $save[$value['field']], './assets/lampiran/' . $value['field'] . '/' . $save[$value['field']]);
					}

				}

			}elseif($value['type'] == 'multiple_file'){
				
			}

			
		}
	}
	
	public function indicator($status){
		$data['indicator'] = $indicator = array(
			array(
				'image'=>'001',
				'label'=>'Upload <br> Kelengkapan <br> Sertifikasi'
			),array(
				'image'=>'003',
				'label'=>'Penilaian <br> Auditor'
			),array(
				'image'=>'002',
				'label'=>'Verifikasi GIS'
			),array(
				'image'=>'004',
				'label'=>'Cetak <br> Sertifikat'
			)
		);
		$data['indicator'][$status]['status'] = 'active';
		return $this->load->view('indicator.php', $data, TRUE);
	}

	public function download($path,$file)
	{
		$this->load->helper('download');
		$dir = 'assets/lampiran/'.$path.'/'.$file;
		// echo $dir;die;
		force_download($dir, null);
	}

	function send_mail($to,$sub,$msg)
	{
		$url = 'http://dekodr.co.id/send_mail/mail_services.php';
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,
		            "to=".$to."&sub=".$sub."&msg=".$msg."");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);

		$out = curl_exec($ch);
		if (curl_errno($ch)) {
		    $error_msg = curl_error($ch);
		}
		curl_close($ch);
	}
}
include('VMS.php');